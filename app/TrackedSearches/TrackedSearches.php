<?php

namespace Pickstar\TrackedSearches;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class TrackedSearches extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    public $table = 'tracked_searches';

    /**
     * Enable timestamps.
     *
     * @var boolean
     */
    public $timestamps = true;

    /**
     * Fillable columns.
     *
     * @var array
     */
    protected $fillable = [
        'sport_category',
        'club_event',
        'location',
        'gender',
        'social_following',
        'opportunity_type',
        'search_keywords',
        'themes',
        'user_ip'
    ];
}

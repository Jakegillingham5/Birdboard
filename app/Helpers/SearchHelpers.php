<?php

namespace Pickstar\Helpers;

class SearchHelpers {
    /**
     * Sanitize a string for database input
     * This should not be used exclusively and
     * should be used through eloquent for added
     * security against SQL injection
     *
     * @param String $string
     *
     * @return String
     */
    public static function sanitizeSearchString($string) {
        return preg_replace('/[^A-Za-z0-9\-\' ]/', '', trim($string));
    }

    /**
     * Strip all non-numeric characters from an input.
     *
     * @param integer $number
     * @return integer
     */
    public static function sanitizeNumber($number)
    {
        if($number === null){
            return null;
        }
        return preg_replace('/[^0-9]/', '', trim($number));
    }

    /**
     * Strip all non-numeric characters from an input, except for a decimal
     *
     * @param integer|array $number
     * @return integer|array
     */
    public static function sanitizeDecimal($number)
    {
        if($number === null){
            return null;
        }
        return preg_replace('/[^0-9.]/', '', trim($number));
    }
}

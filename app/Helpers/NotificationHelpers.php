<?php

namespace Pickstar\Helpers;

use Carbon\Carbon;
use Pickstar\Message\Message;
use Pickstar\Talent\Profile;
use Pickstar\User\User;
use Pickstar\Notifications\Client\NewMessage as NewMessageClient;
use Pickstar\Notifications\Talent\NewMessage as NewMessageTalent;
use Pickstar\Notifications\Manager\NewMessage as NewMessageManager;
use Pickstar\Notifications\Client\UnreadMessages as UnreadMessagesClient;
use Pickstar\Notifications\Talent\UnreadMessages as UnreadMessagesTalent;
use Pickstar\Notifications\Manager\UnreadMessages as UnreadMessagesManager;

class NotificationHelpers {
    /**
     * Code used to determine which notification should be sent to a user when they have a new message
     *
     * @param User $user
     * @param Message $message
     */
    public static function notifyNewMessage (User $user, Message $message) {
        if ($user->isClient()) {
            $user->notify(new NewMessageClient($message));
        } else if ($user->isTalent() && $user->profile->notification_preference !== Profile::NOTIFICATION_DELIVERY_MANAGER) {
            $user->notify(new NewMessageTalent($message));
        } else if ($user->isManager() && $user->profile->notification_preference !== Profile::NOTIFICATION_DELIVERY_TALENT) {
            $user->notify(new NewMessageManager($message));
        }
        $user->fill(['message_notified_at' => Carbon::now()])->save();
    }

    /**
     * Code used to determine which notification should be sent to a user when they are notified about their messages on a chron
     *
     * @param User $user
     */
    public static function notifyUnreadMessages (User $user) {
        if ($user->isClient()) {
            $user->notify(new UnreadMessagesClient($user->threads));
        } else if ($user->isTalent() && $user->profile->notification_preference !== Profile::NOTIFICATION_DELIVERY_MANAGER) {
            $user->notify(new UnreadMessagesTalent($user->threads));
        } else if ($user->isManager() && $user->profile->notification_preference !== Profile::NOTIFICATION_DELIVERY_TALENT) {
            $user->notify(new UnreadMessagesManager($user->threads));
        }
        $user->fill(['message_notified_at' => Carbon::now()])->save();
    }
}

<?php

namespace Pickstar\SearchedKeywords;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class SearchedKeywords extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    public $table = 'searched_keywords';

    /**
     * Enable timestamps.
     *
     * @var boolean
     */
    public $timestamps = true;

    /**
     * Fillable columns.
     *
     * @var array
     */
    protected $fillable = [
            'keywords'
    ];
}

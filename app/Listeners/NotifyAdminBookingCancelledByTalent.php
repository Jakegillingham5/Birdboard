<?php

namespace Pickstar\Listeners;

use Pickstar\Events;
use Pickstar\Notifications;
use Pickstar\User\AdminUser;

class NotifyAdminBookingCancelledByTalent
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\TalentCancelledBooking $event
     *
     * @return void
     */
    public function handle(Events\TalentCancelledBooking $event)
    {
        (new AdminUser)->notify(new Notifications\System\TalentCancelledBooking($event->booking, $event->talent));
    }
}

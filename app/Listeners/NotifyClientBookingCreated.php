<?php

namespace Pickstar\Listeners;

use Pickstar\Events;
use Pickstar\Notifications;

class NotifyClientBookingCreated
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\TalentApplied $event
     *
     * @return void
     */
    public function handle(Events\BookingRequestProgressedToBooking $event)
    {
        $event->booking->client->notify(new Notifications\Client\BookingCreated($event->booking));
    }
}

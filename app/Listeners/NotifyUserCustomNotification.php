<?php

namespace Pickstar\Listeners;

use Nexmo\Client;
use Pickstar\Events\CustomNotification;
use Pickstar\Notifications;

class NotifyUserCustomNotification
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\UserRegistered $event
     *
     * @return void
     */
    public function handle(CustomNotification $event)
    {
        $event->user->notify(new Notifications\General\CustomNotification($event->data, $event->channels));
    }
}

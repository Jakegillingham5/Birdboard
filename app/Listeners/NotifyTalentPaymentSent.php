<?php

namespace Pickstar\Listeners;

use Pickstar\Events;
use Pickstar\Notifications;

class NotifyTalentPaymentSent
{
    /**
     * Handle the event.
     *
     * @param Events\BookingTermsAccepted $event
     *
     * @return void
     */
    public function handle(Events\TalentPaymentSent $event)
    {
        $event->talent->notify(new Notifications\Talent\PaymentSent($event->booking));
    }
}

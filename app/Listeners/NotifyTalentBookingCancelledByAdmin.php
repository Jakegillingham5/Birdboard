<?php

namespace Pickstar\Listeners;

use Pickstar\Events;
use Pickstar\Notifications;

class NotifyTalentBookingCancelledByAdmin
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\BookingCancelled $event
     *
     * @return void
     */
    public function handle(Events\BookingCancelled $event)
    {
        $event->booking->talentAttending->each(function ($talent) use ($event) {
            $talent->notify(new Notifications\Talent\BookingCancelled($event->booking));
        });
    }
}

<?php

namespace Pickstar\Listeners;

use Exception;
use Pickstar\Booking\Booking;
use Pickstar\Events;
use Pickstar\Notifications;
use Pickstar\User\AdminUser;

class NotifyAdminTalentConfirmedAttendance
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\TalentConfirmedAttendance $event
     *
     * @return void
     */
    public function handle(Events\TalentConfirmedAttendance $event)
    {
        switch($event->eventType) {
            case Booking::APPEARANCE_IN_PERSON:
                (new AdminUser)->notify(new Notifications\System\TalentConfirmedAttendance($event->booking, $event->talent));
                break;
            case Booking::APPEARANCE_ONLINE:
                (new AdminUser)->notify(new Notifications\System\TalentConfirmedOnlineParticipation($event->booking, $event->talent));
                break;
            default:
                throw new Exception('Invalid event type.');
        }
    }
}

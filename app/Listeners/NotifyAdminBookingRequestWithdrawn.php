<?php

namespace Pickstar\Listeners;

use Pickstar\Events;
use Pickstar\Notifications;
use Pickstar\User\AdminUser;

class NotifyAdminBookingRequestWithdrawn
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\BookingRequestWithdrawn $event
     *
     * @return void
     */
    public function handle(Events\BookingRequestWithdrawn $event)
    {
        (new AdminUser)->notify(new Notifications\System\BookingRequestWithdrawn($event->booking));
    }
}

<?php

namespace Pickstar\Listeners;

use Pickstar\Events;
use Pickstar\Notifications;
use Pickstar\User\AdminUser;

class NotifyAdminDepositPaid
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\DepositPaid $event
     *
     * @return void
     */
    public function handle(Events\DepositPaid $event)
    {
        (new AdminUser)->notify(new Notifications\System\DepositPaid($event->booking));
    }
}

<?php

namespace Pickstar\Listeners;

use Illuminate\Support\Facades\Event;
use Pickstar\Events;
use Pickstar\Notifications;

class NotifyClientMessagingOpen
{
    /**
     * Handle the event.
     *
     * @param $event
     *
     * @return void
     */
    public function handle($event)
    {
        if (!$event->booking->notification->messaging_open) {
            $event->booking->client->notify(new Notifications\Client\MessagingOpen($event->booking));
        }
    }
}

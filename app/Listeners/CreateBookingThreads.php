<?php

namespace Pickstar\Listeners;

use Pickstar\Talent\Profile;
use Pickstar\User\User;
use Pickstar\Booking\Booking;
use Pickstar\Message\Thread;

class CreateBookingThreads
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\CreateBookingThreads $event
     *
     * @return void
     */
    public function handle(\Pickstar\Events\CreateBookingThreads $event)
    {
        $booking = $event->booking;

        if ($booking->threads->count()) {
            return;
        }

        // Create chat threads between clients and each talent
        $talents = $booking->talentAttending;
        $managers = collect();
        foreach ($talents as $talent) {
            // create thread between client and talent
            $thread = $this->createThread($booking);
            $thread->participants()->attach([$talent->id, $booking->client->id]);
            $this->addManager($thread, $talent);
            $thread->save();

            // create thread between admin and talent
            if ($talent->profile->notification_preference !== Profile::NOTIFICATION_DELIVERY_MANAGER) {
                $thread = $this->createThread($booking);
                $thread->participants()->attach([$talent->id]);
                $this->addManager($thread, $talent);
                $thread->save();
            }

            if ($talent->management) {
                $managers->push($talent->management);
            }
            if ($talent->manager) {
                $managers->push($talent->manager);
            }
        }
        $managers = $managers->unique();
        if ($managers->count()) {
            $thread = $this->createThread($booking);
            foreach ($managers as $manager) {
                $thread->participants()->attach([$manager->id]);
            }
            $thread->save();
        }

        // create thread between client and admin
        $thread = $this->createThread($booking);
        $thread->participants()->attach([$booking->client->id]);
        $thread->save();
    }

    /**
     * Build the default settings for a thread
     *
     * @param Booking $booking
     * @return Thread
     */
    private function createThread (Booking $booking) {
        $thread = new Thread(['active' => true]);
        $thread->booking()->associate($booking);
        $thread->save();
        $this->addAdmins($thread);
        return $thread;
    }

    /**
     * Add a talents manager to a thread
     *
     * @param Thread $thread
     * @param User $talent
     */
    private function addManager (Thread $thread, User $talent) {
        if ($talent->management) {
            $thread->participants()->attach([$talent->management->id]);
        }
        if ($talent->manager) {
            $thread->participants()->attach([$talent->manager->id]);
        }
    }

    /**
     * Add PickStar admins to a thread
     *
     * @param Thread $thread
     */
    private function addAdmins (Thread $thread) {
        foreach (User::admin()->get() as $admin) {
            $thread->participants()->attach([$admin->id]);
        }
    }
}

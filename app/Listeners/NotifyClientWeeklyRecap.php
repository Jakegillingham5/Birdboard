<?php

namespace Pickstar\Listeners;

use Pickstar\Events;
use Pickstar\Notifications;

class NotifyClientWeeklyRecap
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\TalentApplied $event
     *
     * @return void
     */
    public function handle(Events\WeeklyRecap $event)
    {
        $event->booking->client->notify(new Notifications\Client\WeeklyRecap($event->booking));
    }
}

<?php

namespace Pickstar\Listeners;

use Pickstar\User\AdminUser;
use Pickstar\Events;
use Pickstar\Notifications;

class NotifyAdminTermsDeclined
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\BookingTermsDeclined $event
     *
     * @return void
     */
    public function handle(Events\BookingTermsDeclined $event)
    {
        (new AdminUser)->notify(new Notifications\System\ClientTermsDeclined($event->booking));
    }
}

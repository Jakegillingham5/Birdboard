<?php

namespace Pickstar\Listeners;

use Pickstar\Events;
use Pickstar\Notifications;

class NotifyClientBankTransferPaymentReceived
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\BankTransferPaymentReceived $event
     *
     * @return void
     */
    public function handle(Events\BankTransferPaymentReceived $event)
    {
        $event->payment->user->notify(new Notifications\Client\BankTransferPaymentReceived($event->payment));
    }
}

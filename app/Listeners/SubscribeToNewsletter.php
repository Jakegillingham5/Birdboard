<?php
/**
 * Created by PhpStorm.
 * User: jpickstar
 * Date: 2019-04-29
 * Time: 09:28
 */

namespace Pickstar\Listeners;


use Pickstar\Jobs\SubscribeToNewsletter as SubscribeJob;
use Pickstar\Services\ActiveCampaignService;

class SubscribeToNewsletter
{
    public $api;

    public $event;

    public function __construct(ActiveCampaignService $api)
    {
        $this->api = $api;
    }

    public function handle($event)
    {
        $this->event = $event;
        $email = $event->user->email;
        SubscribeJob::dispatch($email);
    }
}
<?php

namespace Pickstar\Listeners;

use Pickstar\Events;
use Pickstar\Notifications;
use Pickstar\User\AdminUser;

class NotifyAdminEsportsEnquiry
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\EsportsEnquiry $event
     *
     * @return void
     */
    public function handle(Events\EsportsEnquiry $event)
    {
        (new AdminUser)->notify(new Notifications\System\EsportsEnquiry($event->request_details));
    }
}

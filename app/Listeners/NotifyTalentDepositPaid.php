<?php

namespace Pickstar\Listeners;

use Pickstar\Events;
use Pickstar\Notifications;

class NotifyTalentDepositPaid
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\DepositPaid $event
     *
     * @return void
     */
    public function handle($event)
    {
        if (!$event->booking->notification->messaging_open) {
            $event->booking->talentAttending->each(function ($talent) use ($event) {
                $talent->notify(new Notifications\Talent\DepositPaid($event->booking));
            });
        }
    }
}

<?php


namespace Pickstar\Listeners;

use Pickstar\Events;
use Pickstar\Notifications;

class NotifyTalentBookingRequestCompleteByDateChanged
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\BookingRequestEventDateChanged $event
     *
     * @return void
     */
    public function handle(Events\BookingRequestCompleteByDateChanged $event)
    {
        $event->booking->talentApplied->each(function ($talent) use ($event) {
            $talent->notify(new Notifications\Talent\CompleteByDateChanged($event->booking));
        });
    }
}

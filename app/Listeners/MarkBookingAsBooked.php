<?php

namespace Pickstar\Listeners;

use Pickstar\Booking\Booking;
use Pickstar\Events\Contracts\HasBookingInstance;

class MarkBookingAsBooked
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\Contracts\HasBookingInstance $event
     *
     * @return void
     */
    public function handle(HasBookingInstance $event)
    {
        // If the client has accepted the terms and conditions and the bookings deposit has been paid
        // then we'll mark the booking as booked.
        if (
            $event->booking->client_accepted_terms &&
            $event->booking->payments()->deposit()->paid()->exists()
        ) {
            $event->booking->fill([
                'status' => Booking::STATUS_BOOKED
            ])->save();
        }
    }
}

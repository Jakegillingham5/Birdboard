<?php

namespace Pickstar\Listeners;

use Pickstar\Events;
use Pickstar\Notifications;
use Pickstar\User\User;

class NotifyTalentBookingAccepted
{
    /**
     * Handle the event.
     *
     * @param Events\BookingTermsAccepted $event
     *
     * @return void
     */
    public function handle(Events\BookingTermsAccepted $event)
    {
        $event->booking->talentAttending->each(function($talent) use ($event) {
            /* @var User $talent */
            $talent->notify(new Notifications\Talent\BookingAccepted($event->booking));
        });
    }
}

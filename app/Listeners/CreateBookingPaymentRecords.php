<?php

namespace Pickstar\Listeners;

use Carbon\Carbon;
use Pickstar\Payment\Payment;
use Pickstar\Booking\Booking;
use Pickstar\Setting\Setting;
use Pickstar\Events\BookingRequestProgressedToBooking;

class CreateBookingPaymentRecords
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\BookingRequestProgressedToBooking $event
     *
     * @return void
     */
    public function handle(BookingRequestProgressedToBooking $event)
    {
        $booking = $event->booking;
        if (
            $booking->type === Booking::TYPE_BOOKING &&
            $booking->payments()->exists() === false
        ) {
            // Calculate the booking total budget. This is the budget per star multipled by the number
            // of stars that are attending the booking.
            $budgetTotal = $booking->talentAttending()->count() * $booking->budget_per_star;

            $depositDueDate = $event->booking->calculateDepositPaymentDueDate();
            $finalDueDate = $event->booking->calculateFinalPaymentDueDate();

            $deposit = $booking->payments()->create([
                'user_id' => $booking->client->id,
                'type' => Payment::TYPE_DEPOSIT,
                'status' => Payment::STATUS_UNPAID,
                'due_date' => $depositDueDate,
                'amount' => round((Setting::depositPayment() / 100) * $budgetTotal, 2)
            ]);

            $final = $booking->payments()->create([
                'user_id' => $booking->client->id,
                'type' => Payment::TYPE_FINAL,
                'status' => Payment::STATUS_UNPAID,
                'due_date' => $finalDueDate,
                'amount' => $budgetTotal - $deposit->amount
            ]);
        }
    }
}

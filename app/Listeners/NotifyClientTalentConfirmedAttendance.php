<?php

namespace Pickstar\Listeners;

use Exception;
use Pickstar\Booking\Booking;
use Pickstar\Events;
use Pickstar\Notifications;
use Pickstar\User\AdminUser;

class NotifyClientTalentConfirmedAttendance
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\TalentConfirmedAttendance $event
     * or
     * @param \Pickstar\Events\TalentConfirmedAttendanceClientOnly $event
     *
     * @return void
     */
    public function handle($event)
    {
        switch($event->eventType) {
            case Booking::APPEARANCE_IN_PERSON:
                $event->booking->client->notify(new Notifications\Client\TalentConfirmedAttendance($event->booking, $event->talent));
                break;
            case Booking::APPEARANCE_ONLINE:
                $event->booking->client->notify(new Notifications\Client\TalentConfirmedOnlineParticipation($event->booking, $event->talent));
                break;
            default:
                throw new Exception('Invalid event type.');
        }
    }
}

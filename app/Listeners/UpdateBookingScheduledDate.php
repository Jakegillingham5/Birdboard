<?php

namespace Pickstar\Listeners;

use Pickstar\Booking\Booking;
use Pickstar\Events\BookingRequestProgressedToBooking;

class UpdateBookingScheduledDate
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\BookingRequestProgressedToBooking $event
     *
     * @return void
     */
    public function handle(BookingRequestProgressedToBooking $event)
    {
        if (
            $event->booking->type === Booking::TYPE_BOOKING &&
            $event->booking->status === Booking::STATUS_SCHEDULED
        ) {
            $event->booking->fill(['scheduled_date' => now()])->save();
        }
    }
}

<?php

namespace Pickstar\Listeners;

use Pickstar\User\AdminUser;
use Pickstar\Events\UserRegistered;
use Pickstar\Notifications\System\TalentRegistered;

class NotifyAdminNewTalentRegistration
{
    /**
     * Handle the event.
     *
     * Notifies the admin user with the "talent registered" notification.
     *
     * @param \Pickstar\Events\UserRegistered $event
     *
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        if ($event->user->isTalent()) {
            (new AdminUser)->notify(new TalentRegistered($event->user));
        }
    }
}

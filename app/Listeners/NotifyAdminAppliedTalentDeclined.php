<?php

namespace Pickstar\Listeners;

use Pickstar\User\AdminUser;
use Pickstar\Events;
use Pickstar\Notifications;

class NotifyAdminAppliedTalentDeclined
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\AppliedTalentDeclining $event
     *
     * @return void
     */
    public function handle(Events\AppliedTalentDeclining $event)
    {
        (new AdminUser)->notify(new Notifications\System\AppliedTalentDeclined($event->talent, $event->booking));
    }
}

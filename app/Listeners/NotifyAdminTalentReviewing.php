<?php

namespace Pickstar\Listeners;

use Pickstar\User\AdminUser;
use Pickstar\Events;
use Pickstar\Notifications;

class NotifyAdminTalentReviewing
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\TalentReviewing $event
     *
     * @return void
     */
    public function handle(Events\TalentReviewing $event)
    {
        (new AdminUser)->notify(new Notifications\System\TalentReviewing($event->talent, $event->booking));
    }
}

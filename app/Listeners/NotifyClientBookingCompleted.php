<?php

namespace Pickstar\Listeners;

use Pickstar\Events;
use Pickstar\Notifications;

class NotifyClientBookingCompleted
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\BookingCompleted $event
     *
     * @return void
     */
    public function handle(Events\BookingCompleted $event)
    {
        $event->booking->client->notify(new Notifications\Client\BookingCompleted($event->booking));
    }
}

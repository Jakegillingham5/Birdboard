<?php

namespace Pickstar\Listeners;

use Pickstar\Events;
use Pickstar\Notifications;

class NotifyTalentBookingRequestToBeReviewed
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\TalentResponsePending $event
     *
     * @return void
     */
    public function handle(Events\TalentResponsePending $event)
    {
        // If the talent has already been notified about this booking then we won't notify them again
        // that their response has been changed to pending. This might happen when an admin toggles
        // their status between pending and applied.
        if ($event->booking->talentNotified->contains($event->talent)) {
            return;
        }

        // Flag the talent as having been notified and then dispatch the notification.
        $event->booking->talentNotified()->attach($event->talent);

        $event->talent->notify(new Notifications\Talent\BookingRequestToBeReviewed($event->booking));
    }
}

<?php

namespace Pickstar\Listeners;

use Pickstar\Events;
use Pickstar\Notifications;
use Pickstar\User\AdminUser;

class NotifyAdminCustomNotificationResponseReceived
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\DepositPaid $event
     *
     * @return void
     */
    public function handle(Events\CustomNotificationResponseReceived $event)
    {
        (new AdminUser)->notify(new Notifications\System\CustomNotificationResponseReceived($event->data));
    }
}

<?php

namespace Pickstar\Listeners;

use Pickstar\Events\BookingCompleted;

class CloseBookingThreads
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\BookingCompleted $event
     *
     * @return void
     */
    public function handle(BookingCompleted $event)
    {
        foreach ($event->booking->threads as $thread) {
            $thread->fill(['active' => false])->save();
        }
    }
}

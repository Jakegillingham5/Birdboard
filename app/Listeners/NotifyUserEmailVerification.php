<?php

namespace Pickstar\Listeners;

use Pickstar\Events\UserRegistered;
use Illuminate\Support\Facades\Mail;
use Pickstar\Notifications\System\EmailVerification;

class NotifyUserEmailVerification
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\UserRegistered $event
     *
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        if ($event->user->email_verification_token) {
            $event->user->notify(new EmailVerification($event->user));
        }
    }
}

<?php

namespace Pickstar\Listeners;

use Pickstar\Events;
use Pickstar\Notifications;

class NotifyClientAppliedTalentDeclined
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\AppliedTalentDeclining $event
     *
     * @return void
     */
    public function handle(Events\AppliedTalentDeclining $event)
    {
        $event->booking->client->notify(new Notifications\Client\AppliedTalentDeclined($event->talent, $event->booking));
    }
}

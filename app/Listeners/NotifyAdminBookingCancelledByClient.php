<?php

namespace Pickstar\Listeners;

use Pickstar\Notifications;
use Pickstar\User\AdminUser;
use Pickstar\Events\ClientCancelledBooking;

class NotifyAdminBookingCancelledByClient
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\ClientCancelledBooking $event
     *
     * @return void
     */
    public function handle(ClientCancelledBooking $event)
    {
        (new AdminUser)->notify(new Notifications\System\ClientCancelledBooking($event->booking));
    }
}

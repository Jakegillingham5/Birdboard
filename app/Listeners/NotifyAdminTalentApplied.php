<?php

namespace Pickstar\Listeners;

use Pickstar\User\AdminUser;
use Pickstar\Events;
use Pickstar\Notifications;

class NotifyAdminTalentApplied
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\TalentApplied $event
     *
     * @return void
     */
    public function handle(Events\TalentApplied $event)
    {
        (new AdminUser)->notify(new Notifications\System\TalentApplied($event->talent, $event->booking, $event->comment));
    }
}

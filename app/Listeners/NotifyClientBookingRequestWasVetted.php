<?php

namespace Pickstar\Listeners;

use Pickstar\Events;
use Pickstar\Notifications;

class NotifyClientBookingRequestWasVetted
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\BookingRequestVetted $event
     *
     * @return void
     */
    public function handle(Events\BookingRequestVetted $event)
    {
        $event->booking->client->notify(new Notifications\Client\BookingRequestVetted($event->booking));
    }
}

<?php

namespace Pickstar\Listeners;

use Pickstar\Events;
use Pickstar\Notifications;

class NotifyClientBookingRequestReceived
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\BookingRequestCreated $event
     *
     * @return void
     */
    public function handle(Events\BookingRequestCreated $event)
    {
        $event->booking->client->notify(new Notifications\Client\BookingRequestReceived($event->booking));
    }
}

<?php

namespace Pickstar\Listeners;

use Pickstar\Events;
use Pickstar\Notifications;

class NotifyBookingTalentReplaced
{
    /**
     * Handle the event.
     *
     * @param Events\BookingTalentReplaced $event
     *
     * @return void
     */
    public function handle(Events\BookingTalentReplaced $event)
    {
        $event->replacement->notify(new Notifications\Talent\BookingAccepted($event->booking));
    }
}
<?php

namespace Pickstar\Listeners;

use Pickstar\Events;
use Pickstar\Notifications;

class NotifyClientBookingCancelledByAdmin
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\BookingCancelled $event
     *
     * @return void
     */
    public function handle(Events\BookingCancelled $event)
    {
        $event->booking->client->notify(new Notifications\Client\BookingCancelled($event->booking));
    }
}

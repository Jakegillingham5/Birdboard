<?php

namespace Pickstar\Listeners;

use Carbon\Carbon;
use Faker\Provider\Payment;
use Illuminate\Support\Facades\DB;
use Pickstar\Events;
use Pickstar\Notifications;

class NotifyClientTalentApplied
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\TalentApplied $event
     *
     * @return void
     */
    public function handle(Events\TalentApplied $event)
    {
        if ($event->booking->talentApplied()->where(DB::raw('booking_talent.created_at'), '>', Carbon::now()->subDays(2))->count() >= 4 && !$event->booking->notification->running_hot) {
           $event->booking->client->notify(new Notifications\Client\TalentAppliedRunningHot($event->talent, $event->booking, $event->comment));
           $event->booking->notification->fill(['running_hot' => true])->save();
        } else {
            $event->booking->client->notify(new Notifications\Client\TalentApplied($event->talent, $event->booking, $event->comment));
        }
    }
}

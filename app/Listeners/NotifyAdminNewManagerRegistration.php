<?php

namespace Pickstar\Listeners;

use Pickstar\User\AdminUser;
use Pickstar\Events\UserRegistered;
use Pickstar\Notifications\System\ManagerRegistered;

class NotifyAdminNewManagerRegistration
{
    /**
     * Handle the event.
     *
     * Notifies the admin user with the "manager registered" notification.
     *
     * @param \Pickstar\Events\UserRegistered $event
     *
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        if ($event->user->isManager()) {
            (new AdminUser)->notify(new ManagerRegistered($event->user));
        }
    }
}

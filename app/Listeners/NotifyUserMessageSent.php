<?php

namespace Pickstar\Listeners;

use Carbon\Carbon;
use Pickstar\Events\MessageSent;
use Pickstar\Helpers\NotificationHelpers;
use Pickstar\Services\PusherService;

class NotifyUserMessageSent
{
    /** @var PusherService  */

    protected $_api;

    public function __construct(PusherService $service)
    {
        $this->_api = $service;
    }

    /**
     * Handle the event.
     *
     * A user is notified about a message that has been sent in the system
     *
     * They are notified if:
     * 1. They are not currently active in the system
     * 2. They have not been notified in the past 12 hours
     *    or, they have read all current messages and
     *    not been notified about them (message_notified_at set to null)
     *
     * These notifications are also sent out from a console command
     * triggered in the kernel every 24 hours
     *
     * @param \Pickstar\Events\MessageSent $event
     * @throws \Pusher\PusherException
     *
     * @return void
     */
    public function handle(MessageSent $event)
    {
        $user = $event->recipient;
        if (!$this->_api::checkIfChannelOccupied('private-chat.' . $user->id)) {
            if ($user->message_notified_at === null || $user->message_notified_at <= Carbon::now()->subHours(12)) {
                NotificationHelpers::notifyNewMessage($user, $event->message);
            }
        } else {
            $user->fill(['message_notified_at' => null])->save();
        }
    }
}

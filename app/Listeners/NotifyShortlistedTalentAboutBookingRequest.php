<?php

namespace Pickstar\Listeners;

use Pickstar\Events\BookingRequestVetted;
use Pickstar\Notifications\Talent\ShortlistedForBookingRequest;

class NotifyShortlistedTalentAboutBookingRequest
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\BookingRequestVetted $event
     *
     * @return void
     */
    public function handle(BookingRequestVetted $event)
    {
        $talent = $event->booking
            ->talentShortlisted()
            ->whereDoesntHave('talentBookingNotifications', function ($query) use ($event) {
                $query->where('id', $event->booking->id);
            })
            ->orderBy('priority')
            ->take(5)
            ->get();

        if ($talent->isNotEmpty()) {
            $talent->each(function ($talent) use ($event) {
                $talent->notify(new ShortlistedForBookingRequest($event->booking, $talent->response->priority));
            });

            $event->booking->talentNotified()->syncWithoutDetaching($talent->pluck('id'));
        }
    }
}

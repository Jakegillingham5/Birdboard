<?php

namespace Pickstar\Listeners;

use Pickstar\Notifications;
use Pickstar\User\User;
use Pickstar\Events\BidMade;

class NotifyAdminBidMade
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\BidMade $event
     *
     * @return void
     */
    public function handle(BidMade $event)
    {
        $user = User::find(3);
        $user->notify(new Notifications\System\BidMade($event->name, $event->email, $event->company, $event->bid, $event->type));
    }
}

<?php

namespace Pickstar\Listeners;

use Pickstar\User\AdminUser;
use Pickstar\Events;
use Pickstar\Notifications;

class NotifyAdminTalentAccepted
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\TalentApplied $event
     *
     * @return void
     */
    public function handle(Events\TalentAccepted $event)
    {
        (new AdminUser)->notify(new Notifications\System\TalentAccepted($event->user, $event->booking));
    }
}

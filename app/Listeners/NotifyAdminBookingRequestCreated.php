<?php

namespace Pickstar\Listeners;

use Pickstar\Events;
use Pickstar\Notifications;
use Pickstar\User\AdminUser;

class NotifyAdminBookingRequestCreated
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\BookingRequestCreated $event
     *
     * @return void
     */
    public function handle(Events\BookingRequestCreated $event)
    {
        (new AdminUser)->notify(new Notifications\System\BookingRequestCreated($event->booking));
    }
}

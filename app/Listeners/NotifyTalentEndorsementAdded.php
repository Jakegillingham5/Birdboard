<?php

namespace Pickstar\Listeners;

use Pickstar\Events;
use Pickstar\Notifications;

class NotifyTalentEndorsementAdded
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\TalentEndorsementAdded $event
     *
     * @return void
     */
    public function handle(Events\TalentEndorsementAdded $event)
    {
        $event->talent->notify(new Notifications\Talent\EndorsementAdded($event->endorsement));
    }
}

<?php

namespace Pickstar\Listeners;

use Pickstar\Events;
use Pickstar\Notifications;
use Pickstar\User\AdminUser;

class NotifyAdminFinalPaymentPaid
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\FinalPaymentPaid $event
     *
     * @return void
     */
    public function handle(Events\FinalPaymentPaid $event)
    {
        (new AdminUser)->notify(new Notifications\System\FinalPaymentPaid($event->booking));
    }
}

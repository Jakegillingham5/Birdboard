<?php

namespace Pickstar\Listeners;

use Pickstar\Events;
use Pickstar\Notifications;

class NotifyTalentBookingRequestEventDateChanged
{
    /**
     * Handle the event.
     *
     * @param \Pickstar\Events\BookingRequestEventDateChanged $event
     *
     * @return void
     */
    public function handle(Events\BookingRequestEventDateChanged $event)
    {
        $event->booking->talentApplied->each(function ($talent) use ($event) {
            $talent->notify(new Notifications\Talent\EventDateChanged($event->booking));
        });
    }
}

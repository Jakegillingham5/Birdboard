<?php
/**
 * Created by PhpStorm.
 * User: jpickstar
 * Date: 2019-06-04
 * Time: 12:14
 */

namespace Pickstar\Listeners;


use Pickstar\Jobs\XeroCreateInvoices;
use Pickstar\Services\XeroService;
use XeroPHP\Exception;

class CreateXeroRecordsForBooking
{

    public function handle($event)
    {
        if(env('XERO_CERT_CONTENT') !== null) {

            $booking = $event->booking;

            XeroCreateInvoices::dispatch($booking->id);
        }
    }

}

<?php
/**
 * Created by PhpStorm.
 * User: jpickstar
 * Date: 2019-06-04
 * Time: 15:36
 */

namespace Pickstar\Listeners;


use Pickstar\Jobs\XeroContactUpdate;

class UpdateXeroContact
{
    public function handle($event)
    {
        if(env('XERO_CERT_CONTENT') !== null){
            $user      = $event->user;
            XeroContactUpdate::dispatch($user);
        }
    }
}
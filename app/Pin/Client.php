<?php
/**
 * Created by PhpStorm.
 * User: jpickstar
 * Date: 2019-05-21
 * Time: 10:38
 */

namespace Pickstar\Pin;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\RequestOptions;

/**
 * Class Client
 * @package Pickstar\ActiveCampaign
 */
class Client
{
    /**
     * @var \GuzzleHttp\Client
     */
    public static $_client;

    /**
     * Client constructor.
     */
    public function __construct () {
        $config = [
            'base_uri'        => env('PIN_API_URL'),
            'timeout'         => 0,
            'allow_redirects' => true,
            'auth'         => [
                env('PIN_API_KEY'),
                ''
            ]
        ];

        static::$_client = new GuzzleClient($config);
    }

    /**
     * @param $path
     * @param array $options
     * @param string $requestType
     * @return mixed
     */
    public function call($path, $options = [], $requestType = 'get')
    {
        $response = static::$_client->$requestType($path, $options);

        return static::decodeResponse($response);
    }

    /**
     * @param $path
     * @param array $options
     * @return mixed
     */
    public function post($path, $options = [])
    {
        return $this->call($path, [RequestOptions::JSON => $options], 'post');
    }

    /**
     * @param $path
     * @param array $options
     * @return mixed
     */
    public function put($path, $options = [])
    {
        return $this->call($path, [RequestOptions::JSON => $options], 'put');
    }

    /**
     * @param $path
     * @param $params
     * @param array $options
     * @return mixed
     */
    public function query($path, $params, $options = [])
    {
        $response = static::$_client->get($path . '?' . http_build_query($params), $options);

        return static::decodeResponse($response);
    }

    /**
     * @param $response
     * @return mixed
     */
    protected static function decodeResponse($response)
    {
        return json_decode($response->getBody()->getContents());
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: jpickstar
 * Date: 2019-03-08
 * Time: 12:56
 */

namespace Pickstar\Providers;


use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\ServiceProvider;
use Pickstar\Services\ActiveCampaignService;

class ActiveCampaignServiceProvider extends ServiceProvider
{
   public function register()
   {
       $this->app->singleton(ActiveCampaignService::class, ActiveCampaignService::class);
   }


}
<?php
/**
 * Created by PhpStorm.
 * User: jpickstar
 * Date: 2019-03-08
 * Time: 12:56
 */

namespace Pickstar\Providers;


use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Contracts\Encryption\Encrypter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;
use Pickstar\GAParse;

class UTMServiceProvider extends ServiceProvider
{
    static $utm_tags = [
        'utm_source'    => 'campaign_source',
        'utm_medium'    => 'campaign_name',
        'utm_campaign'  => 'campaign_medium',
        'utm_term'      => 'campaign_content',
        'utm_content'   => 'campaign_term',
    ];

    static $prefix = 'pxt';

    protected static $_cookie;

    private static $collection_location = 'cookieData';

    public static function storeUtm(Request $request)
    {
        $GA_cookie  = new GAParse($_COOKIE);
        $cookieData = static::retrieveCookie();

        if(!is_object($cookieData)){
            $client = static::sentry();
            $client->captureMessage('Retrieved cookie not an object');
        }

        foreach (static::$utm_tags as $tag => $field) {

            $value = $request->request->get($tag);

            if($value !== null){
                $cookieData->$tag = $value;
            } else if((!property_exists($cookieData, $tag) || $cookieData->$tag === null)) {
                if ($value === null) {
                    static::$collection_location = 'GA_cookie';
                    $cookieData->$tag = $GA_cookie->$field;
                }
            }
        }
        $cookieData->collection_location = static::$collection_location;

        if(self::referer() === null){
            if(array_key_exists('HTTP_REFERER', $_SERVER)){
                $cookieData->referer = $_SERVER['HTTP_REFERER'];
            } else {
                $cookieData->referer = 'direct';
            }
        }

        static::storeData($cookieData);
    }

    public static function source()
    {
        $source = self::getAndDecrypt('utm_source');
        if($source === null){
            $source = self::getAndDecrypt('referer');
        }
        return $source;
    }

    public static function collectionLocation()
    {
        return self::getAndDecrypt('collection_location');
    }

    public static function medium()
    {
        return self::getAndDecrypt('utm_medium');
    }

    public static function campaign()
    {
        return self::getAndDecrypt('utm_campaign');
    }

    public static function term()
    {
        return self::getAndDecrypt('utm_term');
    }

    public static function content()
    {
        return self::getAndDecrypt('utm_content');
    }

    public static function referer()
    {
        return self::getAndDecrypt('referer');
    }

    public static function getAndDecrypt($string)
    {
        $cookie = static::$_cookie;

        if($cookie !== null) {

            if(property_exists($cookie, $string)){
                return $cookie->$string;
            }
        }
        return null;
    }

    public static function getAndDecryptAll()
    {
        $results = [];

        foreach (static::$utm_tags as $tag){
            $results[$tag] = self::getAndDecrypt($tag);
        }

        return $results;
    }

    protected static function retrieveCookie()
    {

        $cookie          = Cookie::get(static::$prefix);
        $client          = static::sentry();

        try {
            $cookieObj = json_decode($cookie);
        } catch (\Exception $e) {
            $client->captureException($e);
            $cookieObj = static::initCookie();
        }

        if($cookieObj !== null && !is_object($cookieObj)) {
            $client->captureMessage('Marketing source did not return object as expected.', [], [], true);
        }

        if($cookieObj !== null){
            static::$_cookie = $cookieObj;
        } else {
            static::initCookie();
        }

        return static::$_cookie;
    }

    /**
     * @return \stdClass;
     */
    protected static function initCookie()
    {
        $cookie = new \stdClass();
        static::$_cookie = $cookie;
        Cookie::queue(static::$prefix, json_encode($cookie), 3600);
        return static::$_cookie;
    }

    protected static function storeData($cookieData)
    {
        Cookie::queue(static::$prefix, json_encode($cookieData), 3600);
        static::$_cookie = $cookieData;
    }

    /**
     * @return \Raven_Client
     */
    protected static function sentry()
    {
        return app('sentry');
    }


    /**
     * @return Encrypter
     */
    protected static function decrypter()
    {
        return app(Encrypter::class);
    }
}

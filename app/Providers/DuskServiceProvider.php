<?php

namespace Pickstar\Providers;

use Laravel\Dusk\Browser;
use Illuminate\Support\ServiceProvider;
use PHPUnit\Framework\Assert as PHPUnit;

class DuskServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        if (app()->environment('local', 'testing')) {
            $this->registerMacros();
        }
    }

    /**
     * Register Dusk testing macros.
     *
     * @return void
     */
    protected function registerMacros()
    {
        Browser::macro('assertElementCountIs', function ($expected, $element, $message = '') {
            PHPUnit::assertCount($expected, $this->elements($element), $message);

            return $this;
        });

        Browser::macro('scrollToTop', function () {
            $this->script('window.scrollTo(0, 0)');

            return $this;
        });
    }
}

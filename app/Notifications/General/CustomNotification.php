<?php

namespace Pickstar\Notifications\General;

use Pickstar\Mail;
use Illuminate\Bus\Queueable;
use Pickstar\Notifications\OneSignalData;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Pickstar\Channels\NexmoSignedSmsChannel;
use Illuminate\Notifications\Messages\NexmoMessage;
use Pickstar\Notifications\ResolvesDeliveryChannels;

class CustomNotification extends Notification implements ShouldQueue
{
    use Queueable,
        OneSignalData;

    public $data;

    public $channels = [];

    /**
     * Create a new notification instance.
     *
     * @param array $data
     * @param array $channels
     */
    public function __construct(array $data, array $channels)
    {
        $this->data = $data;
        $this->channels = $channels;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(ResolvesDeliveryChannels $notifiable)
    {
        return $notifiable->viaChannels([NexmoSignedSmsChannel::class]);
    }
    /**
     * Get the SMS representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\NexmoMessage
     */
    public function toNexmoSignedSms($notifiable)
    {
        return (new NexmoMessage)->content($this->data['text']);
    }
}

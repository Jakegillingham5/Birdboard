<?php

namespace Pickstar\Notifications\Client;

use Pickstar\Mail;
use Illuminate\Bus\Queueable;
use Pickstar\Booking\Booking;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Pickstar\Message\SystemNotification;
use Pickstar\Notifications\ResolvesDeliveryChannels;

class BookingCompleted extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Booking instance.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * Create a new notification instance.
     *
     * @param \Pickstar\Booking\Booking $booking
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(ResolvesDeliveryChannels $notifiable)
    {
        return $notifiable->viaChannels(['mail', 'broadcast', 'database']);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Mail\Mailable
     */
    public function toMail($notifiable)
    {
        return (new Mail\Client\BookingCompleted($this->booking))
            ->to($notifiable->email);
    }

    public function toBroadcast($notifiable)
    {
        return [
            'type' => SystemNotification::TYPE_BOOKING_COMPLETED,
            'link_type' => SystemNotification::LINK_BOOKINGS_SHOW,
            'link_id' => $this->booking->id,
            'related_id' => $this->booking->id,
            'text' => 'Your PickStar booking has been completed: ' . $this->booking->name . ', ' . $this->booking->date
        ];
    }

    public function toDatabase($notifiable)
    {
        return [
            'type' => SystemNotification::TYPE_BOOKING_COMPLETED,
            'link_type' => SystemNotification::LINK_BOOKINGS_SHOW,
            'link_id' => $this->booking->id,
            'related_id' => $this->booking->id,
            'text' => 'Thanks for using PickStar! We have marked your booking as complete: ' . $this->booking->name . '.',
            'sub' => 'If we can help with anything else, please let our team know.'
        ];
    }
}

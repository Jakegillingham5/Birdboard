<?php

namespace Pickstar\Notifications\Client;

use Pickstar\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Collection;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Pickstar\Channels\NexmoSignedSmsChannel;
use Illuminate\Notifications\Messages\NexmoMessage;
use Pickstar\Notifications\ResolvesDeliveryChannels;

class TextRecap extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Collection of booking objects
     *
     * @var Collection
     */
    public $bookings;

    /**
     * Create a new notification instance.
     *
     * @param Collection $bookings
     * @param boolean $many
     */
    public function __construct($bookings)
    {
        $this->bookings = $bookings;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(ResolvesDeliveryChannels $notifiable)
    {
        return $notifiable->viaChannels([NexmoSignedSmsChannel::class]);
    }


    /**
     * Get the SMS representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\NexmoMessage
     */
    public function toNexmoSignedSms($notifiable)
    {
        if ($this->bookings->count() > 1) {
            $talent_count = 0;
            foreach ($this->bookings as $booking) {
                $talent_count += $booking->talentApplied()->count();
            }
            $replacements = [
                ':url' => get_bitly_url(url()->toWebApp('/')),
                ':client_name' => $notifiable->first_name,
                ':talent_count' => $talent_count,
            ];

            $message = 'Hey :client_name, :talent_count sports stars have applied for your opportunities. So you don\'t miss out, view your events here :url and book now or call us on 1300 657 601';

            return (new NexmoMessage)->content(strtr($message, $replacements))->from(config('services.nexmo.sms_from'));
        } elseif ($this->bookings->count() === 1) {
            $talent_count = $this->bookings->first()->talentApplied()->count();
            $replacements = [
                ':url' => $this->bookings->first()->generateBitlyUrl(),
                ':client_name' => $notifiable->first_name,
                ':talent_count' => $talent_count,
            ];

            $message = 'Hey :client_name, :talent_count sports stars have applied for your opportunity. So you don\'t miss out, view the list here :url and book now or call us on 1300 657 601';
            if ($talent_count === 1) {
                $message = 'Hey :client_name, a sports star has applied for your opportunity. So you don\'t miss out, view them here :url and book now or call us on 1300 657 601';
            }

            return (new NexmoMessage)->content(strtr($message, $replacements))->from(config('services.nexmo.sms_from'));
        }

    }
}

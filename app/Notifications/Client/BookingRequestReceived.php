<?php

namespace Pickstar\Notifications\Client;

use Pickstar\Mail;
use Illuminate\Bus\Queueable;
use Pickstar\Booking\Booking;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Pickstar\Message\SystemNotification;
use Pickstar\Notifications\ResolvesDeliveryChannels;

class BookingRequestReceived extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Booking instance.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * Create a new notification instance.
     *
     * @param \Pickstar\Booking\Booking $booking
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(ResolvesDeliveryChannels $notifiable)
    {
        return $notifiable->viaChannels(['mail', 'broadcast', 'database']);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Mail\Mailable
     */
    public function toMail($notifiable)
    {
        return (new Mail\Client\BookingRequestReceived($this->booking))
            ->to($notifiable->email);
    }

    public function toBroadcast($notifiable)
    {
        return [
            'notif_type' => SystemNotification::TYPE_REQUEST_CREATED,
            'link_type' => SystemNotification::LINK_REQUESTS_SHOW,
            'link_id' => $this->booking->id,
            'related_id' => $this->booking->id,
            'text' => 'You submitted a sports star booking request: ' . $this->booking->name
        ];
    }

    public function toDatabase($notifiable)
    {
        return [
            'notif_type' => SystemNotification::TYPE_REQUEST_CREATED,
            'link_type' => SystemNotification::LINK_REQUESTS_SHOW,
            'link_id' => $this->booking->id,
            'related_id' => $this->booking->id,
            'text' => 'We\'ve received your booking request!',
            'sub' => 'Stay tuned because we\'ll be in touch again within one working day to confirm the details, then we\'ll post your request direct to our network of sports stars!'
        ];
    }
}

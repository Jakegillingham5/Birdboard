<?php

namespace Pickstar\Notifications\Client;

use Pickstar\Mail;
use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Pickstar\Booking\Booking;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Pickstar\Channels\NexmoSignedSmsChannel;
use Illuminate\Notifications\Messages\NexmoMessage;
use Pickstar\Message\SystemNotification;
use Pickstar\Notifications\ResolvesDeliveryChannels;

class FinalPaymentWarning extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Booking instance.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * Create a new notification instance.
     *
     * @param \Pickstar\Booking\Booking $booking
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(ResolvesDeliveryChannels $notifiable)
    {
        return $notifiable->viaChannels([NexmoSignedSmsChannel::class, 'mail']);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Mail\Mailable
     */
    public function toMail($notifiable)
    {
        return (new Mail\Client\FinalPaymentWarning($this->booking, $this->booking->finalPayment))
            ->to($notifiable->email);
    }

    /**
     * Get the Nexmo / SMS representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\NexmoMessage
     */
    public function toNexmoSignedSms($notifiable)
    {
        $message = 'Your PickStar full payment is due by %s. To view your booking and pay online go to %s or call 1300 657 601.';

        return (new NexmoMessage)
            ->content(sprintf(
                $message,
                $this->booking->finalPayment->due_date->format(User::DATE_FORMAT),
                $this->booking->generateBitlyUrl()
            ))
            ->from(config('services.nexmo.sms_from'));
    }

    public function toBroadcast($notifiable)
    {
        return [
            'notif_type' => SystemNotification::TYPE_DEPOSIT_WARNING,
            'link_type' => SystemNotification::LINK_BOOKINGS_PAYMENT,
            'link_id' => $this->booking->id,
            'related_id' => $this->booking->id,
            'text' => 'Full and final payment for ' . $this->booking->name . ' is now due.'
        ];
    }

    public function toDatabase($notifiable)
    {
        $message = 'Final payment of $%s is due by %s.';
        return [
            'notif_type' => SystemNotification::TYPE_DEPOSIT_WARNING,
            'link_type' => SystemNotification::LINK_BOOKINGS_PAYMENT,
            'link_id' => $this->booking->id,
            'related_id' => $this->booking->id,
            'text' => sprintf($message, $this->booking->finalPayment->amount, $this->booking->finalPayment->due_date->format('d/m/Y')),
            'sub' => 'Pay online or call 1300 657 601'
        ];
    }
}

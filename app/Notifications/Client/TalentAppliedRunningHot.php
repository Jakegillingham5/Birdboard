<?php

namespace Pickstar\Notifications\Client;

use Pickstar\Mail;
use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Pickstar\Booking\Booking;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Pickstar\Channels\NexmoSignedSmsChannel;
use Illuminate\Notifications\Messages\NexmoMessage;
use Pickstar\Notifications\ResolvesDeliveryChannels;

class TalentAppliedRunningHot extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Talent user instance.
     *
     * @var \Pickstar\User\User
     */
    public $talent;

    /**
     * Booking instance.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * Talents comment on why they are suitable
     */
    public $comment;

    /**
     * Create a new notification instance.
     *
     * @param \Pickstar\Booking\Booking $booking
     * @param \Pickstar\User\User $talent
     * @param $comment
     */
    public function __construct(User $talent, Booking $booking, $comment = null)
    {
        $this->talent = $talent;
        $this->booking = $booking;
        $this->comment = $comment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(ResolvesDeliveryChannels $notifiable)
    {
        return $notifiable->viaChannels(['mail', NexmoSignedSmsChannel::class]);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Mail\Mailable
     */
    public function toMail($notifiable)
    {
        return (new Mail\Client\TalentApplied($this->talent, $this->booking, $this->comment))
            ->to($notifiable->email);
    }

    /**
     * Get the SMS representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\NexmoMessage
     */
    public function toNexmoSignedSms($notifiable)
    {
        $replacements = [
            ':url' => $this->booking->generateBitlyUrl(),
            ':client_name' => $notifiable->first_name,
            ':talent_name' => $this->talent->name
        ];

        $message = 'Hey :client_name, you’re running hot - four sports stars have now applied for your opportunity. We recommend booking a star as soon as possible! View applicants now :url or call us on 1300 657 601';

        return (new NexmoMessage)->content(strtr($message, $replacements))->from(config('services.nexmo.sms_from'));
    }
}

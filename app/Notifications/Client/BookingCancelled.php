<?php

namespace Pickstar\Notifications\Client;

use Pickstar\Mail;
use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Pickstar\Booking\Booking;
use Pickstar\Message\SystemNotification;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Pickstar\Channels\NexmoSignedSmsChannel;
use Illuminate\Notifications\Messages\NexmoMessage;
use Pickstar\Notifications\ResolvesDeliveryChannels;

class BookingCancelled extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Booking instance.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * Create a new notification instance.
     *
     * @param \Pickstar\Booking\Booking $booking
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(ResolvesDeliveryChannels $notifiable)
    {
        return $notifiable->viaChannels(['mail', NexmoSignedSmsChannel::class, 'broadcast', 'database']);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Mail\Mailable
     */
    public function toMail($notifiable)
    {
        return (new Mail\Client\BookingCancelled($this->booking))
            ->to($notifiable->email);
    }

    /**
     * Get the SMS representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\NexmoMessage
     */
    public function toNexmoSignedSms($notifiable)
    {
        $replacements = [
            ':opportunity' => $this->booking->name,
            ':date' => $this->booking->tz_date_instance->format(User::DATE_FORMAT_TZ),
            ':url' => $this->booking->generateBitlyUrl()
        ];

        if ($this->booking->appearanceIsInPerson()) {
            $message = 'BOOKING CANCELLED: We\'re sorry, your PickStar booking has unfortunately been cancelled. :opportunity, :date. Booking Details: :url.';
        } else {
            $message = 'BOOKING CANCELLED: We\'re sorry, your PickStar booking has unfortunately been cancelled. :opportunity. Booking Details: :url.';
        }

        $message .= config('services.nexmo.signature');

        return (new NexmoMessage)->content(strtr($message, $replacements))->from(config('services.nexmo.sms_from'));
    }

    public function toBroadcast($notifiable)
    {
        return [
            'type' => SystemNotification::TYPE_BOOKING_CANCELLED,
            'link_type' => SystemNotification::LINK_BOOKINGS_SHOW,
            'link_id' => $this->booking->id,
            'related_id' => $this->booking->id,
            'text' => 'Your PickStar booking has now been cancelled:' . $this->booking->name . ', ' . $this->booking->tz_date
        ];
    }

    public function toDatabase($notifiable)
    {
        return [
            'type' => SystemNotification::TYPE_BOOKING_CANCELLED,
            'link_type' => SystemNotification::LINK_BOOKINGS_SHOW,
            'link_id' => $this->booking->id,
            'related_id' => $this->booking->id,
            'text' => 'Your PickStar booking has now been cancelled:' . $this->booking->name . ', ' . $this->booking->tz_date,
            'sub' => 'Review your booking details here. Questions? Call us on 1300 657 601'
        ];
    }
}

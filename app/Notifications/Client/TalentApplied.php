<?php

namespace Pickstar\Notifications\Client;

use Pickstar\Mail;
use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Pickstar\Booking\Booking;
use Pickstar\Message\SystemNotification;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Pickstar\Channels\NexmoSignedSmsChannel;
use Illuminate\Notifications\Messages\NexmoMessage;
use Pickstar\Notifications\ResolvesDeliveryChannels;

class TalentApplied extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Talent user instance.
     *
     * @var \Pickstar\User\User
     */
    public $talent;

    /**
     * Booking instance.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * Talents comment on why they are suitable
     */
    public $comment;

    /**
     * Create a new notification instance.
     *
     * @param \Pickstar\Booking\Booking $booking
     * @param \Pickstar\User\User $talent
     * @param $comment
     */
    public function __construct(User $talent, Booking $booking, $comment = null)
    {
        $this->talent = $talent;
        $this->booking = $booking;
        $this->comment = $comment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(ResolvesDeliveryChannels $notifiable)
    {
        if ($this->booking->talentShortlisted->pluck('id')->contains($this->talent->id)) {
            return $notifiable->viaChannels(['mail', 'broadcast', 'database', NexmoSignedSmsChannel::class]);
        }
        return $notifiable->viaChannels(['mail', 'broadcast', 'database']);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Mail\Mailable
     */
    public function toMail($notifiable)
    {
        return (new Mail\Client\TalentApplied($this->talent, $this->booking, $this->comment))
            ->to($notifiable->email);
    }

    public function toBroadcast($notifiable)
    {
        return [
            'notif_type' => SystemNotification::TYPE_TALENT_APPLIED,
            'link_type' => SystemNotification::LINK_REQUESTS_SHOW,
            'link_id' => $this->booking->id,
            'related_id' => $this->talent->id,
            'text' => $this->talent->name . ' has applied for your booking request!'
        ];
    }

    public function toDatabase($notifiable)
    {
        return [
            'notif_type' => SystemNotification::TYPE_TALENT_APPLIED,
            'link_type' => SystemNotification::LINK_REQUESTS_SHOW,
            'link_id' => $this->booking->id,
            'related_id' => $this->talent->id,
            'text' => $this->talent->name . ' has applied for your request: ' . $this->booking->name,
            'sub' => 'Ready to book? Click here!'
        ];
    }

    /**
     * Get the SMS representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\NexmoMessage
     */
    public function toNexmoSignedSms($notifiable)
    {
        $replacements = [
            ':url' => $this->booking->generateBitlyUrl(),
            ':client_name' => $notifiable->first_name,
            ':talent_name' => $this->talent->name
        ];

        $message = 'Hey :client_name, good news - a sports star you shortlisted for your opportunity has just applied! Book :talent_name now! :url or call us on 1300 657 601';

        return (new NexmoMessage)->content(strtr($message, $replacements))->from(config('services.nexmo.sms_from'));
    }
}

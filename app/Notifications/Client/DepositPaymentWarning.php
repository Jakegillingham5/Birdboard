<?php

namespace Pickstar\Notifications\Client;

use Pickstar\Mail;
use Illuminate\Bus\Queueable;
use Pickstar\Booking\Booking;
use Pickstar\Message\SystemNotification;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Pickstar\Channels\NexmoSignedSmsChannel;
use Illuminate\Notifications\Messages\NexmoMessage;
use Pickstar\Notifications\ResolvesDeliveryChannels;

class DepositPaymentWarning extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Booking instance.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * Create a new notification instance.
     *
     * @param \Pickstar\Booking\Booking $booking
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(ResolvesDeliveryChannels $notifiable)
    {
        return $notifiable->viaChannels([NexmoSignedSmsChannel::class, 'mail', 'broadcast', 'database']);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Mail\Mailable
     */
    public function toMail($notifiable)
    {
        return (new Mail\Client\DepositPaymentWarning($this->booking, $this->booking->depositPayment))
            ->to($notifiable->email);
    }

    /**
     * Get the SMS representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\NexmoMessage
     */
    public function toNexmoSignedSms($notifiable)
    {
        $message = 'Your PickStar deposit is due by %s. To view your booking and pay online go to %s or call 1300 657 601.';

        return (new NexmoMessage)
            ->content(sprintf($message, $this->booking->depositPayment->due_date->format('d/m/Y'), $this->booking->generateBitlyUrl()))->from(config('services.nexmo.sms_from'));
    }

    public function toBroadcast($notifiable)
    {
        return [
            'notif_type' => SystemNotification::TYPE_DEPOSIT_WARNING,
            'link_type' => SystemNotification::LINK_BOOKINGS_PAYMENT,
            'link_id' => $this->booking->id,
            'related_id' => $this->booking->id,
            'text' => 'Your deposit for ' . $this->booking->name . ' is due.'
        ];
    }

    public function toDatabase($notifiable)
    {
        $message = 'Your PickStar deposit $%s is due by %s. Please pay promptly to ensure your sports star booking is secured.';
        return [
            'notif_type' => SystemNotification::TYPE_DEPOSIT_WARNING,
            'link_type' => SystemNotification::LINK_BOOKINGS_PAYMENT,
            'link_id' => $this->booking->id,
            'related_id' => $this->booking->id,
            'text' => sprintf($message, $this->booking->depositPayment->amount, $this->booking->depositPayment->due_date->format('d/m/Y')),
            'sub' => 'Pay online or call 1300 657 601'
        ];
    }
}

<?php

namespace Pickstar\Notifications\Client;

use Illuminate\Notifications\Messages\BroadcastMessage;
use Pickstar\Mail;
use Pickstar\Message\SystemNotification;
use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Pickstar\Booking\Booking;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\NexmoMessage;
use Pickstar\Notifications\ResolvesDeliveryChannels;

class AppliedTalentDeclined extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Talent user instance.
     *
     * @var \Pickstar\User\User
     */
    public $talent;

    /**
     * Booking instance.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * Create a new notification instance.
     *
     * @param \Pickstar\Booking\Booking $booking
     * @param \Pickstar\User\User $talent
     */
    public function __construct(User $talent, Booking $booking)
    {
        $this->talent = $talent;
        $this->booking = $booking;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(ResolvesDeliveryChannels $notifiable)
    {
        return $notifiable->viaChannels(['mail', 'broadcast', 'database']);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Mail\Mailable
     */
    public function toMail($notifiable)
    {
        return (new Mail\Client\AppliedTalentDeclined($this->talent, $this->booking))
            ->to($notifiable->email);
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'type' => SystemNotification::TYPE_APPLIED_TALENT_DECLINED,
            'link_type' => SystemNotification::LINK_REQUESTS_SHOW,
            'link_id' => $this->booking->id,
            'related_id' => $this->talent->id,
            'text' => $this->talent->name . ' is no longer available for your opportunity.'
        ]);
    }

    public function toDatabase($notifiable)
    {
        return [
            'type' => SystemNotification::TYPE_APPLIED_TALENT_DECLINED,
            'link_type' => SystemNotification::LINK_REQUESTS_SHOW,
            'link_id' => $this->booking->id,
            'related_id' => $this->talent->id,
            'text' => 'Sorry' . $this->talent->name . ' is no longer available for your opportunity.',
            'sub' => 'Review other applicants by clicking on this notification. To avoid missing out, we recommend you make a decision and book your preferred star as soon as possible.'
        ];
    }
}

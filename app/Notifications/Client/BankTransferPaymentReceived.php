<?php

namespace Pickstar\Notifications\Client;

use Pickstar\Mail;
use Illuminate\Bus\Queueable;
use Pickstar\Payment\Payment;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Pickstar\Notifications\ResolvesDeliveryChannels;

class BankTransferPaymentReceived extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Payment instance.
     *
     * @var \Pickstar\Payment\Payment
     */
    public $payment;

    /**
     * Create a new notification instance.
     *
     * @param \Pickstar\Payment\Payment $payment
     */
    public function __construct(Payment $payment)
    {
        $this->payment = $payment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(ResolvesDeliveryChannels $notifiable)
    {
        return $notifiable->viaChannels(['mail']);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Mail\Mailable
     */
    public function toMail($notifiable)
    {
        return (new Mail\Client\BankTransferPaymentReceived($this->payment->booking, $this->payment))
            ->to($notifiable->email);
    }
}

<?php

namespace Pickstar\Notifications\Client;

use Illuminate\Support\Collection;
use Pickstar\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Pickstar\Notifications\ResolvesDeliveryChannels;

class UnreadMessages extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var Collection $threads
     */
    public $threads;

    /**
     * NewMessage constructor.
     * @param Collection $threads
     */
    public function __construct(Collection $threads)
    {
        $this->threads = $threads;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(ResolvesDeliveryChannels $notifiable)
    {
        return $notifiable->viaChannels(['mail']);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Mail\Mailable
     */
    public function toMail($notifiable)
    {
        return (new Mail\Client\UnreadMessages($this->threads, $notifiable))
            ->to($notifiable->email);
    }
}

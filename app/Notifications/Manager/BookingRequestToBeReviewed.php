<?php

namespace Pickstar\Notifications\Manager;

use Pickstar\Mail;
use Pickstar\Message\SystemNotification;
use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Pickstar\Booking\Booking;
use Pickstar\Notifications\OneSignalData;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\NexmoMessage;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use Pickstar\Notifications\ResolvesDeliveryChannels;

class BookingRequestToBeReviewed extends Notification implements ShouldQueue
{
    use Queueable,
        OneSignalData;

    /**
     * Booking instance.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * Create a new notification instance.
     *
     * @param \Pickstar\Booking\Booking $booking
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(ResolvesDeliveryChannels $notifiable)
    {
        return $notifiable->viaChannels(['mail', 'broadcast', 'database']);
    }



    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Mail\Mailable
     */
    public function toMail($notifiable)
    {
        return (new Mail\Manager\BookingRequestToBeReviewed($notifiable, $this->booking))
            ->to($notifiable->routeNotificationForMail($this));
    }

    public function toBroadcast($notifiable)
    {
        return [
            'notif_type' => SystemNotification::TYPE_REQUEST_TO_BE_REVIEWED,
            'link_type' => SystemNotification::LINK_REQUESTS_SHOW,
            'link_id' => $this->booking->id,
            'related_id' => $this->booking->id,
            'text' => 'We think your talent will be interested in this opportunity'
        ];
    }

    public function toDatabase($notifiable)
    {
        return [
            'notif_type' => SystemNotification::TYPE_REQUEST_TO_BE_REVIEWED,
            'link_type' => SystemNotification::LINK_REQUESTS_SHOW,
            'link_id' => $this->booking->id,
            'related_id' => $this->booking->id,
            'text' => 'We\'ve got a PickStar opportunity your talent might be interested in!'
        ];
    }
}

<?php

namespace Pickstar\Notifications\System;

use Pickstar\Mail;
use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Pickstar\Booking\Booking;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\NexmoMessage;
use Pickstar\Notifications\ResolvesDeliveryChannels;
use Illuminate\Notifications\Messages\SlackMessage;

class TalentAccepted extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Talent user instance.
     *
     * @var \Pickstar\User\User
     */
    public $talent;

    /**
     * Booking instance.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * Create a new notification instance.
     *
     * @param \Pickstar\Booking\Booking $booking
     * @param \Pickstar\User\User $talent
     */
    public function __construct(User $talent, Booking $booking)
    {
        $this->talent = $talent;
        $this->booking = $booking;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(ResolvesDeliveryChannels $notifiable)
    {
        return $notifiable->viaChannels(['mail', 'slack']);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Mail\Mailable
     */
    public function toMail($notifiable)
    {
        return (new Mail\System\TalentAccepted($this->talent, $this->booking))
            ->to($notifiable->email);
    }

    /**
     * Get the Slack representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return SlackMessage
     */
    public function toSlack($notifiable)
    {
        $url = url()->toWebApp("/bookings/{$this->booking->id}");
        return (new SlackMessage)
            ->content($this->talent->name . ' has been booked for $' . $this->booking->budget_per_star . ' :wook:')
            ->attachment(function ($attachment) use ($url) {
                $attachment->title('Booked for \'' . $this->booking->name . '\'', $url)
                            ->fields([
                                'Budget' => $this->booking->budget_per_star,
                                'Opportunity Name' => $this->booking->name,
                                'Opportunity Type' => $this->booking->opportunity_name,
                                'Budget' => $this->booking->budget_per_star,
                                'Client' => $this->booking->client->name
                                ]);
            });
    }
}

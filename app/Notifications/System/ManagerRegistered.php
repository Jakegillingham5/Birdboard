<?php

namespace Pickstar\Notifications\System;

use Pickstar\Mail;
use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Pickstar\Notifications\ResolvesDeliveryChannels;

class ManagerRegistered extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * New manager user.
     *
     * @var \Pickstar\User\User
     */
    public $manager;

    /**
     * Create a new notification instance.
     *
     * @param \Pickstar\User\User $$alent
     *
     * @return void
     */
    public function __construct(User $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(ResolvesDeliveryChannels $notifiable)
    {
        return $notifiable->viaChannels(['mail']);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Mail\Mailable
     */
    public function toMail($notifiable)
    {
        return (new Mail\System\ManagerRegistered($this->manager))
                ->to($notifiable->email);
    }
}

<?php

namespace Pickstar\Notifications\System;

use Pickstar\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Pickstar\Notifications\ResolvesDeliveryChannels;

class BidMade extends Notification implements ShouldQueue
{
    use Queueable;

    public $name;
    public $email;
    public $company;
    public $bid;
    public $type;

    /**
     * Create a new notification instance.
     */
    public function __construct($name, $email, $company, $bid, $type)
    {
        $this->name = $name;
        $this->email = $email;
        $this->company = $company;
        $this->bid = $bid;
        $this->type = $type;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(ResolvesDeliveryChannels $notifiable)
    {
        return $notifiable->viaChannels(['mail']);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Mail\Mailable
     */
    public function toMail($notifiable)
    {
        return (new Mail\System\BidMade($this->name, $this->email, $this->company, $this->bid, $this->type))
                ->to($notifiable->email);
    }
}

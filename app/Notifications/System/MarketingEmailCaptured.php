<?php

namespace Pickstar\Notifications\System;

use Pickstar\Mail;
use Illuminate\Bus\Queueable;
use Pickstar\Marketing\Email;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Pickstar\Notifications\ResolvesDeliveryChannels;

class MarketingEmailCaptured extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Email instance.
     *
     * @var \Pickstar\Marketing\Email
     */
    public $email;

    /**
     * Create a new notification instance.
     *
     * @param \Pickstar\Marketing\Email $email
     */
    public function __construct(Email $email)
    {
        $this->email = $email;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(ResolvesDeliveryChannels $notifiable)
    {
        return $notifiable->viaChannels(['mail']);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Mail\Mailable
     */
    public function toMail($notifiable)
    {
        return (new Mail\System\MarketingEmailCaptured($this->email))
            ->to($notifiable->email);
    }
}

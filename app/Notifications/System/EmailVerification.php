<?php

namespace Pickstar\Notifications\System;

use Pickstar\Mail;
use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Pickstar\Notifications\ResolvesDeliveryChannels;

class EmailVerification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(ResolvesDeliveryChannels $notifiable)
    {
        return $notifiable->viaChannels(['mail']);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Mail\Mailable
     */
    public function toMail($notifiable)
    {
        return (new Mail\System\EmailVerification($notifiable))
            ->to($notifiable->email);
    }
}

<?php

namespace Pickstar\Notifications\System;

use Illuminate\Contracts\Queue\ShouldQueue;
use Pickstar\Mail;
use Illuminate\Bus\Queueable;
use Pickstar\Booking\Booking;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\NexmoMessage;
use Pickstar\Notifications\ResolvesDeliveryChannels;

class CustomNotificationResponseReceived extends Notification implements ShouldQueue
{
    use Queueable;

    public $data;

    /**
     * Create a new notification instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(ResolvesDeliveryChannels $notifiable)
    {
        return $notifiable->viaChannels(['mail']);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Mail\Mailable
     */
    public function toMail($notifiable)
    {
        return (new Mail\System\CustomNotificationResponseReceived($this->data))
            ->to($notifiable->email);
    }
}

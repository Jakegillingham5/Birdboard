<?php

namespace Pickstar\Notifications\System;

use Pickstar\Mail;
use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Pickstar\Booking\Booking;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Pickstar\Channels\NexmoSignedSmsChannel;
use Illuminate\Notifications\Messages\NexmoMessage;
use Pickstar\Notifications\ResolvesDeliveryChannels;

class NoTalentResponse extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Talent who has not responded.
     *
     * @var \Pickstar\User\User
     */
    public $talent;

    /**
     * Booking that talent has not responded to.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * Create a new notification instance.
     *
     * @param \Pickstar\User\User $talent
     * @param \Pickstar\Booking\Booking $booking
     */
    public function __construct(Booking $booking, User $talent)
    {
        $this->booking = $booking;
        $this->talent = $talent;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(ResolvesDeliveryChannels $notifiable)
    {
        return $notifiable->viaChannels(['mail', NexmoSignedSmsChannel::class]);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new Mail\System\NoTalentResponse($this->booking, $this->talent))
            ->to($notifiable->email);
    }

    /**
     * Get the Nexmo / SMS representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return NexmoMessage
     */
    public function toNexmoSignedSms($notifiable)
    {
        $message = 'URGENT: Talent has not responded to an opportunity starting within 12 hours. Talent: %s. Booking Details: %s';

        return (new NexmoMessage)
            ->content(sprintf($message, $this->talent->name, $this->booking->generateBitlyUrl()))
            ->from(config('services.nexmo.sms_from'));
    }
}

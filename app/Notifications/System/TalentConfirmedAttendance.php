<?php

namespace Pickstar\Notifications\System;

use Pickstar\Mail;
use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Pickstar\Booking\Booking;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Pickstar\Notifications\ResolvesDeliveryChannels;

class TalentConfirmedAttendance extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Booking instance.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * User instance.
     *
     * @var \Pickstar\User\User
     */
    public $talent;

    /**
     * Create a new notification instance.
     *
     * @param \Pickstar\Booking\Booking $booking
     * @param \Pickstar\User\User $talent
     */
    public function __construct(Booking $booking, User $talent)
    {
        $this->booking = $booking;
        $this->talent = $talent;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(ResolvesDeliveryChannels $notifiable)
    {
        return $notifiable->viaChannels(['mail']);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Mail\Mailable
     */
    public function toMail($notifiable)
    {
        return (new Mail\System\TalentConfirmedAttendance($this->booking, $this->talent))
            ->to($notifiable->email);
    }
}

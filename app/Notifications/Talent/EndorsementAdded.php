<?php

namespace Pickstar\Notifications\Talent;

use Pickstar\Mail;
use Illuminate\Bus\Queueable;
use Pickstar\Talent\Endorsement;
use Pickstar\Notifications\OneSignalData;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Pickstar\Notifications\ResolvesDeliveryChannels;

class EndorsementAdded extends Notification implements ShouldQueue
{
    use Queueable,
        OneSignalData;

    /**
     * Endorsement
     *
     * @var Endorsement
     */
    public $endorsement;

    /**
     * Create a new notification instance.
     *
     * @param Endorsement $endorsement
     */
    public function __construct($endorsement)
    {
        $this->endorsement = $endorsement;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(ResolvesDeliveryChannels $notifiable)
    {
        return $notifiable->viaChannels(['mail']);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Mail\Mailable
     */
    public function toMail($notifiable)
    {
        return (new Mail\Talent\EndorsementAdded($notifiable, $this->endorsement))
            ->to($notifiable->routeNotificationForMail($this));
    }
}

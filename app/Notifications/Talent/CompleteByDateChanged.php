<?php

namespace Pickstar\Notifications\Talent;

use Pickstar\Mail;
use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Pickstar\Booking\Booking;
use Pickstar\Notifications\OneSignalData;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Pickstar\Channels\NexmoSignedSmsChannel;
use Illuminate\Notifications\Messages\NexmoMessage;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use Pickstar\Notifications\ResolvesDeliveryChannels;

class CompleteByDateChanged extends Notification implements ShouldQueue
{
    use Queueable,
        OneSignalData;

    /**
     * Booking instance.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * Create a new notification instance.
     *
     * @param \Pickstar\Booking\Booking $booking
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(ResolvesDeliveryChannels $notifiable)
    {
        return $notifiable->viaChannels(['mail', NexmoSignedSmsChannel::class, OneSignalChannel::class]);
    }

    /**
     * Get the One Signal representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \NotificationChannels\OneSignal\OneSignalMessage
     */
    public function toOneSignal($notifiable): OneSignalMessage
    {
        $message = 'Important: The client has updated the due date for online deliverables in an opportunity you applied for.';

        return $this->withOneSignalData(
            OneSignalMessage::create()
                ->setParameter('ios_badgeType', 'Increase')
                ->setParameter('ios_badgeCount', '1')
                ->subject('PickStar opportunity date changed.')
                ->body($message)
                ->url($this->booking->generateRequestBitlyUrl())
                ->setData('id', $this->booking->id)
        );
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Mail\Mailable
     */
    public function toMail($notifiable)
    {
        return (new Mail\Talent\CompleteByDateChanged($notifiable, $this->booking))
            ->to($notifiable->routeNotificationForMail($this));
    }

    /**
     * Get the Nexmo / SMS representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return NexmoMessage
     */
    public function toNexmoSignedSms(User $notifiable)
    {
        $replacements = [
            ':url' => $this->booking->generateRequestBitlyUrl()
        ];
        $message = 'Important: The client has updated the due date for online deliverables in an opportunity you applied for. Click here to view :url';

        return (new NexmoMessage)->content(strtr($message, $replacements))->from(config('services.nexmo.sms_from'));
    }
}

<?php

namespace Pickstar\Notifications\Talent;

use Illuminate\Support\Collection;
use Pickstar\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Pickstar\Message\Message;
use Pickstar\Notifications\ResolvesDeliveryChannels;

class NewMessage extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var Message $message
     */
    public $message;

    /**
     * NewMessage constructor.
     * @param Message $message
     */
    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(ResolvesDeliveryChannels $notifiable)
    {
        return $notifiable->viaChannels(['mail']);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Mail\Mailable
     */
    public function toMail($notifiable)
    {
        return (new Mail\Talent\NewMessage($this->message, $notifiable))
            ->to($notifiable->email);
    }
}

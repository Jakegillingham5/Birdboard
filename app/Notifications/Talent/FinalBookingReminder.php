<?php

namespace Pickstar\Notifications\Talent;

use Pickstar\Mail;
use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Pickstar\Booking\Booking;
use Pickstar\Message\SystemNotification;
use Pickstar\Notifications\OneSignalData;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Pickstar\Channels\NexmoSignedSmsChannel;
use Illuminate\Notifications\Messages\NexmoMessage;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use Pickstar\Notifications\ResolvesDeliveryChannels;

class FinalBookingReminder extends Notification implements ShouldQueue
{
    use Queueable,
        OneSignalData;

    /**
     * Booking instance.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * Create a new notification instance.
     *
     * @param string $accessToken
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(ResolvesDeliveryChannels $notifiable)
    {
        return $notifiable->viaChannels(['mail', NexmoSignedSmsChannel::class, OneSignalChannel::class, 'broadcast', 'database']);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Mail\Mailable
     */
    public function toMail($notifiable)
    {
        return (new Mail\Talent\FinalBookingReminder($notifiable, $this->booking))
            ->to($notifiable->routeNotificationForMail($this));
    }

    /**
     * Get the Nexmo / SMS representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return NexmoMessage
     */
    public function toNexmoSignedSms(User $notifiable)
    {
        $replacements = [
            ':time' => system_setting('final_talent_reminder'),
            ':url' => $this->booking->generateBitlyUrl()
        ];

        $message = "REMINDER! Your PickStar opportunity is just :time hours away! Booking Details: :url\nIssues? Call us as soon as possible on 1300 657 601.";

        return (new NexmoMessage)->content(strtr($message, $replacements))->from(config('services.nexmo.sms_from'));
    }

    /**
     * Get the One Signal representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \NotificationChannels\OneSignal\OneSignalMessage
     */
    public function toOneSignal($notifiable): OneSignalMessage
    {
        $replacements = [
            ':time' => system_setting('final_talent_reminder'),
        ];

        $message = 'REMINDER! Your PickStar opportunity is just :time hours away!';

        return $this->withOneSignalData(
            OneSignalMessage::create()
                ->setParameter('ios_badgeType', 'Increase')
                ->setParameter('ios_badgeCount', '1')
                ->subject('Your PickStar opportunity is happening soon!')
                ->body(strtr($message, $replacements))
                ->setData('id', $this->booking->id)
        );
    }

    public function toBroadcast($notifiable)
    {
        return [
            'notif_type' => SystemNotification::TYPE_TALENT_FINAL_REMINDER,
            'link_type' => SystemNotification::LINK_BOOKINGS_SHOW,
            'link_id' => $this->booking->id,
            'related_id' => $this->booking->id,
            'text' => 'REMINDER: It is almost time to do your thing!'
        ];
    }

    public function toDatabase($notifiable)
    {
//        TODO - customize for manager
        $replacements = [
            ':time' => system_setting('final_talent_reminder'),
        ];

        $message = 'REMINDER! Your PickStar opportunity is just :time hours away!';
        return [
            'notif_type' => SystemNotification::TYPE_TALENT_FINAL_REMINDER,
            'link_type' => SystemNotification::LINK_BOOKINGS_SHOW,
            'link_id' => $this->booking->id,
            'related_id' => $this->booking->id,
            'text' => strtr($message, $replacements),
            'sub' => 'View all the details here. If there are any questions or issues, please call 1300 657 601'
        ];
    }
}

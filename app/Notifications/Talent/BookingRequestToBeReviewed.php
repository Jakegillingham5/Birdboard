<?php

namespace Pickstar\Notifications\Talent;

use Pickstar\Mail;
use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Pickstar\Booking\Booking;
use Pickstar\Message\SystemNotification;
use Pickstar\Notifications\OneSignalData;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Pickstar\Channels\NexmoSignedSmsChannel;
use Illuminate\Notifications\Messages\NexmoMessage;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use Pickstar\Notifications\ResolvesDeliveryChannels;

class BookingRequestToBeReviewed extends Notification implements ShouldQueue
{
    use Queueable,
        OneSignalData;

    /**
     * Booking instance.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * Create a new notification instance.
     *
     * @param \Pickstar\Booking\Booking $booking
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(ResolvesDeliveryChannels $notifiable)
    {
        return $notifiable->viaChannels(['mail', NexmoSignedSmsChannel::class, OneSignalChannel::class, 'broadcast', 'database']);
    }

    /**
     * Get the One Signal representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \NotificationChannels\OneSignal\OneSignalMessage
     */
    public function toOneSignal($notifiable): OneSignalMessage
    {
        $replacements = [
            ':opportunity' => $this->booking->name,
            ':date' => $this->booking->getAppropriateDateString(),
            ':budget' => number_format($this->booking->calculateBudgetPerStarAsUserRate($notifiable), 2),
        ];

        if ($this->booking->appearanceIsInPerson()) {
            $message = 'PLEASE RESPOND. Booking request from PickStar. :opportunity, :date, Budget $:budget.';
        } else {
            $message = 'PLEASE RESPOND. Booking request from PickStar. :opportunity, Budget $:budget.';
        }

        return $this->withOneSignalData(
            OneSignalMessage::create()
                ->setParameter('ios_badgeType', 'Increase')
                ->setParameter('ios_badgeCount', '1')
                ->subject('We\'ve got a PickStar opportunity you might be interested in!')
                ->body(strtr($message, $replacements))
                ->url($this->booking->generateRequestBitlyUrl())
                ->setData('id', $this->booking->id)
        );
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Mail\Mailable
     */
    public function toMail($notifiable)
    {
        return (new Mail\Talent\BookingRequestToBeReviewed($notifiable, $this->booking))
            ->to($notifiable->routeNotificationForMail($this));
    }

    /**
     * Get the Nexmo / SMS representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return NexmoMessage
     */
    public function toNexmoSignedSms(User $notifiable)
    {
        $replacements = [
            ':opportunity' => $this->booking->name,
            ':date' => $this->booking->getAppropriateDateString(),
            ':budget' => number_format($this->booking->calculateBudgetPerStarAsUserRate($notifiable), 2),
            ':url' => $this->booking->generateRequestBitlyUrl()
        ];
        $message = 'PLEASE RESPOND. PickStar have a request you may be interested in for $:budget! Click here to view :url';

        return (new NexmoMessage)->content(strtr($message, $replacements))->from(config('services.nexmo.sms_from'));
    }

    public function toBroadcast($notifiable)
    {
        return [
            'notif_type' => SystemNotification::TYPE_REQUEST_TO_BE_REVIEWED,
            'link_type' => SystemNotification::LINK_REQUESTS_SHOW,
            'link_id' => $this->booking->id,
            'related_id' => $this->booking->id,
            'text' => 'We think you might be interested in this opportunity.'
        ];
    }

    public function toDatabase($notifiable)
    {
        return [
            'notif_type' => SystemNotification::TYPE_REQUEST_TO_BE_REVIEWED,
            'link_type' => SystemNotification::LINK_REQUESTS_SHOW,
            'link_id' => $this->booking->id,
            'related_id' => $this->booking->id,
            'text' => 'We think you could be a good fit for this PickStar opportunity!',
            'sub' => 'Check it out and apply!'
        ];
    }
}

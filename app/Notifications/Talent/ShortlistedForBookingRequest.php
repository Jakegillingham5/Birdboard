<?php

namespace Pickstar\Notifications\Talent;

use Pickstar\Mail;
use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Pickstar\Booking\Booking;
use Pickstar\Message\SystemNotification;
use Pickstar\Notifications\OneSignalData;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Pickstar\Channels\NexmoSignedSmsChannel;
use Illuminate\Notifications\Messages\NexmoMessage;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use Pickstar\Notifications\ResolvesDeliveryChannels;

class ShortlistedForBookingRequest extends Notification implements ShouldQueue
{
    use Queueable,
        OneSignalData;

    /**
     * Booking instance.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * Shortlist priority.
     *
     * @var int
     */
    public $shortlistPriority;

    /**
     * Create a new notification instance.
     *
     * @param \Pickstar\Booking\Booking $booking
     * @param int $shortlistPriority
     */
    public function __construct(Booking $booking, int $shortlistPriority)
    {
        $this->booking = $booking;
        $this->shortlistPriority = $shortlistPriority;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(ResolvesDeliveryChannels $notifiable)
    {
        return $notifiable->viaChannels(['mail', NexmoSignedSmsChannel::class, OneSignalChannel::class, 'broadcast', 'database']);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Mail\Mailable
     */
    public function toMail($notifiable)
    {
        return (new Mail\Talent\ShortlistedForBookingRequest($notifiable, $this->booking, $this->shortlistPriority))
            ->to($notifiable->routeNotificationForMail($this));
    }

    /**
     * Get the Nexmo / SMS representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return NexmoMessage
     */
    public function toNexmoSignedSms($notifiable)
    {
        $replacements = $this->replacements($notifiable);

        $message = 'PLEASE RESPOND. :priority for $:budget! Click here to view :url';

        return (new NexmoMessage)->content(strtr($message, $replacements))->from(config('services.nexmo.sms_from'));
    }

    /**
     * Get the One Signal representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \NotificationChannels\OneSignal\OneSignalMessage
     */
    public function toOneSignal($notifiable): OneSignalMessage
    {
        $replacements = $this->replacements($notifiable);

        if ($this->booking->appearanceIsInPerson()) {
            $message = 'PLEASE RESPOND. :priority! :opportunity, :date, Budget $:budget.';
        } else {
            $message = 'PLEASE RESPOND. :priority! :opportunity, Budget $:budget.';
        }

        if ($this->shortlistPriority === 1) {
            $subject = 'You\'re top of the shortlist for a PickStar request for $:budget!';
        } elseif ($this->shortlistPriority > 1 && $this->shortlistPriority <= 5) {
            $subject = 'You\'re in the top 5 of the shortlist for a PickStar request for $:budget!';
        } else {
            $subject = 'You\'re in the top 10 of the shortlist for a PickStar request for $:budget!';
        }

        return $this->withOneSignalData(
            OneSignalMessage::create()
                ->setParameter('ios_badgeType', 'Increase')
                ->setParameter('ios_badgeCount', '1')
                ->subject(strtr($subject, $replacements))
                ->body(strtr($message, $replacements))
                ->setData('id', $this->booking->id)
        );
    }

    /**
     * Get the replacements for messages.
     *
     * @return array
     */
    protected function replacements($notifiable): array
    {
        $replacements = [
            ':opportunity' => $this->booking->name,
            ':date' => $this->booking->getAppropriateDateString(),
            ':budget' => number_format($this->booking->calculateBudgetPerStarAsUserRate($notifiable), 2),
            ':url' => $this->booking->generateRequestBitlyUrl()
        ];

        if ($this->shortlistPriority === 1) {
            $replacements[':priority'] = 'You\'re top of the shortlist for a PickStar request';
        } elseif ($this->shortlistPriority > 1 && $this->shortlistPriority <= 5) {
            $replacements[':priority'] = 'You\'re in the top 5 of the shortlist for a PickStar request';
        } else {
            $replacements[':priority'] = 'You\'re in the top 10 of the shortlist for a PickStar request';
        }

        return $replacements;
    }

    public function toBroadcast($notifiable)
    {
        return [
            'notif_type' => SystemNotification::TYPE_SHORTLISTED_FOR_REQUEST,
            'link_type' => SystemNotification::LINK_REQUESTS_SHOW,
            'link_id' => $this->booking->id,
            'related_id' => $this->booking->id,
            'text' => 'You\'ve been shortlisted for an opportunity!'
        ];
    }

    public function toDatabase($notifiable)
    {
        $replacements = $this->replacements($notifiable);

        $message = ':priority for $:budget!';

        return [
            'notif_type' => SystemNotification::TYPE_SHORTLISTED_FOR_REQUEST,
            'link_type' => SystemNotification::LINK_REQUESTS_SHOW,
            'link_id' => $this->booking->id,
            'related_id' => $this->booking->id,
            'text' => strtr($message, $replacements)
        ];
    }
}

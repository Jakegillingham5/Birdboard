<?php

namespace Pickstar\Notifications\Talent;

use Illuminate\Support\Collection;
use Pickstar\Mail;
use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Pickstar\Booking\Booking;
use Pickstar\Message\SystemNotification;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\NexmoMessage;
use Pickstar\Notifications\ResolvesDeliveryChannels;

class UnreadMessages extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var Collection $threads
     */
    public $threads;

    /**
     * NewMessage constructor.
     * @param Collection $threads
     */
    public function __construct(Collection $threads)
    {
        $this->threads = $threads;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(ResolvesDeliveryChannels $notifiable)
    {
        return $notifiable->viaChannels(['mail']);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Mail\Mailable
     */
    public function toMail($notifiable)
    {
        return (new Mail\Talent\UnreadMessages($this->threads, $notifiable))
            ->to($notifiable->email);
    }
}

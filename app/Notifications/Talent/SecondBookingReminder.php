<?php

namespace Pickstar\Notifications\Talent;

use Pickstar\Mail;
use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Pickstar\Booking\Booking;
use Pickstar\Message\SystemNotification;
use Pickstar\Notifications\OneSignalData;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Pickstar\Channels\NexmoSignedSmsChannel;
use Illuminate\Notifications\Messages\NexmoMessage;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use Pickstar\Notifications\ResolvesDeliveryChannels;

class SecondBookingReminder extends Notification implements ShouldQueue
{
    use Queueable,
        OneSignalData;

    /**
     * Booking instance.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * Access token for the link.
     *
     * @var string
     */
    public $accessToken;

    /**
     * Create a new notification instance.
     *
     * @param string $accessToken
     */
    public function __construct(Booking $booking, string $accessToken)
    {
        $this->booking = $booking;
        $this->accessToken = $accessToken;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(ResolvesDeliveryChannels $notifiable)
    {
        return $notifiable->viaChannels(['mail', NexmoSignedSmsChannel::class, OneSignalChannel::class, 'broadcast', 'database']);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Mail\Mailable
     */
    public function toMail($notifiable)
    {
        return (new Mail\Talent\SecondBookingReminder($notifiable, $this->booking, $this->accessToken))
            ->to($notifiable->routeNotificationForMail($this));
    }

    /**
     * Get the Nexmo / SMS representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return NexmoMessage
     */
    public function toNexmoSignedSms(User $notifiable)
    {
        $message = "ACTION REQUIRED: Your PickStar opportunity is just %s hours away! Booking Details: %s\nPlease press the link below to confirm you are good to go! %s";

        return (new NexmoMessage)->content(sprintf(
            $message,
            now()->addDays(system_setting('second_talent_reminder'))->diffInRealHours(),
            $this->booking->generateBitlyUrl(),
            get_bitly_url(url()->temporarySignedRoute(
                'booking.attendance',
                now()->addDays(system_setting('second_talent_reminder')),
                ['token' => $this->accessToken]
            ))
        ))->from(config('services.nexmo.sms_from'));
    }

    /**
     * Get the One Signal representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \NotificationChannels\OneSignal\OneSignalMessage
     */
    public function toOneSignal($notifiable): OneSignalMessage
    {
        $replacements = [
            ':time' => now()->addDays(system_setting('second_talent_reminder'))->diffInHours(),
        ];

        return $this->withOneSignalData(
            OneSignalMessage::create()
                ->setParameter('ios_badgeType', 'Increase')
                ->setParameter('ios_badgeCount', '1')
                ->subject(strtr('Your PickStar opportunity is just :time hours away', $replacements))
                ->body('Please confirm you are good to go!')
                ->setData('id', $this->booking->id)
        );
    }

    public function toBroadcast($notifiable)
    {
        $replacements = [
            ':time' => system_setting('second_talent_reminder'),
        ];

        $message = 'REMINDER! Your PickStar opportunity is just :time hours away!';
        return [
            'notif_type' => SystemNotification::TYPE_TALENT_SECOND_REMINDER,
            'link_type' => SystemNotification::LINK_BOOKINGS_SHOW,
            'link_id' => $this->booking->id,
            'related_id' => $this->booking->id,
            'text' => strtr($message, $replacements)
        ];
    }

    public function toDatabase($notifiable)
    {
//        TODO - customize for managers
        $replacements = [
            ':time' => system_setting('second_talent_reminder'),
        ];

        $message = 'REMINDER! Your PickStar opportunity is just :time hours away!';
        return [
            'notif_type' => SystemNotification::TYPE_TALENT_SECOND_REMINDER,
            'link_type' => SystemNotification::LINK_BOOKINGS_SHOW,
            'link_id' => $this->booking->id,
            'related_id' => $this->booking->id,
            'text' => strtr($message, $replacements),
            'sub' => 'If there are any questions or issues, please call 1300 657 601.'
        ];
    }
}

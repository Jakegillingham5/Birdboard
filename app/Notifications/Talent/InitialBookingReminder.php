<?php

namespace Pickstar\Notifications\Talent;

use Pickstar\Mail;
use Illuminate\Bus\Queueable;
use Pickstar\Booking\Booking;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Pickstar\Message\SystemNotification;
use Pickstar\Notifications\ResolvesDeliveryChannels;

class InitialBookingReminder extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Booking instance.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * Create a new notification instance.
     *
     * @param \Pickstar\Booking\Booking $booking
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(ResolvesDeliveryChannels $notifiable)
    {
        return $notifiable->viaChannels(['mail', 'database', 'broadcast']);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new Mail\Talent\InitialBookingReminder($this->booking, $notifiable))
            ->to($notifiable->routeNotificationForMail($this));
    }

    public function toBroadcast($notifiable)
    {
        return [
            'notif_type' => SystemNotification::TYPE_TALENT_INITIAL_REMINDER,
            'link_type' => SystemNotification::LINK_BOOKINGS_SHOW,
            'link_id' => $this->booking->id,
            'related_id' => $this->booking->id,
            'text' => 'REMINDER: You are booked for a PickStar event in 14 days.'
        ];
    }

    public function toDatabase($notifiable)
    {
//        TODO - customize for manager
        $replacements = [
            ':time' => system_setting('initial_talent_reminder'),
        ];

        $message = 'A quick reminder that you have a PickStar event coming up in :time days!';
        return [
            'notif_type' => SystemNotification::TYPE_TALENT_INITIAL_REMINDER,
            'link_type' => SystemNotification::LINK_BOOKINGS_SHOW,
            'link_id' => $this->booking->id,
            'related_id' => $this->booking->id,
            'text' => strtr($message, $replacements),
            'sub' => 'If there are any questions or issues, please call 1300 657 601.'
        ];
    }
}

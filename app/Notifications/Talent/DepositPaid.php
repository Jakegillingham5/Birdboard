<?php

namespace Pickstar\Notifications\Talent;

use Pickstar\Mail;
use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Pickstar\Booking\Booking;
use Pickstar\Message\SystemNotification;
use Pickstar\Notifications\OneSignalData;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Pickstar\Channels\NexmoSignedSmsChannel;
use Illuminate\Notifications\Messages\NexmoMessage;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use Pickstar\Notifications\ResolvesDeliveryChannels;

class DepositPaid extends Notification implements ShouldQueue
{
    use Queueable,
        OneSignalData;

    /**
     * Booking that had deposit paid.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * Create a new notification instance.
     *
     * @param \Pickstar\Booking\Booking $booking
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(ResolvesDeliveryChannels $notifiable)
    {
        return $notifiable->viaChannels(['mail', NexmoSignedSmsChannel::class, OneSignalChannel::class, 'database', 'broadcast']);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Mail\Mailable
     */
    public function toMail($notifiable)
    {
        return (new Mail\Talent\DepositPaid($notifiable, $this->booking))
            ->to($notifiable->routeNotificationForMail($this));
    }

    /**
     * Get the SMS representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\NexmoMessage
     */
    public function toNexmoSignedSms($notifiable)
    {
        $replacements = [
            ':opportunity' => $this->booking->name,
            ':date' => $this->booking->tz_date_instance->format(User::DATE_FORMAT_TZ),
            ':budget' => number_format($this->booking->calculateBudgetPerStarAsUserRate($notifiable), 2),
            ':url' => $this->booking->generateBitlyUrl()
        ];

        if ($this->booking->appearanceIsInPerson()) {
            $message = 'It\'s official, the client has paid a deposit and your PickStar opportunity is locked in! :opportunity (:date) for $:budget. :url.';
        } else {
            $message = 'It\'s official, the client has paid a deposit and your PickStar opportunity is locked in! :opportunity for $:budget. :url.';
        }

        $message .= config('services.nexmo.signature');

        return (new NexmoMessage)->content(strtr($message, $replacements))->from(config('services.nexmo.sms_from'));
    }

    /**
     * Get the One Signal representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \NotificationChannels\OneSignal\OneSignalMessage
     */
    public function toOneSignal($notifiable): OneSignalMessage
    {
        $replacements = [
            ':opportunity' => $this->booking->name,
            ':date' => $this->booking->tz_date_instance->format(User::DATE_FORMAT_TZ),
            ':budget' => number_format($this->booking->calculateBudgetPerStarAsUserRate($notifiable), 2),
        ];

        if ($this->booking->appearanceIsInPerson()) {
            $message = 'It\'s official, the client has paid a deposit and your PickStar opportunity is locked in! :opportunity (:date) for $:budget.';
        } else {
            $message = 'It\'s official, the client has paid a deposit and your PickStar opportunity is locked in! :opportunity for $:budget.';
        }

        return $this->withOneSignalData(
            OneSignalMessage::create()
                ->setParameter('ios_badgeType', 'Increase')
                ->setParameter('ios_badgeCount', '1')
                ->subject('Your PickStar opportunity is locked in!')
                ->body(strtr($message, $replacements))
                ->setData('id', $this->booking->id)
        );
    }

    public function toBroadcast($notifiable)
    {
        return [
            'notif_type' => SystemNotification::TYPE_TALENT_DEPOSIT_PAID,
            'link_type' => SystemNotification::LINK_BOOKINGS_SHOW,
            'link_id' => $this->booking->id,
            'related_id' => $this->booking->id,
            'text' => 'The client has paid the deposit for your opportunity: ' . $this->booking->name
        ];
    }

    public function toDatabase($notifiable)
    {
        return [
            'notif_type' => SystemNotification::TYPE_TALENT_DEPOSIT_PAID,
            'link_type' => SystemNotification::LINK_BOOKINGS_SHOW,
            'link_id' => $this->booking->id,
            'related_id' => $this->booking->id,
            'text' => 'It\'s official, the client has paid a deposit and your PickStar opportunity is locked in!' . $this->booking->name
        ];
    }
}

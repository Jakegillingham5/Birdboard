<?php

namespace Pickstar\Notifications\Talent;

use Pickstar\Mail;
use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Pickstar\Booking\Booking;
use Pickstar\Message\SystemNotification;
use Pickstar\Notifications\OneSignalData;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Pickstar\Channels\NexmoSignedSmsChannel;
use Illuminate\Notifications\Messages\NexmoMessage;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use Pickstar\Notifications\ResolvesDeliveryChannels;

class PaymentSent extends Notification implements ShouldQueue
{
    use Queueable,
        OneSignalData;

    /**
     * Booking that had deposit paid.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * Create a new notification instance.
     *
     * @param \Pickstar\Booking\Booking $booking
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(ResolvesDeliveryChannels $notifiable)
    {
        return $notifiable->viaChannels(['mail', NexmoSignedSmsChannel::class, OneSignalChannel::class, 'broadcast', 'database']);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Mail\Mailable
     */
    public function toMail($notifiable)
    {
        return (new Mail\Talent\PaymentSent($notifiable, $this->booking))
            ->to($notifiable->routeNotificationForMail($this));
    }

    /**
     * Get the SMS representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\NexmoMessage
     */
    public function toNexmoSignedSms($notifiable)
    {
        $replacements = [
            ':opportunity' => $this->booking->name,
            ':date' => $this->booking->tz_date_instance->format(User::DATE_FORMAT_TZ),
            ':budget' => number_format($this->booking->calculateBudgetPerStarAsUserRate($notifiable), 2),
            ':url' => $this->booking->generateBitlyUrl()
        ];

        if ($this->booking->appearanceIsInPerson()) {
            $message = 'The payment for your PickStar event has been sent! :opportunity (:date) for $:budget. :url.';
        } else {
            $message = 'The payment for your PickStar event has been sent! :opportunity for $:budget. :url.';
        }

        $message .= config('services.nexmo.signature');

        return (new NexmoMessage)->content(strtr($message, $replacements))->from(config('services.nexmo.sms_from'));
    }

    /**
     * Get the One Signal representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \NotificationChannels\OneSignal\OneSignalMessage
     */
    public function toOneSignal($notifiable): OneSignalMessage
    {
        $replacements = [
            ':opportunity' => $this->booking->name,
            ':date' => $this->booking->tz_date_instance->format(User::DATE_FORMAT_TZ),
            ':budget' => number_format($this->booking->calculateBudgetPerStarAsUserRate($notifiable), 2),
        ];

        if ($this->booking->appearanceIsInPerson()) {
            $message = 'The payment for your PickStar event has been sent! :opportunity (:date) for $:budget.';
        } else {
            $message = 'The payment for your PickStar event has been sent! :opportunity for $:budget.';
        }

        return $this->withOneSignalData(
            OneSignalMessage::create()
                ->setParameter('ios_badgeType', 'Increase')
                ->setParameter('ios_badgeCount', '1')
                ->subject('PickStar Payment Sent')
                ->body(strtr($message, $replacements))
                ->setData('id', $this->booking->id)
        );
    }

    public function toBroadcast($notifiable)
    {
        return [
            'notif_type' => SystemNotification::TYPE_TALENT_PAYMENT_SENT,
            'link_type' => SystemNotification::LINK_BOOKINGS_SHOW,
            'link_id' => $this->booking->id,
            'related_id' => $this->booking->id,
            'text' => 'Good news! We\'ve paid you!'
        ];
    }

    public function toDatabase($notifiable)
    {
        return [
            'notif_type' => SystemNotification::TYPE_TALENT_PAYMENT_SENT,
            'link_type' => SystemNotification::LINK_BOOKINGS_SHOW,
            'link_id' => $this->booking->id,
            'related_id' => $this->booking->id,
            'text' => 'You\'ve just been paid for a PickStar opportunity!' . $this->booking->name
        ];
    }
}

<?php

namespace Pickstar\Notifications\Talent;

use Pickstar\Mail;
use Pickstar\Message\SystemNotification;
use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Pickstar\Notifications\ResolvesDeliveryChannels;

class AccountActivated extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(ResolvesDeliveryChannels $notifiable)
    {
        return $notifiable->viaChannels(['mail', 'broadcast', 'database']);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param \Pickstar\User\User $notifiable
     *
     * @return \Illuminate\Mail\Mailable
     */
    public function toMail(User $notifiable)
    {
        return (new Mail\Talent\AccountActivated($notifiable))
            ->to($notifiable->routeNotificationForMail($this));
    }

    public function toBroadcast($notifiable)
    {
        return [
            'notif_type' => SystemNotification::TYPE_ACCOUNT_ACTIVATED,
            'link_type' => SystemNotification::LINK_PROFILE_SHOW,
            'link_id' => $notifiable->id,
            'related_id' => $notifiable->id,
            'text' => 'Your account and profile is now live.'
        ];
    }

    public function toDatabase($notifiable)
    {
        return [
            'notif_type' => SystemNotification::TYPE_ACCOUNT_ACTIVATED,
            'link_type' => SystemNotification::LINK_PROFILE_SHOW,
            'link_id' => $notifiable->id,
            'related_id' => $notifiable->id,
            'text' => 'Congratulations, your PickStar account has been activated.',
            'sub' => 'You can start applying for the paid opportunities you\'re interested in!'
        ];
    }
}

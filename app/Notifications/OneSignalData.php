<?php

namespace Pickstar\Notifications;

use BadMethodCallException;
use NotificationChannels\OneSignal\OneSignalMessage;

trait OneSignalData
{
    /**
     * Add default data to a OneSignal message.
     *
     * @return \NotificationChannels\OneSignal\OneSignalMessage
     */
    public function withOneSignalData(): OneSignalMessage
    {
        if (func_num_args() === 1) {
            list($data, $message) = [[], func_get_arg(0)];
        } else if (func_num_args() === 2) {
            list($data, $message) = func_get_args();
        }

        if (!isset($message) || !$message instanceof OneSignalMessage) {
            throw new BadMethodCallException('Method "withOneSignalData" expected a OneSignalMessage instance.');
        }

        if (!isset($data['notification_type'])) {
            $data['notification_type'] = static::class;
        }

        foreach ($data as $key => $value) {
            $message->setData($key, $value);
        }

        return $message;
    }
}

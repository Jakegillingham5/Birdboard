<?php

namespace Pickstar\Notifications;

interface ResolvesDeliveryChannels
{
    /**
     * Get the notifiables delivery channels.
     *
     * @return array
     */
    public function viaChannels($channels): array;
}

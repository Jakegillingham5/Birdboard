<?php

namespace Pickstar\Validation\Rules;

use Illuminate\Contracts\Validation\Rule;

class NotPhone implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return !preg_match("/[0-9 -]{9,}/", $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute must not contain a phone number.';
    }
}

<?php

namespace Pickstar\Validation\Rules;

class Password
{
    /**
     * Validate that a string is a strong enough password.
     *
     * Password must contain a lowercase letter, an uppercase letter, a number, and
     * be at least 6 characters.
     *
     * @param string $attribute
     * @param string $value
     * @param array $parameters
     *
     * @return bool
     */
    public function validate($attribute, $value, $parameters): bool
    {
        return !! preg_match('#^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{6,}$#', $value);
    }
}

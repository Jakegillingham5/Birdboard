<?php

namespace Pickstar\Validation\Rules;

use GuzzleHttp\Client;

class Recaptcha
{
    /**
     * Validate google recaptcha response
     *
     * @param string $attribute
     * @param string $value
     * @param array $parameters
     *
     * @return bool
     */
    public function validate($attribute, $value, $parameters): bool
    {
        $http = new Client();

        $googleRecaptchaResponse = $http->post(config('services.recaptcha.url'), ['query' => [
            'secret' => config('services.recaptcha.secret'),
            'response' => $value,
        ]]);

        $response = json_decode($googleRecaptchaResponse->getBody());

        if ($response && $response->success) {
            return true;
        }

        return false;
    }
}

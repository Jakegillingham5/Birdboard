<?php

namespace Pickstar\Validation\Rules;

class Domain
{
    /**
     * Validate that a domain matches the given format.
     *
     * Example: "domain:codium.com.au" expects given value to match "https://codium.com.au".
     *
     * Allows empty values.
     *
     * @param string $attribute
     * @param string $value
     * @param array $parameters
     *
     * @return bool
     */
    public function validate($attribute, $value, $parameters): bool
    {
        if (count($parameters)) {
            if (empty($value)) {
                return true;
            }

            $domain = $parameters[0];
            $pattern = "#^(https|http)://([a-z0-9-]+\.)*".preg_quote($domain)."(/.*)?$#";

            return !! preg_match($pattern, $value);
        }

        return false;
    }
}

<?php

namespace Pickstar\Validation\Rules;

class Mobile
{
    /**
     * Validate that a string is valid mobile
     *
     * Mobile must contains digits or + symbol only
     * and at least 10 characters.
     *
     * @param string $attribute
     * @param string $value
     * @param array $parameters
     *
     * @return bool
     */
    public function validate($attribute, $value, $parameters): bool
    {
        $value = str_replace(' ', '', $value);
        return !! preg_match('/^(\d|\+)[0-9]{9,12}$/', $value);
    }
}

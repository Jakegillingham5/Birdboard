<?php

namespace Pickstar\Services;

use SendGrid;
use Exception;
use SendGrid\Mail\Mail;
use Pickstar\User\AdminUser;
use Illuminate\Support\Collection;

class SendGridTransactionalTemplateService
{
    /**
     * SendGrid instance.
     *
     * @var \SendGrid
     */
    protected $sendGrid;

    /**
     * Create a new SendGrid transactional template service instance.
     *
     * @param \SendGrid $sendGrid
     */
    public function __construct(SendGrid $sendGrid)
    {
        $this->sendGrid = $sendGrid;
    }

    /**
     * Dispatch a SendGrid mail for a given template with a collection of personalizations.
     *
     * @param string $template
     * @param \Illuminate\Support\Collection $personalizations
     * @param callable $before
     *
     * @return void
     */
    public function dispatch(string $template, Collection $personalizations, Array $categories, callable $before = null): void
    {
        if ($personalizations->isEmpty()) {
            return;
        }

        $mail = new Mail(config('pickstar.marketing_from.address'));

        $mail->setFrom(config('pickstar.marketing_from.address'), config('pickstar.marketing_from.name'));

        $mail->setTemplateId($template);

        $mail->setSubscriptionTracking(true);

        $mail->addCategories($categories);

        $personalizations->each(function ($personalization) use ($mail) {
            $mail->addPersonalization($personalization);
        });

        if ($before) {
            $before($mail);
        }

        try {
            $response = $this->sendGrid->send($mail);

            if (!in_array($response->statusCode(), [200, 202], true)) {
                logger()->alert('Unable to dispatch SendGrid transactional template with given personalizations.', [
                    'sendgrid_response_body' => $response->body(),
                    'personalizations' => $personalizations->toArray()
                ]);
            }
        } catch (Exception $exception) {
            report($exception);
        }
    }
}

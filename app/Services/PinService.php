<?php
/**
 * Created by PhpStorm.
 * User: jpickstar
 * Date: 2019-05-30
 * Time: 14:29
 */

namespace Pickstar\Services;

use Pickstar\Pin\Client;

class PinService
{
    /**
     * @var Client
     */
    protected static $_client;

    /**
     * PinService constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        static::$_client = $client;
    }

    /**
     * Fetches the API client which runs the Pin API calls.
     *
     * @return Client
     */
    public function getClient()
    {
        return static::$_client;
    }

    public function getTransactionAmount($token)
    {
        return $this->getTransaction($token)->amount / 100;
    }

    public function getTransaction($token)
    {
        return $this->getClient()->call('charges/' . $token )->response;
    }


}
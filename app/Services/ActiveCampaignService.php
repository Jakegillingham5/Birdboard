<?php
/**
 * Created by PhpStorm.
 * User: jpickstar
 * Date: 2019-04-24
 * Time: 09:43
 */

namespace Pickstar\Services;


use Pickstar\ActiveCampaign\Client;

/**
 * Class ActiveCampaignService
 * @package Pickstar\Services
 */
class ActiveCampaignService
{
    const LIST_NEWSLETTER = 'Newsletter';
    const STATUS_SUBSCRIBED = '1';
    const STATUS_UNSUBSCRIBED = '2';

    /**
     * @var Client
     */
    protected static $_client;

    /**
     * @var array
     */
    protected static $_contacts = [];

    /**
     * @var array
     */
    protected static $_contact_ids = [];

    /**
     * @var array
     */
    protected static $_lists = [];

    /**
     * @var array
     */
    protected static $_tags = [];

    /**
     * @var array
     */
    protected static $_fields = [];

    /**
     * @var array
     */
    protected static $_fieldIndex = [];

    /**
     * ActiveCampaignService constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        static::$_client = $client;
    }

    /**
     * Fetches the API client which runs the ActiveCampaign API calls.
     *
     * @return Client
     */
    public function getClient()
    {
        return static::$_client;
    }

    /**
     * Update a contact record in ActiveCampaign with a new payload.
     *
     * @param $email
     * @param $payload
     */
    public function updateContact($email, $payload)
    {
        $payload['email'] = $email;
        $this->getClient()->post('contact/sync', ['contact' => $payload]);
    }

    /**
     * Update tags in ActiveCampaign.  Structure should be ['tag 1', 'tag 2'].  The names are resolved against
     * Tag Id's in the ActiveCampaign System.  You do not need to suppy Pickstar Tag Id's or AC ones.
     *
     * @param string $email
     * @param array $tags
     */
    public function updateTags($email, array $tags)
    {
        if(count($tags) === 0){
            throw new \InvalidArgumentException('Tags must be provided, empty array detected');
        }
        $payload = $this->buildTagPayload($email, $tags);
        foreach ($payload as $tag){
            $this->getClient()->post('contactTags',['contactTag' => $tag]);
        }
    }

    /**
     * Takes the payload and applies an API call for any custom fields that have data.
     *
     * @param $email
     * @param $payload
     */
    public function updateCustomFields($email, $payload)
    {
        $contact_id = $this->getContactId($email);
        $customFieldPayload = [];
        foreach ($this->getCustomFieldIndexes() as $id => $name){
            if(array_key_exists($name, $payload)){
                $customFieldPayload[] = [
                    'contact'   => $contact_id,
                    'field'     => $id,
                    'value'     => $payload[$name],
                ];
            }
        }

        foreach ($customFieldPayload as $field) {
            $this->getClient()->post('fieldValues', ['fieldValue' => $field]);
        }
    }

    /**
     * Updates the subscription status of a contact in ActiveCampaign against a list of your choice.
     *
     * @param string $email     The email address of the contact being updated
     * @param string $list      The name of the list being subscribed to
     * @param bool $subscribe   Subscription status, true for subscribe, false for unsubscribe.
     */
    public function updateListSubscription($email, $list, $subscribe = true)
    {
        $payload = [
            [
                'list'      => $this->getListAttribute($list, 'id'),
                'contact'   => $this->getContactAttribute($email, 'id'),
                'status'    => $subscribe ? static::STATUS_SUBSCRIBED: static::STATUS_UNSUBSCRIBED,
            ]
        ];
        foreach ($payload as $listPayload) {
            $this->getClient()->post('contactLists',['contactList' => $listPayload]);
        }
    }

    /**
     * Similar function to updateListSubscription(), however this allows you to simply specify the ActiveCampaign ID's
     * to update subscription status.  Useful for when you have already retrieved the AC ID's for other business logic.
     *
     * @param $contact_id       The AC ID of an email contact
     * @param $list_id          The AC List ID the contact is subscribed to
     * @param bool $subscribe   Status of subscription
     */
    public function updateSubscriptionById($contact_id, $list_id, $subscribe = true)
    {
        $payload = [
            [
                'list'      => $list_id,
                'contact'   => $contact_id,
                'status'    => $subscribe ? static::STATUS_SUBSCRIBED: static::STATUS_UNSUBSCRIBED,
            ]
        ];
        foreach ($payload as $listPayload) {
            $this->getClient()->post('contactLists',['contactList' => $listPayload]);
        }

    }

    /**
     * Checks current subscriptions, and moves them from one to the other depending on their status in the system.  The
     * different lists move clients from lead -> extended lead -> client.  Other user types wouldn't tend to change lists.
     *
     * @param string $email     The email of the AC contact record
     * @param string $listName  The name of the AC subscription list
     */
    public function moveListSubscriptionIfRequired($email, $listName)
    {

        $contact    = $this->getContact($email);
        $listId     = $this->getListId($listName);
        $subscriptions = [];

        foreach ($contact->contactLists as $listObject){
            //Newsletter is the only list not managed as a transition
            if($listObject->list === $this->getListId(static::LIST_NEWSLETTER)){
                continue;
            }

            if($listObject->status === static::STATUS_SUBSCRIBED && $listObject->list !== $listId) {
                $this->updateSubscriptionById($contact->contact->id, $listObject->list, false);
            } else if($listObject->status === static::STATUS_SUBSCRIBED){
                $subscriptions[$listObject->list] = $listObject;
            }
        }

        if(!array_key_exists($listId, $subscriptions)){
            $this->updateListSubscription($email, $listName);
        }
    }

    /**
     * A shorthand method to fetch a contact, and then the attribute of that contact, or throw an error in the process.
     *
     * @param string $email        The email of the AC contact record
     * @param string $attribute    The attribute of the record you are looking to fetch
     * @return mixed
     *
     * @throws \InvalidArgumentException
     */
    public function getContactAttribute($email, $attribute)
    {
        $contact = $this->getContact($email);

        if(property_exists($contact->contact, $attribute)){
            return $contact->contact->$attribute;
        } else {
            throw new \InvalidArgumentException(('Unable to retrieve attribute ' . $attribute));
        }
    }

    /**
     * Get the AC contact object for a given email.
     *
     * @param string $email    The email of the AC record
     * @return mixed
     */
    public function getContact($email)
    {
        if(array_key_exists($email, static::$_contacts)){
            $contact = static::$_contacts[$email];
        } else {
            $contact = $this->fetchContact($email);
        }

        return $contact;
    }

    /**
     * Get the AC contact id for a given email
     *
     * @param string $email
     * @return mixed|null
     */
    public function getContactId($email)
    {
        if(array_key_exists($email, static::$_contact_ids)){
            $id = static::$_contact_ids[$email];
        } else {
            $id = $this->fetchContactId($email);
        }

        return $id;
    }

    /**
     * Get the values of custom fields for a specific client.
     * @return array
     */
    public function getCustomFields()
    {
        if(count(static::$_fields) === 0){
            return $this->fetchCustomFields();
        }

        return static::$_fields;
    }

    public function getCustomField($name)
    {
        if(count(static::$_fields) === 0){
            $this->fetchCustomFields();
        }

        if(!array_key_exists($name, static::$_fields)){
            $this->fetchCustomFields($name);
        }

        if(array_key_exists($name, static::$_fields)){
            return static::$_fields[$name];
        }

        throw new \InvalidArgumentException('Unable to field field ' . $name);
    }

    /**
     * Get all of the lists from the ActiveCampaign API.  Note, this call is cached in a static property of the class.
     * @return array
     */
    public function getLists()
    {
        if(count(static::$_lists) === 0){
            $this->initialiseLists();
        }

        return static::$_lists;
    }

    /**
     * A shorthand method to fetch a list, and then the attribute of that list, or throw an error in the process.
     *
     * @param string $name           The name of the list you want to fetch
     * @param string $attribute      The attribute of the list you want to access
     * @return mixed
     *
     * @throws \InvalidArgumentException
     */
    public function getListAttribute($name, $attribute)
    {
        $list = $this->getList($name);

        if($list === null){
            throw new \InvalidArgumentException('List ' . $name .' does not exist');
        }
        if(property_exists($list, $attribute)){
            return $list->$attribute;
        }
    }

    /**
     * Get the ActiveCampaign tag object.
     *
     * Note, this request is cached in a class property
     *
     * @param string $name      The name of the tag you are looking for
     * @return \stdClass mixed  The ActiveCampaign tag object.
     */
    public function getTag($name)
    {
        if(count(static::$_tags) === 0){
            $this->fetchTags();
        }

        if(!array_key_exists($name, static::$_tags)){
            $this->fetchTags($name);
        }

        if(array_key_exists($name, static::$_tags)){
            return static::$_tags[$name];
        }

        static::$_tags[$name] = $this->createTag($name);

        return static::$_tags[$name];
    }

    /**
     * Get a specific list object from ActiveCampaign.  Note that this relies on the cached getLists() method.
     *
     * @param string name       The name of the ActiveCampaign list being retrieved.
     * @return \stdClass|null   The ActiveCampaign list object.
     */
    public function getList($name)
    {

        $lists = $this->getLists();

        if(array_key_exists($name, $lists)){
            return $lists[$name];
        }

        return null;
    }

    /**
     * Get the ActiveCampaign custom field indexes, used for updating or adding custom field values via the API
     *
     * @return integer[]
     */
    public function getCustomFieldIndexes()
    {
        if(count(static::$_fieldIndex)=== 0){
            $this->fetchCustomFields();
        }

        return static::$_fieldIndex;
    }

    /**
     * Create a new tag in activeCampaign if it doesn't already exist
     *
     * @param string $name  The name of the tag being created
     * @return \stdClass    The created tag object when succeesful.
     */
    protected function createTag($name)
    {
        $response = $this->getClient()->post('tags', ['tag' => [
            'tag' => trim($name),
            'tagType' => 'contact',
        ]]);

        return $response->tag;
    }

    /**
     * Takes user input of an email address and an array of text tags and converts them to the payload needed to update
     * ActiveCampaign
     *
     * @param string $email         The email of the AC record
     * @param array $tags           An array of tag objects returned by the ActiveCampaign API.
     * @return array
     */
    protected function buildTagPayload($email, $tags)
    {
        $id = $this->getContactAttribute($email, 'id');

        $payload = [];
        foreach ($tags as $tag) {
            try {
                $acTag = $this->getTag($tag);
                $payload[] = [
                    'contact' => $id,
                    'tag' => $acTag->id,
                ];
            } catch (\Exception $e) {
                $acTag = $this->getTag($tag);
                $payload[] = [
                    'contact' => $id,
                    'tag' => $acTag->id,
                ];
            }
        }

        return $payload;
    }

    /**
     * @param null $searchTerm
     * @return array
     */
    protected function fetchTags($searchTerm = null)
    {
        if($searchTerm !== null) {
            $tags = $this->getClient()->call('tags', ['query' => [
                'limit' => '100',
                'search' => trim($searchTerm),
            ]]);
        } else {
            $tags = $this->getClient()->call('tags', ['query' => [
                'limit' => '100'
            ]]);
        }

        foreach ($tags->tags as $tag) {
            static::$_tags[$tag->tag] = $tag;
        }

        return static::$_tags;
    }

    /**
     *
     */
    protected function initialiseLists()
    {
        $lists = $this->getClient()->call('lists');

        foreach ($lists->lists as $list) {
            static::$_lists[trim($list->name)] = $list;
        }
    }

    /**
     * @param $email
     * @return mixed
     */
    protected function fetchContact($email)
    {

        $id = $this->fetchContactId($email);

        if($id !== null){
            $path = 'contacts/' . $id;
            $contact = $this->getClient()->call($path);

            static::$_contacts[$email] = $contact;
            return static::$_contacts[$email];
        } else {
            throw new \InvalidArgumentException('Unable to fetch record, no id found for email ' . $email);
        }
    }

    /**
     * @param $email
     * @return |null
     */
    protected function fetchContactId($email)
    {
        $data = $this->getClient()->query('contacts', [
            'email' => $email
        ]);

        foreach ($data->contacts as $contact) {
            return $contact->id;
        }
        return null;
    }

    /**
     * @param $name
     * @return mixed
     */
    protected function getListId($name)
    {
        $lists = $this->getLists();
        if(array_key_exists($name, $lists)){
            return $lists[$name]->id;
        } else {
            throw new \InvalidArgumentException('List ' . $name . ' not found');
        }
    }

    /**
     * @return array
     */
    protected function fetchCustomFields($searchTerm = null)
    {
        if($searchTerm !== null) {
            $fields = $this->getClient()->call('fields', ['query' => [
                'limit' => '100',
                'search' => trim($searchTerm),
            ]]);
        } else {
            $fields = $this->getClient()->call('fields', ['query' => [
                'limit' => '100'
            ]]);
        }
        static::$_fields = $fields->fields;

        foreach (static::$_fields as $field) {
            static::$_fieldIndex[$field->id] = static::slug($field->title);
        }

        return static::$_fields;
    }

    /**
     * @param $string
     * @return string
     */
    protected static function slug($string)
    {
        return strtolower(str_replace(' ','_', $string));
    }

}
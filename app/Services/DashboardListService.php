<?php

namespace Pickstar\Services;

use Illuminate\Support\Facades\DB;
use Pickstar\Message\Thread;
use Pickstar\User\User;
use Pickstar\Booking\Booking;
use Pickstar\Payment\Payment;
use Pickstar\Booking\TalentPivot;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Pickstar\Http\Transformers\BookingTransformer;
use Pickstar\Http\Resources\Thread as ThreadResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Pickstar\Http\Transformers\BookingRequestTransformer;

class DashboardListService
{
    /**
     * Get the dashboard lists and other data for the given user.
     *
     * @param \Pickstar\User\User $user
     *
     * @return array
     */
    public function getListsForUser(User $user): array
    {
        if ($user->isAdmin()) {
            return [
                'active_threads' => $this->getActiveThreads($user)->each->append('booking_name'),
                'active_threads_count' => $this->getActiveThreadsCount($user),
                'vetting_requests' => $this->getVettingRequests()->each->append('latest_note'),
                'vetting_requests_count' => $this->getVettingRequestsCount(),
                'expiring_requests' => $this->getExpiringRequests()->each->append('latest_note'),
                'expiring_requests_count' => $this->getExpiringRequestsCount(),
                'bookings_not_completed' => $this->getBookingsNotCompleted()->each->append('latest_note'),
                'bookings_not_completed_count' => $this->getBookingsNotCompletedCount()
            ];
        } elseif ($user->isTalent()) {
            return [
                'active_threads' => $this->getActiveThreads($user)->each->append('booking_name'),
                'active_threads_count' => $this->getActiveThreadsCount($user),
                'bookings' => $this->getBookings($user),
                'requests' => $this->getRequests($user),
                'bookings_count' => $this->getBookingsCount($user),
                'requests_count' => $this->getRequestsCount($user)
            ];
        } elseif ($user->isClient()) {
            return [
                'active_threads' => $this->getActiveThreads($user)->each->append('booking_name'),
                'active_threads_count' => $this->getActiveThreadsCount($user),
                'bookings' => $this->getBookings($user),
                'requests' => $this->getRequests($user),
                'payments' => $this->getPayments($user)
            ];
        } elseif ($user->isManager()) {
            return [
                'active_threads' => $this->getActiveThreads($user)->each->append('booking_name'),
                'active_threads_count' => $this->getActiveThreadsCount($user),
                'bookings' => $this->getBookings($user),
                'requests' => $this->getRequests($user)
            ];
        }

        return [];
    }

    /**
     * Get bookings for the given user.
     *
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    protected function getBookings(User $user): Collection
    {
        $bookings = $this->getBookingsQuery($user)
            ->with(['opportunity', 'onlineOpportunity', 'client', 'talent' => function ($query) use ($user) {
                $query->onlyManagedIfManager($user);

                $query->onlyMyselfIfTalent($user);

                $query->onlyAppliedOrShortlistedIfClient($user);
            }])
            ->take(3)
            ->get();

        $bookings->each(function ($booking) use ($user) {
            $booking->transform([new BookingTransformer, 'transform'], [$user]);
        });

        return $bookings;
    }

    /**
     * Get total bookings count for the given user.
     *
     * @param \Pickstar\User\User $user
     *
     * @return integer
     */
    protected function getBookingsCount(User $user)
    {
        $bookingsCount = $this->getBookingsQuery($user)
            ->with(['opportunity', 'onlineOpportunity', 'client', 'talent' => function ($query) use ($user) {
                $query->onlyManagedIfManager($user);

                $query->onlyMyselfIfTalent($user);

                $query->onlyAppliedOrShortlistedIfClient($user);
            }])
            ->get()
            ->count();

        return $bookingsCount;
    }

    /**
     * Get bookings query based on user role.
     *
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function getBookingsQuery(User $user): Builder
    {
        if ($user->isClient()) {
            return $this->getClientBookingsQuery($user);
        } elseif ($user->isTalent()) {
            return $this->getTalentBookingsQuery($user);
        } elseif ($user->isManager()) {
            return $this->getManagerBookingsQuery($user);
        }
    }

    /**
     * Get client bookings query.
     *
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function getClientBookingsQuery(User $user): Builder
    {
        return Booking::where('client_id', $user->id)
            ->whereIn('status', [Booking::STATUS_PENDING, Booking::STATUS_SCHEDULED, Booking::STATUS_BOOKED])
            ->oldest('utc_date');
    }

    /**
     * Get talent bookings query.
     *
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function getTalentBookingsQuery(User $user): Builder
    {
        // If talent then we need to return only the bookings where they have been accepted.
        // Order them with the soonest to happen first (oldest "utc_date").
        return Booking::booked()
            ->whereHas('talent', function ($query) use ($user) {
                $query->haveBeenAccepted()
                    ->where('booking_talent.talent_id', $user->id);
            })
            ->oldest('utc_date');
    }

    /**
     * Get manager bookings query.
     *
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function getManagerBookingsQuery(User $user): Builder
    {
        return Booking::booked()
            ->whereHas('talent', function ($query) use ($user) {
                $query->haveBeenAccepted()
                    ->managedBy($user);
            })
            ->oldest('utc_date');
    }

    /**
     * Get booking requests for the given user.
     *
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    protected function getRequests(User $user): Collection
    {
        $bookings = $this->getRequestsQuery($user)
            ->isNotExpired()
            ->withoutDeclined()
            ->withoutWithdrawn()
            ->groupBy('bookings.id')
            ->take(3)
            ->get();

        $bookings->each(function ($booking) use ($user) {
            $booking->transform([new BookingRequestTransformer, 'transform'], [$user]);
        });

        return $bookings;
    }

    /**
     * Get total booking requests count.
     *
     * @param \Pickstar\User\User $user
     *
     * @return integer
     */
    protected function getRequestsCount(User $user)
    {
        $requestsCount = $this->getRequestsQuery($user)
            ->isNotExpired()
            ->withoutDeclined()
            ->withoutWithdrawn()
            ->groupBy('bookings.id')
            ->get()
            ->count();

        return $requestsCount;
    }

    /**
     * Get booking requests query based on user role.
     *
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function getRequestsQuery(User $user): Builder
    {
        if ($user->isClient()) {
            return $this->getClientRequestsQuery($user);
        } elseif ($user->isTalent()) {
            return $this->getTalentRequestsQuery($user);
        } elseif ($user->isManager()) {
            return $this->getManagerRequestsQuery($user);
        }
    }

    /**
     * Get client booking requests query.
     *
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function getClientRequestsQuery(User $user): Builder
    {
        return Booking::requests()
            ->with('opportunity', 'onlineOpportunity', 'client')
            ->where('client_id', $user->id)
            ->orderByRaw("FIELD(bookings.status, '".Booking::STATUS_VETTING."', '".Booking::STATUS_PENDING."') ASC")
            ->oldest('utc_date');
    }

    /**
     * Get talent booking requests query.
     *
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function getTalentRequestsQuery(User $user): Builder
    {
        // If talent then we need to return only the booking requests where they have a pending response status.
        // Order them with the soonest to expire first (oldest "date").
        return Booking::requests()
            ->withoutVetting()
            ->with('opportunity', 'onlineOpportunity', 'client')
            ->whereHas('talent', function ($query) use ($user) {
                $query->where('booking_talent.talent_id', $user->id)
                    ->where('booking_talent.status', TalentPivot::STATUS_PENDING);
            })
            ->oldest('date');
    }

    /**
     * Get manager booking requests query.
     *
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function getManagerRequestsQuery(User $user): Builder
    {
        return Booking::requests()
            ->withoutVetting()
            ->with(['opportunity', 'onlineOpportunity', 'client', 'talent' => function ($query) use ($user) {
                $query->onlyManagedIfManager($user);
            }])
            ->whereHas('talent', function ($query) use ($user) {
                $query->managedBy($user)
                    ->where('booking_talent.status', TalentPivot::STATUS_PENDING);
            })
            ->oldest('date');
    }

    /**
     * Get payments for the given user.
     *
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    protected function getPayments(User $user): Collection
    {
        return Payment::unpaid()
            ->whereHas('booking', function ($query) {
                $query->where('client_accepted_terms', true)
                    ->where('status', '<>', Booking::STATUS_CANCELLED);
            })
            ->where('user_id', $user->id)
            ->where('due_date', '<', now()->addWeeks(2))
            ->oldest('due_date')
            ->get();
    }

    /**
     * Get vetting booking requests for admins.
     *
     * @return ResourceCollection
     */
    protected function getActiveThreads(User $user): ResourceCollection
    {
        $threads =  Thread::leftJoin('messages', 'messages.thread_id', '=', 'threads.id')
            ->leftJoin('participants', 'participants.thread_id', '=', 'threads.id')
            ->selectRaw('threads.*, 
                        MAX(messages.created_at) AS latest_message_at,
                        count(case when `messages`.`id` > participants.message_id then 1 else null end) as unread_count')
            ->where('participants.user_id', DB::raw($user->id))
            ->groupBy('threads.id')
            ->orderBy('latest_message_at', 'desc')
            ->orderBy('id', 'desc')
            ->with(['messages', 'participants' => function ($query) {
                $query->select(['user_id', 'first_name', 'last_name', 'role', 'profile_image_hash']);
            }])
            ->when($user->isClient(), function ($query) use ($user) {
                $query->whereHas('participants', function ($query) use ($user) {
                    $query->where('user_id', '=', $user->id);
                });
            })
            ->when($user->isTalent(), function ($query) use ($user) {
                $query->whereHas('participants', function ($query) use ($user) {
                    $query->where('user_id', '=', $user->id);
                });
            })
            ->when($user->isManager(), function ($query) use ($user) {
                $query->whereHas('participants', function ($query) use ($user) {
                    $query->where('management_id', '=', $user->id)->
                    orWhere('manager_id', '=', $user->id);
                });
                $query->orWhereHas('participants', function ($query) use ($user) {
                    $query->where('user_id', '=', $user->id);
                });
            })
            ->active()
            ->with('booking', 'clients', 'talents')
            ->take(3)
            ->get();
        return ThreadResource::collection($threads);
    }

    /**
     * Get count of vetting booking requests for admins.
     *
     * @return int
     */
    protected function getActiveThreadsCount(User $user): int
    {
        return Thread::active()
            ->when($user->isClient(), function ($query) use ($user) {
                $query->whereHas('participants', function ($query) use ($user) {
                    $query->where('user_id', '=', $user->id);
                });
            })
            ->when($user->isTalent(), function ($query) use ($user) {
                $query->whereHas('participants', function ($query) use ($user) {
                    $query->where('user_id', '=', $user->id);
                });
            })
            ->when($user->isManager(), function ($query) use ($user) {
                $query->whereHas('participants', function ($query) use ($user) {
                    $query->where('management_id', '=', $user->id)->
                    orWhere('manager_id', '=', $user->id);
                });
                $query->orWhereHas('participants', function ($query) use ($user) {
                    $query->where('user_id', '=', $user->id);
                });
            })
            ->count();
    }

    /**
     * Get vetting booking requests for admins.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    protected function getVettingRequests(): Collection
    {
        return $this->getVettingRequestsQuery()
            ->with('opportunity', 'onlineOpportunity', 'client')
            ->take(3)
            ->get();
    }

    /**
     * Get count of vetting booking requests for admins.
     *
     * @return int
     */
    protected function getVettingRequestsCount(): int
    {
        return $this->getVettingRequestsQuery()->count();
    }

    /**
     * Get vetting booking requests query for admins.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function getVettingRequestsQuery(): Builder
    {
        return Booking::vetting()
            ->latest();
    }

    /**
     * Get expiring booking requests for admins.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    protected function getExpiringRequests(): Collection
    {
        return $this->getExpiringRequestsQuery()
            ->with('opportunity', 'onlineOpportunity', 'client')
            ->take(3)
            ->get();
    }

    /**
     * Get count of expiring booking requests for admins.
     *
     * @return int
     */
    protected function getExpiringRequestsCount(): int
    {
        return $this->getExpiringRequestsQuery()->count();
    }

    /**
     * Get expiring booking requests query for admins.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function getExpiringRequestsQuery(): Builder
    {
        return Booking::requests()
            ->withoutVetting()
            ->isNotExpired()
            ->where('status', Booking::STATUS_PENDING)
            ->where(function ($query) {
                $query->where('date', '<', now()->addDays(4));
            })
            ->oldest('date');
    }

    /**
     * Get bookings not yet marked as complete for admins.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    protected function getBookingsNotCompleted(): Collection
    {
        // If admin then we show the bookings that have passed the date of the booking but have
        // not yet been marked as completed.
        // Order them with the most recent to have passed the date first (oldest "utc_date")
        return $this->getBookingsNotCompletedQuery()
            ->with('opportunity', 'onlineOpportunity', 'client', 'talent')
            ->take(3)
            ->get();
    }

    /**
     * Get count of bookings not yet marked as complete for admins.
     *
     * @return int
     */
    protected function getBookingsNotCompletedCount(): int
    {
        return $this->getBookingsNotCompletedQuery()->count();
    }

    /**
     * Get bookings not yet marked as complete query for admins.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function getBookingsNotCompletedQuery(): Builder
    {
        return Booking::booked()
            ->where('utc_date', '<', now())
            ->oldest('utc_date');
    }
}

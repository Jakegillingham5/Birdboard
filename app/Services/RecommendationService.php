<?php

namespace Pickstar\Services;

use Pickstar\Booking\Booking;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Tag\Tag;
use Pickstar\User\User;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Pickstar\Talent\Profile;
use Pickstar\Location\Location;
use Pickstar\Helpers\SearchHelpers;
use Pickstar\Opportunity\Opportunity;
use Pickstar\TrackedSearches\TrackedSearches;
use Illuminate\Contracts\Pagination\Paginator;
use Pickstar\SearchedKeywords\SearchedKeywords;
use Pickstar\Http\Transformers\TalentTransformer;

class RecommendationService
{
    public function getStars(Request $request, Booking $booking): Paginator
    {

        $budget             = SearchHelpers::sanitizeDecimal($booking->budget_per_star);
        if(empty($budget)){
            return [];
        }
        $location           = $booking->appearance_type === 'online' ? null : $booking->location_id;
        $gender             = $request->get('gender', []);
        $socialFollowings   = $request->get('social_following', []);
        $esports            = $request->get('esports', false);
        $sanitised_opportunity_id = SearchHelpers::sanitizeNumber($booking->opportunity_id);
        $sanitised_online_opportunity_id = SearchHelpers::sanitizeNumber($booking->online_opportunity_id);
        $appliedAverage     = 'avg(case when booking_talent.client_accepted = 0 && booking_talent.status = "applied" then bookings.budget_per_star else null end)';
        $budgetAverage      = 'avg(case when booking_talent.client_accepted = 1 && booking_talent.status = "applied" then bookings.budget_per_star else null end)';
        $budgetScore = 'case when ' . $budget . ' / avg(case when booking_talent.client_accepted = 1 && booking_talent.status = "applied" then bookings.budget_per_star else null end) between 0.9 and 1.1 then '
            . '(' . $budget . ' / ' . $budgetAverage . ') * 2 when '
            . $budget . ' / ' . $budgetAverage . '  between 0.5 and 0.9 || '
            . $budget . ' / ' . $budgetAverage . '  between 1.1 and 1.5 then 0.3 when '
            . $budget . ' / ' . $budgetAverage . '  < 0.3 then 0 when '
            . $budgetAverage . ' is null then 0 else 0.2 end';
        $appliedScore = 'case when ' . $budget . ' / avg(case when booking_talent.client_accepted = 1 && booking_talent.status = "applied" then bookings.budget_per_star else null end) between 0.9 and 1.1 then '
            . '(' . $budget . ' / ' . $appliedAverage . ')  when '
            . $budget . ' / ' . $appliedAverage . '  between 0.5 and 0.9 || '
            . $budget . ' / ' . $appliedAverage . '  between 1.1 and 1.5 then 0.3 when '
            . $budget . ' / ' . $appliedAverage . '  < 0.3 then 0 when '
            . $appliedAverage . ' is null then 0 else 0.2 end';
        $opportunityScore   = '(avg(case when bookings.opportunity_id in ('.$sanitised_opportunity_id.') then 1 else 0 end)) / 2';
        $onlineOpportunityScore   = '(avg(case when bookings.online_opportunity_id in ('.$sanitised_online_opportunity_id.') then 1 else 0 end)) / 2';
        $shortlistScore     = '(avg(booking_talent.shortlisted) - avg(case when booking_talent.shortlisted = 1 && booking_talent.status = "declined" then 1 else 0 end)) / 2';
        $scores[] = $budgetScore;
        $scores[] = $appliedScore;
        $scores[] = $shortlistScore;
        if($sanitised_opportunity_id){
            $scores[] = $opportunityScore;
        }
        if($sanitised_online_opportunity_id){
            $scores[] = $onlineOpportunityScore;
        }

        $ultimateScore      = 'if(' . $budgetScore . ' = 0, 0, ' . implode(' + ', $scores) . ')';

        $users = User::active()
            ->talent()
            ->with('profile', 'sportTags', 'clubTags', 'themeTags')
            ->select('users.*')
            ->selectRaw('talent_profile.description')
            ->selectRaw($ultimateScore . ' score')
            ->selectRaw($shortlistScore . ' score_shortlist')
            ->selectRaw($budgetScore . ' score_budget')
            ->selectRaw($appliedScore . ' score_applied')
            ->selectRaw($budgetAverage . ' score_budget_average')
            ->join('talent_profile', 'users.id', 'talent_profile.user_id')
            ->join('booking_talent','users.id','booking_talent.talent_id')
            ->join('bookings','bookings.id','booking_talent.booking_id')
            ->where('bookings.appearance_type', $booking->appearance_type)
            ->whereHas('profile', function ($query) {
                $query->public();
            })
            ->having('score', '>', '0')
            ->groupBy('users.id');

        // Handle sports tag fields
        $sports = $request->get('sports', []);
        if (count($sports)) {
            $users->where(function ($query) use ($sports) {
                $query->whereHas('tags', function ($q) use ($sports) {
                    $q->whereIn('tag_id', $sports);
                });
            });
        }

        // Handle themes tag fields
        $themes = $request->get('themes', []);
        if (count($themes)) {
            $users->where(function ($query) use ($themes) {
                $query->whereHas('tags', function ($q) use ($themes) {
                    $q->whereIn('tag_id', $themes);
                });
            });
        }

        // Handle clubs tag fields
        $clubs = $request->get('clubs', []);
        if (count($clubs)) {
            $users->where(function ($query) use ($clubs) {
                $query->whereHas('tags', function ($q) use ($clubs) {
                    $q->whereIn('tag_id', $clubs);
                });
            });
        }

        if ($sanitised_opportunity_id !== null) {
            $users->whereHas('opportunities', function ($q) use ($sanitised_opportunity_id) {
                $q->where('opportunity_id', $sanitised_opportunity_id);
            });
            $users->join('opportunities','opportunities.id','bookings.opportunity_id');
            $users->selectRaw('sum(case when bookings.opportunity_id in ('.$sanitised_opportunity_id.') then 1 else 0 end) count_opportunity');
            $users->selectRaw($opportunityScore . ' score_opportunity');
        }

        if ($sanitised_online_opportunity_id !== null) {
            $users->whereHas('opportunities', function ($q) use ($sanitised_online_opportunity_id) {
                $q->where('online_opportunity_id', $sanitised_online_opportunity_id);
            });
            $users->join('opportunities AS online_opportunities','online_opportunities.id','bookings.opportunity_id');
            $users->selectRaw('sum(case when bookings.online_opportunity_id in ('.$sanitised_online_opportunity_id.') then 1 else 0 end) score_online_opportunity');
            $users->selectRaw($onlineOpportunityScore . ' score_online_opportunity');
        }

        if ($location !== null) {
             $users->where('location_id', $location);
        }

        if (count($gender)) {
            $users->whereHas('profile', function ($q) use ($gender) {
                $q->whereIn('gender', $gender);
            });
        }

        if ($esports) {
            $users->where('can_set_esports', true);
        }

        // Need to loop over each social range selected and check that the greatest following value
        // they have is in one of those ranges.
        $social_values = collect([]);
        if (count($socialFollowings)) {
            $users->where(function ($query) use ($socialFollowings, $social_values) {
                foreach ($socialFollowings as $key => $socialFollowing) {
                    $query->orWhereHas('profile', function ($query) use ($socialFollowing, $social_values) {
                        $values = Profile::getSocialRange($socialFollowing);
                        $social_values->push($values);

                        // Needed to use HAVING instead of WHERE in order to be able to query custom select value.
                        if (count($values) > 1) {
                            $query->selectRaw('GREATEST(COALESCE(facebook_follower_count, 0), COALESCE(instagram_follower_count, 0), COALESCE(twitter_follower_count, 0)) as greatest_social')
                                ->having('greatest_social', '<', $values[1])
                                ->having('greatest_social', '>', $values[0]);
                        } elseif (count($values) === 1) {
                            $query->selectRaw('GREATEST(COALESCE(facebook_follower_count, 0), COALESCE(instagram_follower_count, 0), COALESCE(twitter_follower_count, 0)) as greatest_social')
                                ->having('greatest_social', '>', $values[0]);
                        }
                    });
                }
            });
        }

        $users = $users
            ->orderByRaw($ultimateScore . ' DESC')
            ->orderBy('users.last_name')
            ->orderBy('users.first_name')
            ->simplePaginate(32);

        return $users;
    }

    /**
     * Get the real client ip address
     */
    protected function getUserIp()
    {
        $headers = ['HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_FORWARDED', 'REMOTE_ADDR'];

        foreach ($headers as $header) {
            if (array_key_exists($header, $_SERVER) === true) {
                foreach (explode(',', $_SERVER[$header]) as $userIP) {
                    $userIP = trim($userIP);
                    if (filter_var($userIP, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false) {
                        return $userIP;
                    }
                }
            }
        }

        return null;
    }

    /**
     * Check if user ip address belongs to do-not-track ip address table
     *
     * @param $userIP
     * @return bool
     */
    protected function trackUser($userIP)
    {
        return ! in_array($userIP, config('pickstar.do_not_track_ips'));
    }

    /**
     * Create a new track search in database if search hasn't been made in the last hour
     *
     * @param $trackSearch
     */
    protected function recordSearch($trackSearch)
    {
        foreach($trackSearch as $key => $value) {
            $checkVariables[] = [$key, $value];
        }

        $checkVariables[] = ['created_at', '>', now()->subHour()]; // Check against searches made by same ip within last hour

        TrackedSearches::firstOrCreate($checkVariables, $trackSearch);
    }
}

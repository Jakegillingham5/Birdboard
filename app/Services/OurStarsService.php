<?php

namespace Pickstar\Services;

use Pickstar\Tag\Tag;
use Pickstar\User\User;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Pickstar\Talent\Profile;
use Pickstar\Location\Location;
use Pickstar\Helpers\SearchHelpers;
use Pickstar\Opportunity\Opportunity;
use Pickstar\TrackedSearches\TrackedSearches;
use Illuminate\Contracts\Pagination\Paginator;
use Pickstar\SearchedKeywords\SearchedKeywords;
use Pickstar\Http\Transformers\TalentTransformer;

class OurStarsService
{
    public function getStars(Request $request): Paginator
    {
        $locations = $request->get('locations', []);
        $gender = $request->get('gender', []);
        $socialFollowings = $request->get('social_following', []);
        $search_terms = collect(preg_split('/\s+/', $request->get('search_terms')));
        $esports = $request->get('esports', false);

        $users = User::active()
            ->talent()
            ->with('profile', 'sportTags', 'clubTags', 'themeTags')
            ->select('users.*')
            ->selectRaw('IF(talent_profile.date_featured_until > ?, TRUE, FALSE) AS is_featured', [now()->toDateTimeString()])
            ->selectRaw('IF(users.profile_image_hash IS NOT NULL, TRUE, FALSE) AS has_profile_image')
            ->selectRaw('talent_profile.description')

            ->join('talent_profile', 'users.id', 'talent_profile.user_id')
            ->whereHas('profile', function ($query) use ($request) {
                if ($request->user()) {
                    if (!($request->user()->isAdmin() && $request->input('show_hidden') === 'true')) {
                        $query->public();
                    }
                } else {
                    $query->public();
                }
            });

        // If we have string based search terms we'll query against each of those next.
        if (count($search_terms)) {
            $users->where(function ($query) use ($search_terms) {
                $search_terms->each(function ($term) use ($query) {
                    $sanitizedString = SearchHelpers::sanitizeSearchString($term);
                    switch ($sanitizedString) {
                        case 'men':
                        case 'man':
                        case 'male':
                            $gender = ['male'];
                            $query->whereHas('profile', function ($q) use ($gender) {
                                $q->whereIn('gender', $gender);
                            });
                            break;
                        case 'female':
                        case 'woman':
                        case 'women':
                            $gender = ['female'];
                            $query->whereHas('profile', function ($q) use ($gender) {
                                $q->whereIn('gender', $gender);
                            });
                            break;
                        default:
                            $query->orWhere(\DB::raw('concat(first_name, " ", last_name)'), 'like', '%' . $sanitizedString . '%');
                            $query->orWhere('description', 'LIKE', '%' . $sanitizedString . '%');
                            break;
                    }

                    if(!empty(trim($term))) {
                        SearchedKeywords::create(['keywords' => $term]);
                    }
                });
            });
        }

        // Handle sports tag fields
        $sports = $request->get('sports', []);
        if (count($sports)) {
            $users->where(function ($query) use ($sports) {
                $query->whereHas('tags', function ($q) use ($sports) {
                    $q->whereIn('tag_id', $sports);
                });
            });
        }

        // Handle themes tag fields
        $themes = $request->get('themes', []);
        if (count($themes)) {
            $users->where(function ($query) use ($themes) {
                $query->whereHas('tags', function ($q) use ($themes) {
                    $q->whereIn('tag_id', $themes);
                });
            });
        }

        // Handle clubs tag fields
        $clubs = $request->get('clubs', []);
        if (count($clubs)) {
            $users->where(function ($query) use ($clubs) {
                $query->whereHas('tags', function ($q) use ($clubs) {
                    $q->whereIn('tag_id', $clubs);
                });
            });
        }

        // Handle all opportunity fields together.
        $opportunities = $request->get('opportunity_types', []);

        if (count($opportunities)) {
            $users->whereHas('opportunities', function ($q) use ($opportunities) {
                $q->whereIn('opportunity_id', $opportunities);
            });
        }


        if (count($locations)) {
            $users->whereHas('locations', function ($q) use ($locations) {
                $q->whereIn('location_id', $locations);
            });
        }

        if (count($gender)) {
            $users->whereHas('profile', function ($q) use ($gender) {
                $q->whereIn('gender', $gender);
            });
        }

        if ($esports) {
            $users->where('can_set_esports', true);
        }

        // Need to loop over each social range selected and check that the greatest following value
        // they have is in one of those ranges.
        $social_values = collect([]);
        if (count($socialFollowings)) {
            $users->where(function ($query) use ($socialFollowings, $social_values) {
                foreach ($socialFollowings as $key => $socialFollowing) {
                    $query->orWhereHas('profile', function ($query) use ($socialFollowing, $social_values) {
                        $values = Profile::getSocialRange($socialFollowing);
                        $social_values->push($values);

                        // Needed to use HAVING instead of WHERE in order to be able to query custom select value.
                        if (count($values) > 1) {
                            $query->selectRaw('GREATEST(COALESCE(facebook_follower_count, 0), COALESCE(instagram_follower_count, 0), COALESCE(twitter_follower_count, 0)) as greatest_social')
                                ->having('greatest_social', '<', $values[1])
                                ->having('greatest_social', '>', $values[0]);
                        } elseif (count($values) === 1) {
                            $query->selectRaw('GREATEST(COALESCE(facebook_follower_count, 0), COALESCE(instagram_follower_count, 0), COALESCE(twitter_follower_count, 0)) as greatest_social')
                                ->having('greatest_social', '>', $values[0]);
                        }
                    });
                }
            });
        }

        // Do not track Pickstar, Codium, Lumin ip addresses
        // Record entire search in database if source ip address is new.
        // Otherwise, only record if keywords have not been searched in the last hour.
        $sourceIP = $this->getUserIp();

        if ($this->trackUser($sourceIP)) {

            $sports_names = Tag::select('name')->whereIn('id', $sports)->get()->toArray();
            $themes_names = Tag::select('name')->whereIn('id', $themes)->get()->toArray();
            $club_names = Tag::select('name')->whereIn('id', $clubs)->get()->toArray();
            $location_names = Location::select('name')->whereIn('id', $locations)->get()->toArray();
            $opportunity_names = Opportunity::select('name')->whereIn('id', $opportunities)->get()->toArray();

            $trackSearch = [
                'sport_category' => http_build_query(Arr::flatten($sports_names)),
                'themes' => http_build_query(Arr::flatten($themes_names)),
                'club_event' => http_build_query(Arr::flatten($club_names)),
                'location' => http_build_query(Arr::flatten($location_names)),
                'gender' => http_build_query($gender),
                'social_following' => http_build_query(Arr::flatten($social_values)),
                'opportunity_type' => http_build_query(Arr::flatten($opportunity_names)),
                'search_keywords' => implode(",", $search_terms->toArray()),
                'user_ip' => $sourceIP
            ];

            $this->recordSearch($trackSearch);
        }

        // Paginate all returned users.

        //If we have two words in the search term, we'll try and order them as if the two words are the stars name and put the people with a match first
        if($search_terms->count() > 1) {
            $sanitized_first = preg_replace('/[^A-Za-z0-9\- ]/', '', trim($search_terms[0]));
            $sanitized_second = preg_replace('/[^A-Za-z0-9\- ]/', '', trim($search_terms[1]));
            $users = $users
                ->orderByRaw("FIELD(users.first_name, '$sanitized_first') DESC")
                ->orderByRaw("FIELD(users.last_name, '$sanitized_second') DESC")
                ->orderByRaw("FIELD(users.first_name, '$sanitized_second') DESC")
                ->orderByRaw("FIELD(users.last_name, '$sanitized_first') DESC")
                ->orderByDesc('is_featured')
                ->orderByDesc('has_profile_image')
                ->orderBy('users.last_name')
                ->orderBy('users.first_name')
                ->paginate(32);
        } else {
            $sanitized_first = preg_replace('/[^A-Za-z0-9\- ]/', '', trim($search_terms[0]));
            $users = $users
                ->orderByRaw("FIELD(users.first_name, '$sanitized_first') DESC")
                ->orderByRaw("FIELD(users.last_name, '$sanitized_first') DESC")
                ->orderByDesc('is_featured')
                ->orderByDesc('has_profile_image')
                ->orderBy('users.last_name')
                ->orderBy('users.first_name')
                ->paginate(32);
        }



        // Append the "sport_club" attribute to each of the users, this is used in place of the users
        // tagline and better conveys what sports/club a talent might be a part of. We'll also hide
        // the relations used to build this attribute so we don't clutter the response.
        $collection = $users->getCollection();

        $collection->each->append('sport_club', 'based_in');
        $collection->each->addHidden('sportTags', 'clubTags', 'themeTags');
        $collection->each->transform([new TalentTransformer, 'transform'], [$request->user()]);

        return $users;
    }

    /**
     * Get the real client ip address
     */
    protected function getUserIp()
    {
        $headers = ['HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_FORWARDED', 'REMOTE_ADDR'];

        foreach ($headers as $header) {
            if (array_key_exists($header, $_SERVER) === true) {
                foreach (explode(',', $_SERVER[$header]) as $userIP) {
                    $userIP = trim($userIP);
                    if (filter_var($userIP, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false) {
                        return $userIP;
                    }
                }
            }
        }

        return null;
    }

    /**
     * Check if user ip address belongs to do-not-track ip address table
     *
     * @param $userIP
     * @return bool
     */
    protected function trackUser($userIP)
    {
        return ! in_array($userIP, config('pickstar.do_not_track_ips'));
    }

    /**
     * Create a new track search in database if search hasn't been made in the last hour
     *
     * @param $trackSearch
     */
    protected function recordSearch($trackSearch)
    {
        foreach($trackSearch as $key => $value) {
            $checkVariables[] = [$key, $value];
        }

        $checkVariables[] = ['created_at', '>', now()->subHour()]; // Check against searches made by same ip within last hour

        TrackedSearches::firstOrCreate($checkVariables, $trackSearch);
    }
}

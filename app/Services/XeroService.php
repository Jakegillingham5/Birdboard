<?php
/**
 * Created by PhpStorm.
 * User: jpickstar
 * Date: 2019-05-24
 * Time: 10:02
 */

namespace Pickstar\Services;

use Carbon\Carbon;
use Pickstar\Booking\Booking;
use Pickstar\Payment\Payment;
use Pickstar\User\User;
use Pickstar\Validation\Rules\NotEmail;
use Pickstar\Xero\Client;
use XeroPHP\Models\Accounting\Contact;
use XeroPHP\Models\Accounting\Invoice;
use XeroPHP\Models\Accounting\Invoice\LineItem;
use XeroPHP\Remote\Model;
use XeroPHP\Remote\Response;

/**
 * Class XeroService
 * @package Pickstar\Services
 */
class XeroService
{
    const ACCOUNT_CODE_SCHOOL           = '207';
    const ACCOUNT_CODE_AGENCY           = '208';
    const ACCOUNT_CODE_CLUB             = '209';
    const ACCOUNT_CODE_INDIVIDUAL       = '210';
    const ACCOUNT_CODE_CORPORATE        = '211';
    const ACCOUNT_CODE_ASSOCIATION      = '212';
    const ACCOUNT_CODE_DEFAULT          = '210';
    const ACCOUNT_CODE_TALENT_PAYMENTS  = '415';

    const TAX_TYPE_GST_ON_INCOME        = 'OUTPUT';
    const TAX_TYPE_GST_FREE_ON_EXPENSES = 'EXEMPTEXPENSES';
    const TAX_TYPE_GST_ON_EXPENSES      = 'INPUT';

    const COMMISSION_DEFAULT_RATE       = '0.25';

    const INVOICE_FINAL                 = '50% Final Payment';
    const INVOICE_DEPOSIT               = '50% Deposit';
    const INVOICE_DEPOSIT_FINAL         = 'Deposit + Final';
    const TALENT_BILL                   = 'TB';


    /**
     * @var Client
     */
    protected static $_client;

    /**
     * XeroService constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        static::$_client = $client;
    }

    /**
     * Fetches the API client which runs the Xero API calls.
     *
     * @return Client
     */
    public static function getClient()
    {
        return static::$_client;
    }

    /**
     * Find, creates and updates the xero contact record.
     *
     * @param User $user
     * @return Contact|Model|null
     * @throws \XeroPHP\Exception
     * @throws \XeroPHP\Remote\Exception
     * @throws \XeroPHP\Remote\Exception\NotFoundException
     */
    public function findOrCreateContact(User $user)
    {
        $name   = static::getBestName($user);
        $model  = $this->getContactByUUID($user->xero_contact_reference);

        if($model === null){
            $model  = $this->getContactByEmail($user->email);
        }

        if($model !== null){
            if($model->getContactStatus() === Contact::CONTACT_STATUS_ARCHIVED){
                return $this->createContactFromUser($user);
            }

            $bestName  = $user->getBestName();
            if($model->getName() !== $bestName){
                $checkForExisting = $this->getContactByName($bestName);
                if($checkForExisting !== null && $checkForExisting->getContactStatus() !== Contact::CONTACT_STATUS_ARCHIVED){
                    $model = $checkForExisting;
                }
            }
        }

        if($model === null){
            $model  = $this->getContactByName($name);
        }

        if($model !== null){ //Update existing user with xero contact ID.
            $payload = static::createUserPayload($user);
            static::applyPayloadToModel($model, $payload);
            $user->xero_contact_reference = $model->getGUID();
            $user->save();
            $model->save();
            return $model;
        }

        return $this->createContactFromUser($user);
    }

    public function getContactByEmail($email)
    {
        static::validateEmail($email);

        $whereClause = 'EmailAddress != null AND EmailAddress.toLower()="'.strtolower($email).'"';
        return static::getClient()->xero()->load(Contact::class)->where($whereClause)->execute()->first();
    }

    /**
     * @param string $reference
     * @return \XeroPHP\Remote\Collection
     * @throws \XeroPHP\Remote\Exception
     */
    public function findInvoicesByReference($reference)
    {
        return static::getClient()->xero()->load(Invoice::class)->where('Reference != null AND Reference.Contains("'.$reference.'")')->execute();
    }

    /**
     * Pass a booking and talent to this method to create relevant invoices and bill.
     *
     * @param $booking
     * @param $talent
     * @throws \XeroPHP\Remote\Exception
     */
    public function createAllRecordsForTalentBooking($booking, $talent)
    {
        if(config('xero.splitInvoices')){
            $invoiceModel = $this->createInvoice($booking, $talent, $booking->client, XeroService::INVOICE_DEPOSIT);
            static::updateExistingPivot($booking, $talent, ['xero_deposit_invoice_reference' => $invoiceModel->getGUID()]);

            $invoiceModel =  $this->createInvoice($booking, $talent, $booking->client, XeroService::INVOICE_FINAL);
            static::updateExistingPivot($booking, $talent, ['xero_final_invoice_reference'  => $invoiceModel->getGUID()]);
        } else {
            $invoiceModel   =  $this->createInvoice($booking, $talent, $booking->client, XeroService::INVOICE_DEPOSIT_FINAL);
            $guid           = $invoiceModel->getGUID();
            static::updateExistingPivot($booking, $talent, ['xero_deposit_invoice_reference' => $guid,'xero_final_invoice_reference' => $guid]);
        }

        $billModel =  $this->createBill($booking, $talent, $booking->client);
        static::updateExistingPivot($booking, $talent, ['xero_bill_reference' => $billModel->getGUID()]);
    }

    /**
     * Method used to work around limitation with updating existing pivot table methods.
     *
     * @param $booking
     * @param $talent
     * @param $payload
     */
    protected static function updateExistingPivot($booking, $talent, $payload)
    {
        if ($talent = $booking->talent()->find($talent->id)) {
            $talent->response->fill($payload)->save();
        }

    }

    /**
     * @param Booking $booking
     * @param User $talent
     * @param Contact $contact
     * @throws \XeroPHP\Remote\Exception
     */
    public function createInvoice(Booking $booking, User $talent, User $client, $type)
    {
        $app            = static::getClient()->xero();
        $reference      = static::getInvoiceReference($booking, $talent, $type);
        $description    = static::getInvoiceDescription($booking, $talent, $type);
        $lineItem       = new LineItem($app);
        $model          = new Invoice($app);
        $contact        = $this->findOrCreateContact($client);

        static::applyPayloadToModel($lineItem, [
            'Description'   => $description,
            'Quantity'      => 1,
            'UnitAmount'    => static::getInvoiceAmount($booking->budget_per_star, $type),
            'AccountCode'   => static::getAccountCode($booking),
            'TaxType'       => XeroService::TAX_TYPE_GST_ON_INCOME,
        ]);

        static::applyPayloadToModel($model, [
            'Type'          => Invoice::INVOICE_TYPE_ACCREC,
            'Contact'       => $contact,
            'InvoiceNumber' => $reference,
            'Reference'     => $reference,
            'Date'          => new \DateTime($booking->client_accepted_terms_date),
            'DueDate'       => new \DateTime(static::getInvoiceDueDate($booking, $type)),
            'CurrencyCode'  => $client->currency_code,
            'Status'        => Invoice::INVOICE_STATUS_AUTHORISED
        ]);

        $model->addLineItem($lineItem);

        $model->save();

        return $model;
    }

    /**
     * @param Booking $booking
     * @param User $talent
     * @param Contact $contact
     * @throws \XeroPHP\Remote\Exception
     */
    public function createBill(Booking $booking, User $talent, User $client)
    {
        $app            = static::getClient()->xero();
        $reference      = static::getInvoiceReference($booking, $talent, 'TB');
        $description    = static::getBillDescription($booking, $talent);
        $lineItem       = new LineItem($app);
        $model          = new Invoice($app);
        $contact        = $this->findOrCreateContact($talent);

        static::applyPayloadToModel($lineItem, [
            'Description'   => $description,
            'Quantity'      => 1,
            'UnitAmount'    => static::calculateTalentPayment($booking),
            'AccountCode'   => static::ACCOUNT_CODE_TALENT_PAYMENTS,
            'TaxType'       => static::getTalentGSTCode($talent),
        ]);

        static::applyPayloadToModel($model, [
            'Type'          => Invoice::INVOICE_TYPE_ACCPAY,
            'Contact'       => $contact,
            'InvoiceNumber' => $reference,
            'Reference'     => $reference,
            'Date'          => new \DateTime($booking->client_accepted_terms_date),
            'DueDate'       => new \DateTime($booking->date === null ? now() : $booking->date),
            'CurrencyCode'  => $talent->currency_code,
            'Status'        => Invoice::INVOICE_STATUS_AUTHORISED,
        ]);

        $model->addLineItem($lineItem);

        $model->save();

        return $model;
    }

    /**
     * Guesses the reference previously created manually in xero for invoices.
     *
     * @param $booking
     * @param $talent
     * @return string
     */
    public static function getLegacyReference($booking, $talent)
    {
        return $booking->id . '.'
            . substr($talent->first_name, 0, 1) . substr($talent->last_name, 0, 1);
    }

    /**
     * @param $booking
     * @param $talent
     * @return string
     */
    protected static function getInvoiceReference($booking, $talent, $type)
    {
        switch ($type) {
            case static::INVOICE_DEPOSIT_FINAL:
                $reference = 'DF';
                break;
            case static::INVOICE_DEPOSIT:
                $reference = 'DP';
                break;
            case static::INVOICE_FINAL:
                $reference = 'FN';
                break;
            case static::TALENT_BILL:
                $reference = 'TB';
                break;
            default:
                break;
        }

        return $booking->id . '-'
        . substr($talent->first_name, 0, 1) . substr($talent->last_name, 0, 1) . '-' . $reference;
    }

    /**
     * @param $booking
     * @param $talent
     * @return string
     */
    protected static function getInvoiceDescription($booking, $talent, $type)
    {
        static::validateType($type);

        return $type . ': ' . $talent->first_name . ' '
            . $talent->last_name . ' ' . static::getAttendanceString($booking) . ' '
            . $booking->name . ' on '
            . Carbon::createFromDate($booking->date)->format('d/m/y');
    }

    /**
     * Parses booking and determines correct language to use for invoice line.
     *
     * @param Booking $booking
     * @return string
     */
    protected static function getAttendanceString(Booking $booking)
    {
        switch ($booking->appearance_type){
            case 'online':
                return 'appearance in';
                break;
            case 'in_person':
                return 'attendance at';
                break;
            default:
                return 'involvement in';
                break;
        }
    }

    /**
     * @param $booking
     * @param $talent
     * @return string
     */
    protected static function getBillDescription($booking, $talent)
    {
        return $talent->first_name . ' '
            . $talent->last_name . ' ' . static::getAttendanceString($booking) . ' '
            . $booking->name . ' on '
            . Carbon::createFromDate($booking->date)->format('d/m/y');
    }

    /**
     * @param User $user
     * @return mixed|string
     */
    protected static function getBestName(User $user)
    {
        return $user->getBestName();
    }

    /**
     * @param $name
     * @return Contact
     * @throws \XeroPHP\Remote\Exception
     */
    public function getContactByName($name)
    {
        $whereClause = 'Name.toLower() = "' .htmlentities(strtolower($name)). '"';
        return static::getClient()->xero()->load(Contact::class)->where($whereClause)->execute()->first();
    }

    /**
     * @param $uuid
     * @return Contact|null
     * @throws \XeroPHP\Exception
     * @throws \XeroPHP\Remote\Exception\NotFoundException
     */
    protected function getContactByUUID($uuid)
    {
        if($uuid === null){
            return;
        }

        return static::getClient()->xero()->loadByGUID(Contact::class, $uuid);
    }

    /**
     * @param string $name
     * @param User $user
     * @return Contact|Model
     * @throws \XeroPHP\Remote\Exception
     */
    protected function createContactFromUser(User $user)
    {
        $payload    = static::createUserPayload($user);
        $contact    = static::newXeroContactModel();
        $contact    = static::applyPayloadToModel($contact, $payload);
        $response   = $contact->save();
        $id         = self::getResponseID($response, Contact::getGUIDProperty());

        $user->xero_contact_reference = $id;
        $user->save();
        return $contact;

    }

    /**
     * Helper method to initialise a contact model
     *
     * @return Contact
     */
    public static function newXeroContactModel()
    {
        return new Contact(static::getClient()->xero());
    }

    /**
     * Creates a payload array for the Xero model from the User model.
     *
     * @param User $user
     * @return array
     */
    public static function createUserPayload(User $user)
    {
        $payload = [
            'Name'              => static::getBestName($user),
            'FirstName'         => $user->first_name,
            'LastName'          => $user->last_name,
            'EmailAddress'      => $user->email,
            'DefaultCurrency'   => $user->currency_code,
        ];

        if($user->role === User::ROLE_TALENT){
            $details = $user->getBestPaymentDetails();
            if($details !== null){
                $payload['BankAccountDetail']           = $details->bsb_number . ' ' . $details->account_number;
                $payload['PurchasesDefaultAccountCode'] = static::ACCOUNT_CODE_TALENT_PAYMENTS;
                $payload['TaxNumber']                   = $details->abn;
                $payload['AccountsPayableTaxType']      = static::getTalentGSTCode($user);
            }
        } else {
            $payload['AccountsReceivableTaxType']   = XeroService::TAX_TYPE_GST_ON_INCOME;
        }

        return $payload;

    }

    /**
     * @param Model $model
     * @param array $payload
     * @return Model
     */
    public static function applyPayloadToModel(Model $model, array $payload)
    {
        foreach ($payload as $method => $value){
            $methodName = 'set' . $method;
            $model->$methodName($value);
        }

        return $model;
    }

    /**
     * Method wrapper to reuse email regex within the validator.
     *
     * @param $email
     * @return bool
     */
    public static function validateEmail($email)
    {
        $validator = new NotEmail();

        $notEmail = $validator->passes('email', $email);

        if($notEmail === false){
            return !$notEmail;
        }

        throw new \InvalidArgumentException('Warning, string ' . $email . ' does not conform to an email pattern');

    }

    /**
     * Returns the created ID from a xero response, for saving into our local database.
     *
     * @param Response $response
     * @param $guidProperty
     * @return string
     */
    protected static function getResponseID(Response $response, $guidProperty)
    {
        $elements = $response->getElements();

        return $elements[0][$guidProperty];
    }

    /**
     * @param Booking $booking
     * @return string
     */
    protected static function getAccountCode(Booking $booking)
    {
        switch ($booking->market_segment) {
            case 'school':
                return static::ACCOUNT_CODE_SCHOOL;
                break;
            case 'agency':
                return static::ACCOUNT_CODE_AGENCY;
                break;
            case 'club':
                return static::ACCOUNT_CODE_CLUB;
                break;
            case 'individual':
                return static::ACCOUNT_CODE_INDIVIDUAL;
                break;
            case 'corporate':
                return static::ACCOUNT_CODE_CORPORATE;
                break;
            case 'association':
                return static::ACCOUNT_CODE_ASSOCIATION;
                break;
            default:
                return static::ACCOUNT_CODE_DEFAULT;
                break;
        }
    }

    /**
     * Calculates invoice amount depending on if this is a split invoice or not.
     * @param $amount
     * @param $bookingType
     * @return float|int
     */
    protected static function getInvoiceAmount($amount, $bookingType)
    {
        switch ($bookingType) {
            case static::INVOICE_DEPOSIT:
                return $amount / 2;
                break;
            case static::INVOICE_FINAL:
                return $amount / 2;
                break;
            case static::INVOICE_DEPOSIT_FINAL:
                return $amount;
                break;
            default:
                throw new \InvalidArgumentException('A type must be provided of ' . static::INVOICE_DEPOSIT . ', ' . static::INVOICE_FINAL .', or ' . static::INVOICE_DEPOSIT_FINAL );
                break;
        }
    }

    /**
     * Inline validator to ensure type string provided  matches our expectations.  Reused in several methods.
     *
     * @param $type
     */
    protected static function validateType($type)
    {
        switch ($type) {
            case static::INVOICE_DEPOSIT:
            case static::INVOICE_FINAL:
            case static::INVOICE_DEPOSIT_FINAL:
                break;
            default:
                throw new \InvalidArgumentException('A type must be provided of ' . static::INVOICE_DEPOSIT . ', ' . static::INVOICE_FINAL .', or ' . static::INVOICE_DEPOSIT_FINAL );
                break;
        }
    }
    /**
     * @param Booking $booking
     * @return float
     */
    protected static function calculateTalentPayment(Booking $booking)
    {
        $budget = $booking->budget_per_star;

        switch (true) {
            case $booking->commission_type === 'rate' && $booking->commission_rate !== null:
                return $budget - ($budget * ($booking->commission_rate / 100));
                break;
            case $booking->commission_type === 'dollar' && $booking->commission_dollar !== null:
                return $budget - $booking->commission_dollar;
                break;
            default:
                return $budget - ($budget * static::getCommissionDefaultRate());
                break;
        }
    }

    /**
     * @param User $user
     * @return string
     */
    protected static function getTalentGSTCode(User $user)
    {
        if($user->paymentDetails === null) {
            return XeroService::TAX_TYPE_GST_FREE_ON_EXPENSES;
        }
        return $user->paymentDetails->gst_registered ? XeroService::TAX_TYPE_GST_ON_EXPENSES : XeroService::TAX_TYPE_GST_FREE_ON_EXPENSES;
    }

    /**
     * @param Booking $booking
     * @param $invoiceType
     * @return string
     */
    protected static function getInvoiceDueDate(Booking $booking, $invoiceType)
    {
        static::validateType($invoiceType);

        if($booking->date === null || Carbon::create($booking->date)->lessThan(Carbon::now()->addDays(7))
        ) {
            return today()->format('Y-m-d');
        }

        return Carbon::create($booking->date)->subDays(7)->format('Y-m-d');

    }

    /**
     * @return string
     */
    protected static function getCommissionDefaultRate()
    {
        $storedSetting = system_setting('commission_rate');
        return $storedSetting === null ? $storedSetting : static::COMMISSION_DEFAULT_RATE;
    }


}
<?php

namespace Pickstar\Services\Registration;

use Pickstar\User\User;
use Laravel\Socialite\AbstractUser as SocialiteUser;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SocialRegistrationService
{
    /**
     * If a user has registered before using social auth, return the user, otherwise we'll
     * create a new user.
     *
     * @param \Laravel\Socialite\AbstractUser $socialiteUser
     * @param string $provider
     *
     * @return \Pickstar\User\User
     */
    public function findOrCreateUser(SocialiteUser $socialiteUser, $provider): User
    {
        // Try to find the user by matching the exact provider ID we got from the socialite
        // user in our response.
        if ($user = User::where($this->providerColumnName($provider), $socialiteUser->id)->first()) {
            return $user;
        }

        // Otherwise check to see if our socialite provider returned an e-mail address, if
        // it did we'll attempt to find a matching e-mail address and associate this
        // social account with the user and then log them in.
        if (isset($socialiteUser->email) && $socialiteUser->email) {
            try {
                $user = User::where('email', $socialiteUser->email)->firstOrFail();

                $user->fill([
                    $this->providerColumnName($provider) => $socialiteUser->id,
                    'email_verified' => true
                ]);

                $user->save();

                return $user;
            } catch (ModelNotFoundException $exception) {
                // Fall through to below where we create the user from the provider.
            }
        }

        return $this->createUserFromProvider($socialiteUser, $provider);
    }


    /**
     * Get the column name for the given provider.
     *
     * @param string $provider
     *
     * @return string
     */
    protected function providerColumnName($provider): string
    {
        return sprintf('%s_provider_id', $provider);
    }

    /**
     * Create a new user from the provider.
     *
     * @param \Laravel\Socialite\AbstractUser $socialiteUser
     * @param string $provider
     *
     * @return \Pickstar\User\User
     */
    protected function createUserFromProvider(SocialiteUser $socialiteUser, $provider): User
    {
        return User::create([
            'name' => $socialiteUser->name,
            'email' => $socialiteUser->email,
            $this->providerColumnName($provider) => $socialiteUser->id,
            'status' => User::STATUS_PENDING,
            'email_verified' => true
        ]);
    }
}

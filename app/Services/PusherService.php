<?php

namespace Pickstar\Services;

use Pickstar\Pusher\Client;

/**
 * Class PusherService
 * @package Pickstar\Services
 */
class PusherService
{
    /**
     * @var Client
     */
    protected static $_client;

    /**
     * PusherService constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        static::$_client = $client;
    }

    /**
     * Fetches the API client which runs the Pusher API calls.
     *
     * @return Client
     */
    public static function getClient()
    {
        return static::$_client;
    }

    public static function checkIfChannelOccupied($channel_name)
    {
        return static::getClient()->getChannelInfo($channel_name)->occupied;
    }
}

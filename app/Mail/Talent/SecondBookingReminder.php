<?php

namespace Pickstar\Mail\Talent;

use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Pickstar\Booking\Booking;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SecondBookingReminder extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * User instance.
     *
     * @var \Pickstar\User\User
     */
    public $user;

    /**
     * Booking instance.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * Attendance confirmation URL.
     *
     * @var string
     */
    public $url;

    /**
     * Create a new message instance.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     * @param string $accessToken
     */
    public function __construct(User $user, Booking $booking, $accessToken)
    {
        $this->user = $user;
        $this->booking = $booking;
        $this->url = url()->temporarySignedRoute(
            'booking.attendance',
            now()->addDays(system_setting('second_talent_reminder')),
            ['token' => $accessToken]
        );
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.bookings.talent.second_talent_reminder')
            ->subject('Confirm your attendance');
    }
}

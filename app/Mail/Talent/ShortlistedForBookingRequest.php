<?php

namespace Pickstar\Mail\Talent;

use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Pickstar\Booking\Booking;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ShortlistedForBookingRequest extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * User instance.
     *
     * @var \Pickstar\User\User
     */
    public $user;

    /**
     * Booking instance.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * Shortlist priority.
     *
     * @var int
     */
    public $shortlistPriority;

    /**
     * Create a new message instance.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     * @param int $shortlistPriority
     */
    public function __construct(User $user, Booking $booking, int $shortlistPriority)
    {
        $this->user = $user;
        $this->booking = $booking;
        $this->shortlistPriority = $shortlistPriority;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->shortlistPriority === 1) {
            $subject = 'You\'re top of the list for a PickStar request!';
        } elseif ($this->shortlistPriority > 1 && $this->shortlistPriority <= 5) {
            $subject = 'You\'re in the top 5 for a PickStar request!';
        } else {
            $subject = 'You\'re in the top 10 for a PickStar request!';
        }

        return $this->view('emails.bookings.talent.shortlisted_for_booking_request')
            ->subject($subject);
    }
}

<?php

namespace Pickstar\Mail\Talent;

use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Pickstar\Booking\Booking;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CompleteByDateChanged extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * User instance.
     *
     * @var \Pickstar\User\User
     */
    public $user;

    /**
     * Booking instance.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * Create a new message instance.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     */
    public function __construct(User $user, Booking $booking)
    {
        $this->user = $user;
        $this->booking = $booking;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('An online requirement due date has changed')
            ->view('emails.bookings.talent.complete_by_date_changed');
    }
}

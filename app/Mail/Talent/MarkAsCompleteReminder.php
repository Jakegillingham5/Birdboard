<?php

namespace Pickstar\Mail\Talent;

use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Pickstar\Booking\Booking;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MarkAsCompleteReminder extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Booking instance.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * User instance.
     *
     * @var \Pickstar\User\User
     */
    public $user;

    /**
     * Create a new message instance.
     *
     * @param \Pickstar\Booking\Booking $booking
     * @param \Pickstar\User\User $user
     * @param \Illuminate\Support\String $eventType
     *
     * @return void
     */
    public function __construct(Booking $booking, User $user)
    {
        $this->booking = $booking;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.bookings.talent.mark_as_complete_reminder')
            ->subject('Complete your booking now');
    }
}

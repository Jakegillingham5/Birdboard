<?php

namespace Pickstar\Mail\Talent;

use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AccountActivated extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Talent who was activated.
     *
     * @var \Pickstar\User\User
     */
    public $talent;

    /**
     * Create a new message instance.
     *
     * @param \Pickstar\User\User $talent
     *
     * @return void
     */
    public function __construct(User $talent)
    {
        $this->talent = $talent;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.talent_account_activated')
            ->subject('Welcome to PickStar!');
    }
}

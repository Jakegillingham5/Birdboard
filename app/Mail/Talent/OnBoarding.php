<?php

namespace Pickstar\Mail\Talent;

use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OnBoarding extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Talent who was activated.
     *
     */
    public $csvDetails;

    /**
     * Create a new message instance.
     *
     * @param $csvDetails
     *
     * @return void
     */
    public function __construct($csvDetails)
    {
        $this->csvDetails = $csvDetails;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.talent_on_boarding')
            ->subject('Welcome to PickStar!');
    }
}

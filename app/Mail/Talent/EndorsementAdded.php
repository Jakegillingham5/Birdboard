<?php

namespace Pickstar\Mail\Talent;

use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Pickstar\Talent\Endorsement;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EndorsementAdded extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * User instance.
     *
     * @var \Pickstar\User\User
     */
    public $user;

    /**
     * Endorsement instance.
     *
     * @var Endorsement
     */
    public $endorsement;

    /**
     * Create a new message instance.
     *
     * @param \Pickstar\User\User $user
     * @param Endorsement $endorsement
     */
    public function __construct(User $user, Endorsement $endorsement)
    {
        $this->user = $user;
        $this->endorsement = $endorsement;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('You\'ve been endorsed!')
            ->view('emails.bookings.talent.endorsement_added');
    }
}

<?php

namespace Pickstar\Mail\Client;

use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Pickstar\Booking\Booking;

class WeeklyRecap extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Booking request instance.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * Create a new message instance.
     *
     * @param \Pickstar\Booking\Booking $booking
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Action Required: These sports stars are waiting to hear from you')
                ->view('emails.bookings.client.weekly_recap');
    }
}

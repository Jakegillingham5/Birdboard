<?php

namespace Pickstar\Mail\Client;

use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Pickstar\Booking\Booking;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TalentApplied extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Talent user instance.
     *
     * @var \Pickstar\User\User
     */
    public $talent;

    /**
     * Booking instance.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * Talents comment on why they are suitable
     */
    public $comment;

    /**
     * Create a new message instance.
     *
     * @param \Pickstar\User\User $talent
     * @param \Pickstar\Booking\Booking $booking
     * @param $comment
     *
     * @return void
     */
    public function __construct(User $talent, Booking $booking, $comment = null)
    {
        $this->talent = $talent;
        $this->booking = $booking;
        $this->comment = $comment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.bookings.client.talent_applied')
            ->subject('A sports star has applied for your request!');
    }
}

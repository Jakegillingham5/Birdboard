<?php

namespace Pickstar\Mail\Client;

use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Pickstar\Booking\Booking;
use Pickstar\Payment\Payment;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EsportsEnquiry extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $request_details;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request_details)
    {
        $this->request_details = $request_details;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.bookings.client.esports_enquiry')
            ->subject('Thanks for your esports enquiry');
    }
}

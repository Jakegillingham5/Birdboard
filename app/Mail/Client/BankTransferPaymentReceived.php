<?php

namespace Pickstar\Mail\Client;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Pickstar\Booking\Booking;
use Pickstar\Payment\Payment;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BankTransferPaymentReceived extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Booking instance.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * Payment instance.
     *
     * @var \Pickstar\Payment\Payment
     */
    public $payment;

    /**
     * Create a new message instance.
     *
     * @param \Pickstar\Booking\Booking $booking
     * @param \Pickstar\Payment\Payment $payment
     */
    public function __construct(Booking $booking, Payment $payment)
    {
        $this->booking = $booking;
        $this->payment = $payment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Payment confirmed')
            ->view('emails.bookings.client.payment_confirmation');
    }
}

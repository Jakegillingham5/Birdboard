<?php

namespace Pickstar\Mail\Client;

use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Pickstar\Booking\Booking;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TalentConfirmedAttendance extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Booking instance.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * User instance.
     *
     * @var \Pickstar\User\User
     */
    public $talent;

    /**
     * Create a new message instance.
     *
     * @param \Pickstar\Booking\Booking $booking
     * @param \Pickstar\User\User $talent
     *
     * @return void
     */
    public function __construct(Booking $booking, User $talent)
    {
        $this->booking = $booking;
        $this->talent = $talent;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.bookings.client.talent_confirmed_attendance')
                ->subject('Talent attendance confirmed');
    }
}

<?php

namespace Pickstar\Mail\Client;

use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Pickstar\Booking\Booking;
use Pickstar\Payment\Payment;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DepositPaymentWarning extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Booking instance.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * Payment instance.
     *
     * @var \Pickstar\Payment\Payment
     */
    public $payment;

    /**
     * Create a new message instance.
     *
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return void
     */
    public function __construct(Booking $booking, Payment $payment)
    {
        $this->booking = $booking;
        $this->payment = $payment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.bookings.client.deposit_payment_warning')
            ->subject('Action required: Your deposit is due');
    }
}

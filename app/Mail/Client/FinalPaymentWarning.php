<?php

namespace Pickstar\Mail\Client;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Pickstar\Booking\Booking;
use Pickstar\Payment\Payment;
use Illuminate\Contracts\Queue\ShouldQueue;

class FinalPaymentWarning extends Mailable implements ShouldQueue
{
    use Queueable;

    /**
     * Booking instance.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * Payment instance.
     *
     * @var \Pickstar\Payment\Payment
     */
    public $payment;

    /**
     * Create a new message instance.
     *
     * @param \Pickstar\Booking\Booking $booking
     * @param \Pickstar\Payment\Payment $payment
     *
     * @return void
     */
    public function __construct(Booking $booking, Payment $payment)
    {
        $this->booking = $booking;
        $this->payment = $payment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.bookings.client.final_payment_warning')
            ->subject('Action required: Your sports star payment is due');
    }
}

<?php

namespace Pickstar\Mail\System;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Pickstar\Marketing\Email;
use Illuminate\Contracts\Queue\ShouldQueue;

class MarketingEmailCaptured extends Mailable implements ShouldQueue
{
    use Queueable;

    /**
     * Email instance.
     *
     * @var \Pickstar\Marketing\Email
     */
    public $email;

    /**
     * Create a new message instance.
     *
     * @param \Pickstar\Marketing\Email $email
     *
     * @return void
     */
    public function __construct(Email $email)
    {
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.marketing_email_captured')
            ->subject('ALERT: A new lead was captured!');
    }
}

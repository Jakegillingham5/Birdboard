<?php

namespace Pickstar\Mail\System;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EsportsForm extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Email of enquirer.
     *
     * @var String
     */
    public $email;

    /**
     * Description of enquirer.
     *
     * @var String
     */
    public $description;

    /**
     * Create a new message instance.
     *
     * @param string $name
     * @param string $email
     * @param string $number
     * @param string $description
     */
    public function __construct($email, $description)
    {
        $this->email = $email;
        $this->description = $description;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('New Esports Website Enquiry')
            ->view('emails.esports_contact')
            ->cc(config('mail.cc_montesi'))
            ->cc(config('mail.cc_philcox'))
            ->cc(config('mail.cc_duffy'));
    }
}

<?php

namespace Pickstar\Mail\System;

use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Pickstar\Booking\Booking;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TalentApplied extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Talent user instance.
     *
     * @var \Pickstar\User\User
     */
    public $talent;

    /**
     * Booking instance.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    public $comment;

    public $subject = '';

    /**
     * Create a new message instance.
     *
     * @param \Pickstar\User\User $talent
     * @param \Pickstar\Booking\Booking $booking
     * @param $comment
     *
     * @return void
     */
    public function __construct(User $talent, Booking $booking, $comment = null)
    {
        $this->talent = $talent;
        $this->booking = $booking;
        $this->comment = $comment;
        $this->subject = 'A talent has applied for an opportunity!';
        if ($comment) {
            $this->subject = 'A talent has applied for an opportunity with a comment!';
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.bookings.system.talent_applied')
            ->subject($this->subject)
            ->cc(config('mail.cc_montesi'))
            ->cc(config('mail.cc_philcox'))
            ->cc(config('mail.cc_smith'));
    }
}

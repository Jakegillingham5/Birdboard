<?php

namespace Pickstar\Mail\System;

use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TalentRegistered extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Talent user that registered.
     *
     * @var \Pickstar\User\User
     */
    public $talent;

    /**
     * Create a new message instance.
     *
     * @param \Pickstar\User\User $talent
     *
     * @return void
     */
    public function __construct(User $talent)
    {
        $this->talent = $talent;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.auth.new_talent_registration')
            ->subject('New talent registration')
            ->cc(config('mail.cc_montesi'))
            ->cc(config('mail.cc_philcox'))
            ->cc(config('mail.cc_duffy'))
            ->cc(config('mail.cc_smith'));
    }
}

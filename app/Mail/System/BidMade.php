<?php

namespace Pickstar\Mail\System;

use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Pickstar\Booking\Booking;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BidMade extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $name;
    public $email;
    public $company;
    public $bid;
    public $type;
    public $athleteName;

    /**
     * Create a new message instances
     *
     * @return void
     */
    public function __construct($name, $email, $company, $bid, $type)
    {
        $this->name = $name;
        $this->email = $email;
        $this->company = $company;
        $this->bid = $bid;
        $this->type = $type;
        $this->athleteName = null;
        if ($type === 'mcdermott') {
            $this->athleteName = 'Ben McDermott';
        } else if ($type === 'swepson') {
            $this->athleteName = 'Mitchell Swepson';
        } else if ($type === 'stanlake') {
            $this->athleteName = 'Billy Stanlake';
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.bid_made')
                ->subject('ALERT: Bid Made for the ' . $this->athleteName . ' opportunity')
                ->cc(config('mail.cc_millowick'))
                ->cc(config('mail.cc_begley'))
                ->cc(config('mail.cc_hockney'));
    }
}

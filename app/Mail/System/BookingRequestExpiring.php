<?php

namespace Pickstar\Mail\System;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Pickstar\Booking\Booking;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BookingRequestExpiring extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Booking instance.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * Create a new message instance.
     *
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return void
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.bookings.system.request_expiring')
            ->subject('ACTION REQUIRED: No responses to client booking request');
    }
}

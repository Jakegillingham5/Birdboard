<?php

namespace Pickstar\Mail\System;

use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EsportsEnquiry extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $request_details;

    /**
     * Create a new message instance.
     */
    public function __construct($request_details)
    {
        $this->request_details = $request_details;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Esports booking request!')
            ->view('emails.bookings.system.esports_enquiry')
            ->cc('mail.cc_montesi')
            ->cc('mail.cc_philcox');
    }
}

<?php

namespace Pickstar\Mail\System;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactForm extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Name of enquirer.
     *
     * @var String
     */
    public $name;

    /**
     * Email of enquirer.
     *
     * @var String
     */
    public $email;

    /**
     * Contact number of enquirer.
     *
     * @var String
     */
    public $number;

    /**
     * Description of enquirer.
     *
     * @var String
     */
    public $description;

    /**
     * Create a new message instance.
     *
     * @param string $name
     * @param string $email
     * @param string $number
     * @param string $description
     */
    public function __construct($name, $email, $number, $description)
    {
        $this->name = $name;
        $this->email = $email;
        $this->number = $number;
        $this->description = $description;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('New Website Enquiry')
            ->view('emails.contact')
            ->cc(config('mail.cc_montesi'))
            ->cc(config('mail.cc_philcox'))
            ->cc(config('mail.cc_duffy'));
    }
}

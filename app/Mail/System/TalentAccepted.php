<?php

namespace Pickstar\Mail\System;

use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Pickstar\Booking\Booking;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TalentAccepted extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Talent user instance.
     *
     * @var \Pickstar\User\User
     */
    public $talent;

    /**
     * Booking instance.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * Create a new message instance.
     *
     * @param \Pickstar\User\User $talent
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return void
     */
    public function __construct(User $talent, Booking $booking)
    {
        $this->talent = $talent;
        $this->booking = $booking;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.bookings.system.talent_accepted')
            ->subject('BOOM! Talent booked!')
            ->cc(config('mail.cc_montesi'))
            ->cc(config('mail.cc_philcox'))
            ->cc(config('mail.cc_duffy'))
            ->cc(config('mail.cc_kearns'))
            ->cc(config('mail.cc_small'))
            ->cc(config('mail.cc_begley'))
            ->cc(config('mail.cc_ngo'))
            ->cc(config('mail.cc_smith'));
    }
}

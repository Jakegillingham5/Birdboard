<?php

namespace Pickstar\Mail\System;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Pickstar\Booking\Booking;
use Pickstar\Payment\Payment;
use Illuminate\Contracts\Queue\ShouldQueue;

class FinalPaymentWarning extends Mailable implements ShouldQueue
{
    use Queueable;

    /**
     * Booking instance.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * Payment instance.
     *
     * @var \Pickstar\Payment\Payment
     */
    public $payment;

    /**
     * Create a new message instance.
     *
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return void
     */
    public function __construct(Booking $booking, Payment $payment)
    {
        $this->booking = $booking;
        $this->payment = $payment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.bookings.system.final_payment_warning')
            ->subject('Final payment warning')
            ->cc(config('mail.cc_begley'))
            ->cc(config('mail.cc_philcox'));
    }
}

<?php

namespace Pickstar\Mail\System;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TalentRequested extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * The name of the user submitting the form
     *
     * @var String
     */
    public $name;

    /**
     * The email of the user submitting the form
     *
     * @var String
     */
    public $email;

    /**
     * The name of the star the user wants
     *
     * @var String
     */
    public $starName;

    /**
     * The description the user left
     *
     * @var String
     */
    public $description;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $email, $starName, $description)
    {
        $this->name = $name;
        $this->email = $email;
        $this->starName = $starName;
        $this->description = $description;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.talent_request')
            ->cc(config('mail.cc_montesi'))
            ->cc(config('mail.cc_philcox'))
            ->cc(config('mail.cc_duffy'));
    }
}

<?php

namespace Pickstar\Mail\System;

use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Collection;

class EmailShortlist extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Email of enquirer.
     *
     * @var String
     */
    public $email;

    /**
     * Arrow of the shortlist data
     *
     * @var \Illuminate\Database\Eloquent\Collection
     */
    public $shortlist;

    /**
     * Create a new message instance.
     *
     * @param string $email
     * @param \Illuminate\Database\Eloquent\Collection $shortlist
     */
    public function __construct($email, Collection $shortlist)
    {
        $this->email = $email;
        $this->shortlist = $shortlist;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Take a look at my PickStar shortlist!')
            ->view('emails.shortlist');
    }
}

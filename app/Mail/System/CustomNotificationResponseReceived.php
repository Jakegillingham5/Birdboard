<?php

namespace Pickstar\Mail\System;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomNotificationResponseReceived extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Booking instance.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $data;

    /**
     * Create a new message instance.
     *
     * @param $data
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.custom_notification_response_received')
            ->subject('NOTICE: Custom Notification Response Received')
            ->cc(config('mail.cc_kearns'))
            ->cc(config('mail.cc_montesi'))
            ->cc(config('mail.cc_philcox'));
    }
}

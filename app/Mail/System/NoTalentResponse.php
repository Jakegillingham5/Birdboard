<?php

namespace Pickstar\Mail\System;

use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Pickstar\Booking\Booking;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NoTalentResponse extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Booking instance.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * Talent user instance.
     *
     * @var \Pickstar\User\User
     */
    public $talent;

    /**
     * Create a new message instance.
     *
     * @param \Pickstar\Booking\Booking $booking
     * @param \Pickstar\User\User $talent
     */
    public function __construct(Booking $booking, User $talent)
    {
        $this->booking = $booking;
        $this->talent = $talent;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.bookings.system.no_talent_response')
            ->subject('Urgent: No talent confirmation!')
            ->cc(config('mail.cc_philcox'))
            ->cc(config('mail.cc_duffy'))
            ->cc(config('mail.cc_smith'));
    }
}

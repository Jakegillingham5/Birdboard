<?php

namespace Pickstar\Mail\Manager;

use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AccountActivated extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Manager who was activated.
     *
     * @var \Pickstar\User\User
     */
    public $manager;

    /**
     * Create a new message instance.
     *
     * @param \Pickstar\User\User $manager
     *
     * @return void
     */
    public function __construct(User $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.manager_account_activated')
                ->subject('Your PickStar account has been approved');
    }
}

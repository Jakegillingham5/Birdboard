<?php

namespace Pickstar\Mail\Manager;

use Illuminate\Support\Collection;
use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Pickstar\Booking\Booking;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UnreadMessages extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var User $user
     */
    public $user;

    /**
     * @var Collection $threads
     */
    public $threads;

    /**
     * NewMessage constructor.
     * @param Collection $threads
     * @param User $user
     */
    public function __construct(Collection $threads, User $user)
    {
        $this->threads = $threads;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.bookings.manager.unread_messages')
            ->subject('You have new messages on PickStar!');
    }
}

<?php

namespace Pickstar\Console\Commands;

use GuzzleHttp\RequestOptions;
use Illuminate\Console\Command;
use Pickstar\Booking\Booking;
use Pickstar\Marketing\Email;
use Pickstar\Marketing\Source;
use Pickstar\Providers\ActiveCampaignServiceProvider;
use Pickstar\Services\ActiveCampaignService;
use Pickstar\User\ActiveCampaignUser;
use Pickstar\User\User;

class UpdateAcBooking extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pickstar:ac-booking {--booking_id=}';

    /**
     * @var ActiveCampaignService
     */
    protected $api;

    /**
     * @var ActiveCampaignUser
     */
    protected $activeCampaignUser;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update ActiveCampaign contact record';

    public function __construct(ActiveCampaignService $service)
    {
        $this->api = $service;
       return parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $id = $this->option('booking_id');
        if($id !== null){
            $bookings = Booking::query()->where('id', $id)->all()->get();
        } else {
            $bookings = Booking::query()->all()->get();
        }

        $this->output->text($bookings->count() . ' bookings found for processing');

        $skip = 0;
        $clients = [];
        foreach ($bookings as $index => $booking) {
            /* @var Booking $booking */
            if ($booking->market_segment === null) {
                $skip++;
                continue;
            }

            $clients[$booking->client_id] = '';

            /* Initialise Client */
            $contact    = $booking->client->email;
            $id         = $booking->id;

            $this->output->text('Attempting sync on email ' . $contact . ' from booking id ' . $booking->id . '  ' . $booking->type . ' ' . $booking->status);
            $this->activeCampaignUser  = ActiveCampaignUser::findByEmail($contact);
            $payload = $this->activeCampaignUser->buildContactPayload();
            $this->api->updateContact($contact, $payload);
            $this->api->updateCustomFields($contact, $payload);

            $tags = $this->activeCampaignUser->getTagsAndSegments();
            if(count($tags) > 0){
                $this->output->text("Attempting to update/create tags");
                $this->api->updateTags($contact, $tags);
            }
            $this->output->text("Attempting to move list if appropriate");

            try {
                $this->output->text("Updating segment for $contact from booking $id ... ");
                $this->api->updateTags($contact, ['segment: ' . $booking->market_segment]);
            } catch (\Exception $e) {
                $this->output->error('bam' . $id);
            }

        }

        $this->output->text($skip . ' records skipped');
        $this->output->text(count($clients) . ' records');
    }
}

<?php

namespace Pickstar\Console\Commands;

use Pickstar\User\AdminUser;
use Pickstar\Booking\Booking;
use Illuminate\Console\Command;
use Pickstar\Notifications\System\BookingRequestExpiring;

class ExpiringRequests extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pickstar:expiring-requests';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notify admins of requests that are expiring in 7 days';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Booking::activeRequests()
            ->where(function ($query) {
                $query->where('date', '<=', now()->addDays(7));
            })
            ->whereHas('notification', function ($query) {
                $query->where('request_expiring', false);
            })
            ->whereDoesntHave('talentApplied')
            ->get()
            ->each(function ($booking) {
                (new AdminUser)->notify(new BookingRequestExpiring($booking));

                $booking->notification->fill(['request_expiring' => true])->save();
            });
    }
}

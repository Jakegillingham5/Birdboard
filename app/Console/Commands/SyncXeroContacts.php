<?php

namespace Pickstar\Console\Commands;

use Illuminate\Console\Command;
use Pickstar\Booking\Booking;
use Pickstar\Services\XeroService;
use Pickstar\User\User;
use XeroPHP\Remote\Exception\BadRequestException;

class SyncXeroContacts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pickstar:sync-xero-contacts';

    /**
     * @var XeroService
     */
    protected $service;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Attach Xero contact id to the local record';

    public function __construct(XeroService $service)
    {
        $this->service = $service;
        return parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $users = User::active()->talent()->get();

        foreach ($users as $user) {

            $this->output->text('Looking for user #' . $user->id . ', ' . $user->name . ' ' . $user->email);
            if($user->email === 'jmillowick@pickstar.com.au') {
                continue;
            }
            try {
                $this->service->findOrCreateContact($user);
            } catch (\InvalidArgumentException $e) {
                $this->output->text('Bad Email - skipping');
            }
            sleep(3);
        }
    }
}

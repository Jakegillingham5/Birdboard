<?php


namespace Pickstar\Console\Commands;


use Carbon\Carbon;
use Illuminate\Console\Command;
use Pickstar\Location\Location;
use Pickstar\Tag\Tag;
use Pickstar\Talent\Endorsement;
use Pickstar\Talent\Profile as TalentProfile;
use Pickstar\User\User;

class CreateTalentForCypress extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cypress:new-talent';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Called through e2e testing and returns the inserted talent";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Create single user for Cypress Profile Tests
        $user = factory(User::class)
            ->states('verified','talent')
            ->create(['updated_at' => Carbon::now()->toDateTimeString(),
                'created_at' => Carbon::now()->toDateTimeString()
            ]);

        factory(TalentProfile::class)->create([
            'user_id' => $user->id
        ]);

        $endorse = factory(Endorsement::class, 5)->create([
            'user_id' => $user->id
        ]);

        $sponsorTag = Tag::sponsor()->get()->shuffle()->take(3);
        $sportTag = Tag::sport()->get()->shuffle()->take(3);
        $clubTag = Tag::club()->get()->shuffle()->take(2);
        $charityTag = Tag::charity()->get()->shuffle()->take(2);
        $locations = Location::all()->shuffle()->take(2);

        $user->tags()
             ->attach($sponsorTag->pluck('id')
             ->merge($sportTag->pluck('id'),
                $clubTag->pluck('id'),
                $charityTag->pluck('id'))
            );

        $user->opportunities()->attach(
            $locations->pluck('id')
        );

        //Merge all into response json
        $data[] = ['user' => $user->toArray(), 'endorsements' => $endorse->toArray(),
            'sportTags' => $sportTag->toArray(), 'clubTags' => $clubTag,
            'charityTags' => $charityTag->toArray(), 'sponsorTags' => $sponsorTag->toArray(),
            'locations' => $locations->toArray()];

        $this->info(json_encode($data));

        return 0;
    }

}

<?php

namespace Pickstar\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use Pickstar\Talent\Importer\CsvImporter;

class ImportTalent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pickstar:import-talent';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import talent into the system via a CSV';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $importer = new CsvImporter(
            app('filesystem')->disk('local'),
            $this->laravel['db']->connection()
        );

        $talent = $importer->readTalentFromCsv();

        $this->line('');

        $this->output->title('Importing Talent');

        $talent->each(function ($talent) use ($importer) {
            if (! $talent->validate()) {
                $this->warn('Failed to validate row: '.$talent->getIndex());

                return;
            }

            try {
                $user = $importer->writeTalentToDatabase($talent);

                $this->info('Created: '.$user->name);
            } catch (Exception $exception) {
                $this->error('Failed to create row: '.$talent->getIndex());
            }
        });

        $this->line('');
    }
}

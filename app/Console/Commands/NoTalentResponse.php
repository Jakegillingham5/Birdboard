<?php

namespace Pickstar\Console\Commands;

use Exception;
use Pickstar\Notifications;
use Pickstar\User\AdminUser;
use Pickstar\Booking\Booking;
use Illuminate\Console\Command;

class NoTalentResponse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pickstar:no-talent-response';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notify admins of any talent that have not confirmed their attendance to a booking 12 hours out from the event';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $bookings = Booking::booked()
            ->has('talentUnconfirmed')
            ->where('utc_date', '<=', now()->addHours(12))
            ->where('utc_date', '>', now())
            ->whereHas('notification', function ($query) {
                $query->where('talent_unconfirmed', false);
            })
            ->get();

        foreach ($bookings as $booking) {
            foreach ($booking->talentUnconfirmed as $talent) {
                try {
                    (new AdminUser)->notify(new Notifications\System\NoTalentResponse($booking, $talent));

                    $booking->notification->fill(['talent_unconfirmed' => true])->save();

                    $this->info(sprintf('Notified admin of no response from %s.', $talent->name));
                } catch (Exception $exception) {
                    $this->error(sprintf('Failed to notify admin of no response from %s.', $talent->name));

                    report($exception);
                }
            }
        }
    }
}

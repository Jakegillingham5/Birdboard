<?php

namespace Pickstar\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Pickstar\Booking\Booking;
use Pickstar\Notifications;

class WeeklyRecap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pickstar:weekly-recap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mail clients a weekly summary of their booking';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Booking::activeRequests()
                ->isNotExpired()
                ->with('client')
                ->whereHas('talentApplied')
                ->get()
                ->each(function ($booking) {
                    if (Carbon::parse($booking->updated_at)->diffInHours(Carbon::now()) >= 24) {
                        $booking->client->notify(new Notifications\Client\WeeklyRecap($booking));
                    }
                });
    }
}

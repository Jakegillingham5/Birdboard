<?php

namespace Pickstar\Console\Commands;

use Illuminate\Console\Command;
use Pickstar\Booking\Booking;
use Pickstar\Services\XeroService;
use XeroPHP\Remote\Exception\BadRequestException;

class CreateXeroInvoice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pickstar:create-xero-invoice {booking_id}';

    /**
     * @var XeroService
     */
    protected $service;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates Xero invoices and bills for a booking';

    public function __construct(XeroService $service)
    {
        $this->service = $service;
        return parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $id = $this->argument('booking_id');

        /* @var Booking $booking */

        $booking    = Booking::find($id);
        $client     = $booking->client;
        $clientName = $client->name;

        $this->output->text('Creating Invoices and Bills for booking #' . $booking->id . ': ' . $clientName);

        foreach ($booking->talentAttending as $talent){

            $this->service->createAllRecordsForTalentBooking($booking, $talent);
        }
    }
}

<?php

namespace Pickstar\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Pickstar\Booking\Booking;
use Pickstar\Notifications;

class OneWeekNoApplicants extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pickstar:no-applicants';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'ONCE ONLY mails the client to tell them they have not had any applicants';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Booking::activeRequests()
                ->isNotExpired()
                ->with('client')
                ->whereDoesntHave('talentApplied')
                ->whereHas('notification', function ($query) {
                    $query->where('one_week_no_applicants', false);
                })
                ->get()
                ->each(function ($booking) {
                    if (Carbon::parse($booking->updated_at)->diffInDays(Carbon::now()) >= 7) {
                        $booking->client->notify(new Notifications\Client\OneWeekNoApplicants($booking));
                        $booking->notification->fill(['one_week_no_applicants' => true])->save();
                    }
                });
    }
}

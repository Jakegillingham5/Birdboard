<?php

namespace Pickstar\Console\Commands;

use Illuminate\Support\Str;
use Pickstar\Booking\Booking;
use Illuminate\Console\Command;
use Pickstar\Notifications\Talent\MarkAsCompleteReminder;

class MarkAsCompleteReminders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pickstar:mark-as-complete-booking-reminder
                            {--reminder= : Which reminder to send (initialInPerson, initialOnline, autoInPerson, autoOnline)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send mark booking as complete reminder to talent';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $reminders = ['initialInPerson', 'initialOnline', 'autoInPerson', 'autoOnline'];

        if (!in_array($this->option('reminder'), $reminders)) {
            $this->warn('Invalid reminder given, must be one of "initialInPerson", "initialOnline", "autoInPerson", or "autoOnline".');

            return;
        }

        $this->{'handle' . Str::studly($this->option('reminder')) . 'Reminder'}();
    }

    /**
     * Handle initial reminder command.
     *
     * Notifies talent to mark in_person requirement as complete.
     *
     * @return void
     */
    protected function handleInitialInPersonReminder()
    {
        $reminderDate = now()->subDays(system_setting('mark_as_complete_initial_reminder'))->format('Y-m-d');

        Booking::booked()
            ->whereIn('appearance_type', ['in_person', 'both'])
            ->where('date', '=', $reminderDate)
            ->whereHas('notification', function ($query) {
                $query->where('mark_as_complete_initial_in_person_reminder', false);
            })
            ->get()
            ->each(function ($booking) {
                $booking->talentNotCompletedInPerson()
                    ->each(function ($talent) use ($booking) {
                        $talent->notify(new MarkAsCompleteReminder($booking));
                    });

                $booking->notification->fill(['mark_as_complete_initial_in_person_reminder' => true])->save();
            });
    }

    /**
     * Handle final reminder command.
     *
     * Notifies talent to mark online requirement as complete.
     *
     * @return void
     */
    protected function handleInitialOnlineReminder()
    {
        $reminderDate = now()->subDays(system_setting('mark_as_complete_initial_reminder'))->format('Y-m-d');

        Booking::booked()
            ->whereIn('appearance_type', ['online', 'both'])
            ->where('complete_by_date', '=', $reminderDate)
            ->whereHas('notification', function ($query) {
                $query->where('mark_as_complete_initial_online_reminder', false);
            })
            ->get()
            ->each(function ($booking) {
                $booking->talentNotCompletedOnline()
                    ->each(function ($talent) use ($booking) {
                        $talent->notify(new MarkAsCompleteReminder($booking));
                    });

                $booking->notification->fill(['mark_as_complete_initial_online_reminder' => true])->save();
            });
    }

    /**
     * Handle admin reminder command.
     *
     * Automatically mark in_person requirement as complete.
     *
     * @return void
     */
    protected function handleAutoInPersonReminder()
    {
        $reminderDate = now()->subDays(system_setting('mark_as_complete_auto_fill'))->format('Y-m-d');

        Booking::booked()
            ->whereIn('appearance_type', ['in_person', 'both'])
            ->where('date', '=', $reminderDate)
            ->whereHas('notification', function ($query) {
                $query->where('mark_as_complete_in_person_auto_fill', false);
            })
            ->get()
            ->each(function ($booking) {
                $booking->talentNotCompletedInPerson()
                    ->each(function ($talent) use ($booking) {
                        $booking->markTalentHasComplete($talent->response, Booking::APPEARANCE_IN_PERSON);
                    });

                $booking->notification->fill(['mark_as_complete_in_person_auto_fill' => true])->save();
            });
    }

    /**
     * Handle admin reminder command.
     *
     * Automatically mark online requirement as complete.
     *
     * @return void
     */
    protected function handleAutoOnlineReminder()
    {
        $reminderDate = now()->subDays(system_setting('mark_as_complete_auto_fill'))->format('Y-m-d');

        Booking::booked()
            ->whereIn('appearance_type', ['online', 'both'])
            ->where('complete_by_date', '=', $reminderDate)
            ->whereHas('notification', function ($query) {
                $query->where('mark_as_complete_online_auto_fill', false);
            })
            ->get()
            ->each(function ($booking) {
                $booking->talentNotCompletedOnline()
                    ->each(function ($talent) use ($booking) {
                        $booking->markTalentHasComplete($talent->response, Booking::APPEARANCE_ONLINE);
                    });
                $booking->notification->fill(['mark_as_complete_online_auto_fill' => true])->save();
            });
    }
}

<?php

namespace Pickstar\Console\Commands;

use Pickstar\User\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Console\ConfirmableTrait;
use Illuminate\Support\Facades\Password;
use Pickstar\Jobs\Concerns\WritesCsvFiles;

class TalentPasswordResets extends Command
{
    use WritesCsvFiles,
        ConfirmableTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pickstar:talent-password-resets
                            {--force : Force password resetting}
                            {--output=file : Where to output CSV (stdout, file)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset talent passwords and export CSV';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (! in_array($this->option('output'), ['stdout', 'file'])) {
            $this->error('Invalid output format.');

            return;
        }

        $usingStdOut = $this->option('output') === 'stdout';

        // This is a potentially dangerous action, we'll confirm it before proceeding.
        if (! $usingStdOut) {
            $this->output->note(sprintf('Password reset tokens are set to expire in %s minutes.', config('auth.passwords.users.expire')));
        }

        if (
            ! $this->confirmToProceed('This will reset all talent passwords!', function () {
                return true;
            })
        ) {
            return;
        }

        $csv = $this->getCsvWriterInstance()
            ->setDelimiter(',');

        $csv->insertOne([
            'first_name',
            'last_name',
            'email',
            'password_reset_token'
        ]);

        $talent = User::talent()->get();

        $progress = $this->output->createProgressBar($talent->count());

        $csv->insertAll($talent->map(function ($talent) use ($progress) {
            $record = [
                $talent->first_name,
                $talent->last_name,
                $talent->email,
                Password::broker()->createToken($talent)
            ];

            $progress->advance();

            return $record;
        })->toArray());

        if ($this->option('output') === 'stdout') {
            $this->line($csv->getContent());

            return;
        } else {
            $this->line('');
            $this->line('');

            Storage::disk('local')->put(
                $file = sprintf('talent_passwords_%s.csv', now()->format('Y-m-d_h-i-s')),
                $csv->getContent()
            );

            $this->output->success(sprintf('Wrote CSV to file %s', $file));
        }
    }
}

<?php

namespace Pickstar\Console\Commands;

use Pickstar\User\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Console\ConfirmableTrait;
use Illuminate\Support\Facades\Password;
use Pickstar\Jobs\Concerns\WritesCsvFiles;

class ManagerPasswordResets extends Command
{
    use WritesCsvFiles,
        ConfirmableTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pickstar:manager-password-resets
                            {--force : Force password resetting}
                            {--output=file : Where to output CSV (stdout, file)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset blank manager passwords and export CSV';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->output->note(sprintf('Password reset tokens are set to expire in %s minutes.', config('auth.passwords.users.expire')));

        if (
            ! $this->confirmToProceed('This will reset passwords for managers with no password!', function () {
                return true;
            })
        ) {
            return;
        }

        $csv = $this->getCsvWriterInstance()
            ->setDelimiter(',');

        $csv->insertOne([
            'first_name',
            'last_name',
            'email',
            'password_reset_token'
        ]);

        $managers = User::masterManagers()->whereNull('password')->get();

        $progress = $this->output->createProgressBar($managers->count());

        $csv->insertAll($managers->map(function ($manager) use ($progress) {
            $record = [
                $manager->first_name,
                $manager->last_name,
                $manager->email,
                Password::broker()->createToken($manager)
            ];

            $progress->advance();

            return $record;
        })->toArray());

        $this->line('');
        $this->line('');

        Storage::disk('local')->put(
            $file = sprintf('manager_passwords_%s.csv', now()->format('Y-m-d_h-i-s')),
            $csv->getContent()
        );

        $this->output->success(sprintf('Wrote manager passwords CSV to file %s', $file));

        // Generate manager/talent associations.
        $csv = $this->getCsvWriterInstance()
            ->setDelimiter(',');

        $csv->insertOne([
            'manager_email',
            'talent_emails'
        ]);

        $csv->insertAll($managers->map(function ($manager) {
            $record = [
                $manager->email,
                $manager->managementTalent
                    ->map(function ($talent) {
                        return ['value' => $talent->email ?? $talent->name];
                    })
                    ->implode('value', ', ')
            ];

            return $record;
        })->toArray());

        Storage::disk('local')->put(
            $file = sprintf('manager_talent_associations_%s.csv', now()->format('Y-m-d_h-i-s')),
            $csv->getContent()
        );

        $this->output->success(sprintf('Wrote manager/talent associations CSV to file %s', $file));
    }
}

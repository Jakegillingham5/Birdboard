<?php

namespace Pickstar\Console\Commands;

use Pickstar\Booking\Booking;
use Illuminate\Console\Command;

class UpdateDateAppliedfor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pickstar:update-date-applied-for
                            {--booking_id= : Which booking to update talents\' date_applied_for}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update all applied talents\' date_applied_for to booking date if one exists and there are no date_options';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $booking_id = $this->option('booking_id');

        $booking = Booking::requests()->find($booking_id);

        if ($booking === null) {
            $this->warn('Invalid booking_id given, must be the id of a request (pending)');

            return;
        }

        $this->handleUpdateBooking($booking);
    }

    /**
     * Update all applied talents' date_applied_for to booking date.
     **
     * @param $booking
     * @return void
     */
    protected function handleUpdateBooking($booking)
    {
        if ($booking->date === null) {
            return;
        }

        $booking->talentApplied->each(function($talentQuery) use ($booking) {
            if ($talentQuery->response->date_applied_for === null) {
                $talentQuery->response->fill(['date_applied_for' => $booking->date])->save();
            }
        });
    }
}

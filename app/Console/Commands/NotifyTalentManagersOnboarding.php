<?php

namespace Pickstar\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use League\Csv\Reader;
use League\Csv\Statement;
use Pickstar\Mail\Talent\OnBoarding;
use Pickstar\User\User;

class NotifyTalentManagersOnboarding extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pickstar:on-boarding';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Talent On-Boarding via a CSV';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $csv = Reader::createFromPath(app('filesystem')->disk('local')->path('csv/onboarding.csv'), 'r');
        $csv->setHeaderOffset(0);

        $this->line('');

        $this->output->title('Sending Talent On-Boarding');

        $records = (new Statement)
            ->process($csv);

        foreach ($records as $record) {
            $user = User::whereEmail($record['Email Address'])->first();

            if($user) {
                $this->line(sprintf('Setting manager email to %s for %s', $record['Manager Email Address'], $record['Email Address']) );
                $user->profile->manager_email = $record['Manager Email Address'];
                $user->profile->save();
            }

            $this->line(sprintf('Mail queued for: %s', $record['Manager Email Address']) );

            Mail::to($record['Manager Email Address'])->queue(new OnBoarding($record));
        }

        $this->line('');
    }
}

<?php

namespace Pickstar\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Pickstar\Booking\Booking;
use Pickstar\Notifications;
use Pickstar\User\User;

class TextRecap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pickstar:text-recap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Text clients a summary of their booking';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $clients = User::onlyClients()->whereHas('clientBookingRequests')->get();
        foreach ($clients as $client) {
            $bookings = $client->clientBookingRequests()
                            ->where('updated_at', '<=', Carbon::now()->subDay())
                            ->activeRequests()
                            ->whereHas('talentApplied');
            if ($bookings->count() >= 1) {
                $client->notify(new Notifications\Client\TextRecap($bookings->get()));
            }
        }
    }
}

<?php

namespace Pickstar\Console\Commands;

use SendGrid\Mail\To;
use SendGrid\Mail\Mail;
use Pickstar\Marketing\Email;
use Illuminate\Console\Command;
use SendGrid\Mail\Personalization;
use Pickstar\Services\SendGridTransactionalTemplateService;

class MarketingEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pickstar:marketing-emails
                            {--stage= : Which campaign stage to handle}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends marketing emails using SendGrid transactional templates';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(SendGridTransactionalTemplateService $sendGridService)
    {
        $stages = [
            1 => 'one',
            2 => 'two',
            3 => 'three',
            4 => 'four',
            5 => 'five'
        ];

        $stage = $this->option('stage');

        if (!$stage || !isset($stages[$stage])) {
            foreach ($stages as $stage) {
                $this->{'handleStage'.ucfirst($stage)}($sendGridService);
            }

            return;
        }

        $this->{'handleStage'.ucfirst($stages[$stage])}($sendGridService);
    }

    /**
     * Map emails to a personalization object.
     *
     * @param \Pickstar\Marketing\Email $email
     *
     * @return \SendGrid\Mail\Personalization
     */
    public function mapEmailToPersonalization(Email $email): Personalization
    {
        $personalization = new Personalization;

        $personalization->addTo(new To($email->email));
        $personalization->addSubstitution('email', $email->email);
        $personalization->addSubstitution('first_name', $email->first_name);
        $personalization->addSubstitution('last_name', $email->last_name);

        return $personalization;
    }

    /**
     * Handle stage 1 emails 3 hours after record is created and does not
     * have an associated user record.
     *
     * @param \Pickstar\Services\SendGridTransactionalTemplateService $sendGridService
     *
     * @return void
     */
    protected function handleStageOne(SendGridTransactionalTemplateService $sendGridService): void
    {
        $query = Email::whereDoesntHave('user')
            ->whereNull('campaign_stage')
            ->where('created_at', '<=', now()->subHours(1))->where('id','<',4691);

        $personalizations = (clone $query)
            ->get()
            ->map([$this, 'mapEmailToPersonalization']);

        $query->update(['campaign_stage' => 1, 'campaign_last_sent_date' => now()]);

        $sendGridService->dispatch(config('pickstar.marketing_transactional_templates.stage_1'), $personalizations, ['lead_funnel', 'lead_funnel_stage_1'], [$this, 'registerSendGridAsm']);
    }

    /**
     * Handle stage 2 emails 48 hours after stage 1 where the record does not
     * have an associated user record.
     *
     * @param \Pickstar\Services\SendGridTransactionalTemplateService $sendGridService
     *
     * @return void
     */
    protected function handleStageTwo(SendGridTransactionalTemplateService $sendGridService): void
    {
        $query = Email::whereDoesntHave('user')
            ->where('campaign_stage', 1)
            ->where('campaign_last_sent_date', '<=', now()->subHours(48))->where('id','<',4691);

        $personalizations = (clone $query)
            ->get()
            ->map([$this, 'mapEmailToPersonalization']);

        $query->update(['campaign_stage' => 2, 'campaign_last_sent_date' => now()]);

        $sendGridService->dispatch(config('pickstar.marketing_transactional_templates.stage_2'), $personalizations, ['lead_funnel', 'lead_funnel_stage_2'], [$this, 'registerSendGridAsm']);
    }

    /**
     * Handle stage 3 emails 48 hours after stage 2 where the record does not
     * have an associated user record.
     *
     * @param \Pickstar\Services\SendGridTransactionalTemplateService $sendGridService
     *
     * @return void
     */
    protected function handleStageThree(SendGridTransactionalTemplateService $sendGridService): void
    {
        $query = Email::whereDoesntHave('user')
            ->where('campaign_stage', 2)
            ->where('campaign_last_sent_date', '<=', now()->subHours(144))->where('id','<',4691);

        $personalizations = (clone $query)
            ->get()
            ->map([$this, 'mapEmailToPersonalization']);

        $query->update(['campaign_stage' => 3, 'campaign_last_sent_date' => now()]);

        $sendGridService->dispatch(config('pickstar.marketing_transactional_templates.stage_3'), $personalizations, ['lead_funnel', 'lead_funnel_stage_3'], [$this, 'registerSendGridAsm']);
    }

    /**
     * Handle stage 4 emails 48 hours after stage 3 where the record does not
     * have an associated user record.
     *
     * @param \Pickstar\Services\SendGridTransactionalTemplateService $sendGridService
     *
     * @return void
     */
    protected function handleStageFour(SendGridTransactionalTemplateService $sendGridService): void
    {
        $query = Email::whereDoesntHave('user')
            ->where('campaign_stage', 3)
            ->where('campaign_last_sent_date', '<=', now()->subHours(168))->where('id','<',4691);

        $personalizations = (clone $query)
            ->get()
            ->map([$this, 'mapEmailToPersonalization']);

        $query->update(['campaign_stage' => 4, 'campaign_last_sent_date' => now()]);

        $sendGridService->dispatch(config('pickstar.marketing_transactional_templates.stage_4'), $personalizations, ['lead_funnel', 'lead_funnel_stage_4'], [$this, 'registerSendGridAsm']);
    }

    /**
     * Handle stage 5 emails 48 hours after stage 4 where the record does not
     * have an associated user record.
     *
     * @param \Pickstar\Services\SendGridTransactionalTemplateService $sendGridService
     *
     * @return void
     */
    protected function handleStageFive(SendGridTransactionalTemplateService $sendGridService): void
    {
        $query = Email::whereDoesntHave('user')
            ->where('campaign_stage', 4)
            ->where('campaign_last_sent_date', '<=', now()->subHours(144))->where('id','<',4691);

        $personalizations = (clone $query)
            ->get()
            ->map([$this, 'mapEmailToPersonalization']);

        $query->update(['campaign_stage' => 5, 'campaign_last_sent_date' => now()]);

        $sendGridService->dispatch(config('pickstar.marketing_transactional_templates.stage_5'), $personalizations, ['lead_funnel', 'lead_funnel_stage_5'], [$this, 'registerSendGridAsm']);
    }

    /**
     * Register the unsubscribe group on the SendGrid mail.
     *
     * @param \SendGrid\Mail\Mail $mail
     *
     * @return void
     */
    public function registerSendGridAsm(Mail $mail): void
    {
        $mail->setAsm(config('pickstar.unsubscribe_groups.marketing_emails'));
    }
}

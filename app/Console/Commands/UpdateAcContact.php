<?php

namespace Pickstar\Console\Commands;

use GuzzleHttp\RequestOptions;
use Illuminate\Console\Command;
use Pickstar\Marketing\Email;
use Pickstar\Marketing\Source;
use Pickstar\Providers\ActiveCampaignServiceProvider;
use Pickstar\Services\ActiveCampaignService;
use Pickstar\User\ActiveCampaignUser;
use Pickstar\User\User;

class UpdateAcContact extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pickstar:ac-contact {email}';

    /**
     * @var ActiveCampaignService
     */
    protected $api;

    /**
     * @var ActiveCampaignUser
     */
    protected $activeCampaignUser;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update ActiveCampaign contact record';

    public function __construct(ActiveCampaignService $service)
    {
        $this->api = $service;
        return parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->argument('email');
        $this->activeCampaignUser  = ActiveCampaignUser::findByEmail($email);
        $payload = $this->activeCampaignUser->buildContactPayload();
        $this->output->text("Attempting to update/create contact");

        $this->api->updateContact($email, $payload);
        $tags = $this->activeCampaignUser->getTagsAndSegments();
        if(count($tags) > 0){
            $this->output->text("Attempting to update/create tags");
            $this->api->updateTags($email, $tags);
        }

        $this->output->text("Attempting to update custom fields");
        $this->api->updateCustomFields($email, $payload);

        $this->output->text("Attempting to move list if appropriate");
        $this->api->moveListSubscriptionIfRequired($email, $this->activeCampaignUser->getUserSubscribedListAttribute());
    }
}

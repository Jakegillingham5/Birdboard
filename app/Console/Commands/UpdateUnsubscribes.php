<?php

namespace Pickstar\Console\Commands;

use GuzzleHttp\RequestOptions;
use Illuminate\Console\Command;
use Pickstar\Providers\ActiveCampaignServiceProvider;

class UpdateUnsubscribes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pickstar:unsubscribes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update new unsubscribe requests within ActiveCampaign';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $app = app();
        $ac = new ActiveCampaignServiceProvider($app);
        $ac->boot();
        $unsubscribes = include('./unsubscribes.php');

        foreach ($unsubscribes as $email){
            $data = $ac->query('contacts', [
                'email' => $email
            ]);
            foreach ($data as $contact){

                $path = 'contacts/' . $contact->id;
                $actual = $ac->api($path);

                foreach ($actual->contactLists as $list){
                    if($list->status != 2){
                        $package = [
                            'list' => $list->list,
                            'contact' => $contact->id,
                            'status' => 2
                        ];
                        $ac->post('contactLists', ['contactList' => $package]);
                        $this->output->note($email . ' unsubscribed');
                    } else {
                        $this->output->note($email . ' has already been unsubscribed');
                    }

                }
            }
        }

    }
}

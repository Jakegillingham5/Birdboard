<?php

namespace Pickstar\Console\Commands;

use Illuminate\Support\Str;
use Pickstar\Booking\Booking;
use Illuminate\Console\Command;
use Pickstar\Booking\TalentPivot;
use Pickstar\Notifications\Talent\FinalBookingReminder;
use Pickstar\Notifications\Talent\SecondBookingReminder;
use Pickstar\Notifications\Talent\InitialBookingReminder;

class BookingReminders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pickstar:booking-reminders
                            {--reminder= : Which reminder to send (initial, second, final)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send booking reminder to talent';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $reminders = ['initial', 'second', 'final'];

        if (! in_array($this->option('reminder'), $reminders)) {
            $this->warn('Invalid reminder given, must be one of "initial", "second", or "final".');

            return;
        }

        $this->{'handle'.studly_case($this->option('reminder')).'Reminder'}();
    }

    /**
     * Handle initial reminder command.
     *
     * Notifies talent that they have an upcoming booking.
     *
     * @return void
     */
    protected function handleInitialReminder()
    {
        $reminderDate = now()->addDays(system_setting('initial_talent_reminder'));

        Booking::booked()
            ->where('utc_date', '>=', $reminderDate->copy()->startOfDay())
            ->where('utc_date', '<=', $reminderDate->copy()->endOfDay())
            ->whereHas('notification', function ($query) {
                $query->where('initial_talent_reminder', false);
            })
            ->get()
            ->each(function ($booking) {
                $booking->talentAttending->each(function ($talent) use ($booking) {
                    $talent->notify(new InitialBookingReminder($booking));
                });

                $booking->notification->fill(['initial_talent_reminder' => true])->save();
            });
    }

    /**
     * Handle second reminder command.
     *
     * Notifies talent they need to confirm their attendance for an upcoming booking.
     *
     * @return void
     */
    protected function handleSecondReminder()
    {
        Booking::booked()
            ->has('talentUnconfirmed')
            ->where('utc_date', '<=', now()->addDays(system_setting('second_talent_reminder')))
            ->whereHas('notification', function ($query) {
                $query->where('second_talent_reminder', false);
            })
            ->get()
            ->each(function ($booking) {
                $booking->talentUnconfirmed->each(function ($talent) use ($booking) {
                    if (is_null($talent->sanitisedNumber)) {
                        return;
                    }

                    if (is_null($accessToken = $talent->response->attendance_access_token)) {
                        // TODO -figure out why this block is failing for auditing
                        TalentPivot::disableAuditing();
                        $booking->talent()->updateExistingPivot($talent->id, [
                            'attendance_access_token' => $accessToken = Str::random(32)
                        ]);
                        TalentPivot::enableAuditing();
                    }

                    $talent->notify(new SecondBookingReminder($booking, $accessToken));
                });

                $booking->notification->fill(['second_talent_reminder' => true])->save();
            });
    }

    /**
     * Handle final reminder command.
     *
     * @return void
     */
    protected function handleFinalReminder()
    {
        Booking::booked()
            ->where('utc_date', '<=', now()->addHours(system_setting('final_talent_reminder')))
            ->whereHas('notification', function ($query) {
                $query->where('final_talent_reminder', false);
            })
            ->get()
            ->each(function ($booking) {
                $booking->talentAttending->each(function ($talent) use ($booking) {
                    $talent->notify(new FinalBookingReminder($booking));
                });

                $booking->notification->fill(['final_talent_reminder' => true])->save();
            });
    }
}

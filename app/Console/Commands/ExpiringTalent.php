<?php

namespace Pickstar\Console\Commands;

use Pickstar\User\AdminUser;
use Carbon\Carbon;
use Pickstar\Booking\Booking;
use Illuminate\Console\Command;
use Pickstar\Notifications\System\BookingRequestExpiring;

class ExpiringTalent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pickstar:expiring-talent';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Expire all talent that are scheduled to be expired';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Booking::activeRequests()
            ->whereHas('talentApplied')
            ->each(function ($booking) {
                $talents = $booking->talent()->whereNotNull('expiry_date')->get();
                foreach($talents as $talent) {
                    if(Carbon::now()->format('Y-m-d') >= date('Y-m-d', strtotime($talent->response->expiry_date))){
                        $talent->response->fill(['expired' => true])->save();
                    }
                }
            });
    }
}

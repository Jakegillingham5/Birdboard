<?php

namespace Pickstar\Console\Commands;

use Exception;
use Pickstar\Booking\Booking;
use Illuminate\Console\Command;
use Pickstar\Notifications\Talent\ShortlistedForBookingRequest;

class RemainingShortlist extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pickstar:remaining-shortlist';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notify remaining shortlist about a booking request';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $bookings = Booking::activeRequests()
            ->has('talentShortlisted')
            ->where('vetted_date', '<=', now()->subHours(48))
            ->get();

        if ($bookings->count() === 0) {
            $this->warn('There are no bookings to process.');

            return;
        }

        $notificationsDispatched = 0;
        $notificationsFailed = 0;

        foreach ($bookings as $booking) {
            $this->output->title(sprintf('Processing Booking #%s', $booking->id));

            // Grab the shortlisted talent where they have a priority greater than 5 and where they have
            // not already been notified about the booking.
            $talentShortlisted = $booking->talentShortlisted()
                ->wherePivot('priority', '>', 5)
                ->wherePivot('priority', '<=', 10)
                ->whereDoesntHave('talentBookingNotifications', function ($query) use ($booking) {
                    $query->where('bookings.id', $booking->id);
                })
                ->get();

            foreach ($talentShortlisted as $talent) {
                try {
                    $talent->notify(new ShortlistedForBookingRequest($booking, $talent->response->priority));

                    $booking->talentNotified()->attach($talent);

                    $this->info(sprintf('Notified %s about booking request.', $talent->name));

                    $notificationsDispatched++;
                } catch (Exception $exception) {
                    $this->error(sprintf('Failed to notify %s about booking request.', $talent->name));

                    $notificationsFailed++;

                    report($exception);
                }
            }
        }

        if ($notificationsDispatched > 0) {
            $this->output->success(sprintf('Successfully notified %s talent.', $notificationsDispatched));
        }

        if ($notificationsFailed > 0) {
            $this->output->error(sprintf('Failed to notify %s talent.', $notificationsFailed));
        }
    }
}

<?php

namespace Pickstar\Console\Commands;

use Pickstar\User\AdminUser;
use Pickstar\Booking\Booking;
use Illuminate\Console\Command;
use Pickstar\Notifications\System\BookingRequestExpired;

class ExpiredRequests extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pickstar:expired-requests';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notify admins of requests that have expired';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Booking::activeRequests()
            ->isExpiring()
            ->whereHas('notification', function ($query) {
                $query->where('request_expired', false);
            })
            ->get()
            ->each(function ($booking) {
                (new AdminUser)->notify(new BookingRequestExpired($booking));
                $booking->client->notify(new \Pickstar\Notifications\Client\BookingRequestExpired($booking));

                $booking->notification->fill(['request_expired' => true])->save();
                $booking->fill([
                    'status' => Booking::STATUS_WITHDRAWN,
                    'withdraw_reason' => 'expired',
                ])->save();
            });
        Booking::activeRequests()
            ->isExpiring()
            ->get()
            ->each(function ($booking) {
                $booking->fill([
                    'status' => Booking::STATUS_WITHDRAWN,
                    'withdraw_reason' => 'expired',
                ])->save();
            });
    }
}

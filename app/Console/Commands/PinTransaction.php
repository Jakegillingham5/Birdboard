<?php

namespace Pickstar\Console\Commands;

use GuzzleHttp\RequestOptions;
use Illuminate\Console\Command;
use Pickstar\Marketing\Email;
use Pickstar\Marketing\Source;
use Pickstar\Providers\ActiveCampaignServiceProvider;
use Pickstar\Services\ActiveCampaignService;
use Pickstar\Services\PinService;
use Pickstar\User\ActiveCampaignUser;
use Pickstar\User\User;

class PinTransaction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pickstar:pin-transaction {token}';

    /**
     * @var ActiveCampaignService
     */
    protected $api;

    /**
     * @var ActiveCampaignUser
     */
    protected $activeCampaignUser;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Pin Transaction Record';

    public function __construct(PinService $service)
    {
        $this->api = $service;
        return parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $token  = $this->argument('token');
        $record = $this->api->getClient()->call('charges/' . $token )->response->amount;

        if($record !== null){
            $this->output->text('Record retrieved successfully');
        }

    }
}

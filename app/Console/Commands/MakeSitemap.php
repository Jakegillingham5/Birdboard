<?php

namespace Pickstar\Console\Commands;

use Laravelium\Sitemap\Sitemap;
use Pickstar\User\User;
use Illuminate\Console\Command;

class MakeSitemap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:sitemap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new sitemap';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $maps = collect([
            'static' => $this->laravel->make(Sitemap::class),
            'talent' => $this->laravel->make(Sitemap::class)
        ]);

        // Add static pages to the static sitemap.
        collect([
            ['name' => 'home', 'priority' => '1', 'frequency' => 'monthly'],
            ['name' => 'howitworks.index', 'priority' => '0.8', 'frequency' => 'monthly'],
            ['name' => 'about.index', 'priority' => '0.5', 'frequency' => 'monthly'],
            ['name' => 'privacy.index', 'priority' => '0.5', 'frequency' => 'monthly'],
            ['name' => 'case-studies.index', 'priority' => '0.5', 'frequency' => 'monthly'],
            ['name' => 'contact.index', 'priority' => '0.5', 'frequency' => 'monthly'],
            ['name' => 'login', 'priority' => '0.5', 'frequency' => 'monthly'],
            ['name' => 'register.talent', 'priority' => '0.6', 'frequency' => 'monthly'],
            ['name' => 'ourstars.index', 'priority' => '0.8', 'frequency' => 'weekly'],
            ['name' => 'esports.index', 'priority' => '0.8', 'frequency' => 'monthly'],
            ['name' => 'booking.request.index', 'priority' => '0.7', 'frequency' => 'monthly'],
            ['name' => 'landing.influencers', 'priority' => '0.7', 'frequency' => 'weekly'],
            ['name' => 'ourstars.landing', 'parameters' => ['men'], 'priority' => '0.5', 'frequency' => 'weekly'],
            ['name' => 'ourstars.landing', 'parameters' => ['male'], 'priority' => '0.5', 'frequency' => 'weekly'],
            ['name' => 'ourstars.landing', 'parameters' => ['women'], 'priority' => '0.5', 'frequency' => 'weekly'],
            ['name' => 'ourstars.landing', 'parameters' => ['female'], 'priority' => '0.5', 'frequency' => 'weekly'],
            ['name' => 'ourstars.landing', 'parameters' => ['mc'], 'priority' => '0.7', 'frequency' => 'weekly'],
            ['name' => 'ourstars.landing', 'parameters' => ['keynote'], 'priority' => '0.7', 'frequency' => 'weekly'],
            ['name' => 'ourstars.landing', 'parameters' => ['ambassadors'], 'priority' => '0.7', 'frequency' => 'weekly'],
            ['name' => 'ourstars.landing', 'parameters' => ['influencers'], 'priority' => '0.7', 'frequency' => 'weekly'],
            ['name' => 'ourstars.landing', 'parameters' => ['celebrities'], 'priority' => '0.7', 'frequency' => 'weekly'],
            ['name' => 'ourstars.landing', 'parameters' => ['skills-clinic'], 'priority' => '0.6', 'frequency' => 'weekly'],
            ['name' => 'ourstars.landing', 'parameters' => ['gaming'], 'priority' => '0.7', 'frequency' => 'weekly'],
            ['name' => 'ourstars.landing', 'parameters' => ['esports'], 'priority' => '0.7', 'frequency' => 'weekly'],
            ['name' => 'ourstars.landing', 'parameters' => ['party'], 'priority' => '0.7', 'frequency' => 'weekly'],
            ['name' => 'ourstars.landing', 'parameters' => ['meet-and-greet'], 'priority' => '0.7', 'frequency' => 'weekly'],
            ['name' => 'ourstars.landing', 'parameters' => ['matildas'], 'priority' => '0.7', 'frequency' => 'weekly'],
            ['name' => 'ourstars.landing', 'parameters' => ['fundraiser'], 'priority' => '0.7', 'frequency' => 'weekly'],
            ['name' => 'ourstars.landing', 'parameters' => ['appearance'], 'priority' => '0.7', 'frequency' => 'weekly'],
            ['name' => 'ourstars.landing', 'parameters' => ['indigenous-spokespeople'], 'priority' => '0.7', 'frequency' => 'weekly'],

        ])->each(function ($route) use ($maps) {
            $maps['static']->add(route($route['name'], $route['parameters'] ?? []), now(), $route['priority'] ?? '0.5', $route['frequency'] ?? 'monthly');
        });

        // Add talent profiles to the talent sitemap.
        User::talent()
            ->active()
            ->public()
            ->get()
            ->each(function ($user) use ($maps) {
                $maps['talent']->add(route('ourstars.show', [$user->id, $user->slug]), $user->updated_at, '0.8', 'weekly');
            });

        // Store each of the sitemaps.
        $maps->each(function ($map, $name) {
            $map->store('xml', sprintf('sitemap-%s', $name), storage_path('app'));
        });

        // Create the sitemap index file that links to both the talent and static sitemaps.
        $index = $this->laravel->make(Sitemap::class);
        $index->addSitemap(url('sitemap-talent.xml'));
        $index->addSitemap(url('sitemap-static.xml'));

        $index->store('sitemapindex', 'sitemap', storage_path('app'));
    }
}

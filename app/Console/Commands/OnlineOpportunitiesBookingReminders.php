<?php

namespace Pickstar\Console\Commands;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Pickstar\Booking\Booking;
use Illuminate\Console\Command;
use Pickstar\Booking\TalentPivot;
use Pickstar\Notifications\System\AdminOnlineBookingReminder;
use Pickstar\Notifications\Talent\FinalOnlineBookingReminder;
use Pickstar\Notifications\Talent\InitialOnlineBookingReminder;
use Pickstar\User\AdminUser;

class OnlineOpportunitiesBookingReminders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pickstar:online-opportunities-booking-reminders
                            {--reminder= : Which reminder to send (initial, final, admin)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send online opportunity booking reminder to talent';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $reminders = ['initial', 'final', 'admin'];

        if (!in_array($this->option('reminder'), $reminders)) {
            $this->warn('Invalid reminder given, must be one of "initial", "final", or "admin".');

            return;
        }

        $this->{'handle' . Str::studly($this->option('reminder')) . 'Reminder'}();
    }

    /**
     * Handle initial reminder command.
     *
     * Notifies talent that they have an online commitment due soon.
     *
     * @return void
     */
    protected function handleInitialReminder()
    {
        $reminderDate = now()->addDays(system_setting('initial_talent_reminder_online'));

        Booking::booked()
            ->whereIn('appearance_type', ['online', 'both'])
            ->where('complete_by_date', '=', $reminderDate->copy()->format('Y-m-d'))
            ->whereHas('notification', function ($query) {
                return $query->where('initial_talent_reminder_online', false);
            })
            ->get()
            ->each(function ($booking) {
                $booking->talentNotCompletedOnline()
                    ->each(function ($talent) use ($booking) {
                        $talent->notify(new InitialOnlineBookingReminder($booking));
                });

                $booking->notification->fill(['initial_talent_reminder_online' => true])->save();
            });
    }

    /**
     * Handle final reminder command.
     *
     * Notifies talent that they have an online commitment due soon.
     *
     * @return void
     */
    protected function handleFinalReminder()
    {
        $reminderDate = now()->addDays(system_setting('final_talent_reminder_online'));

        Booking::booked()
            ->whereIn('appearance_type', ['online', 'both'])
            ->where('complete_by_date', '=', $reminderDate->copy()->format('Y-m-d'))
            ->whereHas('notification', function ($query) {
                $query->where('final_talent_reminder_online', false);
            })
            ->get()
            ->each(function ($booking) {
                $booking->talentNotCompletedOnline()
                    ->each(function ($talent) use ($booking) {
                        $talent->notify(new FinalOnlineBookingReminder($booking));
                    });

                $booking->notification->fill(['final_talent_reminder_online' => true])->save();
            });
    }

    /**
     * Handle admin reminder command.
     *
     * Notifies talent that they have an online commitment due soon.
     *
     * @return void
     */
    protected function handleAdminReminder()
    {
        $reminderDate = now()->addDays(system_setting('admin_reminder_online'));

        Booking::booked()
            ->whereIn('appearance_type', ['online', 'both'])
            ->where('complete_by_date', '=', $reminderDate->copy()->format('Y-m-d'))
            ->whereHas('notification', function ($query) {
                $query->where('admin_reminder_online', false);
            })
            ->get()
            ->each(function ($booking) {
                if ($booking->talentNotCompletedOnline()->count() > 0) {
                    (new AdminUser())->notify(new AdminOnlineBookingReminder($booking));
                }
                $booking->notification->fill(['admin_reminder_online' => true])->save();
            });
    }
}

<?php

namespace Pickstar\Console\Commands;

use Pickstar\Notifications;
use Pickstar\User\AdminUser;
use Pickstar\Booking\Booking;
use Pickstar\Payment\Payment;
use Illuminate\Console\Command;

class PaymentWarnings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pickstar:payment-warnings
                            {--payment= : Which payment warning to send (deposit or final or urgent)}
                            {--recipient= : Who the warning is intended for (admin or client)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send payment warnings to admin or clients';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (! in_array($this->option('payment'), [Payment::TYPE_DEPOSIT, Payment::TYPE_FINAL, PAYMENT::TYPE_URGENT])) {
            $this->warn(sprintf('Invalid payment given, must be either "%s" or "%s" or "%s".', Payment::TYPE_DEPOSIT, Payment::TYPE_FINAL, Payment::TYPE_URGENT));

            return;
        }

        if (! in_array($this->option('recipient'), ['admin', 'client'])) {
            $this->warn('Invalid recipient given, must be either "admin" or "client".');

            return;
        }

        $this->{'handle'.studly_case($this->option('payment')).'WarningFor'.studly_case($this->option('recipient'))}();
    }

    /**
     * Handle final payment warning for admin.
     *
     * Admin will be notified via text and email when a booking that is happening in the next X days
     * is missing the final payment.
     *
     * @return void
     */
    protected function handleFinalWarningForAdmin()
    {
        Booking::active()
            ->where('utc_date', '<=', now()->addDays(system_setting('final_payment_warning_admin'))->endOfDay())
            ->whereHas('paymentsUnpaid')
            ->whereHas('notification', function ($query) {
                $query->where('final_payment_warning_admin', false);
            })
            ->get()
            ->each(function ($booking) {
                (new AdminUser)->notify(new Notifications\System\FinalPaymentWarning($booking));

                $booking->notification->fill(['final_payment_warning_admin' => true])->save();
            });
    }

    /**
     * Handle final payment warning for admin.
     *
     * Admin will be notified via text and email when a booking that is happening in the next X days
     * is missing the final payment.
     *
     * @return void
     */
    protected function handleUrgentWarningForAdmin()
    {
        Booking::active()
            ->where('utc_date', '<=', now()->addDays(system_setting('urgent_payment_warning_admin'))->endOfDay())
            ->whereHas('paymentsUnpaid')
            ->whereHas('notification', function ($query) {
                $query->where('urgent_payment_warning_admin', false)->where('final_payment_warning_admin', true);
            })
            ->get()
            ->each(function ($booking) {
                (new AdminUser)->notify(new Notifications\System\UrgentPaymentWarning($booking));

                $booking->notification->fill(['urgent_payment_warning_admin' => true])->save();
            });
    }

    /**
     * Handle final payment warning for client.
     *
     * Client will be notified via text when a booking that is happening in the next X days is missing the
     * final payment.
     *
     * @return void
     */
    protected function handleUrgentWarningForClient()
    {
        Booking::active()
            ->where('utc_date', '<=', now()->addDays(system_setting('urgent_payment_warning_client'))->endOfDay())
            ->whereHas('paymentsUnpaid')
            ->whereHas('notification', function ($query) {
                $query->where('urgent_payment_warning_client', false)->where('final_payment_warning_client', true);
            })
            ->get()
            ->each(function ($booking) {
                $booking->client->notify(new Notifications\Client\UrgentPaymentWarning($booking));

                $booking->notification->fill(['urgent_payment_warning_client' => true])->save();
            });
    }

    /**
     * Handle final payment warning for client.
     *
     * Client will be notified via text when a booking that is happening in the next X days is missing the
     * final payment.
     *
     * @return void
     */
    protected function handleFinalWarningForClient()
    {
        Booking::active()
            ->where('utc_date', '<=', now()->addDays(system_setting('final_payment_warning_client'))->endOfDay())
            ->whereHas('paymentsUnpaid')
            ->whereHas('notification', function ($query) {
                $query->where('final_payment_warning_client', false);
            })
            ->get()
            ->each(function ($booking) {
                $booking->client->notify(new Notifications\Client\FinalPaymentWarning($booking));

                $booking->notification->fill(['final_payment_warning_client' => true])->save();
            });
    }

    /**
     * Handle deposit payment warning for admin.
     *
     * Notify admin of unpaid deposits where they have not paid within X days of the booking becoming scheduled.
     *
     * @return void
     */
    protected function handleDepositWarningForAdmin()
    {
        Booking::active()
            ->where('scheduled_date', '<=', now()->subDays(system_setting('deposit_payment_warning_admin'))->startOfDay())
            ->whereHas('notification', function ($query) {
                $query->where('deposit_payment_warning_admin', false);
            })
            ->whereHas('payments', function ($query) {
                $query->where('type', Payment::TYPE_DEPOSIT)
                    ->where('status', Payment::STATUS_UNPAID);
            })
            ->get()
            ->each(function ($booking) {
                (new AdminUser)->notify(new Notifications\System\DepositPaymentWarning($booking));

                $booking->notification->fill(['deposit_payment_warning_admin' => true])->save();
            });
    }

    /**
     * Handle deposit payment warning for client.
     *
     * Notify clients of unpaid deposits where they have not paid within X days of the booking becoming scheduled.
     *
     * @return void
     */
    protected function handleDepositWarningForClient()
    {
        Booking::active()
            ->where('scheduled_date', '<=', now()->subDays(system_setting('deposit_payment_warning_client'))->startOfDay())
            ->whereHas('notification', function ($query) {
                $query->where('deposit_payment_warning_client', false);
            })
            ->whereHas('payments', function ($query) {
                $query->where('type', Payment::TYPE_DEPOSIT)
                    ->where('status', Payment::STATUS_UNPAID);
            })
            ->get()
            ->each(function ($booking) {
                $booking->client->notify(new Notifications\Client\DepositPaymentWarning($booking));

                $booking->notification->fill(['deposit_payment_warning_client' => true])->save();
            });
    }
}

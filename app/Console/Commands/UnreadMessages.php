<?php

namespace Pickstar\Console\Commands;

use Carbon\Carbon;
use Pickstar\User\User;
use Illuminate\Console\Command;
use Pickstar\Services\PusherService;
use Pickstar\Helpers\NotificationHelpers;

class UnreadMessages extends Command
{
    /** @var PusherService  */

    protected $_api;

    public function __construct(PusherService $service)
    {
        $this->_api = $service;
        parent::__construct();
    }

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pickstar:unread-messages';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notify users about their unread messages in the system';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        User::where('message_notified_at', '<=', Carbon::now()->subDay())
            ->get()
            ->each(function ($user) {
                if ($user->threads()->count()) {
                    foreach ($user->threads as $thread) {
                        if ($thread->userUnreadCount($user->id)) {
                            if (!$this->_api::checkIfChannelOccupied('private-chat.' . $user->id)) {
                                NotificationHelpers::notifyUnreadMessages($user);
                            } else {
                                $user->fill(['message_notified_at' => null])->save();
                            }
                            break;
                        }
                    }
                }
            });
    }
}

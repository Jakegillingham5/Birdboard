<?php

namespace Pickstar\Talent;

use Carbon\Carbon;
use Pickstar\User\User;
use Illuminate\Database\Eloquent\Model;

class Endorsement extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    public $table = 'endorsements';

    /**
     * Enable timestamps.
     *
     * @var boolean
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_name',
        'description',
        'date_endorsed'
    ];

    /**
     * Talent the endorsement belongs to a user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Set the date endorsed as a Carbon instance so it's stored as a UTC date.
     *
     * @param null|string $value
     *
     * @return void
     */
    public function setDateEndorsedAttribute($value)
    {
        if ($value) {
            $this->attributes['date_endorsed'] = Carbon::parse($value);
        } else {
            $this->attributes['date_endorsed'] = null;
        }
    }

    /**
     * Get the date endorsed as an ISO8601 string so it can be properly display within the date
     * components as the correct date.
     *
     * @param null|string $value
     *
     * @return null|string
     */
    public function getDateEndorsedAttribute($value)
    {
        if (! is_null($value)) {
            return Carbon::parse($value)->toIso8601String();
        }

        return $value;
    }
}

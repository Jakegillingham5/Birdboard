<?php

namespace Pickstar\Talent\Concerns;

use Exception;
use Intervention\Image\Image;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;
use Intervention\Image\ImageManagerStatic as ImageManager;

trait InteractsWithProfileImages
{
    /**
     * Upload a profile image against a talent.
     *
     * @param \Illuminate\Http\UploadedFile $file
     * @param array $cropPayload
     *
     * @return void
     */
    public function handleProfileImage($file, $cropPayload) : void
    {
        // If we do not have an uploaded file then we'll assume that we are attempting to recrop
        // an existing profile image.
        if (! $file instanceof UploadedFile) {
            $this->recropExistingProfileImage($cropPayload);
        } else {
            try {
                $this->uploadProfileImage($file, $cropPayload);
            } catch (Exception $exception) {
                report($exception);

                throw ValidationException::withMessages(['profile_image' => 'There was a problem uploading the profile image.']);
            }
        }
    }

    /**
     * Delete the existing profile image.
     *
     * @return void
     */
    protected function deleteExistingProfileImage() : void
    {
        if (! $this->profile_image_hash) {
            return;
        }

        Storage::disk('local')->delete($this->getOriginalProfileImagePath());
        Storage::disk('local')->delete($this->getProfileImagePath());

        $this->profile_image_hash = null;
        $this->profile_image_crop_payload = null;

        $this->save();
    }

    /**
     * Upload and crop a new profile image.
     *
     * @param \Illuminate\Http\UploadedFile $file
     * @param array $cropPayload
     *
     * @return void
     */
    public function uploadProfileImage(UploadedFile $file, $cropPayload) : void
    {
        $image = ImageManager::make($file);

        // Before uploading the new image we will delete the existing one.
        $this->deleteExistingProfileImage();

        Storage::disk('local')->put($file->hashName('uploads/profiles/originals'), (string) $image->encode('jpg'));

        $this->cropProfileImage($image, $file->hashName(), $cropPayload);
    }

    /**
     * Recrop an existing profile image if we have one.
     *
     * @param array $cropPayload
     *
     * @return void
     */
    public function recropExistingProfileImage($cropPayload) : void
    {
        // No point in attempting to recrop if they do not have an existing profile image.
        if (! $this->profile_image_hash) {
            return;
        }

        $image = ImageManager::make(
            Storage::disk('local')->path($this->getOriginalProfileImagePath())
        );

        $this->cropProfileImage($image, $this->profile_image_hash, $cropPayload);
    }

    /**
     * Crop and save the profile image.
     *
     * @param \Intervention\Image\Image $image
     * @param string $hashName
     * @param array $cropPayload
     *
     * @return void
     */
    protected function cropProfileImage(Image $image, $hashName, $cropPayload) : void
    {
        // Prevent weird values coming through from the crop payload. If we get a weird value we'll
        // default it to 0 to avoid any potential errors.
        $cropPayload = array_map(function ($value) {
            $value = intval($value);

            if (! $value) {
                return 0;
            }

            return $value;
        }, $cropPayload);

        // Crop the image then fit it to a 512x512 image which is what we will be storing it as.
        $image->crop($cropPayload['width'], $cropPayload['height'], $cropPayload['x'], $cropPayload['y'])
                ->fit(512, 512);

        Storage::disk('local')->put(sprintf('uploads/profiles/%s', $hashName), (string) $image->encode('jpg'));

        $image->destroy();

        // Store the hash name of the profile image against the talent profile.
        $this->fill([
            'profile_image_hash' => $hashName,
            'profile_image_crop_payload' => $cropPayload
        ])->save();
    }

    /**
     * Get the data URI of a profile image.
     *
     * @return string
     */
    public function getProfileImageDataUri() : ?string
    {
        if ($this->profile_image_hash) {
            $path = sprintf('%s/%s', 'uploads/profiles', $this->profile_image_hash);

            if (Storage::disk('local')->exists($path)) {
                return sprintf('data:image/png;base64,%s', base64_encode(Storage::disk('local')->get($path)));
            }
        }

        return null;
    }

    /**
     * Get the data URI of an original profile image.
     *
     * @return string
     */
    public function getOriginalProfileImageDataUri() : ?string
    {
        $path = sprintf('%s/%s', 'uploads/profiles/originals', $this->profile_image_hash);

        if (Storage::disk('local')->exists($path)) {
            return sprintf('data:image/png;base64,%s', base64_encode(Storage::disk('local')->get($path)));
        }

        return null;
    }

    /**
     * Get the profile image path.
     *
     * @return string|null
     */
    public function getProfileImagePath() : ?string
    {
        if ($this->profile_image_hash) {
            return sprintf('%s/%s', 'uploads/profiles', $this->profile_image_hash);
        }

        return null;
    }

    /**
     * Get the original profile image path.
     *
     * @return string|null
     */
    public function getOriginalProfileImagePath() : ?string
    {
        if ($this->profile_image_hash) {
            return sprintf('%s/%s', 'uploads/profiles/originals', $this->profile_image_hash);
        }

        return null;
    }

    /**
     * Get the original profile image URL.
     *
     * @return string|null
     */
    public function getOriginalProfileImageUrl() : ?string
    {
        if ($this->profile_image_hash) {
            return route('storage', $this->getOriginalProfileImagePath());
        }

        return null;
    }

    /**
     * Get the profile image URL.
     *
     * @return string|null
     */
    public function getProfileImageUrl() : ?string
    {
        if ($this->profile_image_hash) {
            return route('storage', $this->getProfileImagePath());
        }

        return asset('img/profile-default.jpg');
    }

    /**
     * Get the original profile image crop payload as a JSON string.
     *
     * @return string
     */
    public function getOriginalProfileImageCropPayload() : string
    {
        if ($this->profile_image_crop_payload) {
            return json_encode($this->profile_image_crop_payload);
        }

        return json_encode([]);
    }
}

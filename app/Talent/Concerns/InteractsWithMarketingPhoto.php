<?php


namespace Pickstar\Talent\Concerns;

use Exception;
use Intervention\Image\Image;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;
use Intervention\Image\ImageManagerStatic as ImageManager;
use Pickstar\Talent\TalentPhoto;
use Pickstar\User\User;

/**
 * Trait InteractsWithMarketingPhoto
 * @package Pickstar\Talent\Concerns
 */
trait InteractsWithMarketingPhoto
{
    /**
     * Delete marketing photo from storage and delete entry from database
     *
     * @param $photoHash
     * @throws Exception
     */
    public function handleDeleteMarketingPhoto()
    {
        Storage::disk('local')->delete('uploads/marketing_photos/' . $this->photo_hash);
        $this->delete();
    }


    /**
     * Add marketing photo to storage and add entry to database
     *
     * @param UploadedFile $file
     * @param User $user
     * @return mixed
     */
    public static function addMarketingPhoto(UploadedFile $file, User $user)
    {
        // Ensure that talent doesn't already have 3 photos
        if (count($user->profile->photos) < 3) {

            $image = ImageManager::make($file);

            // Change uploaded file extension to 'jpeg' since the image will be encoded in said format
            // It is safe to use explode with '.' delimiter since hashName returns an md5 hash of file content
            $tempFilename = explode('.', $file->hashName());
            $tempFilename[1] = 'jpeg';
            $tempFilename = implode('.', $tempFilename);

            Storage::disk('local')->put('uploads/marketing_photos/' . $tempFilename, (string)$image->encode('jpg'));

            $newPhoto = TalentPhoto::create(['photo_hash' => $tempFilename, 'talent_profile_id' => $user->profile->id]);

            return $newPhoto;
        }

        // Added for development purposes. This should never occur in production.
        if (count($user->profile->photos) > 3) {
            throwException(new Exception('Error: Talent has more than 3 photos'));
        }
    }
}

<?php

namespace Pickstar\Talent;

use Illuminate\Database\Eloquent\Model;
use Pickstar\Talent\Concerns\InteractsWithMarketingPhoto;

/**
 * Class TalentPhoto
 * @package Pickstar\Talent
 */
class TalentPhoto extends Model
{
    use InteractsWithMarketingPhoto;

    /**
     * The attributes that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['talent_profile_id', 'photo_hash'];
}

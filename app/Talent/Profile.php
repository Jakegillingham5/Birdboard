<?php

namespace Pickstar\Talent;

use Carbon\Carbon;
use Pickstar\User\User;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use Concerns\InteractsWithProfileImages;

    /**
     * Table name.
     *
     * @var string
     */
    public $table = 'talent_profile';

    /**
     * Enable timestamps.
     *
     * @var boolean
     */
    public $timestamps = true;

    /**
     * Gender constants.
     *
     * @var string
     */
    const GENDER_MALE = 'male';
    const GENDER_FEMALE = 'female';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'gender',
        'manager',
        'agency',
        'tagline',
        'description',
        'date_featured_until',
        'facebook_link',
        'twitter_link',
        'instagram_link',
        'linkedin_link',
        'facebook_follower_count',
        'twitter_follower_count',
        'instagram_follower_count',
        'profile_image_hash',
        'profile_image_crop_payload',
        'public',
        'manager_email',
        'notification_preference'
    ];

    /**
     * Dynamically append columns.
     *
     * @var array
     */
    protected $appends = [
        'highest_followers',
        'highest_followers_icon'
    ];

    /**
     * Type-casted columns.
     *
     * @var array
     */
    protected $casts = [
        'public' => 'bool',
        'notification_preference' => 'int'
    ];

    /**
     * Date columns.
     *
     * @var array
     */
    protected $dates = [
        'date_featured_until'
    ];

    /**
     * Notification delivery constants.
     *
     * @var int
     */
    const NOTIFICATION_DELIVERY_BOTH = 0;
    const NOTIFICATION_DELIVERY_MANAGER = 1;
    const NOTIFICATION_DELIVERY_TALENT = 2;

    /**
     * Get the social media followers range for a given predefined ID.
     *
     * @param int|string $id
     *
     * @return array
     */
    public static function getSocialRange($id): array
    {
        switch (strval($id)) {
            case 1:
                return [3000, 10000];
                break;
            case 2:
                return [10000, 30000];
                break;
            case 3:
                return [30000, 50000];
                break;
            case 4:
                return [50000, 100000];
                break;
            case 5:
                return [100000, 500000];
                break;
            case 6:
                return [500000];
                break;
        }

        return [];
    }

    /**
     * Talent profile belongs to a user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class)
            ->withTrashed();
    }

    /**
     * Scope only public talent profiles.
     *
     * @param \Illuminate\Database\Query\Builder $query
     *
     * @return void
     */
    public function scopePublic($query)
    {
        $query->where('public', true);
    }

    /**
     * Get the highest followers attribtue.
     *
     * @return integer
     */
    public function getHighestFollowersAttribute()
    {
        return max($this->facebook_follower_count, $this->twitter_follower_count, $this->instagram_follower_count);
    }

    /**
     * Get the highest followers icon attribtue.
     *
     * @return null|string
     */
    public function getHighestFollowersIconAttribute(): ?string
    {
        if ($this->highest_followers_provider === 'facebook') {
            return 'fa-facebook';
        } elseif ($this->highest_followers_provider === 'twitter') {
            return 'fa-twitter';
        } elseif ($this->highest_followers_provider === 'instagram') {
            return 'fa-instagram';
        }

        return null;
    }

    /**
     * Get the highest followers provider attribtue.
     *
     * @return null|string
     */
    public function getHighestFollowersProviderAttribute(): ?string
    {
        $provider = 'facebook';
        $highestCount = $this->facebook_follower_count;

        if ($this->twitter_follower_count > $highestCount) {
            $provider = 'twitter';

            $highestCount = $this->twitter_follower_count;
        }

        if ($this->instagram_follower_count > $highestCount) {
            $provider = 'instagram';
        }

        return $provider;
    }

    /**
     * Mutator for profile image crop payload to store it as a JSON encoded string.
     *
     * @param array|null $value
     *
     * @return void
     */
    public function setProfileImageCropPayloadAttribute($value)
    {
        $this->attributes['profile_image_crop_payload'] = is_array($value)
            ? json_encode($value)
            : $value;
    }

    /**
     * Accessor for profile image crop payload to return it as an array if stored as JSON.
     *
     * @param string|null $value
     *
     * @return array|null
     */
    public function getProfileImageCropPayloadAttribute($value)
    {
        if (($payload = json_decode($value)) === null) {
            return $value;
        }

        return $payload;
    }

    /**
     * Get the profile image URL attribute.
     *
     * @return string
     */
    public function getProfileImageUrlAttribute()
    {
        return $this->getProfileImageUrl();
    }

    /**
     * Determine if profile has a filled in social media link.
     *
     * @return bool
     */
    public function filledSocialMediaLink(): bool
    {
        return ! empty($this->facebook_link) ||
            ! empty($this->twitter_link) ||
            ! empty($this->instagram_link) ||
            ! empty($this->linkedin_link);
    }

    /**
     * Fallback attribute to get a talents commission rate.
     *
     * Commission rate was migrated to the "users" table, this method exists to act as a fallback in
     * case some references to the commission rate on this table are missed.
     *
     * @return int|null
     */
    public function getCommissionRateAttribute(): ?int
    {
        return optional($this->user)->commission_rate;
    }

    /**
     * Update profile from request payload.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function updateFromRequest(Request $request, bool $save = true): void
    {
        $this->fill([
            'gender' => $request->input('profile.gender', $this->gender),
            'manager' => $request->input('profile.manager', $this->manager),
            'agency' => $request->input('profile.agency', $this->agency),
            'manager_email' => $request->input('profile.manager_email', $this->manager_email),
            'tagline' => $request->input('profile.tagline', $this->tagline),
            'facebook_link' => $request->input('profile.facebook_link', $this->facebook_link),
            'twitter_link' => $request->input('profile.twitter_link', $this->twitter_link),
            'instagram_link' => $request->input('profile.instagram_link', $this->instagram_link),
            'linkedin_link' => $request->input('profile.linkedin_link', $this->linkedin_link),
            'description' => $request->input('profile.description', $this->description),
        ]);

        if ($request->user()->isAdmin()) {
            $this->fill([
                'facebook_follower_count' => $request->input('profile.facebook_follower_count', $this->facebook_follower_count),
                'twitter_follower_count' => $request->input('profile.twitter_follower_count', $this->twitter_follower_count),
                'instagram_follower_count' => $request->input('profile.instagram_follower_count', $this->instagram_follower_count),
                'date_featured_until' => $request->input('profile.date_featured_until', $this->date_featured_until),
                'public' => $request->input('profile.public', $this->public),
            ]);
        }

        if ($request->user()->isAdmin() || $request->user()->isManager()) {
            $this->notification_preference = $request->input('profile.notification_preference', $this->notification_preference);
        }

        if ($save) {
            $this->save();
        }
    }

    /**
     * A profile may have at most three photos
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function photos() {
        return $this->hasMany(TalentPhoto::class, 'talent_profile_id');
    }

    /**
     *  Get the talents url
     *
     * @return string
     */
    public function getUrlAttribute()
    {
        return sprintf("/our-stars/%d/%s-%s", $this->user->id, strtolower($this->user->first_name), strtolower($this->user->last_name));
    }
}

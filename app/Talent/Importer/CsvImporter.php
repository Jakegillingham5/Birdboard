<?php

namespace Pickstar\Talent\Importer;

use Exception;
use Pickstar\Tag\Tag;
use League\Csv\Reader;
use Pickstar\User\User;
use League\Csv\ResultSet;
use League\Csv\Statement;
use Pickstar\Location\Location;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Database\Connection;
use Pickstar\Opportunity\Opportunity;
use Intervention\Image\ImageManagerStatic;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;

class CsvImporter
{
    /**
     * Filesystem adapter instance.
     *
     * @var \Illuminate\Filesystem\FilesystemAdapter
     */
    protected $files;

    /**
     * Database connection instance.
     *
     * @var \Illuminate\Database\Connection
     */
    protected $db;

    /**
     * CSV reader instance.
     *
     * @var \League\Csv\Reader
     */
    protected $reader;

    /**
     * CSV record statement.
     *
     * @var \League\Csv\Statement
     */
    protected $records;

    /**
     * Opportunities collection.
     *
     * @var \Illuminate\Database\Eloquent\Collection
     */
    protected $opportunities;

    /**
     * Create a new CSV importer instance.
     *
     * @param \Illuminate\Filesystem\FilesystemAdapter $files
     * @param \Illuminate\Database\Connection $db
     */
    public function __construct(FilesystemAdapter $files, Connection $db)
    {
        $this->files = $files;
        $this->db = $db;
        $this->reader = Reader::createFromPath($files->path('csv/talent.csv'));
    }

    /**
     * Get the CSV records from the CSV reader.
     *
     * @return \League\Csv\ResultSet
     */
    protected function getReaderRecords(): ResultSet
    {
        if ($this->records) {
            return $this->records;
        }

        $this->reader->setHeaderOffset(0);

        return $this->records = (new Statement)->process($this->reader);
    }

    /**
     * Read talent from the CSV.
     *
     * @return \Illumiante\Support\Collection
     */
    public function readTalentFromCsv(): Collection
    {
        $talent = collect([]);

        foreach ($this->getReaderRecords() as $index => $record) {
            $talent->push(new Talent($record, $index));
        }

        return $talent;
    }

    /**
     * Write a talent to the database. Returns the new user instance.
     *
     * @param \Pickstar\Talent\Importer\Talent $talent
     *
     * @return \Pickstar\User\User
     */
    public function writeTalentToDatabase(Talent $talent): User
    {
        $this->db->beginTransaction();

        try {
            $user = User::create([
                'first_name' => $talent->first_name,
                'last_name' => $talent->last_name,
                'role' => User::ROLE_TALENT,
                'email' => $talent->email,
                'phone' => $talent->mobile,
                'password' => null,
                'status' => User::STATUS_ACTIVE
            ]);

            $user->profile()->create([
                'tagline' => $talent->tagline,
                'gender' => $talent->gender,
                'description' => $talent->description,
                'twitter_link' => $talent->twitter_link,
                'facebook_link' => $talent->facebook_link,
                'instagram_link' => $talent->instagram_link,
                'twitter_follower_count' => str_replace(',', '', $talent->twitter_follower_count),
                'facebook_follower_count' => str_replace(',', '', $talent->facebook_follower_count),
                'instagram_follower_count' => str_replace(',', '', $talent->instagram_follower_count)
            ]);

            $this->associateStatesWithTalent($user, $talent->state);
            $this->associateTagsWithTalent($user, 'sport', $talent->sports_played);
            $this->associateTagsWithTalent($user, 'club', $talent->club_event);
            $this->associateAllOpportunitiesWithTalent($user);

            $this->db->commit();
        } catch (Exception $exception) {
            report($exception);

            $this->db->rollBack();

            throw $exception;
        }

        // Process the talents profile image outside of the transaction. This is because we want to make sure
        // everything else has passed before attempting to upload the profile image.
        $this->processTalentProfileImage($user, $talent);

        return $user;
    }

    /**
     * Associate states (locations) with a user.
     *
     * Will not create states. States must the proper shortname, e.g., VIC or SA.
     *
     * @param \Pickstar\User\User $user
     * @param array $states
     *
     * @return void
     */
    protected function associateStatesWithTalent(User $user, array $states)
    {
        $user->locations()->sync(
            Location::whereIn('shortname', $states)->pluck('id')
        );
    }

    /**
     * Associate tags of a given tag type with a user.
     *
     * The tag type will be one of the tag types defined on the tag model.
     *
     * This creates non-existent tags but does not check if a tag with a similar name (pluralized/spelling) exists.
     *
     * @param \Pickstar\User\User $user
     * @param string $tagType
     * @param array $tags
     *
     * @return void
     */
    protected function associateTagsWithTalent(User $user, string $tagType, array $tags): void
    {
        collect($tags)
            ->diff(Tag::{$tagType}()->whereIn('name', $tags)->pluck('name'))
            ->each(function ($tag) use ($tagType) {
                if ($tag === null || $tag === '') {
                    return;
                }

                Tag::firstOrCreate(['type' => $tagType, 'name' => $tag]);
            });

        $user->tags()->{$tagType}()->syncWithoutDetaching(Tag::{$tagType}()->whereIn('name', $tags)->pluck('id'));
    }

    /**
     * Associate all opportunities with a user.
     *
     * @param \Pickstar\User\User $user
     *
     * @return void
     */
    protected function associateAllOpportunitiesWithTalent(User $user): void
    {
        $user->opportunities()->sync($this->getOpportunities()->pluck('id'));
    }

    /**
     * Get all opportunities.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    protected function getOpportunities(): EloquentCollection
    {
        if ($this->opportunities) {
            return $this->opportunities;
        }

        return $this->opportunities = Opportunity::all();
    }

    /**
     * Process a talents profile image.
     *
     * This attempts a crop of the biggest possible space in the center of the image. Uses the existing profile
     * image handler and simply "fakes" an uploaded file that it can handle.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Talent\Importer\Talent $talent
     *
     * @return void
     */
    protected function processTalentProfileImage(User $user, Talent $talent): void
    {
        if (! $talent->profile_image_name) {
            return;
        }

        $path = sprintf('csv/images/%s', $talent->profile_image_name);

        if ($this->files->exists($path)) {
            $image = ImageManagerStatic::make($this->files->get($path));

            // Determine the crop size, we'll take as much as we can from the center of the image.
            // If width is greater than height we'll use the height, otherwise use the width.
            $width = $height = null;

            if ($image->width() > $image->height()) {
                $width = $height = $image->height();
            } else {
                $width = $height = $image->width();
            }

            $x = ($image->width() / 2) - ($width / 2);
            $y = ($image->height() / 2) - ($height / 2);

            $user->profile->handleProfileImage(
                new UploadedFile($this->files->path($path), $talent->profile_image_name, $image->mime),
                compact('width', 'height', 'x', 'y')
            );
        }
    }
}

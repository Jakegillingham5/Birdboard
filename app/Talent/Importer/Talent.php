<?php

namespace Pickstar\Talent\Importer;

use Pickstar\Talent\Profile;
use InvalidArgumentException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class Talent
{
    /**
     * Talent attributes.
     *
     * @var array
     */
    protected $attributes = [];

    /**
     * Talents CSV row index.
     *
     * @var int
     */
    protected $index;

    /**
     * Create a talent instance.
     *
     * @param array $talent
     * @param int $index
     */
    public function __construct(array $talent, int $index)
    {
        $this->hydrate($talent);

        $this->index = $index;
    }

    /**
     * Hydrate the talent by setting all the attributes.
     *
     * @param array $talent
     *
     * @return void
     */
    protected function hydrate(array $talent) : void
    {
        foreach ($talent as $key => $value) {
            $this->{$key} = $value;
        }
    }

    /**
     * Get the talents CSV row index.
     *
     * @return int
     */
    public function getIndex() : int
    {
        return $this->index;
    }

    /**
     * Validate a talent meets the necessary requirements to create them.
     *
     * @return bool
     */
    public function validate() : bool
    {
        try {
            Validator::validate($this->attributes, [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|email|unique:users'
            ]);

            return true;
        } catch (ValidationException $exception) {
            return false;
        }
    }

    /**
     * Mutate mobile by making sure it's of the correct format.
     *
     * @param string $value
     *
     * @return void
     */
    public function setMobileAttribute($value)
    {
        $value = str_replace(' ', '', $value);

        if ($value === '') {
            $this->attributes['mobile'] = null;

            return;
        }

        // Determine if the number starts with "0". If it does, and it's 10 characters long, then the
        // mobile is assumed to be correct.
        if (starts_with($value, '0') && strlen($value) === 10) {
            $this->attributes['mobile'] = $value;

            return;

        // If the number starts with "+61" then we'll strip the first characters from the number and
        // then proceed to prefixing the number with "0".
        } elseif (starts_with($value, '+61')) {
            $value = substr($value, 3);
        }

        $this->attributes['mobile'] = sprintf('0%s', $value);
    }

    /**
     * Mutate sports played by exploding on pipe.
     *
     * @param string $value
     *
     * @return void
     */
    public function setSportsPlayedAttribute($value)
    {
        $this->attributes['sports_played'] = explode('|', $value);
    }

    /**
     * Mutate club event by exploding on pipe.
     *
     * @param string $value
     *
     * @return void
     */
    public function setClubEventAttribute($value)
    {
        $this->attributes['club_event'] = explode('|', $value);
    }

    /**
     * Mutate state by exploding on pipe.
     *
     * @param string $value
     *
     * @return void
     */
    public function setStateAttribute($value)
    {
        $this->attributes['state'] = explode('|', $value);
    }

    /**
     * Set first name by trimming extra whitespace.
     *
     * @param string $value
     *
     * @return void
     */
    public function setFirstNameAttribute($value)
    {
        $this->attributes['first_name'] = trim($value);
    }

    /**
     * Set last name by trimming extra whitespace.
     *
     * @param string $value
     *
     * @return void
     */
    public function setLastNameAttribute($value)
    {
        $this->attributes['last_name'] = trim($value);
    }

    /**
     * Mutate gender by getting the model friendly version.
     *
     * @param string $value
     *
     * @return void
     */
    public function setGenderAttribute($value)
    {
        $value = strtolower($value);

        $genders = [
            'm' => Profile::GENDER_MALE,
            'f' => Profile::GENDER_FEMALE
        ];

        if (! isset($genders[$value])) {
            $this->attributes['gender'] = $value;
        } else {
            $this->attributes['gender'] = $genders[$value];
        }
    }

    /**
     * Dynamically set attributes on the talent.
     *
     * @param string $key
     * @param mixed $value
     *
     * @return void
     */
    public function __set($key, $value)
    {
        if (method_exists($this, 'set'.studly_case($key).'Attribute')) {
            $this->{'set'.studly_case(trim($key)).'Attribute'}($value);
        } else {
            $this->attributes[$key] = trim($value);
        }
    }

    /**
     * Dynamically get attributes from the talent.
     *
     * @param string $key
     *
     * @return void
     */
    public function __get($key)
    {
        if (! array_key_exists($key, $this->attributes)) {
            throw new InvalidArgumentException('Talent attribute ['.$key.'] does not exist.');
        }

        return $this->attributes[$key];
    }
}

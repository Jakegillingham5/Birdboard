<?php
/**
 * Created by PhpStorm.
 * User: jpickstar
 * Date: 2019-05-21
 * Time: 10:38
 */

namespace Pickstar\Xero;

use XeroPHP\Application\PrivateApplication;

/**
 * Class Client
 * @package Pickstar\Xero
 */
class Client
{
    /**
     * @var PrivateApplication
     */
    protected static $_client;

    /**
     * Client constructor.
     */
    public function __construct () {
        $config = [
            'oauth' => [
                'consumer_key'    => env('XERO_CONSUMER_KEY'),
                'consumer_secret' => env('XERO_CONSUMER_SECRET'),
                'rsa_private_key' => self::getXeroCertContent(),
                'callback'        => 'thingsnstuff'
            ],
        ];

        static::$_client = new PrivateApplication($config);
    }

   public function xero()
   {
       return static::$_client;
   }

   protected static function getXeroCertContent()
   {
       return str_replace('\n',PHP_EOL,env('XERO_CERT_CONTENT'));
   }


}
<?php

namespace Pickstar\Model;

use Pickstar\User\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Venturecraft\Revisionable\Revision as BaseRevision;

class Revision extends BaseRevision
{
    /**
     * Revision has a responsible user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function responsible(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Get the table name that was affected.
     *
     * @return string
     */
    public function tableAffected(): string
    {
        return (new $this->revisionable_type)->getTable();
    }


    /**
     * Field Name
     *
     * Returns the field that was updated, in the case that it's a foreign key
     * denoted by a suffix of "_id", then "_id" is simply stripped
     *
     * @return string field
     */
    public function fieldName()
    {
        if ($formatted = $this->formatFieldName($this->key)) {
            return $formatted;
        } elseif (strpos($this->key, '_id')) {
            return str_replace('_id', '', $this->key);
        } else {
            return $this->key;
        }
    }

    /**
     * Format field name.
     *
     * Allow overrides for field names.
     *
     * @param $key
     *
     * @return bool
     */
    private function formatFieldName($key)
    {
        $related_model = $this->revisionable_type;
        $related_model = new $related_model;
        $revisionFormattedFieldNames = $related_model->revision_formatted_field_names;
        if (isset($revisionFormattedFieldNames[$key])) {
            return $revisionFormattedFieldNames[$key];
        }
        return false;
    }
}

<?php

namespace Pickstar\Model\Concerns;

use Pickstar\Model\Revision;
use Illuminate\Database\Eloquent\Relations\MorphMany;

trait Revisionable
{
    /**
     * Create a nicer named revisions relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function revisions(): MorphMany
    {
        return $this->morphMany(Revision::class, 'revisionable');
    }
}

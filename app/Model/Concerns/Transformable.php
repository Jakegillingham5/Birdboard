<?php

namespace Pickstar\Model\Concerns;

trait Transformable
{
    /**
     * Call a transformer callable with the model instance.
     *
     * @param callable $callback
     *
     * @return array
     */
    public function transform(callable $callback, array $parameters = []) : array
    {
        return call_user_func_array($callback, array_merge([$this], $parameters));
    }
}

<?php

namespace Pickstar\Setting;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    /**
     * Cached settings instance.
     *
     * @var \Pickstar\Setting\Setting
     */
    protected static $settingsCache = null;

    /**
     * Static array of settings.
     *
     * @var array
     */
    protected static $settings = [
        'commission_rate',
        'deposit_payment',
        'notification_email',
        'notification_phone',
        'billboard_post_length',
        'deposit_payment_warning_admin',
        'deposit_payment_warning_client',
        'final_payment_warning_admin',
        'urgent_payment_warning_admin',
        'final_payment_warning_client',
        'initial_talent_reminder',
        'second_talent_reminder',
        'final_talent_reminder'
    ];

    /**
     * Set the fillable columns on construct.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->fillable = static::$settings;

        parent::__construct($attributes);
    }

    /**
     * Table name.
     *
     * @var string
     */
    public $table = 'settings';

    /**
     * Get the setting keys.
     *
     * @return array
     */
    public static function getSettingKeys(): array
    {
        return static::$settings;
    }

    /**
     * Get a setting from the settings table.
     *
     * @param string $setting
     *
     * @return mixed
     */
    public static function getSetting($setting)
    {
        if (! static::$settingsCache) {
            static::$settingsCache = static::first();
        }

        return static::$settingsCache->{$setting};
    }

    /**
     * Set a setting on the settings instance.
     *
     * Does not save to the database.
     *
     * @param string $setting
     *
     * @return mixed
     */
    public static function setSetting($setting, $value)
    {
        if (! static::$settingsCache) {
            static::$settingsCache = static::first();
        }

        return static::$settingsCache->{$setting} = $value;
    }

    /**
     * Dynamically handle calls to individual settings.
     *
     * @param string $method
     * @param mixed $parameters
     *
     * @return mixed
     */
    public static function __callStatic($method, $parameters)
    {
        if (array_search($setting = Str::snake($method), static::$settings) !== false) {
            if (count($parameters) === 1) {
                return static::setSetting($setting, $parameters[0]);
            }

            return static::getSetting($setting);
        }

        return parent::__callStatic($method, $parameters);
    }
}

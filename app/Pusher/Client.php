<?php

namespace Pickstar\Pusher;

use Pusher\Pusher;

/**
 * Class Client
 * @package Pickstar\Pusher
 */
class Client
{
    /**
     * @var \GuzzleHttp\Client
     */
    public static $_client;

    /**
     * Client constructor.
     *
     * @throws \Pusher\PusherException
     */
    public function __construct () {
        static::$_client = new Pusher(
            config('broadcasting.connections.pusher.key'),
            config('broadcasting.connections.pusher.secret'),
            config('broadcasting.connections.pusher.app_id'),
            ['cluster' => config('broadcasting.connections.pusher.options.cluster')]
        );
    }

    /**
     * @param $channel_name
     * @return mixed
     */
    public static function getChannelInfo ($channel_name) {
        return static::$_client->get_channel_info($channel_name);
    }
}

<?php

namespace Pickstar\Policies;

use Pickstar\User\User;

class UserPolicy
{
    use Concerns\HandlesAuthorization;

    /**
     * Actions that fallthru past the before.
     *
     * @var array
     */
    protected $fallthru = [
        'deleteAdmin'
    ];

    /**
     * Before running the policy action.
     *
     * @param \Pickstar\User\User $user
     *
     * @return mixed
     */
    public function before(User $user, $action)
    {
        if ($user->isAdmin() && !in_array($action, $this->fallthru)) {
            return true;
        }
    }

    /**
     * Policy for viewing users list.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\User\User $viewing
     *
     * @return bool
     */
    public function index(User $user): bool
    {
        return false;
    }

    /**
     * Policy for listing talent.
     *
     * @param \Pickstar\User\User $user
     *
     * @return bool
     */
    public function listTalent(User $user): bool
    {
        if ($user->isManager()) {
            return true;
        }

        return false;
    }

    /**
     * Policy for viewing a user.
     *
     * This is different from "access" as this is for generic information that should be public.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\User\User $viewing
     *
     * @return bool
     */
    public function view(User $authenticated, User $user): bool
    {
        if ($authenticated->id === $user->id) {
            return true;
        }

        if ($authenticated->isManager()) {
            return $authenticated->isParentOf($user) ||
                $user->isManagedBy($authenticated);
        }

        return false;
    }

    /**
     * Policy for managing sensitive user information.
     *
     * @param \Pickstar\User\User $authenticated
     * @param \Pickstar\User\User $user
     *
     * @return bool
     */
    public function manage(User $authenticated, User $user): bool
    {
        if ($authenticated->id === $user->id) {
            return true;
        }

        if ($authenticated->isManager()) {
            return $authenticated->isParentOf($user) ||
                $user->isManagedBy($authenticated);
        }

        return false;
    }

    /**
     * User can update themselves.
     *
     * @param User $user
     * @param User $updating
     *
     * @return bool
     */
    public function update(User $authenticated, User $user)
    {
        if ($authenticated->id === $user->id) {
            return true;
        }

        if ($authenticated->isManager()) {
            return $authenticated->isParentOf($user) ||
                $user->isManagedBy($authenticated);
        }

        return false;
    }

    /**
     * Policy for verifying a user.
     *
     * @param User $user
     * @param User $verifying
     *
     * @return bool
     */
    public function verify(User $user, User $verifying)
    {
        return false;
    }

    /**
     * Policy for resending user verification.
     *
     * @param User $user
     * @param User $resending
     *
     * @return bool
     */
    public function resendVerification(User $user, User $resending)
    {
        return $user->id === $resending->id;
    }

    /**
     * Policy for activating a user.
     *
     * @param User $user
     * @param User $updating
     *
     * @return bool
     */
    public function activate(User $user, User $activating)
    {
        return $user->isManager() && $user->isParentOf($activating);
    }

    /**
     * Policy for deactivating a user.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\User\User $deactivating
     *
     * @return bool
     */
    public function deactivate(User $user, User $deactivating)
    {
        return $user->isManager() && $user->isParentOf($deactivating);
    }

    /**
     * Policy for deleting a user.
     *
     * @param User $user
     * @param User $updating
     *
     * @return bool
     */
    public function delete(User $user, User $deleting)
    {
        if ($user->id === $deleting->id) {
            return true;
        }

        if ($user->isManager() && $user->isParentOf($deleting)) {
            return true;
        }

        return false;
    }

    /**
     * Policy for deleting a user.
     *
     * @param User $user
     * @param User $updating
     *
     * @return bool
     */
    public function deleteAdmin(User $user)
    {
        if ($user->email === system_setting('notification_email')) {
            return true;
        }

        return false;
    }

    /**
     * Policy for creating an admin.
     *
     * @param User $user
     *
     * @return bool
     */
    public function createAdmin(User $user)
    {
        return false;
    }

    /**
     * Policy for listing admins.
     *
     * @param User $user
     *
     * @return bool
     */
    public function listAdmins(User $user)
    {
        return false;
    }

    /**
     * Policy for permanently deleting a user.
     *
     * @param User $user
     * @param User $updating
     *
     * @return bool
     */
    public function permanentlyDelete(User $user, User $deleting)
    {
        return false;
    }

    /**
     * Policy for restoring a user.
     *
     * @param User $user
     * @param User $updating
     *
     * @return bool
     */
    public function restore(User $user, User $restoring)
    {
        if ($user->isManager() && $user->isParentOf($restoring)) {
            return true;
        }

        return false;
    }

    /**
     * Policy for creating staff managers.
     *
     * @param \Pickstar\User\User $user
     *
     * @return bool
     */
    public function createStaffManager(User $authenticated, User $user): bool
    {
        return $authenticated->id === $user->id;
    }

    /**
     * Policy for creating managers.
     *
     * @param \Pickstar\User\User $authenticated
     *
     * @return bool
     */
    public function createManager(User $authenticated): bool
    {
        return false;
    }

    /**
     * Policy for updating talent management.
     *
     * @param \Pickstar\User\User $authenticated
     * @param \Pickstar\User\User $user
     *
     * @return bool
     */
    public function updateManagement(User $authenticated, User $user): bool
    {
        return false;
    }

    /**
     * Policy for updating talent manager.
     *
     * @param \Pickstar\User\User $authenticated
     * @param \Pickstar\User\User $user
     *
     * @return bool
     */
    public function updateManager(User $authenticated, User $user): bool
    {
        return $authenticated->isMasterManager() &&
            $user->isManagedBy($authenticated);
    }

    /**
     * Policy for creating talent.
     *
     * @param \Pickstar\User\User $authenticated
     *
     * @return bool
     */
    public function createTalent(User $authenticated): bool
    {
        return $authenticated->isMasterManager();
    }

    /**
     * Policy for listing a talent's bookings
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function talentBookings(User $user): bool
    {
        return ($user->isAdmin() || $user->isTalent());
    }

    /**
     * Policy for notifying a talent they have been paid
     *
     * @param \Pickstar\User\User $user
     *
     * @return bool
     */
    public function notifyTalentPaid(User $user): bool
    {
        return false;
    }
}

<?php

namespace Pickstar\Policies\Concerns;

use Symfony\Component\HttpKernel\Exception\HttpException;

trait HandlesAuthorization
{
    /**
     * Throws an unauthorized exception.
     *
     * @param string $message
     * @return void
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    protected function deny($message = 'This action is unauthorized.')
    {
        throw new HttpException(403, $message);
    }
}

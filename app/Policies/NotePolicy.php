<?php

namespace Pickstar\Policies;

use Pickstar\User\User;
use Pickstar\Policies\Concerns\HandlesAuthorization;

class NotePolicy
{
  /**
   * Policy for storing a note against a booking.
   *
   * @param \Pickstar\User\User $user
   *
   * @return bool
   */
  public function getNotes(User $user)
  {
    return $user->isAdmin();
  }

  /**
   * Policy for storing a note against a booking.
   *
   * @param \Pickstar\User\User $user
   *
   * @return bool
   */
  public function storeNote(User $user)
  {
    return $user->isAdmin();
  }

  /**
   * Policy for deleting a note against a booking.
   *
   * @param \Pickstar\User\User $user
   *
   * @return bool
   */
  public function deleteNote(User $user)
  {
    return $user->isAdmin();
  }

}

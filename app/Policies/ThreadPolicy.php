<?php

namespace Pickstar\Policies;

use Pickstar\Message\Thread;
use Pickstar\User\User;
use Pickstar\Policies\Concerns\HandlesAuthorization;

class ThreadPolicy
{
    /**
     * Actions that fallthru past the before.
     *
     * @var array
     */
    protected $fallthru = [];

    /**
     * Before running the policy action.
     *
     * @param \Pickstar\User\User $user
     * @param string $action
     *
     * @return bool|void
     */
    public function before(User $user, $action)
    {
        if ($user->isAdmin() && ! in_array($action, $this->fallthru)) {
            return true;
        }

        if ($user->isManager() && $user->isPendingActivation()) {
            return false;
        }
    }

    /**
     * Policy for getting threads
     *
     * @param \Pickstar\User\User $user
     *
     * @return bool
     */
    public function getThreads(User $user)
    {
        return true;
    }

    /**
     * Policy for getting threads
     *
     * @param \Pickstar\User\User $user
     *
     * @return bool
     * @throws \Exception
     */
    public function getThread(User $user, Thread $thread)
    {
        if ($user->isManager()) {
            foreach($user->managedTalent as $talent) {
                if ($thread->participants->pluck('id')->contains($talent->id)) {
                    $user->unsetRelation('managedTalent');
                    return true;
                }
            }
        }

        if ($thread->participants->pluck('id')->contains($user->id)) {
            return true;
        }
        return false;
    }

    /**
     * Policy for sending a message on a thread
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Message\Thread thread
     *
     * @return bool
     * @throws \Exception
     */
    public function sendMessage(User $user, Thread $thread)
    {
        if (!$thread->active) {
            return false;
        }
        if ($user->isManager()) {
            foreach($user->managedTalent as $talent) {
                if ($thread->participants->pluck('id')->contains($talent->id)) {
                    $user->unsetRelation('managedTalent');
                    return true;
                }
            }
        }

        if ($thread->participants->pluck('id')->contains($user->id)) {
            return true;
        }
        return false;
    }

    /**
     * Policy for deleting a message (soft)
     *
     * @param User $user
     * @return bool
     */
    public function deleteMessage(User $user)
    {
        return false;
    }

    /**
     * Policy for restoring a message
     *
     * @param User $user
     * @return bool
     */
    public function restoreMessage(User $user)
    {
        return false;
    }

    /**
     * Policy for reading messages on a thread
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Message\Thread thread
     *
     * @return bool
     */
    public function readMessages(User $user, Thread $thread)
    {
        if ($thread->participants->pluck('id')->contains($user->id)) {
            return true;
        }
        return false;
    }

        /**
     * Policy for closing a thread
     *
     * @param User $user
     * @return bool
     */
    public function closeThread(User $user)
    {
        return false;
    }

    /**
     * Policy for reopening a thread
     *
     * @param User $user
     * @return bool
     */
    public function reopenThread(User $user)
    {
        return false;
    }

    /**
     * Policy for pinning a thread
     *
     * @param User $user
     * @return bool
     */
    public function pinThread(User $user)
    {
        return false;
    }

    /**
     * Policy for unpinning a thread
     *
     * @param User $user
     * @return bool
     */
    public function unpinThread(User $user)
    {
        return false;
    }

    /**
     * Policy for activating a thread
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Message\Thread thread
     *
     * @return bool
     */
    public function activateThread(User $user, Thread $thread)
    {
        if ($thread->participants->pluck('id')->contains($user->id)) {
            return true;
        }
        return false;
    }

    /**
     * Policy for deactivating a thread
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Message\Thread thread
     *
     * @return bool
     */
    public function deactivateThread(User $user, Thread $thread)
    {
        if ($thread->participants->pluck('id')->contains($user->id)) {
            return true;
        }
        return false;
    }
}

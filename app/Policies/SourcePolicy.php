<?php

namespace Pickstar\Policies;

use Pickstar\User\User;
use Pickstar\Policies\Concerns\HandlesAuthorization;

class SourcePolicy
{
    /**
     * Policy for getting a source
     *
     * @param \Pickstar\User\User $user
     *
     * @return bool
     */
    public function getSource(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Policy for updating a source
     *
     * @param \Pickstar\User\User $user
     *
     * @return bool
     */
    public function updateSource(User $user)
    {
        return $user->isAdmin();
    }
}

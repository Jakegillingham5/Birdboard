<?php

namespace Pickstar\Policies;

use Pickstar\User\User;
use Pickstar\Message\SystemNotification;
use Pickstar\Policies\Concerns\HandlesAuthorization;

class SystemNotificationPolicy
{
    /**
     * Actions that fallthru past the before.
     *
     * @var array
     */
    protected $fallthru = [];

    /**
     * Before running the policy action.
     *
     * @param \Pickstar\User\User $user
     * @param string $action
     *
     * @return bool|void
     */
    public function before(User $user, $action)
    {
        if ($user->isAdmin() && ! in_array($action, $this->fallthru)) {
            return true;
        }

        if ($user->isManager() && $user->isPendingActivation()) {
            return false;
        }
    }

    /**
     * Policy for getting notifications
     *
     * @param \Pickstar\User\User $user
     *
     * @return bool
     */
    public function getSystemNotifications(User $user)
    {
        return true;
    }

    /**
     * Policy for reading notifications
     *
     * @param \Pickstar\User\User $user
     *
     * @return bool
     */
    public function readSystemNotifications(User $user)
    {
        return true;
    }
}

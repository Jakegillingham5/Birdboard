<?php

namespace Pickstar\Policies;

use Pickstar\User\User;
use Pickstar\Talent\Endorsement;

class EndorsementPolicy
{
    /**
     * Before running the policy action.
     *
     * @param \Pickstar\User\User $user
     *
     * @return mixed
     */
    public function before(User $user)
    {
        if ($user->isAdmin()) {
            return true;
        }
    }

    /**
     * Policy for viewing an endorsements.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Endorsement\Endorsement $endorsement
     *
     * @return bool
     */
    public function list(User $user)
    {
        return false;
    }

    /**
     * Policy for creating an endorsement.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Endorsement\Endorsement $endorsement
     *
     * @return bool
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Policy for updating an endorsement.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Endorsement\Endorsement $endorsement
     *
     * @return bool
     */
    public function update(User $user, Endorsement $endorsement)
    {
        return false;
    }

    /**
     * Policy for deleting an endorsement.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Endorsement\Endorsement $endorsement
     *
     * @return bool
     */
    public function delete(User $user, Endorsement $endorsement)
    {
        return false;
    }
}

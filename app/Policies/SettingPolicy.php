<?php

namespace Pickstar\Policies;

use Pickstar\User\User;

class SettingPolicy
{
    /**
     * User can update the settings.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Setting\Setting $setting
     *
     * @return bool
     */
    public function update(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * User can view the settings.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Setting\Setting $setting
     *
     * @return bool
     */
    public function index(User $user)
    {
        return $user->isAdmin();
    }
}

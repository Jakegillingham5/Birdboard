<?php

namespace Pickstar\Policies;

use Pickstar\Tag\Tag;
use Pickstar\User\User;

class TagPolicy
{
    use Concerns\HandlesAuthorization;

    /**
     * Actions that fallthru past the before.
     *
     * @var array
     */
    protected $fallthru = [];

    /**
     * Before running the policy action.
     *
     * @param \Pickstar\User\User $user
     *
     * @return mixed
     */
    public function before(User $user, $action)
    {
        if ($user->isAdmin() && ! in_array($action, $this->fallthru)) {
            return true;
        }
    }

    /**
     * Policy for deleting a tag.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Tag\Tag $tag
     *
     * @return bool
     */
    public function delete(User $user, Tag $tag)
    {
        return false;
    }

    /**
     * Policy for updating a tag.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Tag\Tag $tag
     *
     * @return bool
     */
    public function update(User $user, Tag $tag)
    {
        return false;
    }

    /**
     * Policy for merging tags.
     *
     * @param \Pickstar\User\User $user
     *
     * @return bool
     */
    public function merge(User $user)
    {
        return false;
    }

    /**
     * Policy for associating tags.
     *
     * @param \Pickstar\User\User $user
     *
     * @return bool
     */
    public function associate(User $user)
    {
        return false;
    }

    /**
     * Policy for showing tags.
     *
     * @param \Pickstar\User\User $user
     *
     * @return bool
     */
    public function show(User $user)
    {
        return false;
    }

    /**
     * Policy for removing talent from tags.
     *
     * @param \Pickstar\User\User $user
     *
     * @return bool
     */
    public function removeTalent(User $user)
    {
        return false;
    }
}

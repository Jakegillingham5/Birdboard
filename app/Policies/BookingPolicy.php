<?php

namespace Pickstar\Policies;

use Pickstar\User\User;
use Pickstar\Booking\Booking;
use Pickstar\Booking\TalentPivot;
use Pickstar\Policies\Concerns\HandlesAuthorization;

class BookingPolicy
{
    use HandlesAuthorization;

    /**
     * Actions that fallthru past the before.
     *
     * @var array
     */
    protected $fallthru = [
        'applyRequest',
        'declineRequest'
    ];

    /**
     * Before running the policy action.
     *
     * @param \Pickstar\User\User $user
     * @param string $action
     *
     * @return bool|void
     */
    public function before(User $user, $action)
    {
        if ($user->isAdmin() && ! in_array($action, $this->fallthru)) {
            return true;
        }

        if ($user->isManager() && $user->isPendingActivation()) {
            return false;
        }
    }

    /**
     * Policy for listing the billboard bookings.
     *
     * @param \Pickstar\User\User $user
     * @param string $type
     *
     * @return bool
     */
    public function listBillboard(User $user): bool
    {
        return $user->isManager() ||
            $user->isTalent();
    }

    /**
     * Policy for listing bookings.
     *
     * @param \Pickstar\User\User $user
     *
     * @return bool
     */
    public function list(User $user): bool
    {
        return true;
    }

    /**
     * Policy for creating a new booking.
     *
     * @param \Pickstar\User\User $user
     * @param string $type
     *
     * @return bool
     */
    public function create(User $user): bool
    {
        return $user->isClient();
    }

    /**
     * Policy for viewing a booking.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function view(User $user, Booking $booking): bool
    {
        if ($user->isClient()) {
            return $user->id === $booking->client_id;
        }

        return $booking->talent()
            // If user is a manager then only let them see the request if one of their
            // managed talent has been shortlisted or added to it.
            ->when($user->isManager(), function ($query) use ($user) {
                $query->managedBy($user)
                    ->haveBeenAccepted();
            })

            // If user is a talent then only let them see the request if they have been
            // shortlisted or added to it.
            ->when($user->isTalent(), function ($query) use ($user) {
                $query->haveBeenAccepted($user);
            })
            ->exists();
    }

    /**
     * Policy for a seeing a the bookings client.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function seeClient(User $user, Booking $booking): bool
    {
        if ($user->isAdmin() || ($user->isClient() && $user->id === $booking->client_id)) {
            return true;
        }

        // Talent/managers can only see sensitive data if on the booking and have been
        // accepted by the client.
        // They also cannot see the information if the booking is cancelled or completed
        if (($user->isTalent() || $user->isManager()) && $booking->relationLoaded('talent')) {
            return $booking->talent->contains(function ($talent) use ($user, $booking) {
                return ($talent->is($user) || $talent->managedBy($user)) &&
                    $talent->response->client_accepted === true &&
                    $booking->status !== Booking::STATUS_COMPLETED &&
                    $booking->status !== Booking::STATUS_CANCELLED;
            });
        }

        return false;
    }

    /**
     * Policy for seeing a bookings payments.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function seePayments(User $user, Booking $booking): bool
    {
        return $user->isAdmin() || ($user->isClient() && $user->id === $booking->client_id);
    }

    /**
     * Policy for seeing a bookings threads.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function seeThreads(User $user, Booking $booking): bool
    {
        return true;
    }

    /**
     * Policy for generating a booking PDF.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function pdf(User $user, Booking $booking): bool
    {
        return $user->id === $booking->client_id || $user->isTalent() || $user->isManager();
    }

    /**
     * Policy for viewing booking history.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function history(User $user, Booking $booking): bool
    {
        return false;
    }

    /**
     * Policy for confirming attendance for a booking.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function confirmAttendance(User $user, Booking $booking): bool
    {
        return $user->isTalent() || $user->isAdmin();
    }

    /**
     * Policy for marking an in_person event as complete.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function completeInPerson(User $user, Booking $booking): bool
    {
        return $user->isTalent() &&
            $user->talentBookings()
                ->where('bookings.id', $booking->id)
                ->wherePivot('status', TalentPivot::STATUS_APPLIED)
                ->wherePivot('client_accepted', true)
                ->wherePivot('attendance_confirmed', true)
                ->exists();
    }

    /**
     * Policy for marking an online event as complete.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function completeOnline(User $user, Booking $booking): bool
    {
        return $user->isTalent() &&
            $user->talentBookings()
                ->where('bookings.id', $booking->id)
                ->wherePivot('status', TalentPivot::STATUS_APPLIED)
                ->wherePivot('client_accepted', true)
                ->wherePivot('online_participation_confirmed', true)
                ->exists();
    }

    /**
     * Policy for accepting booking terms.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function acceptTerms(User $user, Booking $booking): bool
    {
        return $user->isClient() &&
            $user->id === $booking->client_id;
    }

    /**
     * Policy for updating a booking.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function update(User $user, Booking $booking): bool
    {
        return false;
    }

    /**
     * Policy for updating a bookings social media content.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function updateSocialMediaContent(User $user, Booking $booking): bool
    {
        return $user->isClient() &&
            $user->id === $booking->client_id;
    }

    /**
     * Policy for cancelling a booking.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function cancel(User $user, Booking $booking): bool
    {
        if ($user->isClient()) {
            return $user->id === $booking->client_id;
        } elseif ($user->isTalent()) {
            return $user->talentBookings()
                ->haveBeenAcceptedFor($booking)
                ->exists();
        }
    }

    /**
     * Policy for restoring a booking that is on hold.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function restore(User $user, Booking $booking): bool
    {
        return false;
    }

    /**
     * Policy for replacing talent on a booking.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function replaceTalent(User $user, Booking $booking): bool
    {
        return false;
    }

    /**
     * Policy for duplicating a booking.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function duplicate(User $user, Booking $booking): bool
    {
        return $user->id == $booking->client_id;
    }

    /**
     * Policy for processing payments on a booking.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function processPayment(User $user, Booking $booking): bool
    {
        return $user->isClient() &&
            $user->id === $booking->client_id;
    }

    /**
     * Policy for processing payments as received on a booking.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function processPaymentReceived(User $user, Booking $booking): bool
    {
        return false;
    }

    /**
     * Policy for listing booking requests.
     *
     * @param \Pickstar\User\User $user
     * @param string $type
     *
     * @return bool
     */
    public function listRequests(User $user): bool
    {
        return true;
    }

    /**
     * Policy for updating a booking request.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function updateRequest(User $user, Booking $booking): bool
    {
        return $user->isClient() &&
            $user->id === $booking->client_id;
    }

    /**
     * Policy for withdrawing booking request.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function withdrawRequest(User $user, Booking $booking): bool
    {
        return $user->isClient() &&
            $user->id === $booking->client_id;
    }

    /**
     * Policy for reactivating booking request.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function reactivateRequest(User $user, Booking $booking): bool
    {
        return $user->isAdmin();
    }

    /**
     * Policy for viewing a booking request.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function viewRequest(User $user, Booking $booking): bool
    {
        // If user is a client then they must be the client that initiated the request.
        if ($user->isClient()) {
            return $user->id === $booking->client_id;
        } else {
            if ($user->isTalent() && $booking->tags->count() && !$booking->talentAttached($user)) {
                $contains = false;
                foreach ($booking->tags as $tag) {
                    if ($user->tags->pluck('id')->contains($tag->id)) {
                        $contains = true;
                        break;
                    }
                }
                return $contains;
            }

            if ($booking->others_can_apply) {
                return true;
            }

            return $booking->talent()
                // If the request is PRIVATE ensure the talent can only view it when their response
                // status is not hidden.
                ->when(!$booking->others_can_apply, function ($query) {
                    $query->where('booking_talent.status', '<>', TalentPivot::STATUS_HIDDEN);
                })

                // If user is a manager then only let them see the request if one of their
                // managed talent has been shortlisted or added to it.
                ->when($user->isManager(), function ($query) use ($user) {
                    $query->whereIn('booking_talent.talent_id', $user->managedTalent->pluck('id'));
                })

                // If user is a talent then only let them see the request if they have been
                // shortlisted or added to it.
                ->when($user->isTalent(), function ($query) use ($user) {
                    $query->where('booking_talent.talent_id', $user->id);
                })
                ->exists();
        }

        return false;
    }

    /**
     * Policy for responding to a booking request.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function respondRequest(User $user, Booking $booking): bool
    {
        return $user->isTalent() &&
            $user->isActivated();
    }

    /**
     * Policy for continuing a booking request through to a booking.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function continueRequest(User $user, Booking $booking): bool
    {
        return $user->isClient() &&
            $user->id === $booking->client_id;
    }

    /**
     * Policy for accepting talent for a booking request.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function acceptTalent(User $user, Booking $booking): bool
    {
        return $user->isClient() &&
            $user->id === $booking->client_id;
    }

    /**
     * Policy for updating talent for a booking request.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function updateTalent(User $user, Booking $booking): bool
    {
        if ($user->isManager()) {
            return true;
        }

        return false;
    }

    /**
     * Policy for removing talent from a booking request.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function removeTalent(User $user, Booking $booking): bool
    {
        return false;
    }

    /**
     * Policy for adding additional talent to a booking request.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function addAdditionalTalent(User $user, Booking $booking): bool
    {
        if ($user->isManager()) {
            return true;
        }

        return false;
    }

    /**
     * Policy for notifying talent about a booking request.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function notifyTalent(User $user, Booking $booking): bool
    {
        return false;
    }

    /**
     * Policy for notifying managers about a booking request.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function notifyManagers(User $user, Booking $booking): bool
    {
        return false;
    }

    /**
     * Policy for listing vetting booking requests.
     *
     * @param \Pickstar\User\User $user
     * @param string $type
     *
     * @return bool
     */
    public function listVettingRequests(User $user): bool
    {
        return false;
    }

    /**
     * Policy for viewing a vetting booking request.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function viewVettingRequest(User $user, Booking $booking): bool
    {
        return $user->isClient() &&
            $user->id === $booking->client_id;
    }

    /**
     * Policy for approving vetting booking requests.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function approveVettingRequest(User $user, Booking $booking): bool
    {
        return false;
    }

    /**
     * Policy for declining vetting booking requests.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function declineVettingRequest(User $user, Booking $booking): bool
    {
        return false;
    }

    /**
     * Policy for updating vetting booking requests.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function updateVettingRequest(User $user, Booking $booking): bool
    {
        return false;
    }

    /**
     * Policy for removing shorlisted talent from vetting booking request.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function removeShortlistedTalent(User $user, Booking $booking): bool
    {
        return false;
    }

    /**
     * Policy for marking a booking as booked when unpaid.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return bool
     */
    public function markBookedUnpaid(User $user, Booking $booking): bool
    {
        if ($user->isAdmin() && $booking->status === 'scheduled') {
            return true;
        }
        return false;
    }

    /**
     * @param Booking $booking
     * @return bool
     */
    public function manageBookingTags(User $user, Booking $booking)
    {
        return false;
    }
}

<?php

namespace Pickstar\Policies;

use Pickstar\User\User;
use Pickstar\Payment\Payment;

class PaymentPolicy
{
    /**
     * Before running the policy action.
     *
     * @param \Pickstar\User\User $user
     *
     * @return mixed
     */
    public function before(User $user)
    {
        if ($user->isAdmin()) {
            return true;
        }
    }

    /**
     * Policy for viewing a list of payments.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Payment\Payment $payment
     *
     * @return bool
     */
    public function list(User $user)
    {
        if ($user->isClient()) {
            return true;
        }

        return false;
    }

    /**
     * Policy for viewing a Payment.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Payment\Payment $payment
     *
     * @return bool
     */
    public function view(User $user, Payment $payment) : bool
    {
        return ($user->isClient && $user->id === $payment->user_id);
    }
}

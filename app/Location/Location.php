<?php

namespace Pickstar\Location;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    public $table = 'locations';

    /**
     * Disable timestamps.
     *
     * @var boolean
     */
    public $timestamps = false;

    const AUSTRALIA_ID = 2;

    /**
     * Scope only Australian states.
     *
     * This is hard coded for now as the system is only launching in Australia.
     * This is designed to work with multiple countries with associated states/regions though.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public function scopeAustralianStates($query)
    {
        $query->where('parent_location_id', 2);
    }

    public function scopeWithoutAustralia($query)
    {
        $query->where('id', '!=', static::AUSTRALIA_ID);
    }

    /**
     * A location may have many states.
     *
     * This is a relationship that refers back to itself based on the "parent_location_id".
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function states()
    {
        return $this->hasMany(self::class, 'parent_location_id', 'id');
    }

    /**
     * A location may belong to a parent country.
     *
     * This is a relationship that refers back to itself based on the "parent_location_id".
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(self::class, 'parent_location_id');
    }
}

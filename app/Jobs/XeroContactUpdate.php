<?php
/**
 * Created by PhpStorm.
 * User: jpickstar
 * Date: 2019-04-24
 * Time: 09:58
 */

namespace Pickstar\Jobs;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Pickstar\Services\XeroService;
use Pickstar\User\ActiveCampaignUser;
use Pickstar\User\User;

/**
 * Class ACApiUpdate
 * @package Pickstar\Jobs
 */
class XeroContactUpdate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var User
     */
    public $user;

    /**
     * @var XeroService
     */
    protected $_api;

    public function __construct($user)
    {
        $this->user = $user;
        $this->_api = app()->make(XeroService::class, []);

    }

    public function handle()
    {
        $reference = $this->user->xero_contact_reference;
        $contact   = $this->_api->findOrCreateContact($this->user);

        if($reference !== null){ //has been created previously, details will be old.
            $payload   = XeroService::createUserPayload($this->user);
            XeroService::applyPayloadToModel($contact, $payload);
        }
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: jpickstar
 * Date: 2019-04-24
 * Time: 09:58
 */

namespace Pickstar\Jobs;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Pickstar\Booking\Booking;
use Pickstar\Services\XeroService;
use Pickstar\User\ActiveCampaignUser;

/**
 * Class ACApiUpdate
 * @package Pickstar\Jobs
 */
class XeroCreateInvoices implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var string email of user/lead being updated.
     */
    public $booking_id;

    /**
     * @var Booking
     */
    public $booking;

    /**
     * @var XeroService
     */
    protected $_api;

    public function __construct($id)
    {
        $this->booking_id   = $id;
        $this->booking      = Booking::findOrFail($id);
        $this->_api         = app()->make(XeroService::class, []);
    }

    public function handle()
    {
        foreach ($this->booking->talentAttending as $talent) {

            $this->_api->createAllRecordsForTalentBooking($this->booking, $talent);

        }
    }
}
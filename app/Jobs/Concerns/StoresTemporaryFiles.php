<?php

namespace Pickstar\Jobs\Concerns;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

trait StoresTemporaryFiles
{
    /**
     * Store a report file with the given contents as a given extension.
     *
     * Returns the file hash name.
     *
     * @param string $contents
     * @param string $extension
     *
     * @return string
     */
    public function storeFile(string $contents, string $extension = 'csv'): string
    {
        $fileName = sprintf('%s.%s', Str::random(40), $extension);

        Storage::disk('local')->put(
            sprintf('tmp/%s', $fileName),
            $contents
        );

        return $fileName;
    }
}

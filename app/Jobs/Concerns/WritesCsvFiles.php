<?php

namespace Pickstar\Jobs\Concerns;

use League\Csv\Writer;
use SplTempFileObject;

trait WritesCsvFiles
{
    /**
     * CSV writer instance resolver.
     *
     * @var \Closure
     */
    protected static $csvWriterResolver;

    /**
     * Set the CSV writer resolver.
     *
     * @param \Closure|null $resolver
     *
     * @return void
     */
    public static function setCsvWriterResolver($resolver): void
    {
        static::$csvWriterResolver = $resolver;
    }

    /**
     * Get the CSV writer instance.
     *
     * @return \League\Csv\Writer
     */
    protected function getCsvWriterInstance(): Writer
    {
        if (static::$csvWriterResolver) {
            return call_user_func(static::$csvWriterResolver);
        }

        return Writer::createFromFileObject(new SplTempFileObject);
    }
}

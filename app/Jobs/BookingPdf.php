<?php

namespace Pickstar\Jobs;

use PDF;
use Pickstar\User\User;
use Illuminate\Support\Str;
use Pickstar\Booking\Booking;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Bus\Dispatchable;

class BookingPdf
{
    use Dispatchable;

    /**
     * Booking instance.
     *
     * @var \Pickstar\Booking\Booking
     */
    protected $booking;

    /**
     * Create a new job instance.
     *
     * @param \Pickstar\Booking\Booking $booking
     * @param \Pickstar\User\User $booking
     */
    public function __construct(Booking $booking, User $user)
    {
        $this->booking = $booking;
        $this->user = $user;
    }

    /**
     * Generate booking PDF.
     *
     * @return string
     */
    public function handle(): string
    {
        PDF::setOptions(
            '--javascript-delay 2000 ' .
            '--no-stop-slow-scripts ' .
            '--debug-javascript ' .
            '--disable-smart-shrinking ' .
            '--page-width 210mm ' .
            '-T 0 ' .
            '-B 0 ' .
            '-L 0 ' .
            '-R 0 '
        );

        PDF::setOutputMode('S');

        $fileName = sprintf('%s.pdf', Str::random(40));

        Storage::disk('local')->put(
            sprintf('tmp/%s', $fileName),
            PDF::html('pdf.booking', ['booking' => $this->booking, 'user' => $this->user])
        );

        return $fileName;
    }
}

<?php

namespace Pickstar\Jobs;

use SplFileInfo;
use League\Csv\Reader;
use Illuminate\Support\Arr;
use Pickstar\Talent\Profile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Bus\Dispatchable;

class ImportTalentSocialMediaCsv
{
    use Dispatchable;

    /**
     * CSV reader instance resolver.
     *
     * @var \Closure
     */
    protected static $csvReaderResolver;

    /**
     * CSV file instance.
     *
     * @var \SplFileInfo
     */
    protected $file;

    /**
     * Create a new job instance.
     *
     * @param \SplFileInfo $file
     */
    public function __construct(SplFileInfo $file)
    {
        $this->file = $file;
    }

    /**
     * Set the CSV reader resolver.
     *
     * @param \Closure|null $resolver
     *
     * @return void
     */
    public static function setCsvReaderResolver($resolver): void
    {
        static::$csvReaderResolver = $resolver;
    }

    /**
     * Get the CSV reader instance.
     *
     * @return \League\Csv\Reader
     */
    public function getCsvReaderInstance(): Reader
    {
        if (is_callable(static::$csvReaderResolver)) {
            return call_user_func(static::$csvReaderResolver, $this->file);
        }

        return Reader::createFromPath($this->file);
    }

    /**
     * Import talent social media CSV.
     *
     * @return array
     */
    public function handle(): array
    {
        $reader = $this->getCsvReaderInstance($this->file);

        $reader->setDelimiter(',');

        $reader->setHeaderOffset(0);

        // Create a validator instance with the required rules. We'll fill the data with
        // each record and validate each record within the loop. This will determine
        // whether the record is updated or reported as a failure.
        $validator = Validator::make([], [
            'id' => 'required|numeric',
            'facebook_follower_count' => 'required|numeric',
            'twitter_follower_count' => 'required|numeric',
            'instagram_follower_count' => 'required|numeric'
        ]);

        $records = ['failed' => [], 'updated' => []];

        DB::beginTransaction();

        foreach ($reader->getRecords() as $record) {
            $validator->setData($record);

            if ($validator->fails()) {
                $records['failed'][] = $record;
            } else {
                Profile::where('user_id', $record['id'])->update(Arr::only($record, [
                    'facebook_follower_count',
                    'twitter_follower_count',
                    'instagram_follower_count'
                ]));

                $records['updated'][] = $record;
            }
        }

        DB::commit();

        return $records['failed'];
    }
}

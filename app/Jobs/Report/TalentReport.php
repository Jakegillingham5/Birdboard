<?php

namespace Pickstar\Jobs\Report;

use Exception;
use Pickstar\User\User;
use Pickstar\Booking\Booking;
use Pickstar\Booking\TalentPivot;
use Pickstar\Events\ReportFailed;
use Illuminate\Support\Facades\DB;
use Pickstar\Events\ReportGenerated;
use Pickstar\Jobs\Concerns\WritesCsvFiles;
use Illuminate\Database\Eloquent\Collection;
use Pickstar\Jobs\Concerns\StoresTemporaryFiles;

class TalentReport extends AbstractReport
{
    use WritesCsvFiles,
        StoresTemporaryFiles;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        try {
            $csv = $this->getCsvWriterInstance();

            $csv->setDelimiter(',');

            // Create the CSV headers.
            $csv->insertOne([
                'id',
                'first_name',
                'last_name',
                'email',
                'verified_email',
                'phone',
                'facebook_link',
                'twitter_link',
                'instagram_link',
                'description',
                'sports',
                'clubs',
                'locations',
                'opportunities',
                'manager',
                'manager_agency',
                'abn',
                'gst_registered',
                'bank_details',
                'active_requests',
                'total_requests',
                'active_bookings',
                'completed_bookings',
                'total_bookings',
                'total_paid'
            ]);

            $csv->insertAll($this->users()->map(function ($user) {
                return [
                    $user->id,
                    $user->first_name,
                    $user->last_name,
                    $user->email,
                    $user->email_verified
                        ? 'Yes'
                        : 'No',
                    $user->phone,
                    $user->profile->facebook_link,
                    $user->profile->twitter_link,
                    $user->profile->instagram_link,
                    trim(preg_replace('/\n/',' ', strip_tags($user->profile->description))),
                    $user->tags->where('type', 'sport')->implode('name', ', '),
                    $user->tags->where('type', 'club')->implode('name', ', '),
                    $user->locations->implode('name', ', '),
                    $user->opportunities->implode('name', ', '),
                    $user->profile->manager,
                    $user->profile->manager_agency,
                    $user->paymentDetails
                        ? $user->paymentDetails->abn
                        : null,
                    $user->paymentDetails && $user->paymentDetails->gst_registered
                        ? 'Yes'
                        : 'No',
                    $user->paymentDetails && $user->paymentDetails->bsb_number && $user->paymentDetails->account_name && $user->paymentDetails->account_number
                        ? 'Yes'
                        : 'No',
                    $user->active_requests_count,
                    $user->total_requests_count,
                    $user->active_bookings_count,
                    $user->completed_bookings_count,
                    $user->total_bookings_count,
                    $user->total_paid_sum ?? 0
                ];
            }));

            broadcast(new ReportGenerated(
                $this->user,
                $this->storeFile($csv->getContent()),
                sprintf('%s_report_%s.csv', 'talent', now()->format('Y-m-d_H-i-s'))
            ));
        } catch (Exception $exception) {
            report($exception);

            broadcast(new ReportFailed($this->user));
        }
    }

    /**
     * Get the users for the report.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function users(): Collection
    {
        return User::talent()
            ->with('profile', 'tags', 'paymentDetails', 'locations', 'opportunities')
            ->select('users.*')

            // This sub-select utilizes the "booking_talent_commissions_view" which performs a query for us that
            // gets the correct commission rate for each booking
            ->selectSub(
                DB::query()
                    ->selectRaw('SUM(booking_talent_commissions_view.booking_budget_per_star * booking_talent_commissions_view.talent_commission_rate)')
                    ->from('booking_talent_commissions_view')
                    ->join('bookings', 'booking_talent_commissions_view.booking_id', 'bookings.id')
                    ->join('booking_talent', function ($join) {
                        $join->on('booking_talent_commissions_view.booking_id', 'booking_talent.booking_id')
                            ->on('booking_talent_commissions_view.talent_id', 'booking_talent.talent_id')
                            ->where('booking_talent.status', TalentPivot::STATUS_APPLIED)
                            ->where('booking_talent.client_accepted', true);
                    })
                    ->whereColumn('booking_talent.talent_id', 'users.id')
                    ->where('bookings.status', Booking::STATUS_COMPLETED),
                'total_paid_sum'
            )

            // Add the counts for each of the different booking types and statuses.
            ->withCount([
                'talentBookingRequests as total_requests_count' => function ($query) {
                    $query->withoutVetting();
                },
                'talentBookingRequests as active_requests_count' => function ($query) {
                    $query->haveApplied()
                        ->where('bookings.status', Booking::STATUS_PENDING);
                },
                'talentBookings as total_bookings_count' => function ($query) {
                    $query->haveBeenAcceptedWithCancelled();
                },
                'talentBookings as completed_bookings_count' => function ($query) {
                    $query->haveBeenAccepted()
                        ->where('bookings.status', Booking::STATUS_COMPLETED);
                },
                'talentBookings as active_bookings_count' => function ($query) {
                    $query->haveBeenAccepted()
                        ->whereIn('bookings.status', [Booking::STATUS_SCHEDULED, Booking::STATUS_BOOKED, Booking::STATUS_HOLD]);
                }
            ])
            ->get();
    }
}

<?php

namespace Pickstar\Jobs\Report;

use Exception;
use Carbon\Carbon;
use Pickstar\User\User;
use Pickstar\Booking\Booking;
use Pickstar\Booking\TalentPivot;
use Pickstar\Events\ReportFailed;
use Illuminate\Support\Facades\DB;
use Pickstar\Events\ReportGenerated;
use Pickstar\Jobs\Concerns\WritesCsvFiles;
use Illuminate\Database\Eloquent\Collection;
use Pickstar\Jobs\Concerns\StoresTemporaryFiles;

class DeclineReasonReport extends AbstractReport
{
    use WritesCsvFiles,
        StoresTemporaryFiles;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        try {
            $csv = $this->getCsvWriterInstance();

            $csv->setDelimiter(',');

            // Create the CSV headers.
            $csv->insertOne([
                'talent_id',
                'talent_name',
                'booking_id',
                'booking_name',
                'decline_reason'
            ]);

            $csv->insertAll($this->reasons()->map(function ($pivot) {
                return [
                    $pivot->talent->id,
                    $pivot->talent->name,
                    $pivot->booking->id,
                    $pivot->booking->name,
                    $pivot->decline_reason,
                ];
            }));

            broadcast(new ReportGenerated(
                $this->user,
                $this->storeFile($csv->getContent()),
                sprintf('%s_report_%s.csv', 'decline_reasons', now()->format('Y-m-d_H-i-s'))
            ));
        } catch (Exception $exception) {
            report($exception);

            broadcast(new ReportFailed($this->user));
        }
    }

    /**
     * Get the reasons for the report.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function reasons(): Collection
    {
        return TalentPivot::with('talent', 'booking')
            ->where('status', 'declined')
            ->whereNotNull('decline_reason')
            ->when($this->payload->get('from', false), function ($query, $from) {
                $query->whereHas('booking', function ($query) use ($from) {
                    $query->whereDate('created_at', '>=', Carbon::createFromFormat('Y-m-d', $from)->startOfDay());
                });
            })
            ->when($this->payload->get('to', false), function ($query, $to) {
                $query->whereHas('booking', function ($query) use ($to) {
                    $query->whereDate('created_at', '<=', Carbon::createFromFormat('Y-m-d', $to)->endOfDay());
                });
            })
            ->get();
    }
}

<?php

namespace Pickstar\Jobs\Report;

use Exception;
use Pickstar\User\User;
use Pickstar\Booking\Booking;
use Pickstar\Events\ReportFailed;
use Illuminate\Support\Facades\DB;
use Pickstar\Events\ReportGenerated;
use Pickstar\Jobs\Concerns\WritesCsvFiles;
use Illuminate\Database\Eloquent\Collection;
use Pickstar\Jobs\Concerns\StoresTemporaryFiles;

class ClientReport extends AbstractReport
{
    use WritesCsvFiles,
        StoresTemporaryFiles;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        try {
            $csv = $this->getCsvWriterInstance();

            $csv->setDelimiter(',');

            // Create the CSV headers.
            $csv->insertOne([
                'id',
                'first_name',
                'last_name',
                'email',
                'phone',
                'company',
                'active_requests',
                'total_requests',
                'active_bookings',
                'total_bookings',
                'total_paid'
            ]);

            $csv->insertAll($this->users()->map(function ($user) {
                return [
                    $user->id,
                    $user->first_name,
                    $user->last_name,
                    $user->email,
                    $user->phone,
                    $user->company,
                    $user->active_requests_count,
                    $user->total_requests_count,
                    $user->active_bookings_count,
                    $user->total_bookings_count,
                    $user->total_paid_sum ?? 0
                ];
            }));

            broadcast(new ReportGenerated(
                $this->user,
                $this->storeFile($csv->getContent()),
                sprintf('%s_report_%s.csv', 'clients', now()->format('Y-m-d_H-i-s'))
            ));
        } catch (Exception $exception) {
            report($exception);

            broadcast(new ReportFailed($this->user));
        }
    }

    /**
     * Get the users for the report.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function users(): Collection
    {
        return User::onlyClients()
            ->select('users.*')

            // This sub-select sums the budget for all completed bookings. Note this budget can actually change
            // after a booking has been fully paid so it will represent the most up-to-date information. This
            // change happens when replacing talent in thge "InteractsWithPayments" trait.
            ->selectSub(
                DB::query()
                    ->selectRaw('SUM(bookings.budget)')
                    ->from('bookings')
                    ->whereColumn('bookings.client_id', '=', 'users.id')
                    ->where('bookings.type', Booking::TYPE_BOOKING)
                    ->where('bookings.status', Booking::STATUS_COMPLETED),
                'total_paid_sum'
            )

            // Add the counts for each of the different booking types and statuses.
            ->withCount([
                'clientBookingRequests as total_requests_count',
                'clientBookingRequests as active_requests_count' => function ($query) {
                    $query->whereIn('status', [Booking::STATUS_PENDING, Booking::STATUS_VETTING]);
                },
                'clientBookings as total_bookings_count',
                'clientBookings as active_bookings_count' => function ($query) {
                    $query->whereIn('status', [Booking::STATUS_SCHEDULED, Booking::STATUS_BOOKED, Booking::STATUS_HOLD]);
                }
            ])
            ->get();
    }
}

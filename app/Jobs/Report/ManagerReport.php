<?php

namespace Pickstar\Jobs\Report;

use Exception;
use Pickstar\User\User;
use Pickstar\Booking\Booking;
use Pickstar\Booking\TalentPivot;
use Pickstar\Events\ReportFailed;
use Illuminate\Support\Facades\DB;
use Pickstar\Events\ReportGenerated;
use Pickstar\Jobs\Concerns\WritesCsvFiles;
use Illuminate\Database\Eloquent\Collection;
use Pickstar\Jobs\Concerns\StoresTemporaryFiles;

class ManagerReport extends AbstractReport
{
    use WritesCsvFiles,
        StoresTemporaryFiles;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        try {
            $csv = $this->getCsvWriterInstance();

            $csv->setDelimiter(',');

            // Create the CSV headers.
            $csv->insertOne([
                'id',
                'first_name',
                'last_name',
                'email',
                'phone',
                'company',
                'bank_details',
                'talent_under_management',
                'active_requests',
                'total_requests',
                'active_bookings',
                'completed_bookings',
                'total_bookings',
                'total_paid'
            ]);

            $csv->insertAll($this->users()->map(function ($user) {
                return [
                    $user->id,
                    $user->first_name,
                    $user->last_name,
                    $user->email,
                    $user->phone,
                    $user->company,
                    $user->paymentDetails &&
                        $user->paymentDetails->bsb_number &&
                        $user->paymentDetails->account_name &&
                        $user->paymentDetails->account_number
                            ? 'Yes'
                            : 'No',
                    $user->management_talent_count,
                    $user->active_requests_count,
                    $user->total_requests_count,
                    $user->active_bookings_count,
                    $user->completed_bookings_count,
                    $user->total_bookings_count,
                    $user->total_paid_sum ?? 0
                ];
            }));

            broadcast(new ReportGenerated(
                $this->user,
                $this->storeFile($csv->getContent()),
                sprintf('%s_report_%s.csv', 'managers', now()->format('Y-m-d_H-i-s'))
            ));
        } catch (Exception $exception) {
            report($exception);

            broadcast(new ReportFailed($this->user));
        }
    }

    /**
     * Get the users for the report.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function users(): Collection
    {
        return User::masterManagers()
            ->select('users.*')


            // This sub-select utilizes the "booking_talent_commissions_view" which performs a query for us that
            // gets the correct commission rate for each booking.
            ->selectSub(
                DB::query()
                    ->selectRaw('SUM(booking_talent_commissions_view.booking_budget_per_star * booking_talent_commissions_view.talent_commission_rate)')
                    ->from('booking_talent_commissions_view')
                    ->join('bookings', 'booking_talent_commissions_view.booking_id', 'bookings.id')
                    ->join('booking_talent', function ($join) {
                        $join->on('booking_talent_commissions_view.booking_id', 'booking_talent.booking_id')
                            ->on('booking_talent_commissions_view.talent_id', 'booking_talent.talent_id')
                            ->where('booking_talent.status', TalentPivot::STATUS_APPLIED)
                            ->where('booking_talent.client_accepted', true);
                    })
                    ->whereColumn('booking_talent_commissions_view.management_id', 'users.id')
                    ->where('bookings.status', Booking::STATUS_COMPLETED),
                'total_paid_sum'
            )

            // This sub-select gets the total requests. This includes pending requests and the withdrawn
            // requests where the booking was previously vetted.
            ->selectSub(
                DB::query()
                    ->selectRaw('count(distinct booking_management_lookup_view.booking_id)')
                    ->from('booking_management_lookup_view')
                    ->where(function ($query) {
                        $query->where('booking_management_lookup_view.booking_status', Booking::STATUS_PENDING)
                            ->orWhere(function ($query) {
                                $query->where('booking_management_lookup_view.booking_status', Booking::STATUS_WITHDRAWN)
                                    ->where('booking_management_lookup_view.booking_was_vetted', true);
                            });
                    })
                    ->where('booking_management_lookup_view.booking_type', Booking::TYPE_REQUEST)
                    ->whereColumn('booking_management_lookup_view.management_id', 'users.id'),
                'total_requests_count'
            )

            // This sub-select gets the active requests. This includes pending requests where talent have applied.
            ->selectSub(
                DB::query()
                    ->selectRaw('count(distinct booking_management_lookup_view.booking_id)')
                    ->from('booking_management_lookup_view')
                    ->where('booking_management_lookup_view.booking_talent_status', TalentPivot::STATUS_APPLIED)
                    ->where('booking_management_lookup_view.booking_status', Booking::STATUS_PENDING)
                    ->where('booking_management_lookup_view.booking_type', Booking::TYPE_REQUEST)
                    ->whereColumn('booking_management_lookup_view.management_id', 'users.id'),
                'active_requests_count'
            )

            // This sub-select gets the total bookings. This includes all bookings where a talent has been accepted
            // and where the talent has cancelled.
            ->selectSub(
                DB::query()
                    ->selectRaw('count(distinct booking_management_lookup_view.booking_id)')
                    ->from('booking_management_lookup_view')
                    ->whereIn('booking_management_lookup_view.booking_talent_status', [
                        TalentPivot::STATUS_APPLIED,
                        TalentPivot::STATUS_CANCELLED
                    ])
                    ->where('booking_management_lookup_view.booking_talent_accepted', true)
                    ->where('booking_management_lookup_view.booking_type', Booking::TYPE_BOOKING)
                    ->whereColumn('booking_management_lookup_view.management_id', 'users.id'),
                'total_bookings_count'
            )

            // This sub-select gets the completed bookings. This includes completed bookings where a talent has
            // been accepted.
            ->selectSub(
                DB::query()
                    ->selectRaw('count(distinct booking_management_lookup_view.booking_id)')
                    ->from('booking_management_lookup_view')
                    ->where('booking_management_lookup_view.booking_talent_status', TalentPivot::STATUS_APPLIED)
                    ->where('booking_management_lookup_view.booking_talent_accepted', true)
                    ->where('booking_management_lookup_view.booking_status', Booking::STATUS_COMPLETED)
                    ->where('booking_management_lookup_view.booking_type', Booking::TYPE_BOOKING)
                    ->whereColumn('booking_management_lookup_view.management_id', 'users.id'),
                'completed_bookings_count'
            )

            // This sub-select gets the active bookings. This includes scheduled, booked, and on hold bookings
            // where a talent has been accepted.
            ->selectSub(
                DB::query()
                    ->selectRaw('count(distinct booking_management_lookup_view.booking_id)')
                    ->from('booking_management_lookup_view')
                    ->where('booking_management_lookup_view.booking_talent_status', TalentPivot::STATUS_APPLIED)
                    ->where('booking_management_lookup_view.booking_talent_accepted', true)
                    ->whereIn('booking_management_lookup_view.booking_status', [
                        Booking::STATUS_SCHEDULED,
                        Booking::STATUS_BOOKED,
                        Booking::STATUS_HOLD
                    ])
                    ->where('booking_management_lookup_view.booking_type', Booking::TYPE_BOOKING)
                    ->whereColumn('booking_management_lookup_view.management_id', 'users.id'),
                'active_bookings_count'
            )
            ->withCount(['managementTalent AS management_talent_count'])
            ->get();
    }
}

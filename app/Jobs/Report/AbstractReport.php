<?php

namespace Pickstar\Jobs\Report;

use Pickstar\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Collection;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

abstract class AbstractReport implements ShouldQueue
{
    use Queueable,
        Dispatchable,
        SerializesModels,
        InteractsWithQueue;

    /**
     * User generating report.
     *
     * @var \Pickstar\User\User
     */
    protected $user;

    /**
     * Payload collection.
     *
     * @var \Illuminate\Support\Collection
     */
    protected $payload;

    /**
     * Create a new job instance.
     *
     * @param \Pickstar\User\User $user
     * @param array $payload
     */
    public function __construct(User $user, array $payload = [])
    {
        $this->user = $user;
        $this->payload = collect($payload);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    abstract public function handle(): void;
}

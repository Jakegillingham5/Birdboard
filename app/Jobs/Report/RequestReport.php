<?php

namespace Pickstar\Jobs\Report;

use Carbon\Carbon;
use Pickstar\Booking\Booking;
use Illuminate\Database\Eloquent\Collection;

class RequestReport extends AbstractBookingReport
{
    /**
     * Get the bookings for the report.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function bookings(): Collection
    {
        return Booking::query()
            ->all()
            ->withoutVetting()
            ->withoutCompleted()
            ->when($this->payload->get('from', false), function ($query) {
                $query->whereDate('created_at', '>=', Carbon::createFromFormat('Y-m-d', $this->payload->get('from'))->startOfDay());
            })
            ->when($this->payload->get('to', false), function ($query) {
                $query->whereDate('created_at', '<=', Carbon::createFromFormat('Y-m-d', $this->payload->get('to'))->startOfDay());
            })
            ->with('talentNotified', 'talentApplied', 'talentAttending', 'client')
            ->get();
    }

    /**
     * Get the name of the report.
     *
     * @return string
     */
    public function reportName(): string
    {
        return 'requests';
    }
}

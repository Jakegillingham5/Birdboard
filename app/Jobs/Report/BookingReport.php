<?php

namespace Pickstar\Jobs\Report;

use Carbon\Carbon;
use Pickstar\Booking\Booking;
use Illuminate\Database\Eloquent\Collection;

class BookingReport extends AbstractBookingReport
{
    /**
     * Get the bookings for the report.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function bookings(): Collection
    {
        return Booking::completed()
            ->when($this->payload->get('from', false), function ($query) {
                $query->whereDate('completed_date', '>=', Carbon::createFromFormat('Y-m-d', $this->payload->get('from'))->startOfDay());
            })
            ->when($this->payload->get('to', false), function ($query) {
                $query->whereDate('completed_date', '<=', Carbon::createFromFormat('Y-m-d', $this->payload->get('to'))->endOfDay());
            })
            ->with('talentNotified', 'talentApplied', 'talentAttending', 'client')
            ->get();
    }

    /**
     * Get the name of the report.
     *
     * @return string
     */
    public function reportName(): string
    {
        return 'bookings';
    }
}

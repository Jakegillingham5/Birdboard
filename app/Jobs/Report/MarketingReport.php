<?php

namespace Pickstar\Jobs\Report;

use Carbon\Carbon;
use Exception;
use Pickstar\Marketing\Email;
use Pickstar\Events\ReportFailed;
use Pickstar\Events\ReportGenerated;
use Pickstar\Jobs\Concerns\WritesCsvFiles;
use Illuminate\Database\Eloquent\Collection;
use Pickstar\Jobs\Concerns\StoresTemporaryFiles;

class MarketingReport extends AbstractReport
{
    use WritesCsvFiles,
        StoresTemporaryFiles;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        try {
            $csv = $this->getCsvWriterInstance();

            $csv->setDelimiter(',');

            // Create the CSV headers.
            $csv->insertOne([
                'id',
                'email',
                'first_name',
                'last_name',
                'date',
                'time',
                'matching_request'
            ]);

            $csv->insertAll($this->emails()->map(function ($email) {
                return [
                    $email->id,
                    $email->email,
                    $email->first_name,
                    $email->last_name,
                    $email->created_at
                        ? $email->created_at->format('Y-m-d')
                        : null,
                    $email->created_at
                        ? $email->created_at->format('H:i:s')
                        : null,
                    $email->marketing_email_used === 1
                        ? 'Yes'
                        : 'No'
                ];
            }));

            broadcast(new ReportGenerated(
                $this->user,
                $this->storeFile($csv->getContent()),
                sprintf('%s_report_%s.csv', 'marketing', now()->format('Y-m-d_H-i-s'))
            ));
        } catch (Exception $exception) {
            report($exception);

            broadcast(new ReportFailed($this->user));
        }
    }

    /**
     * Get the emails for the report.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function emails(): Collection
    {
        return Email::select('marketing_emails.*')
            ->selectRaw('IF(bookings.id IS NOT NULL, TRUE, FALSE) AS marketing_email_used')
            ->leftJoin('users', 'marketing_emails.email', 'users.email')
            ->leftJoin('bookings', 'users.id', 'bookings.client_id')
            ->groupBy('marketing_emails.email')
            ->when($this->payload->get('from', false), function ($query) {
                $query->whereDate('marketing_emails.created_at', '>=', Carbon::createFromFormat('Y-m-d', $this->payload->get('from'))->startOfDay());
            })
            ->when($this->payload->get('to', false), function ($query) {
                $query->whereDate('marketing_emails.created_at', '<=', Carbon::createFromFormat('Y-m-d', $this->payload->get('to'))->endOfDay());
            })
            ->get();
    }
}

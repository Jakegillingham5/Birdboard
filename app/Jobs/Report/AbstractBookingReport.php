<?php

namespace Pickstar\Jobs\Report;

use Exception;
use Pickstar\Events\ReportFailed;
use Pickstar\Events\ReportGenerated;
use Pickstar\Jobs\Concerns\WritesCsvFiles;
use Pickstar\Jobs\Concerns\StoresTemporaryFiles;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;

abstract class AbstractBookingReport extends AbstractReport
{
    use WritesCsvFiles,
        StoresTemporaryFiles;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        try {
            $csv = $this->getCsvWriterInstance();

            $csv->setDelimiter(',');

            // Create the CSV headers.
            $csv->insertOne([
                'id',
                'current_status',
                'client_first_name',
                'client_last_name',
                'client_email',
                'client_phone',
                'request_initiated_date',
                'request_initiated_time',
                'appearance_type',
                'location',
                'date',
                'appearance_duration',
                'sms_notifications_count',
                'vetted_date',
                'stars_required',
                'stars_applied',
                'budget_per_star',
                'budget_total',
                'applied_talent',
                'confirmed_talent'
            ]);

            $csv->insertAll($this->bookings()->map(function ($booking) {
                return [
                    $booking->id,
                    $booking->status,
                    $booking->client->first_name,
                    $booking->client->last_name,
                    $booking->client->email,
                    $booking->client->phone,
                    $booking->created_at->format('Y-m-d'),
                    $booking->created_at->format('H:i:s'),
                    $booking->appearance_type,
                    $booking->state,
                    $booking->date,
                    $booking->duration,
                    $booking->talentNotified->count(),
                    $booking->vetted_date
                        ? $booking->vetted_date->format('Y-m-d')
                        : null,
                    $booking->required_stars,
                    $booking->talentApplied->count(),
                    $booking->budget_per_star,
                    $booking->budget,
                    $booking->talentApplied->implode('name', ', '),
                    $booking->talentAttending->implode('name', ', ')
                ];
            }));

            broadcast(new ReportGenerated(
                $this->user,
                $this->storeFile($csv->getContent()),
                sprintf('%s_report_%s.csv', $this->reportName(), now()->format('Y-m-d_H-i-s'))
            ));
        } catch (Exception $exception) {
            report($exception);

            broadcast(new ReportFailed($this->user));
        }
    }

    /**
     * Get the bookings for the report.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    abstract public function bookings(): EloquentCollection;

    /**
     * Get the name of the report.
     *
     * @return string
     */
    abstract public function reportName(): string;
}

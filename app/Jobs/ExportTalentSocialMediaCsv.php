<?php

namespace Pickstar\Jobs;

use Pickstar\User\User;
use Pickstar\Jobs\Concerns\WritesCsvFiles;
use Illuminate\Foundation\Bus\Dispatchable;
use Pickstar\Jobs\Concerns\StoresTemporaryFiles;

class ExportTalentSocialMediaCsv
{
    use Dispatchable,
        WritesCsvFiles,
        StoresTemporaryFiles;

    /**
     * Export the talent social media CSV.
     *
     * @return string
     */
    public function handle(): string
    {
        $csv = $this->getCsvWriterInstance();

        $csv->setDelimiter(',');

        $csv->insertOne([
            'id',
            'first_name',
            'last_name',
            'email',
            'facebook_follower_count',
            'twitter_follower_count',
            'instagram_follower_count'
        ]);

        $talent = User::talent()
            ->with('profile')
            ->get();

        $csv->insertAll($talent->map(function ($talent) {
            return [
                $talent->id,
                $talent->first_name,
                $talent->last_name,
                $talent->email,
                $talent->profile->facebook_follower_count,
                $talent->profile->twitter_follower_count,
                $talent->profile->instagram_follower_count
            ];
        })->toArray());

        return $this->storeFile($csv->getContent());
    }
}

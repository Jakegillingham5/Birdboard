<?php
/**
 * Created by PhpStorm.
 * User: jpickstar
 * Date: 2019-04-24
 * Time: 09:58
 */

namespace Pickstar\Jobs;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Pickstar\User\ActiveCampaignUser;

/**
 * Class ACApiUpdate
 * @package Pickstar\Jobs
 */
class SubscribeToNewsletter implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var string email of user/lead being updated.
     */
    public $email;

    public function __construct($email)
    {
        $this->email = $email;
    }

    public function handle()
    {

        $user = ActiveCampaignUser::findByEmail($this->email);
        $user->subscribeToList('Newsletter');
    }
}
<?php

namespace Pickstar\Opportunity;

use Illuminate\Database\Eloquent\Model;

class Opportunity extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'opportunities';

    /**
     * Fillable columns.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'type',
        'parent'
    ];

    /**
     * Hidden properties.
     *
     * @var array
     */
    protected $hidden = [
        'pivot'
    ];

    /**
     * Type-casted columns.
     *
     * @var array
     */
    protected $casts = [
        'parent' => 'boolean'
    ];

    /**
     * Opportunity type constants.
     *
     * @var string
     */
    const TYPE_IN_PERSON = 'in_person';
    const TYPE_ONLINE = 'online';
    const TYPE_ESPORTS = 'esports';

    /**
     * An opportunity may have many chilren.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function children()
    {
        return $this->belongsToMany(static::class, 'opportunity_relations', 'parent_opportunity_id', 'child_opportunity_id');
    }

    /**
     * An opportunity may have many parents.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function parents()
    {
        return $this->belongsToMany(static::class, 'opportunity_relations', 'child_opportunity_id', 'parent_opportunity_id');
    }

    /**
     * Scope only the parent opportunities.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public function scopeOnlyParents($query)
    {
        $query->where('parent', true);
    }

    /**
     * Scope only the child opportunities.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public function scopeOnlyChildren($query)
    {
        $query->where('parent', false);
    }

    /**
     * Scope only the "in person" opportunities.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public function scopeInPerson($query)
    {
        $query->where('type', static::TYPE_IN_PERSON);
    }

    /**
     * Scope only the "online" opportunities.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public function scopeOnline($query)
    {
        $query->where('type', static::TYPE_ONLINE);
    }
}

<?php

namespace Pickstar\Registration;

use Pickstar\Events\UserRegistered;
use Pickstar\User\User;
use Illuminate\Support\Str;

class TalentRegistration extends AbstractRegistration
{
    /**
     * Handle registration of a talent user.
     *
     * @param array $payload
     *
     * @return \Pickstar\User\User
     */
    public function handle(array $payload) : User
    {
        $this->validate($payload);

        // If we are a guest then we have submitted the registration form as a talent
        // and so we need to fill in the required details.
        if (auth('web')->guest()) {
            $user = User::create([
                'first_name' => $payload['first_name'] ?? null,
                'last_name' => $payload['last_name'] ?? null,
                'role' => User::ROLE_TALENT,
                'email' => $payload['email'] ?? null,
                'password' => $payload['password'] ?? null,
                'phone' => $payload['phone'] ?? null,
                'email_verification_token' => Str::random(32),
                'status' => User::STATUS_PENDING
            ]);

            $user->profile()->create([
                'manager' => $payload['manager'] ?? null,
                'agency' => $payload['agency'] ?? null,
                'gender' => $payload['gender'] ?? null,
            ]);

        // Otherwise we have likely registered as a social user and we have selected our
        // role as a "talent", so we'll fill the role for the user.
        } else {
            $user = auth('web')->user();

            $user->fill(array_merge($payload, [
                'phone' => $payload['phone'] ?? null,
                'role' => User::ROLE_TALENT,
                'status' => User::STATUS_PENDING,
            ]))->save();

            // Create an empty talent profile if we do not already have one...
            $user->profile()->updateOrCreate([
                'gender' => $payload['gender'] ?? null
            ]);
        }

        // Trigger new talent registration event
        event(new UserRegistered($user));

        return $user;
    }
}

<?php

namespace Pickstar\Registration;

use Pickstar\User\User;
use Illuminate\Support\Str;

class ClientRegistration extends AbstractRegistration
{
    /**
     * Handle registration of a client user.
     *
     * @param array $payload
     *
     * @return \Pickstar\User\User
     */
    public function handle(array $payload) : User
    {
        $this->validate($payload);

        // If we are a guest then we have submitted the registration form as a client
        // and so we need to fill in the required details.
        if (auth('web')->guest()) {
            $user = User::create([
                'role' => User::ROLE_CLIENT,
                'email' => $payload['email'],
                'first_name' => $payload['first_name'] ?? null,
                'last_name' => $payload['last_name'] ?? null,
                'password' => $payload['password'] ?? null,
                'company' => $payload['company'] ?? null,
                'email_verification_token' => Str::random(32),
                'phone' => $payload['phone'] ? str_replace(' ', '', $payload['phone']) : null,
                'status' => User::STATUS_ACTIVE
            ]);

        // Otherwise we have likely registered as a social user and we have selected our
        // role as a "client", so we'll fill the role for the user.
        } else {
            $user = auth('web')->user();

            $user->fill(array_merge($payload, [
                'phone' => $payload['phone'],
                'role' => User::ROLE_CLIENT,
                'status' => User::STATUS_ACTIVE,
                'phone' => $payload['phone'] ?? null,
            ]))->save();
        }

        return $user;
    }
}

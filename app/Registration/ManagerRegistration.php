<?php

namespace Pickstar\Registration;

use Pickstar\User\User;
use Illuminate\Support\Str;
use Pickstar\Events\UserRegistered;

class ManagerRegistration extends AbstractRegistration
{
    /**
     * Handle registration of a manager user.
     *
     * @param array $payload
     *
     * @return \Pickstar\User\User
     */
    public function handle(array $payload) : User
    {
        $this->validate($payload);

        // If we are a guest then we have submitted the registration form as a manager
        // and so we need to fill in the required details.
        if (auth('web')->guest()) {
            $user = User::create([
                'role' => User::ROLE_MANAGER,
                'email' => $payload['email'],
                'first_name' => $payload['first_name'] ?? null,
                'last_name' => $payload['last_name'] ?? null,
                'password' => $payload['password'] ?? null,
                'phone' => $payload['phone'] ?? null,
                'company' => $payload['company'] ?? null,
                'position' => $payload['position'] ?? null,
                'email_verification_token' => Str::random(32),
                'status' => User::STATUS_PENDING
            ]);

        // Otherwise we have likely registered as a social user and we have selected our
        // role as a "manager", so we'll fill the role for the user as well as other
        // required fields.
        } else {
            $user = auth('web')->user();

            $user->fill(array_merge($payload, [
                'role' => User::ROLE_MANAGER,
                'status' => User::STATUS_PENDING,
                'company' => $payload['company'] ?? null,
                'position' => $payload['position'] ?? null,
                'phone' => $payload['phone'] ?? null,
            ]))->save();
        }
        
        // Trigger new manager registration event
        event(new UserRegistered($user));

        return $user;
    }
}

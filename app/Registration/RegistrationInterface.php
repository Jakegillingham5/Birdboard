<?php

namespace Pickstar\Registration;

use Pickstar\User\User;

interface RegistrationInterface
{
    /**
     * Handle registration for a user type. Receives the request payload.
     *
     * @param array $payload
     *
     * @return \Pickstar\User\User
     */
    public function handle(array $payload) : User;
}

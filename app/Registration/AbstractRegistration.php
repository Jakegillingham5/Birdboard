<?php

namespace Pickstar\Registration;

use Pickstar\User\User;
use Illuminate\Validation\ValidationException;

abstract class AbstractRegistration implements RegistrationInterface
{
    /**
     * Validate the payload.
     *
     * @param array $payload
     *
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validate(array $payload)
    {
        $user = User::withTrashed()
            ->where('email', $payload['email'])
            ->first();

        if ($user) {
            if (! is_null($user->deleted_at)) {
                throw ValidationException::withMessages(['email' => ['The email belongs to an existing user that has been deleted. If this is you please contact PickStar.']]);
            }
        }
    }
}

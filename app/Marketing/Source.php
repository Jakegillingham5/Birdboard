<?php

namespace Pickstar\Marketing;

use Pickstar\GAParse;
use Pickstar\Http\Middleware\UTMTracker;
use Pickstar\Providers\UTMServiceProvider;
use Pickstar\User\ActiveCampaignUser;
use Pickstar\User\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Source extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    public $table = 'marketing_source';

    /**
     * Enable timestamps.
     *
     * @var boolean
     */
    public $timestamps = true;

    /**
     * Fillable columns.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'campaign_source',
        'campaign_name',
        'campaign_medium',
        'campaign_content',
        'campaign_term',
        'campaign_admin_source',
        'user_id',
        'request_id',
        'sendgrid_stage',
        'existing_user',
        'collection_location'
    ];

    /**
     * Admin source constants.
     *
     * @var string
     */

    const ADMIN_SOURCE_FACEBOOK_ADS         = 'facebook_ads';
    const ADMIN_SOURCE_GOOGLE               = 'google';
    const ADMIN_SOURCE_LINKEDIN             = 'linkedIn';
    const ADMIN_SOURCE_INSTAGRAM            = 'instagram';
    const ADMIN_SOURCE_NEWSLETTER           = 'newsletter';
    const ADMIN_SOURCE_TELEVISION           = 'television';
    const ADMIN_SOURCE_REFERRAL             = 'referral';
    const ADMIN_SOURCE_PREVIOUS_CLIENT      = 'previous_client';
    const ADMIN_SOURCE_INFLUENCER           = 'influencer';
    const ADMIN_SOURCE_OUTBOUND             = 'outbound';

    public static function updateSourceTableWhenCreatedRecently($email, $request_id, $user_id) {

        $sendgrid_stage = null;

        if ($marketing_email = Email::where('email', $email)->first()) {
            $sendgrid_stage = $marketing_email->campaign_stage;
        }

        $row = self::where('email', $email)->orderBy('created_at')->first();

        if ($row) {
            $row->update([
                'sendgrid'      => $sendgrid_stage,
                'request_id'    => $request_id,
                'user_id'       => $user_id
            ]);
        }
    }

    public static function insertOrUpdateWithGA($email, $admin_source = null, $request_id = null, $user_id = null, $existing_user = false, $campaign = null) {
        /* @var UTMServiceProvider $tracker */

        $tracker = app()->getProvider(UTMServiceProvider::class);
        if($campaign === null){
            $campaign = $tracker::campaign();
        }
        if ($campaign === null) {
            $request  = request();
            $tracker::storeUtm($request);
            $campaign = $tracker::campaign();
        }

        $sourceParams = [
            'email'             => $email,
            'campaign_name'     => $campaign,
            'campaign_source'   => $tracker::source(),
            'campaign_medium'   => $tracker::medium(),
            'campaign_content'  => $tracker::content(),
            'campaign_term'     => $tracker::term(),
            'collection_location'     => $tracker::collectionLocation(),
            'request_id'        => $request_id,
            'user_id'           => $user_id,
            'existing_user'     => $existing_user,
            'campaign_admin_source' => $admin_source,
        ];

        self::firstOrCreate($sourceParams);

    }
}

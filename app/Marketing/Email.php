<?php

namespace Pickstar\Marketing;

use Pickstar\User\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Email extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    public $table = 'marketing_emails';

    /**
     * Enable timestamps.
     *
     * @var boolean
     */
    public $timestamps = true;

    /**
     * Fillable columns.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'first_name',
        'last_name',
        'campaign_stage',
        'collection_form',
        'collection_page',
        'campaign_last_sent_date'
    ];

    /**
     * Date columns.
     *
     * @var array
     */
    protected $dates = [
        'campaign_last_sent_date'
    ];

    /**
     * Type-casted columns.
     *
     * @var array
     */
    protected $casts = [
        'campaign_stage' => 'int'
    ];

    /**
     * Marketing email record may belong to a user record.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'email', 'email');
    }

    public static function extractNamesFromString($string)
    {
        $names = explode(" ", $string, 2);
        $first_name = "";
        $last_name = "";
        if (array_key_exists(0, $names)) {
            $first_name = $names[0];
        }

        if (array_key_exists(1, $names)) {
            $last_name = $names[1];
        }

        return [$first_name, $last_name];

    }
}

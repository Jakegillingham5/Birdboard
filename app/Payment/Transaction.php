<?php

namespace Pickstar\Payment;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Transaction extends Model
{

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'payment_transactions';

    /**
     * Enable timestamps.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Fillable columns.
     *
     * @var array
     */
    protected $fillable = [
        'payment_transaction_id',
        'amount',
        'method',
        'billing_street_line_one',
        'billing_street_line_two',
        'billing_country',
        'billing_postcode',
        'billing_state',
        'billing_suburb'
    ];

    /**
     * Type casted columns.
     *
     * @var array
     */
    protected $casts = [
        'amount' => 'float'
    ];

    const METHOD_CREDIT_CARD = 'credit_card';
    const METHOD_BANK_TRANSFER = 'bank_transfer';
    const METHOD_SYSTEM = 'system';
    const METHOD_CHEQUE = 'cheque';

    /**
     * A transaction can have many payments.
     *
     * Refer to the "payment" models "transaction" relation, as this is the inverse.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payments(): HasMany
    {
        return $this->hasMany(Payment::class);
    }
}

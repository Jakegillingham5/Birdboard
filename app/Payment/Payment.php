<?php

namespace Pickstar\Payment;

use Pickstar\User\User;
use Pickstar\Booking\Booking;
use OwenIt\Auditing\Auditable;
use Illuminate\Database\Eloquent\Model;
use Pickstar\Model\Concerns\Revisionable;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Payment extends Model implements AuditableContract
{
    use Auditable,
        Revisionable;

    const TAX_RATE = 0.1;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'payments';

    /**
     * Enable timestamps.
     *
     * @var bool
     */
    public $timetamps = true;

    /**
     * Fillable columns.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'booking_id',
        'type',
        'status',
        'amount',
        'due_date',
        'sent_date',
        'paid_date',
        'method',
    ];

    /**
     * Date columns.
     *
     * @var array
     */
    protected $dates = [
        'due_date',
        'sent_date',
        'paid_date'
    ];

    /**
     * Type casted columns.
     *
     * @var array
     */
    protected $casts = [
        'amount' => 'float'
    ];

    /**
     * Dynamic columns appended.
     *
     * @var array
     */
    protected $appends = [
        'total_inclusive',
    ];


    /**
     * Revisionable formatted field names.
     *
     * @return array
     */
    public function getRevisionFormattedFieldNamesAttribute()
    {
        return [
            'status' => 'Payment Status',
            'amount' => 'Amount',
            'sent_date' => 'Payment Sent Date',
            'paid_date' => 'Payment Paid Date'
        ];
    }

    /**
     * Revisionable formatted fields.
     *
     * @return array
     */
    public function getRevisionFormattedFieldsAttribute()
    {
        return [
            'paid_date' => 'datetime:d/m/Y @ H:i:s',
            'sent_date' => 'datetime:d/m/Y @ H:i:s'
        ];
    }

    /**
     * Columns not to keep revision of.
     *
     * @var array
     */
    protected $dontKeepRevisionOf = [
        'transaction_id',
        'due_date'
    ];

    /**
     * Payment status contsants.
     *
     * @var string
     */
    const STATUS_UNPAID = 'unpaid';
    const STATUS_SENT = 'sent';
    const STATUS_PAID = 'paid';

    /**
     * Payment type constants.
     *
     * @var string
     */
    const TYPE_DEPOSIT  = 'deposit';
    const TYPE_FINAL    = 'final';
    const TYPE_URGENT   = 'urgent';

    /**
     * A payment belongs to a user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() : BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * A payment belongs to a booking.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function booking() : BelongsTo
    {
        return $this->belongsTo(Booking::class);
    }

    /**
     * A payment belongs to a transaction.
     *
     * Generally a payment would have one transaction. In this case, both the deposit and final payments
     * could be paid during a single transaction, hence the transaction cannot contain the foreign key
     * and so the relationship is reveresed.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function transaction(): BelongsTo
    {
        return $this->belongsTo(Transaction::class);
    }

    /**
     * Scope only the deposit payments.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public function scopeDeposit($query)
    {
        $query->where('type', static::TYPE_DEPOSIT);
    }

    /**
     * Scope only the final payments.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public function scopeFinal($query)
    {
        $query->where('type', static::TYPE_FINAL);
    }

    /**
     * Scope only the paid payments.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public function scopePaid($query)
    {
        $query->where('status', static::STATUS_PAID);
    }

    /**
     * Scope only the unpaid payments.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public function scopeUnpaid($query)
    {
        $query->where('status', static::STATUS_UNPAID);
    }

    public function getTotalInclusiveAttribute()
    {
        return $this->amount + ($this->amount * $this->getTaxRate());
    }

    protected function getTaxRate()
    {
        return static::TAX_RATE;
    }

    public static function estimateFees($amount)
    {
        return round(($amount + 0.30) * 0.013, 2);
    }
}

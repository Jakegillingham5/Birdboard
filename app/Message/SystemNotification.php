<?php

namespace Pickstar\Message;

use Pickstar\User\User;
use Pickstar\Booking\Booking;
use OwenIt\Auditing\Auditable;
use Illuminate\Database\Eloquent\Model;
use Pickstar\Model\Concerns\Transformable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class SystemNotification extends Model implements AuditableContract
{
    use Auditable,
        Transformable;

    protected $table = 'notifications';

    /**
     * Fields that are mass assignable
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'link',
        'read'
    ];

    /**
     * Type constants
     *
     * @var string
     */
    const TYPE_TALENT_APPLIED = 'talent_applied';
    const TYPE_EVENT_BOOKED = 'event_booked';
    const TYPE_APPLIED_TALENT_DECLINED = 'applied_talent_declined';
    const TYPE_BANK_TRANSFER_PAYMENT_RECEIVED = 'bank_transfer_payment_received';
    const TYPE_BOOKING_CANCELLED = 'booking_cancelled';
    const TYPE_BOOKING_COMPLETED = 'booking_completed';
    const TYPE_BOOKING_ACCEPTED = 'booking_accepted';
    const TYPE_BOOKING_CREATED = 'booking_created';
    const TYPE_REQUEST_CREATED = 'request_created';
    const TYPE_REQUEST_VETTED = 'request_vetted';
    const TYPE_DEPOSIT_WARNING = 'deposit_warning';
    const TYPE_FINAL_WARNING = 'final_warning';
    const TYPE_TALENT_CONFIRMED_ATTENDANCE = 'talent_confirmed_attendance';
    const TYPE_ACCOUNT_ACTIVATED = 'account_activated';
    const TYPE_REQUEST_TO_BE_REVIEWED = 'request_to_be_reviewed';
    const TYPE_TALENT_ACCEPTED = 'talent_accepted';
    const TYPE_TALENT_DEPOSIT_PAID = 'talent_deposit_paid';
    const TYPE_BOOKING_DATE_CHANGE = 'booking_date_change';
    const TYPE_TALENT_FINAL_REMINDER = 'talent_final_reminder';
    const TYPE_TALENT_SECOND_REMINDER = 'talent_second_reminder';
    const TYPE_TALENT_INITIAL_REMINDER = 'talent_initial_reminder';
    const TYPE_TALENT_PAYMENT_SENT = 'talent_payment_sent';
    const TYPE_SHORTLISTED_FOR_REQUEST = 'shortlisted_for_request';

    /**
     * Link constants
     *
     * @var string
     */
    const LINK_REQUESTS_SHOW = 'requests.show';
    const LINK_BOOKINGS_SHOW = 'bookings.show';
    const LINK_BOOKINGS_PAYMENT = 'bookings.show.payment';
    const LINK_VETTING_SHOW = 'vetting.show';
    const LINK_PROFILE_SHOW = 'profile.show';


    /**
     * A notification belongs to a user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * A message can belong to a booking
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function booking()
    {
        return $this->belongsTo(Booking::class);
    }
}

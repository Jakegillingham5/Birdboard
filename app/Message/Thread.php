<?php

namespace Pickstar\Message;

use Pickstar\User\User;
use Pickstar\Booking\Booking;
use OwenIt\Auditing\Auditable;
use Illuminate\Database\Eloquent\Model;
use Pickstar\Model\Concerns\Transformable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Thread extends Model implements AuditableContract
{
    use Auditable,
        Transformable;

    protected $fillable = [
        'active',
        'booking_id'
    ];

    /**
     * A thread can have many messages
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages() {
        return $this->hasMany(Message::class, 'thread_id');
    }

    /**
     * A thread can have many participants
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function participants() {
        return $this->belongsToMany(User::class, 'participants', 'thread_id', 'user_id')
            ->using(ParticipantsPivot::class)
            ->as('thread_participation')
            ->withPivot('message_id', 'pinned', 'terms_accepted', 'thread_id');
    }

    /**
     * A thread can have many participants that are clients
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function clients() {
        return $this->participants()->where('role', '=', 'client');
    }

    /**
     * A thread can have many participants that are talent
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function talents() {
        return $this->participants()->where('role', '=', 'talent');
    }

    /**
     * A thread belongs to a booking
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function booking() {
        return $this->belongsTo(Booking::class, 'booking_id');
    }

    /**
     * Scope a query to only the threads that a user is allowed to see
     *
     * @param \Illumiante\Database\Eloquent\Builder $query
     * @param \Pickstar\User\User $user
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOnlyMyThreads($query, User $user)
    {
        return $query->whereHas('participants', function ($query) use ($user) {
            $query->where('user_id', '=', $user->id);
        })->with(['participants' => function ($query) {
            $query->select(['user_id', 'first_name', 'last_name']);
        }]);
    }

    /**
     * Scope only active threads.
     *
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('active', '=', true);
    }

    /**
     * Scope only inactive threads.
     *
     * @return mixed
     */
    public function scopeInactive($query)
    {
        return $query->where('active', '=', false);
    }

    /**
     * Get the name of the booking that this thread belongs to
     *
     * @return mixed
     */
    public function getBookingNameAttribute()
    {
        return $this->booking->name;
    }

    /**
     * Get the latest message for the thread
     *
     * @return Model|\Illuminate\Database\Eloquent\Relations\HasMany|object|null
     */
    public function getLatestMessageAttribute()
    {
        return $this->messages()->latest()->first();
    }

    /**
     * Get the unread count for a specific user on a thread
     *
     * @param int $user_id
     * @return int
     */
    public function userUnreadCount(int $user_id)
    {
        $participant = $this->participants()->where('user_id', $user_id)->get()->first();
        if ($participant) {
            $last_read_message = $participant->thread_participation->message_id;
            return $this->messages()->where('id', '>', (string)$last_read_message)->count();
        }
        return 0;
    }

    /**
     * Return if a user has activated the thread
     *
     * @param int $user_id
     * @return boolean
     */
    public function userThreadActivated(int $user_id)
    {
        return $this->participants()->where('user_id', $user_id)->wherePivot('activated', true)->count();
    }
}

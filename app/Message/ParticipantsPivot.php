<?php

namespace Pickstar\Message;

use Pickstar\User\User;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ParticipantsPivot extends Pivot
{
    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'participants';

    protected $fillable = [
        'message_id',
        'pinned',
        'terms_accepted',
        'activated'
    ];

    /**
     * Type-casted columns.
     *
     * @var array
     */
    protected $casts = [
        'pinned' => 'bool',
        'terms_accepted' => 'bool'
    ];

    /**
     * A participants is in a thread
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function thread(): BelongsTo
    {
        return $this->belongsTo(Thread::class, 'thread_id');
    }

    /**
     * A participant is a user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}

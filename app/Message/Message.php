<?php

namespace Pickstar\Message;

use Pickstar\User\User;
use OwenIt\Auditing\Auditable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Message extends Model implements AuditableContract
{
    use SoftDeletes,
        Auditable;

    /**
     * Fields that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['text'];

    protected $appends = [
        'sender_image'
    ];

    /**
     * A message belong to a user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * A message is sent in a thread
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function thread()
    {
        return $this->belongsTo(Thread::class);
    }

    public function getSenderImageAttribute()
    {
        return $this->user->profile_image_url;
    }
}

<?php

namespace Pickstar\Booking;

use Pickstar\User\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Note extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'booking_notes';

    /**
     * Enable timestamps.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Fillable columns.
     *
     * @var array
     */
    protected $fillable = [
        'booking_id',
        'user_id',
        'text',
    ];

    /**
     * A note is written by a user
     * A note belongs to a user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id')
                ->withTrashed();
    }

}

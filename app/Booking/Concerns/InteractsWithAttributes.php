<?php

namespace Pickstar\Booking\Concerns;

use Exception;
use Carbon\Carbon;
use Pickstar\Booking\Booking;

trait InteractsWithAttributes
{
    /**
     * Get the formatted address.
     *
     * @return string
     */
    public function getFormattedAddressAttribute(): string
    {
        return trim(trim(strtr(':street :suburb :state :country :postcode', [
            ':street' => $this->street_address
                ? sprintf('%s,', $this->street_address)
                : null,
            ':suburb' => $this->suburb
                ? sprintf('%s,', $this->suburb)
                : null,
            ':state' => $this->state
                ? sprintf('%s,', $this->state)
                : null,
            ':country' => $this->country
                ? $this->country
                : null,
            ':postcode' => $this->postcode
                ? $this->postcode
                : null
        ])), ',');
    }

    /**
     * Get the timezone aware date as a datetime string.
     *
     * @param string $value
     *
     * @return null|string
     */
    public function getTzDateAttribute($value): ?string
    {
        if (is_null($value)) {
            return null;
        }

        try {
            return Carbon::createFromFormat('Y-m-d H:i:s', $value, $this->attributes['timezone'])
                ->toDateTimeString();
        } catch (Exception $exception) {
            report($exception);

            return $value;
        }
    }

    /**
     * Get the timezone aware date as a Carbon instance.
     *
     * @return \Carbon\Carbon
     */
    public function getTzDateInstanceAttribute(): Carbon
    {
        if (is_null($this->tz_date) || !array_key_exists('timezone', $this->attributes)) {
            return now();
        }

        try {
            return Carbon::createFromFormat('Y-m-d H:i:s', $this->tz_date, $this->attributes['timezone']);
        } catch (Exception $exception) {
            report($exception);

            return now();
        }
    }

    /**
     * Get the opportunity name based on what the opportunity type is.
     *
     * @return string
     */
    public function getOpportunityNameAttribute(): ?string
    {
        switch ($this->appearance_type) {
            case Booking::APPEARANCE_IN_PERSON:
                return $this->opportunity
                    ? $this->opportunity->name
                    : null;
            case Booking::APPEARANCE_ONLINE:
                return $this->onlineOpportunity
                    ? $this->onlineOpportunity->name
                    : null;
            case Booking::APPEARANCE_BOTH:
                return $this->opportunity && $this->onlineOpportunity
                    ? $this->opportunity->name . ' and ' . $this->onlineOpportunity->name
                    : null;
            default:
                return null;
        }
    }

    public function getUtcEndDateAttribute(): ?string
    {
        if (!$this->appearanceIsInPerson() || !$this->utc_date || !$this->duration) {
            return null;
        }

        if ($this->duration === 'Full Day') {
            return $this->utc_date->copy()->endOfDay()->toDateTimeString();
        }

        $durationHours = [
            '< 30 Minutes' => 0.25,
            '30 Minutes' => 0.5,
            '1 Hour' => 1,
            '1.5 Hours' => 1.5,
            '2 Hours' => 2,
            '2.5 Hours' => 2.5,
            '3 Hours' => 3,
            '3.5 Hours' => 3.5,
            '4 Hours' => 4
        ];

        return $this->utc_date->copy()->addHours($durationHours[$this->duration])->toDateTimeString();
    }

    /**
     * Get the budget per star as user rate as an attribute.
     *
     * @return null|float
     */
    public function getBudgetPerStarAsUserRateAttribute(): ?float
    {
        return $this->calculateBudgetPerStarAsUserRate();
    }

    /**
     * Determine if appearance type is "in person" or "both".
     *
     * @return bool
     */
    public function appearanceIsInPerson(): bool
    {
        return $this->appearance_type === 'in_person' ||
            $this->appearance_type === 'both';
    }

    /**
     * Determine if appearance type is "online" or "both".
     *
     * @return bool
     */
    public function appearanceIsOnline(): bool
    {
        return $this->appearance_type === 'online' ||
            $this->appearance_type === 'both';
    }

    /**
     * Determine if booking type is booking.
     *
     * @return bool
     */
    public function typeIsBooking(): bool
    {
        return $this->type === Booking::TYPE_BOOKING;
    }

    /**
     * Determine if booking type is request.
     *
     * @return bool
     */
    public function typeIsRequest(): bool
    {
        return $this->type === Booking::TYPE_REQUEST;
    }

    /**
     * Determine if booking status is vetting.
     *
     * @return bool
     */
    public function statusIsVetting(): bool
    {
        return $this->status === static::STATUS_VETTING;
    }
}

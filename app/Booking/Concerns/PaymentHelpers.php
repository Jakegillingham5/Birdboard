<?php

namespace Pickstar\Booking\Concerns;

use Pickstar\Booking\Booking;
use Pickstar\Events\FinalPaymentPaid;
use Pickstar\Payment\Payment;
use Pickstar\Payment\Transaction;
use Pickstar\User\User;

trait PaymentHelpers
{
    /**
     * Calculate the budget per star as a user rate.
     *
     * This uses the following to determine the rate:
     *
     * 1. The bookings individual commission rate if provided.
     * 2. Logged in talents managers commission rate.
     * 3. Logged in talents commission rate.
     * 4. Logged in managers commission rate.
     * 5. System default commission rate.
     *
     * Worth noting is that all commissions are how much PickStar makes on a booking, to get how much
     * the user makes we need to subtract the PickStar commission from 100.
     *
     * @param \Pickstar\User\User $user
     *
     * @return float|null
     */
    public function calculateBudgetPerStarAsUserRate(User $user = null): ?float
    {
        if (! isset($this->attributes['budget_per_star']) || is_null($this->attributes['budget_per_star'])) {
            return null;
        }

        $user = $user ?? request()->user();

        $commissionTypeKey = $this->attributes['commission_type'] ?? Booking::COMMISSION_TYPE_RATE;
        $commissionType = "commission_{$commissionTypeKey}";

        $commission = system_setting($commissionType);

        if (!is_null($this->{$commissionType})) {
            $commission = $this->{$commissionType};
        } elseif ($user && $user->isTalent()) {
            if ($user->management && $user->management->{$commissionType}) {
                $commission = $user->management->{$commissionType};
            } elseif ($user->{$commissionType}) {
                $commission = $user->{$commissionType};
            }
        } elseif ($user && $user->isManager()) {
            if (optional($user->parent)->{$commissionType}) {
                $commission = optional($user->parent)->{$commissionType};
            } elseif ($user->{$commissionType}) {
                $commission = $user->{$commissionType};
            }
        }

        $calculation = [
            Booking::COMMISSION_TYPE_RATE => function ($commission, $budget) {
                return round(((100 - $commission) / 100) * $budget, 2);
            },
            Booking::COMMISSION_TYPE_DOLLAR => function ($commission, $budget) {
                return round($budget - $commission, 2);
            },
        ];

        return $calculation[$commissionTypeKey]($commission, $this->attributes['budget_per_star']);
    }

    /**
     * Update a bookings payments if the budget was changed.
     *
     * This will also update payments if the booking request does not exist yet.
     *
     * By default this will also save the booking.
     *
     * @param bool $save
     *
     * @return void
     */
    public function updatePaymentsIfBudgetChanged($save = true): void
    {
        if ($this->exists && ! $this->wasChanged('budget_per_star')) {
            return;
        }

        $this->updatePayments($save);
    }

    /**
     * Update the booking payments.
     *
     * By default this will also save the booking.
     *
     * @param bool $save
     *
     * @return void
     */
    public function updatePayments($save = true): void
    {
        // Update the bookings stamped budget that will be used for reporting and queries. This value
        // is always updated even if payments are paid as it is used in reporting and queries and
        // we will always have the tracked payment totals via the transactions.
        $this->fill(['budget' => $this->talentAttending->count() * $this->budget_per_star]);

        $deposit = $this->payments->firstWhere('type', Payment::TYPE_DEPOSIT);
        $final = $this->payments->firstWhere('type', Payment::TYPE_FINAL);

        // Recalculate deposit amount if it is unpaid. This is calculated based on the deposit
        // payment percentage system setting.
        if ($deposit->status === Payment::STATUS_UNPAID) {
            $deposit->fill([
                'amount' => round((system_setting('deposit_payment') / 100) * $this->budget, 2)
            ])->save();
        }

        // Recalculate final amount if it is unpaid. This is the remaining amount from the
        // budget minus the deposit amount.
        if ($final->status === Payment::STATUS_UNPAID) {
            $final->fill([
                'amount' => $this->budget - $deposit->amount
            ]);

            // If the final amount ends up lower than zero then we'll set its amount back to
            // zero and set its status to paid. This is usually the result of removing
            // talent from a booking and ending up with a budget that is lower than
            // what was initially calculated. The resulting transaction is created
            // with the "system" method to indicate it was paid from here. Note
            // all refunds at this stage are handled external to the system.
            if ($final->amount <= 0) {
                $final->fill([
                    'amount' => 0,
                    'status' => Payment::STATUS_PAID,
                    'paid_date' => now()
                ]);

                $final->transaction()->create([
                    'amount' => 0,
                    'method' => Transaction::METHOD_SYSTEM
                ]);

                event(new FinalPaymentPaid($this));
            }

            $final->save();
        }

        if ($save) {
            if ($this->wasChanged('budget') || $this->isDirty('budget')) {
                $this->save();
            }
        }
    }
}

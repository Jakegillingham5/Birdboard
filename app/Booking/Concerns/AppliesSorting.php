<?php

namespace Pickstar\Booking\Concerns;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

trait AppliesSorting
{
    /**
     * Scope to apply sorting to a booking with a default sorting callback.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \Illuminate\Http\Request $request
     * @param \Closure $defaultSortingCallback
     *
     * @return void
     */
    public function scopeApplySorting(
        Builder $query,
        Request $request,
        Closure $defaultSortingCallback,
        string $dateColumn,
        Closure $customDateSortingCallback = null,
        Closure $customPriceSortingCallback = null,
        Closure $customCreatedAtSortingCallback = null,
        Closure $customCompletedDateSortingCallback = null,
        Closure $customScheduledDateSortingCallback = null,
        Closure $customNotesCreatedAtSortingCallback = null
    ): void {
        if ($request->filled('sort')) {
            switch ($request->query('sort')) {
                case 'notes.created_at':
                    if (is_callable($customNotesCreatedAtSortingCallback)) {
                        $customNotesCreatedAtSortingCallback($query, $request);
                    } else {
                        $query->when($request->query('order') === 'asc', function ($query) {
                            $query->leftJoin('booking_notes', 'booking_notes.booking_id', '=', 'bookings.id')
                                ->selectRaw('bookings.*, MAX(booking_notes.created_at) AS latest_note_at')
                                ->orderBy('latest_note_at', 'asc')
                                ->orderBy('created_at', 'asc')
                                ->groupBy('bookings.id');
                        }, function ($query) {
                            $query->leftJoin('booking_notes', 'booking_notes.booking_id', '=', 'bookings.id')
                                ->selectRaw('bookings.*, MAX(booking_notes.created_at) AS latest_note_at')
                                ->orderBy('latest_note_at', 'desc')
                                ->orderBy('created_at', 'desc')
                                ->groupBy('bookings.id');
                        });
                    }
                    break;
                case 'price':
                    if (is_callable($customPriceSortingCallback)) {
                        $customPriceSortingCallback($query, $request);
                    } else {
                        $query->when($request->query('order') === 'asc', function ($query) {
                            $query->orderBy('budget_per_star', 'asc');
                        }, function ($query) {
                            $query->orderBy('budget_per_star', 'desc');
                        });
                    }
                    break;
                case 'date':
                    if (is_callable($customDateSortingCallback)) {
                        $customDateSortingCallback($query, $request);
                    } else {
                        $query->when($request->query('order') === 'asc', function ($query) use ($dateColumn) {
                            $query->oldest($dateColumn);
                        }, function ($query) use ($dateColumn) {
                            $query->latest($dateColumn);
                        });
                    }
                    break;
                case 'created_at':
                    if (is_callable($customCreatedAtSortingCallback)) {
                        $customCreatedAtSortingCallback($query, $request);
                    } else {
                        $query->when($request->query('order') === 'asc', function ($query) {
                            $query->oldest('created_at');
                        }, function ($query) {
                            $query->latest('created_at');
                        });
                    }
                    break;
                case 'completed_date':
                    if (is_callable($customCompletedDateSortingCallback)) {
                        $customCompletedDateSortingCallback($query, $request);
                    } else {
                        $query->when($request->query('order') === 'asc', function ($query) {
                            $query->oldest('completed_date');
                        }, function ($query) {
                            $query->latest('completed_date');
                        });
                    }
                    break;
                case 'scheduled_date':
                    if (is_callable($customScheduledDateSortingCallback)) {
                        $customScheduledDateSortingCallback($query, $request);
                    } else {
                        $query->when($request->query('order') === 'asc', function ($query) {
                            $query->oldest('scheduled_date');
                        }, function ($query) {
                            $query->latest('scheduled_date');
                        });
                    }
                    break;
                default:
                    break;
            }
        } else {
            $defaultSortingCallback($query);
        }
    }
}

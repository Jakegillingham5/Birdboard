<?php

namespace Pickstar\Booking\Concerns;

use Closure;
use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Pickstar\Helpers\SearchHelpers;
use Illuminate\Database\Eloquent\Builder;

trait AppliesFilters
{
    /**
     * Scope to apply filters to a booking.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function scopeApplyFilters(
        Builder $query,
        Request $request,
        string $dateColumn,
        Closure $customStatusFilterCallback = null
    ): void {
        $user = $request->user();

        if ($request->filled('amount_low')) {
            if ($user->isTalent()) {
                $query->whereRaw(
                    '`bookings`.`budget_per_star` * (
                        SELECT `talent_commission_rate`
                        FROM `booking_talent_commissions_view`
                        WHERE `booking_talent_commissions_view`.`booking_id`=`bookings`.`id`
                            AND `booking_talent_commissions_view`.`talent_id`=?
                    ) >= ?',
                    [$user->id, $request->query('amount_low')]
                );
            } elseif ($user->isManager()) {
                $query->whereRaw(
                    '`bookings`.`budget_per_star` * (
                        SELECT `talent_commission_rate`
                        FROM `booking_talent_commissions_view`
                        WHERE `booking_talent_commissions_view`.`booking_id`=`bookings`.`id`
                            AND `booking_talent_commissions_view`.`management_id`=?
                        LIMIT 1
                    ) >= ?',
                    [optional($user->parent)->id ?? $user->id, $request->query('amount_low')]
                );
            } else {
                $query->where('budget_per_star', '>=', $request->query('amount_low'));
            }
        }

        if ($request->filled('amount_high')) {
            if ($user->isTalent()) {
                $query->whereRaw(
                    '`bookings`.`budget_per_star` * (
                        SELECT `talent_commission_rate`
                        FROM `booking_talent_commissions_view`
                        WHERE `booking_talent_commissions_view`.`booking_id`=`bookings`.`id`
                            AND `booking_talent_commissions_view`.`talent_id`=?
                    ) <= ?',
                    [$user->id, $request->query('amount_high')]
                );
            } elseif ($user->isManager()) {
                $query->whereRaw(
                    '`bookings`.`budget_per_star` * (
                        SELECT `talent_commission_rate`
                        FROM `booking_talent_commissions_view`
                        WHERE `booking_talent_commissions_view`.`booking_id`=`bookings`.`id`
                            AND `booking_talent_commissions_view`.`management_id`=?
                        LIMIT 1
                    ) <= ?',
                    [optional($user->parent)->id ?? $user->id, $request->query('amount_high')]
                );
            } else {
                $query->where('budget_per_star', '<=', $request->query('amount_high'));
            }
        }

        if ($request->filled('date_from')) {
            try {
                if ($dateColumn === 'created_at') {
                    $query->where($dateColumn, '>=', Carbon::createFromFormat('Y-m-d', $request->query('date_from'))->startOfDay()->addMinutes($request->get('timezone')));
                } else {
                    $query->where($dateColumn, '>=', Carbon::createFromFormat('Y-m-d', $request->query('date_from'))->startOfDay());
                }
            } catch (Exception $exception) {
                report($exception);
            }
        }

        if ($request->filled('date_to')) {
            try {
                if ($dateColumn === 'created_at') {
                    $query->where($dateColumn, '<=', Carbon::createFromFormat('Y-m-d', $request->query('date_to'))->endOfDay()->addMinutes($request->get('timezone')));
                } else {
                    $query->where($dateColumn, '<=', Carbon::createFromFormat('Y-m-d', $request->query('date_to'))->endOfDay());
                }
            } catch (Exception $exception) {
                report($exception);
            }
        }

        if (is_callable($customStatusFilterCallback)) {
            $query->where(function ($query) use ($customStatusFilterCallback, $request) {
                $customStatusFilterCallback($query, $request);
            });
        }

        if ($request->filled('locations')) {
            $query->whereIn('state', (array) $request->query('locations'));
        }

        if ($request->filled('opportunities')) {
            $query->where(function ($query) use ($request) {
                $query->whereIn('opportunity_id', (array) $request->query('opportunities'))
                    ->orWhereIn('online_opportunity_id', (array) $request->query('opportunities'));
            });
        }

        if ($request->filled('talent')) {
            $query->whereHas('talent', function ($query) use ($user, $request) {
                $query->managedBy($user)
                    ->whereIn('users.id', (array) $request->query('talent'));
            });
        }

        if ($request->filled('keywords')) {
            $query->where(function ($query) use ($request) {
                $query->whereHas('client', function ($query) use ($request) {
                    $sanitizedKeywords = SearchHelpers::sanitizeSearchString($request->query('keywords'));
                    $query->where(\DB::raw('concat(first_name, " ", last_name)'), 'like', '%' . $sanitizedKeywords . '%');
                });

                $query->orWhere('name', 'LIKE', "%{$request->query('keywords')}%");
            });
        }

        if ($request->filled('id')) {
            $query->where('id', $request->query('id'));
        }

        // Filter by assigned admins
        $assigned_admins = $request->input('assigned_admin');

        if ($user->isAdmin() && $assigned_admins !== null) {
            $query->where(function ($query) use ($request, $assigned_admins) {
                $query->whereIn('assigned_admin', $request->query('assigned_admin'));

                // Unassigned admin is represented as 0 in frontend and null in backend.
                if (in_array('0', $assigned_admins)) {
                    $query->orWhere('assigned_admin', null);
                }
            });
        }
    }
}

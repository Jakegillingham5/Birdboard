<?php

namespace Pickstar\Booking;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'booking_notifications';

    /**
     * Enable timestamps.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Fillable columns.
     *
     * @var array
     */
    protected $fillable = [
        'talent_unconfirmed',
        'deposit_payment_warning_client',
        'deposit_payment_warning_admin',
        'urgent_payment_warning_admin',
        'urgent_payment_warning_client',
        'request_expiring',
        'request_expired',
        'initial_talent_reminder',
        'second_talent_reminder',
        'final_talent_reminder',
        'final_payment_warning_admin',
        'final_payment_warning_client',
        'one_week_no_applicants',
        'initial_talent_reminder_online',
        'final_talent_reminder_online',
        'admin_reminder_online',
        'mark_as_complete_initial_in_person_reminder',
        'mark_as_complete_initial_online_reminder',
        'mark_as_complete_in_person_auto_fill',
        'mark_as_complete_online_auto_fill',
        'one_week_no_applicants',
        'running_hot',
    ];

    /**
     * Type casted columns.
     *
     * @var array
     */
    protected $casts = [
        'talent_unconfirmed' => 'bool',
        'deposit_payment_warning_client' => 'bool',
        'deposit_payment_warning_admin' => 'bool',
        'urgent_payment_warning_admin' => 'bool',
        'urgent_payment_warning_client' => 'bool',
        'request_expiring' => 'bool',
        'request_expired' => 'bool',
        'initial_talent_reminder' => 'bool',
        'second_talent_reminder' => 'bool',
        'final_talent_reminder' => 'bool',
        'final_payment_warning_admin' => 'bool',
        'final_payment_warning_client' => 'bool',
        'one_week_no_applicants' => 'bool',
        'initial_talent_reminder_online' => 'bool',
        'final_talent_reminder_online' => 'bool',
        'admin_reminder_online' => 'bool',
        'mark_as_complete_initial_in_person_reminder' => 'bool',
        'mark_as_complete_initial_online_reminder' => 'bool',
        'mark_as_complete_in_person_auto_fill' => 'bool',
        'mark_as_complete_online_auto_fill' => 'bool',
        'one_week_no_applicants' => 'bool',
        'running_hot' => 'bool',
    ];
}

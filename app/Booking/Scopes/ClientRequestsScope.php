<?php

namespace Pickstar\Booking\Scopes;

use Pickstar\Booking\Booking;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class ClientRequestsScope extends RequestsScope
{
    /**
     * Constrain query to client requests.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return void
     */
    public function apply(Builder $query, Model $model)
    {
        $query->requests()
            ->where('client_id', $this->request->user()->id)
            ->when($this->request->query('status', 'active'), function ($query, $status) {
                switch ($status) {
                    case 'active':
                        // Active booking requests are those that are NOT EXPIRED and where the status of the
                        // booking IS NOT withdrawn or declined.
                        $query->isNotExpired()
                            ->whereNotIn('bookings.status', [
                                Booking::STATUS_WITHDRAWN,
                                Booking::STATUS_DECLINED
                            ]);
                        break;

                    case 'archived':
                        $query->where(function ($query) {
                            $query->isExpired()
                                ->orWhereNotIn('bookings.status', [
                                    Booking::STATUS_PENDING,
                                    Booking::STATUS_VETTING
                                ]);
                        });
                        break;
                }
            });
    }
}

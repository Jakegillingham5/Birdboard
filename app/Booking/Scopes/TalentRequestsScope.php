<?php

namespace Pickstar\Booking\Scopes;

use Pickstar\Booking\Booking;
use Pickstar\Booking\TalentPivot;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class TalentRequestsScope extends RequestsScope
{
    /**
     * Constrain query to talent requests.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return void
     */
    public function apply(Builder $query, Model $model)
    {
        $query->withoutVetting()
            ->with(['talent' => function ($query) {
                $query->onlyMyselfIfTalent($this->request->user());
            }])
            ->join('booking_talent', function ($join) {
                $join->on('booking_talent.booking_id', '=', 'bookings.id')
                    ->where('booking_talent.status', '<>', TalentPivot::STATUS_HIDDEN)
                    ->where('booking_talent.talent_id', $this->request->user()->id);
            })
            ->orderByRaw('FIELD(booking_talent.status, ?, ?, ?, ?) ASC', [
                TalentPivot::STATUS_PENDING,
                TalentPivot::STATUS_APPLIED,
                TalentPivot::STATUS_DECLINED,
                TalentPivot::STATUS_HIDDEN
            ])
            ->when($this->request->query('status', 'active'), function ($query, $status) {
                switch ($status) {
                    case 'active':
                        // Active booking requests are those that are NOT EXPIRED and where the status of the
                        // booking IS NOT withdrawn or declined.
                        $query->requests()
                            ->isNotExpired()
                            ->whereNotIn('bookings.status', [
                                Booking::STATUS_WITHDRAWN,
                                Booking::STATUS_DECLINED
                            ]);
                        break;

                    case 'archived':
                        $query->where(function ($query) {
                            $query->isExpired()
                                ->orWhereNotIn('bookings.status', [
                                    Booking::STATUS_PENDING,
                                    Booking::STATUS_VETTING,
                                    Booking::STATUS_DECLINED
                                ]);
                        });

                        $query->where(function ($query) {
                            // We also need to ensure that withdrawn requests are NOT shown UNLESS the request
                            // was vetted before it was withdrawn. This is indicated by the request having
                            // a status of withdrawn and a vetted date.
                            $query->where(function ($query) {
                                $query->where('bookings.type', Booking::TYPE_REQUEST)
                                    ->where('bookings.status', '<>', Booking::STATUS_WITHDRAWN)
                                    ->orWhere(function ($query) {
                                        $query->where('bookings.status', Booking::STATUS_WITHDRAWN)
                                            ->whereNotNull('bookings.vetted_date');
                                    });
                            });

                            // We also want to include any requests that progressed to a booking where the talent
                            // had applied or declined by the client never accepted them.
                            $query->orWhere(function ($query) {
                                $query->where('booking_talent.client_accepted', false)
                                    ->whereIn('booking_talent.status', [
                                        TalentPivot::STATUS_APPLIED,
                                        TalentPivot::STATUS_DECLINED
                                    ]);
                            });
                        });
                        break;
                }
            });
    }
}

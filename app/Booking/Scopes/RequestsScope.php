<?php

namespace Pickstar\Booking\Scopes;

use Illuminate\Http\Request;
use Pickstar\Booking\Booking;
use Illuminate\Database\Eloquent\Scope;

abstract class RequestsScope implements Scope
{
    /**
     * Request instance.
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * Create a new talent scope instance.
     *
     * @param \Illuminate\Http\Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }
}

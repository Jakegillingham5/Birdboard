<?php

namespace Pickstar\Booking\Scopes;

use Pickstar\Booking\Booking;
use Pickstar\Booking\TalentPivot;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class AdminRequestsScope extends RequestsScope
{
    /**
     * Constrain query to admin requests.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return void
     */
    public function apply(Builder $query, Model $model)
    {
        $query->requests()
            ->withoutVetting()
            ->withCount(['talent as talent_response_count' => function ($query) {
                $query->where('booking_talent.status', TalentPivot::STATUS_APPLIED);
            }])
            ->when($this->request->query('clientId'), function ($query, $client) {
                $query->where('client_id', $client);
            })
            ->when($this->request->query('managerId'), function ($query, $manager) {
                $query->whereHas('talent', function ($query) use ($manager) {
                    $query->where('management_id', $manager);
                });
            })
            ->when($this->request->query('status', 'event'), function ($query, $status) {
                $query->where(function ($query) use ($status) {
                    switch ($status) {
                        case 'active':
                            $query->isNotExpired()
                                ->whereNotIn('bookings.status', [
                                    Booking::STATUS_WITHDRAWN,
                                    Booking::STATUS_DECLINED
                                ]);
                            break;

                        case 'event':
                            // Events are active booking requests that are in person or both
                            $query->isNotExpired()
                                ->whereNotIn('bookings.status', [
                                    Booking::STATUS_WITHDRAWN,
                                    Booking::STATUS_DECLINED
                                ])
                                ->whereIn('bookings.appearance_type', [
                                    Booking::APPEARANCE_BOTH,
                                    Booking::APPEARANCE_IN_PERSON
                                ]);
                            break;

                        case 'campaign':
                            // Campaigns are active booking requests that are online or both
                            // Active booking requests are those that are NOT EXPIRED and where the status of the
                            // booking IS NOT withdrawn or declined.
                            $query->isNotExpired()
                                ->whereNotIn('bookings.status', [
                                    Booking::STATUS_WITHDRAWN,
                                    Booking::STATUS_DECLINED
                                ])
                                ->whereIn('bookings.appearance_type', [
                                    Booking::APPEARANCE_BOTH,
                                    Booking::APPEARANCE_ONLINE
                                ]);
                            break;

                        case 'archived':
                            $query->isExpired()
                                ->orWhereNotIn('bookings.status', [
                                    Booking::STATUS_PENDING,
                                    Booking::STATUS_VETTING
                                ]);
                            break;
                    }
                });
            });
    }
}

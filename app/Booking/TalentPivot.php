<?php

namespace Pickstar\Booking;

use Pickstar\User\User;
use Pickstar\Booking\Booking;
use OwenIt\Auditing\Auditable;
use Pickstar\Model\Concerns\Revisionable;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class TalentPivot extends Pivot implements AuditableContract
{
    use Auditable;


    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    /**
     * Auditable events.
     *
     * @var array
     */
    protected $auditEvents = [
        'created',
        'updated',
    ];

    /**
     * Type-casted columns.
     *
     * @var array
     */
    protected $casts = [
        'priority' => 'int',
        'shortlisted' => 'bool',
        'client_accepted' => 'bool',
        'attendance_confirmed' => 'bool',
        'expired' => 'bool',
        'talent_paid_notif_sent' => 'bool'
    ];

    /**
     * Date columns.
     *
     * @var array
     */
    protected $dates = [
        'applied_date',
        'date_applied_for',
        'online_completed_date',
        'in_person_completed_date',
    ];

    /**
     * Revisionable formatted field names.
     *
     * @return array
     */
    public function getRevisionFormattedFieldNamesAttribute()
    {
        return [
            'client_accepted' => 'Client Accepted',
            'attendance_confirmed' => 'Confirmed Attendance',
            'online_participation_confirmed' => 'Confirmed Online Participation',
        ];
    }

    /**
     * Revisionable formatted fields.
     *
     * @return array
     */
    public function getRevisionFormattedFieldsAttribute()
    {
        return [
            'client_accepted' => 'boolean:No|Yes',
            'attendance_confirmed' => 'boolean:No|Yes',
            'expired' => 'boolean:No|Yes',
            'online_participation_confirmed' => 'boolean:No|Yes',
        ];
    }

    /**
     * Columns not to keep revision of.
     *
     * @var array
     */
    protected $dontKeepRevisionOf = [
        'status',
        'priority',
        'shortlisted',
        'decline_reason',
        'attendance_access_token',
        'applied_date',
        'xero_bill_reference',
        'xero_deposit_invoice_reference',
        'xero_final_invoice_reference',
    ];

    /**
     * Disable timestamps.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'booking_talent';

    /**
     * Status constants.
     *
     * @var string
     */
    const STATUS_HIDDEN = 'hidden';
    const STATUS_PENDING = 'pending';
    const STATUS_APPLIED = 'applied';
    const STATUS_DECLINED = 'declined';
    const STATUS_CANCELLED = 'cancelled';
    const STATUS_REPLACED = 'replaced';
    const STATUS_REMOVED = 'removed';
    const STATUS_UNSUCCESSFUL = 'unsuccessful';
    const STATUS_REVIEWING = 'reviewing';

    /**
     * Decline reason constants.
     *
     * @var string
     */
    const DECLINE_BUDGET = 'budget';
    const DECLINE_SCHEDULING = 'scheduling';
    const DECLINE_INFORMATION = 'information';
    const DECLINE_INTEREST = 'interest';
    const DECLINE_MANAGER = 'manager';
    const DECLINE_PICKSTAR = 'pickstar';
    const DECLINE_APP = 'app';

    /**
     * A talent pivot belongs to a talent.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function talent(): BelongsTo
    {
        return $this->belongsTo(User::class, 'talent_id');
    }

    /**
     * A talent pivot belongs to a booking.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function booking(): BelongsTo
    {
        return $this->belongsTo(Booking::class, 'booking_id')
            ->everything();
    }
}

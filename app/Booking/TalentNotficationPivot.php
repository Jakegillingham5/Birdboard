<?php

namespace Pickstar\Booking;

use Illuminate\Database\Eloquent\Relations\Pivot;

class TalentNotificationPivot extends Pivot
{
    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'booking_talent_notifications';

    /**
     * Enable timestamps.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = null;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
}

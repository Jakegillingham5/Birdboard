<?php

namespace Pickstar\Booking;

use Pickstar\Tag\Tag;
use Pickstar\User\User;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class TalentTagPivot extends Pivot
{
    /**
     * Type-casted columns.
     *
     * @var array
     */
    protected $casts = [
        'retired' => 'bool'
    ];

    /**
     * Revisionable formatted field names.
     *
     * @return array
     */
    public function getRevisionFormattedFieldNamesAttribute()
    {
        return [
            'retired' => 'Retired'
        ];
    }

    /**
     * Revisionable formatted fields.
     *
     * @return array
     */
    public function getRevisionFormattedFieldsAttribute()
    {
        return [
            'retired' => 'boolean:No|Yes'
        ];
    }

    /**
     * Disable timestamps.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'talent_tags';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * A talent tag pivot belongs to a talent.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function talent(): BelongsTo
    {
        return $this->belongsTo(User::class, 'talent_id');
    }

    /**
     * A talent pivot belongs to a tag.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tag(): BelongsTo
    {
        return $this->belongsTo(Tag::class, 'tag_id')
            ->everything();
    }
}

<?php

namespace Pickstar\Booking;

use Exception;
use Illuminate\Support\Collection;
use Pickstar\Audit;
use Carbon\Carbon;
use Pickstar\Events\BookingRequestCompleteByDateChanged;
use Pickstar\Helpers\ArrayHelper;
use Pickstar\Location\Location;
use Pickstar\Tag\Tag;
use Pickstar\User\User;
use Pickstar\Booking\Note;
use Pickstar\Message\Thread;
use Illuminate\Http\Request;
use Pickstar\Payment\Payment;
use OwenIt\Auditing\Auditable;
use Pickstar\Marketing\Source;
use Shivella\Bitly\Facade\Bitly;
use Illuminate\Cache\TaggableStore;
use Pickstar\Events\BookingCompleted;
use Illuminate\Support\Facades\Cache;
use Pickstar\Opportunity\Opportunity;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Builder;
use Pickstar\Events\BookingTermsAccepted;
use Pickstar\Model\Concerns\Revisionable;
use Pickstar\Model\Concerns\Transformable;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Pickstar\Events\BookingRequestEventDateChanged;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

/**
 * Class Booking
 *
 * @package Pickstar\Booking
 *
 * @property User $client
 * @property string $market_segment
 *
 */
class Booking extends Model implements AuditableContract
{
    const COMMISSION_TYPE_RATE = 'rate';
    const COMMISSION_TYPE_DOLLAR = 'dollar';

    use Auditable,
        Revisionable,
        Transformable,
        Concerns\AppliesFilters,
        Concerns\AppliesSorting,
        Concerns\PaymentHelpers,
        Concerns\InteractsWithAttributes;

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'bookings';

    /**
     * Enable timestamps.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Fillable columns.
     *
     * @var array
     */
    protected $fillable = [
        'billboard_expiry_date',
        'vetted_date',
        'client_id',
        'opportunity_id',
        'online_opportunity_id',
        'booking_notifications_id',
        'type',
        'status',
        'appearance_type',
        'name',
        'description',
        'requirements',
        'duration',
        'required_stars',
        'budget_per_star',
        'budget',
        'establishment',
        'additional_location_detail',
        'street_address',
        'suburb',
        'state',
        'country',
        'postcode',
        'latitude',
        'longitude',
        'complete_by_date',
        'date_options',
        'date',
        'tz_date',
        'utc_date',
        'timezone',
        'arrival_time',
        'athlete_start_time',
        'others_can_apply',
        'date_flexible_or_unconfirmed',
        'time_flexible_or_unconfirmed',
        'budget_not_confirmed',
        'expenses_covered',
        'promoted_publicly',
        'client_accepted_terms',
        'client_accepted_terms_date',
        'commission_rate',
        'commission_dollar',
        'commission_type',
        'social_media_content',
        'scheduled_date',
        'completed_date',
        'coupon',
        'withdraw_reason',
        'talent_expire',
        'talent_can_comment',
        'market_segment',
        'admin_opportunity_id',
        'request_form_version',
        'initial_budget_per_star',
        'initial_required_stars',
        'expires',
        'assigned_admin',
        'client_agreed_terms_on_request_creation',
        'client_agreed_terms_on_request_creation_date',
    ];

    protected $hidden = [
       'request_form_version'
    ];

    /**
     * Sensitive columns.
     *
     * @var array
     */
    protected $sensitive = [
        'date',
        'date2',
        'date3',
        'utc_date',
        'tz_date',
        'billboard_expiry_date',
        'vetted_date',
        'client_id',
        'type',
        'status',
        'commission_rate',
        'social_media_content',
        'scheduled_date',
        'completed_date',
        'client_accepted_terms',
        'client_accepted_terms_date',
        'complete_by_date',
    ];

    /**
     * Date columns.
     *
     * @var array
     */
    protected $dates = [
        'utc_date',
        'billboard_expiry_date',
        'vetted_date',
        'scheduled_date',
        'completed_date',
        'client_accepted_terms_date',
    ];

    /**
     * Type-casted columns.
     *
     * @var array
     */
    protected $casts = [
        'budget_not_confirmed' => 'bool',
        'expenses_covered' => 'bool',
        'others_can_apply' => 'bool',
        'promoted_publicly' => 'bool',
        'date_flexible_or_unconfirmed' => 'bool',
        'time_flexible_or_unconfirmed' => 'bool',
        'client_accepted_terms' => 'bool',
        'latitude' => 'float',
        'longitude' => 'float',
        'commission_rate' => 'int',
        'budget_per_star' => 'float',
        'budget' => 'float',
        'talent_expire' => 'bool',
        'talent_can_comment' => 'bool',
        'date_options' => 'array',
    ];

    /**
     * Dynamic columns appended.
     *
     * @var array
     */
    protected $appends = [
        'budget_per_star_as_user_rate',
        'opportunity_name',
        'has_date_options_only',
        'complete_by_date_formatted',
        'full_withdraw_reason_string',
        'assigned_admin_name',
    ];

    /**
     * Revisionable formatted fields.
     *
     * @return array
     */
    public function getRevisionFormattedFieldsAttribute()
    {
        return [
            'scheduled_date' => 'datetime:d/m/Y @ H:i:s',
            'completed_date' => 'datetime:d/m/Y @ H:i:s',
            'utc_date' => 'datetime:d/m/Y @ H:i:s',
            'vetted_date' => 'datetime:d/m/Y @ H:i:s',
            'athlete_start_time' => 'Athlete Start Time',
            'others_can_apply' => 'boolean:No|Yes',
            'date_flexible_or_unconfirmed' => 'boolean:No|Yes',
            'time_flexible_or_unconfirmed' => 'boolean:No|Yes',
            'budget_not_confirmed' => 'boolean:No|Yes',
            'expenses_covered' => 'boolean:No|Yes',
            'promoted_publicly' => 'boolean:No|Yes',
            'client_accepted_terms' => 'boolean:No|Yes',
            'commission_rate' => 'isEmpty:|%s%%'
        ];
    }

    /**
     * Revisionable formatted field names.
     *
     * @return array
     */
    public function getRevisionFormattedFieldNamesAttribute()
    {
        return [
            'vetted_date' => 'Vetted Date',
            'client_id' => 'Client ID',
            'opportunity_id' => 'Opportunity ID',
            'online_opportunity_id' => 'Online Opportunity ID',
            'status' => 'Booking Status',
            'appearance_type' => 'Appearance Type',
            'name' => 'Name',
            'description' => 'Description',
            'requirements' => 'Talent Requirements',
            'duration' => 'Duration',
            'required_stars' => 'Required Stars',
            'budget_per_star' => 'Budget Per Star',
            'additional_location_detail' => 'Additional Location Detail',
            'street_address' => 'Street Address',
            'suburb' => 'Suburb',
            'state' => 'State',
            'country' => 'Country',
            'postcode' => 'Postcode',
            'utc_date' => 'Date',
            'arrival_time' => 'Arrival Time',
            'athlete_start_time' => 'Athlete Start Time',
            'others_can_apply' => 'Others Can Apply',
            'date_flexible_or_unconfirmed' => 'Flexible Date or Unconfirmed',
            'time_flexible_or_unconfirmed' => 'Flexible Time or Unconfirmed',
            'budget_not_confirmed' => 'Budget Not Confirmed',
            'expenses_covered' => 'Expenses Covered',
            'promoted_publicly' => 'Promoted Publicly',
            'client_accepted_terms' => 'Client Accepted Terms',
            'client_accepted_terms_date' => 'Client Accepted Terms Date',
            'commission_rate' => 'Commission Rate',
            'social_media_content' => 'Social Media Content',
            'completed_date' => 'Completed Date',
            'scheduled_date' => 'Scheduled Date',
            'coupon' => 'Coupon',
            'admin_opportunity_id' => 'Admin Opportunity ID'
        ];
    }

    /**
     * Columns not to keep revision of.
     *
     * @var array
     */
    protected $dontKeepRevisionOf = [
        'type',
        'booking_notifications_id',
        'billboard_expiry_date',
        'budget',
        'latitude',
        'longitude',
        'tz_date',
        'date',
        'timezone',
    ];

    /**
     * Type constants.
     *
     * @var string
     */
    const TYPE_REQUEST = 'request';
    const TYPE_BOOKING = 'booking';

    /**
     * Appearance type constants.
     *
     * @var string
     */
    const APPEARANCE_IN_PERSON = 'in_person';
    const APPEARANCE_ONLINE = 'online';
    const APPEARANCE_BOTH = 'both';

    /**
     * Status constants.
     *
     * @var string
     */
    const STATUS_VETTING = 'vetting';
    const STATUS_PENDING = 'pending';
    const STATUS_PENDING_PAYMENT = 'pending_payment';
    const STATUS_WITHDRAWN = 'withdrawn';
    const STATUS_SCHEDULED = 'scheduled';
    const STATUS_BOOKED = 'booked';
    const STATUS_COMPLETED = 'completed';
    const STATUS_HOLD = 'hold';
    const STATUS_CANCELLED = 'cancelled';
    const STATUS_DECLINED = 'declined';

    /**
     * Expiring talent constants.
     *
     * @var string
     */
    const TALENT_EXPIRE = 'talent_expire';
    const TALENT_DO_NOT_EXPIRE = 'talent_do_not_expire';

    /**
     * A booking belongs to a client.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client(): BelongsTo
    {
        return $this->belongsTo(User::class, 'client_id')
            ->withTrashed();
    }

    /**
     * A booking belongs to an opportunity.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function opportunity(): BelongsTo
    {
        return $this->belongsTo(Opportunity::class);
    }

    /**
     * A booking belongs to an online opportunity.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function onlineOpportunity(): BelongsTo
    {
        return $this->belongsTo(Opportunity::class, 'online_opportunity_id');
    }

    /**
     * A booking belongs to an opportunity.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function adminOpportunity(): BelongsTo
    {
        return $this->belongsTo(Opportunity::class, 'admin_opportunity_id');
    }

    /**
     * A booking has a booking notifications record.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function notification(): BelongsTo
    {
        return $this->belongsTo(Notification::class, 'booking_notifications_id');
    }

    /**
     * A booking has many notes
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notes(): HasMany
    {
        return $this->hasMany(Note::class)->with('user');
    }


   /**
    * A booking has one source
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasOne
    */
    public function marketingSource(): HasOne
    {
        return $this->hasOne(Source::class, 'request_id');
    }

    /**
     * A booking can have many talents.
     *
     * The pivot is called "response" as the pivot is mostly used as the talents response to the booking.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function talent(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'booking_talent', 'booking_id', 'talent_id')
            ->using(TalentPivot::class)
            ->as('response')
            ->with('profile')
            ->withTimestamps()
            ->withTrashed()
            ->withPivot('id', 'shortlisted', 'priority', 'status', 'decline_reason', 'client_accepted', 'attendance_confirmed', 'applied_date', 'expired', 'expiry_date', 'comment', 'talent_paid_notif_sent','xero_bill_reference','xero_deposit_invoice_reference','xero_final_invoice_reference', 'date_applied_for', 'online_completed_date', 'in_person_completed_date', 'online_participation_confirmed');
    }

    /**
     * A booking can belong to many tags.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'booking_tags', 'booking_id', 'tag_id')
            ->using(BookingTagPivot::class);
    }

    public function talentAudits()
    {
        return $this->hasManyThrough(Audit::class, TalentPivot::class, 'booking_id', 'auditable_id', 'id');
    }

    /**
     * Alias for getting only the talent that are managed by the given user.
     *
     * For non-manager users just returns all talent.
     *
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function talentManagedBy(User $user): BelongsToMany
    {
        return $this->talent()
            ->when($user->isMasterManager(), function ($query) use ($user) {
                $query->where('users.management_id', $user->id);
            })
            ->when($user->isStaffManager(), function ($query) use ($user) {
                $query->where('users.manager_id', $user->id);
            });
    }

    /**
     * Alias for getting only the shortlisted talent.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function talentShortlisted(): BelongsToMany
    {
        return $this->talent()
            ->wherePivot('shortlisted', true)
            ->orderBy('booking_talent.priority');
    }

    /**
     * Alias for getting only the applied talent.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function talentApplied(): BelongsToMany
    {
        return $this->talent()->wherePivot('status', TalentPivot::STATUS_APPLIED);
    }

    /**
     * Alias for getting only the attending talent.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function talentAttending(): BelongsToMany
    {
        return $this->talentApplied()->wherePivot('client_accepted', true);
    }

    /**
     * Alias for getting only the talent that have not confirmed their attendance.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function talentUnconfirmed(): BelongsToMany
    {
        return $this->talentAttending()->wherePivot('attendance_confirmed', false);
    }

    /**
     * A booking has many notified talent.
     *
     * @return HasMany
     */
    public function talentNotified(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'booking_talent_notifications', 'booking_id', 'talent_id')
            ->using(TalentNotificationPivot::class)
            ->as('talent_notification_pivot')
            ->withTrashed();
    }

    /**
     * A booking has many notified managers.
     *
     * @return HasMany
     */
    public function managersNotified(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'booking_manager_notifications', 'booking_id', 'manager_id')
            ->using(ManagerNotificationPivot::class)
            ->as('manager_notification_pivot')
            ->withTrashed();
    }

    /**
     * A booking has many payments.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payments(): HasMany
    {
        return $this->hasMany(Payment::class);
    }

    /**
     * A booking may have many unpaid payments.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function paymentsUnpaid(): HasMany
    {
        return $this->payments()->where('status', Payment::STATUS_UNPAID);
    }

    /**
     * A booking has one final payment.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function finalPayment(): HasOne
    {
        return $this->hasOne(Payment::class)
            ->where('type', Payment::TYPE_FINAL);
    }

    /**
     * A booking has one deposit payment.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function depositPayment(): HasOne
    {
        return $this->hasOne(Payment::class)
            ->where('type', Payment::TYPE_DEPOSIT);
    }

    /**
     * A booking can have many threads
     *
     * @return HasMany
     */
    public function threads(): HasMany
    {
        return $this->hasMany(Thread::class, 'booking_id');
    }

    /**
     * Get the unread count for a specific user for that bookings threads
     *
     * @param int $user_id
     * @return int
     */
    public function userUnreadCount(int $user_id)
    {
        $unread_count = 0;
        foreach ($this->threads as $thread) {
            $unread_count += $thread->userUnreadCount($user_id);
        }
        return $unread_count;
    }

    /**
     * Setting terms value and triggering related event
     */
    public function setTermsAccepted(): void
    {
        $this->fill([
            'client_accepted_terms' => true,
            'client_accepted_terms_date' => now()
        ])->save();
    }

    /**
     * Turn string of dates into an ordered json_encoded array
     *
     * @param $date_options
     * @throws Exception
     */
    public function setDateOptionsAttribute($date_options): void
    {
        if (count((array)$date_options) <= 1) {
            $this->attributes['date_options'] = null;
            return;
        }

        // Sort the date_options from earliest to latest
        try {
            usort($date_options, function ($current, $next) {
                return Carbon::parse($current) > Carbon::parse($next);
            });
            $this->attributes['date_options'] = json_encode($date_options);
        } catch (Exception $e) {
            throw new Exception('Must be array of two or more dates');
        }
    }

    /**
     * Check to see if date_options is available and date is not.
     *
     * @return bool
     */
    public function getHasDateOptionsOnlyAttribute() {
        return ($this->date === null && $this->date_options !== null);
    }

    /**
     * If only date_options is available then return a comma separated string of the dates.
     * Otherwise return the date (this could be null under pre-date_options implementation)
     *
     * @return string
     */
    public function getAppropriateDateString() {
        return $this->has_date_options_only
            ? $this->getDateOptionsString()
            : $this->tz_date_instance->format(User::DATE_FORMAT_TZ);
    }

    public function getDateOptionsString() {
        $formattedDateOptions = array_map(function($date) {
            return Carbon::parse($date)->format(User::DATE_FORMAT);
        }, $this->date_options);

        return implode(', ', $formattedDateOptions);
    }

    public function getCompleteByDateFormattedAttribute() {
        return $this->complete_by_date
            ? Carbon::parse($this->complete_by_date)->format(User::DATE_FORMAT)
            : null;
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot(): void
    {
        parent::boot();

        // Add a global "booking" scope that defaults us to always be querying against bookings
        // instead of all bookings and requests. This can be removed using "withoutGlobalScope"
        // or by applying the "all" scope or "requests" scope.
        static::addGlobalScope('bookings', function (Builder $query) {
            $query->where('bookings.type', static::TYPE_BOOKING);
        });

        // On creating associate a new notification instance.
        static::creating(function ($booking) {
            $booking->notification()->associate(Notification::create());
        });

        // On saving determine if either "date", "timezone", or the "arrival_time" is dirty and if
        // so we'll update the timezone and UTC dates to reflect that. Both these dates have the
        // arrival time appended and are used for both querying and filtering in different
        // scenarios.
        static::saving(function ($booking) {
            if ($booking->isDirty('date', 'timezone', 'arrival_time')) {
                if (is_null($booking->date)) {
                    $booking->fill([
                        'utc_date' => null,
                        'tz_date' => null
                    ]);

                    return;
                }

                try {
                    // We'll create the date instance from the booking date and arrival time in the
                    // timezone of the booking. That's because this date when being set by the user
                    // is being set in the timezone context of the events location.
                    $date = Carbon::createFromFormat('Y-m-d H:i:s', sprintf('%s %s', $booking->date, $booking->arrival_time ?? '00:00:00'), $booking->timezone);

                    // We'll convert this timezone aware date into UTC by converting it.
                    $booking->utc_date = $date->copy()->setTimezone('UTC')->toDateTimeString();

                    // And now we can set the timezone aware date as well.
                    $booking->tz_date = $date->copy()->toDateTimeString();
                } catch (Exception $exception) {
                    report($exception);
                }
            }
        });
    }

    /**
     * Scope to apply filters and sorting with a default sorting callback.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \Illuminate\Http\Request $request
     * @param array $parameters
     *
     * @return void
     */
    public function scopeApplyFiltersAndSorting(Builder $query, Request $request, array $parameters): void
    {
        $query->applyFilters(
            $request,
            $parameters['filteringDateColumn'],
            $parameters['customStatusFilterCallback'] ?? null
        );
        $query->applySorting(
            $request,
            $parameters['defaultSortingCallback'],
            $parameters['sortingDateColumn'],
            $parameters['customDateSortingCallback'] ?? null,
            $parameters['customPriceSortingCallback'] ?? null,
            $parameters['customCreatedAtSortingCallback'] ?? null,
            $parameters['customCompletedDateSortingCallback'] ?? null,
            $parameters['customScheduledDateSortingCallback'] ?? null

        );
    }

    /**
     * Get the most recently completed booking.
     *
     * @return \Illuminate\Database\Eloquent\Model|HasMany|object|null
     */
    public function scopeMostRecentlyCompleted($query)
    {
        $query->completed()->orderBy('completed_date', 'desc');
    }

    /**
     * Get the most recently created booking.
     *
     * @return \Illuminate\Database\Eloquent\Model|HasMany|object|null
     */
    public function scopeMostRecentlyCreated($query)
    {
        $query->orderBy('created_at', 'desc');
    }

    /**
     * Get the most recently completed booking.
     *
     * @return \Illuminate\Database\Eloquent\Model|HasMany|object|null
     */
    public function scopeMostRecentlyWithdrawn($query)
    {
        $query->withdrawnOrCancelled()->orderBy('updated_at', 'desc');
    }

    /**
     * @param $query
     *
     * @return void
     */
    public function scopeEarliestCreated($query)
    {
        $query->orderBy('created_at','asc');
    }

    /**
     * Scope only booking requests.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public function scopeRequests($query): void
    {
        $query->withoutGlobalScope('bookings')
            ->where('bookings.type', static::TYPE_REQUEST);
    }

    /**
     * Scope only active booking requests.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public function scopeActiveRequests($query): void
    {
        $query->requests()->where('status', static::STATUS_PENDING);
    }

    /**
     * Scope bookings without vetting requests.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public function scopeWithoutVetting($query): void
    {
        $query->withoutGlobalScope('bookings')
            ->where('bookings.status', '<>', static::STATUS_VETTING);
    }

    /**
     * Scope only bookings without the declined status.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public function scopeWithoutDeclined($query): void
    {
        $query->where('bookings.status', '<>', static::STATUS_DECLINED);
    }

    /**
     * Scope only bookings without the withdrawn status.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public function scopeWithoutWithdrawn($query): void
    {
        $query->where('bookings.status', '<>', static::STATUS_WITHDRAWN);
    }

    /**
     * Scope only bookings without the completed status.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public function scopeWithoutCompleted($query): void
    {
        $query->where('bookings.status', '<>', static::STATUS_COMPLETED);
    }

    /**
     * Scope only booking requests that require vetting.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public function scopeVetting($query): void
    {
        $query->withoutGlobalScope('bookings')
            ->where('bookings.type', static::TYPE_REQUEST)
            ->where('bookings.status', static::STATUS_VETTING);
    }

    /**
     * Scope both bookings and requests.
     *
     * This is a shortcut to removing the global "booking" scope.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public function scopeAll($query): void
    {
        $query->withoutGlobalScope('bookings');
    }

    /**
     * Scope both bookings and requests.
     *
     * This is a shortcut to removing the global "booking" scope.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public function scopeEverything($query): void
    {
        $query->withoutGlobalScope('bookings');
    }

    /**
     * Scope pending requests.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public function scopePending($query): void
    {
        $query->where('bookings.status', static::STATUS_PENDING);
    }

    /**
     * Scope scheduled bookings.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public function scopeScheduled($query): void
    {
        $query->where('bookings.status', static::STATUS_SCHEDULED);
    }

    /**
     * Scope booked bookings.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public function scopeBooked($query): void
    {
        $query->where('bookings.status', static::STATUS_BOOKED);
    }

    /**
     * Scope active bookings.
     *
     * This includes scheduled, booked, and on hold.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public function scopeActive($query): void
    {
        $query->whereIn('bookings.status', [
            static::STATUS_SCHEDULED,
            static::STATUS_BOOKED,
            static::STATUS_HOLD,
        ]);
    }

    /**
     * Scope in person bookings.
     *
     * @param \Illuminate\Database\Query\Builder $query
     *
     * @return void
     */
    public function scopeInPerson($query): void
    {
        $query->whereIn('bookings.appearance_type', [
            Booking::APPEARANCE_IN_PERSON,
            Booking::APPEARANCE_BOTH
        ]);
    }

    /**
     * Scope whether a user has been accepted for a given booking.
     *
     * This includes if the user has applied or if they have cancelled their response.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return void
     */
    public function scopeHaveBeenAcceptedFor($query, Booking $booking): void
    {
        $query->where('bookings.id', $booking->id)
            ->whereIn('booking_talent.status', [TalentPivot::STATUS_APPLIED, TalentPivot::STATUS_CANCELLED])
            ->where('booking_talent.client_accepted', true);
    }

    /**
     * Scope bookings where a user has been accepted for.
     *
     * This does not include bookings where they were accepted but have then cancelled.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public function scopeHaveBeenAccepted($query): void
    {
        $query->where('booking_talent.status', TalentPivot::STATUS_APPLIED)
            ->where('booking_talent.client_accepted', true);
    }

    /**
     * Scope bookings where a user has been accepted INCLUDING where they have have cancelled.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public function scopeHaveBeenAcceptedWithCancelled($query): void
    {
        $query->whereIn('booking_talent.status', [TalentPivot::STATUS_APPLIED, TalentPivot::STATUS_CANCELLED])
            ->where('booking_talent.client_accepted', true);
    }

    /**
     * Scope bookings where a user has applied.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public function scopeHaveApplied($query): void
    {
        $query->whereIn('booking_talent.status', [TalentPivot::STATUS_APPLIED]);
    }

    /**
     * Scope bookings where a user has applied INCLUDING where they have cancelled.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public function scopeHaveAppliedWithCancelled($query): void
    {
        $query->whereIn('booking_talent.status', [TalentPivot::STATUS_APPLIED, TalentPivot::STATUS_CANCELLED]);
    }

    /**
     * Scope bookings where talent have applied or declined but were not accepted.
     *
     * Provided user should either be a talent or a manager.
     *
     * @param \Illuminate\Database\Query\Builder $query
     * @param \Pickstar\User\User $user
     *
     * @return void
     */
    public function scopeTalentAppliedOrDeclinedButNotAccepted($query, User $user): void
    {
        $query->whereHas('talent', function ($query) use ($user) {
            $query->where('booking_talent.client_accepted', false)
                ->whereIn('booking_talent.status', [
                    TalentPivot::STATUS_APPLIED,
                    TalentPivot::STATUS_DECLINED
                ])
                ->when($user->isTalent(), function ($query) use ($user) {
                    $query->where('booking_talent.talent_id', $user->id);
                })
                ->when($user->isManager(), function ($query) use ($user) {
                    $query->whereIn('booking_talent.talent_id', $user->managedTalent->pluck('id'));
                });
        });
    }

    /**
     * Scope bookings that are completed.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public function scopeCompleted($query): void
    {
        $query->where('status', static::STATUS_COMPLETED);
    }

    /**
     * Scope bookings that are completed.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public function scopeWithdrawn($query): void
    {
        $query->where('status', static::STATUS_WITHDRAWN);
    }

    /**
     * Scope bookings that are completed.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public function scopeWithdrawnOrCancelled($query): void
    {
        $query->whereIn('status', [static::STATUS_WITHDRAWN, static::STATUS_CANCELLED]);
    }

    /**
     * Scope booking requests that will show on the billboard.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public function scopeOnBillboard($query): void
    {
        $query->where('others_can_apply', true);
    }


    /**
     * Scope booking requests that are not expired.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public function scopeIsNotExpired($query): void
    {
        $query->where(function ($query) {
            $query->where(function ($query) {
                $query->whereNull('date');
            })->orWhere(function ($query) {
                $query->whereDate('date', '>', Carbon::now());
            });
        });
    }

    /**
     * Scope booking requests that are expired.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public function scopeIsExpired($query): void
    {
        $query->where(function ($query) {
            $query->whereDate('date', '<', Carbon::now());
        });
        $query->orWhere(function ($query) {
            $query->where('expires', true);
            $query->where('billboard_expiry_date', '<', Carbon::now());
        });
    }

    public function scopeIsExpiring($query): void
    {
        $query->where('expires', true);
        $query->where('billboard_expiry_date', '<', Carbon::now());
    }

    /**
     * Update the date and timezone for the booking from request data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function updateDateAndTimezone(Request $request): void
    {
        $flexibleDate = $request->input('date_flexible_or_unconfirmed');
        $dateOptions[] = $newDate = $request->input('date');
        $dateOptions[] = $request->input('date2');
        $dateOptions[] = $request->input('date3');

        // Filter removes all falsy values, like null, and unique removes duplicates
        $dateOptions = array_unique(array_filter($dateOptions));

        // If date is flexible or unconfirmed, and multiple dates have been chosen,
        // add those dates to $date_options
        if ($flexibleDate === true && count($dateOptions) > 1) {
            $this->updateDateOptions($dateOptions);
            $newDate = null;
        }

        $this->timezone = timezone_for_state($request->get('state')) ?? 'UTC';

        if ($newDate) {

            $oldDate = $this->date;
            $this->date = Carbon::createFromFormat('Y-m-d', $newDate)->format('Y-m-d');
            $date = Carbon::createFromFormat('Y-m-d H:i:s', sprintf('%s %s', $newDate, $request->get('arrival_time') ?? '00:00:00'), $this->timezone);

            // We'll convert this timezone aware date into UTC by converting it.
            $this->utc_date = $date->copy()->setTimezone('UTC')->toDateTimeString();

            // And now we can set the timezone aware date as well.
            $this->tz_date = $date->copy()->toDateTimeString();

            if ($oldDate && Carbon::parse($oldDate)->format('Y-m-d') !== Carbon::parse($newDate)->format('Y-m-d')) {

                if ($this->typeIsRequest()) {
                    $this->save();
                    $this->updateBookingRequestEventDateChanged();
                }
                if ($this->typeIsBooking()) {
                    if ($this->finalPayment->status === Payment::STATUS_UNPAID) {
                        $finalDueDate = $this->calculateFinalPaymentDueDate();
                        $this->finalPayment->fill(['due_date' => $finalDueDate])->save();
                    }
                }
            // If the old date is null and we have applied talents, then set their date_applied_for to $newDate
            } else if ($oldDate === null && $this->date !== null && $this->date_options === null) {
                $this->talentApplied->each( function ($talent) {
                   $talent->response->date_applied_for = $this->date;
                   $talent->response->save();
                });
            }
            $this->date_options = null;
        } else {
            $this->date = null;
        }
    }

    public function calculateFinalPaymentDueDate()
    {
        // Create the final payment. As per user stories and processes the due date of a final payment is
        // 7 days before the bookings event date. If the booking has no event date we'll use a generic
        // 14 days from it being scheduled. The amount for a final payment is whatever is left over
        // after the deposit has been calculated.
        // however if the due date falls within this period the due date will be set to now instead of outside the event timeline
        $finalDueDate = !is_null($this->utc_date) ? Carbon::parse($this->utc_date)->subDays(7) : now()->addDays(14);

        if ($finalDueDate->lte(now())) {
            $finalDueDate = now();
        }

        return $finalDueDate;
    }

    public function calculateDepositPaymentDueDate()
    {
        // Create the deposit payment. As per user stories and processes the due date of a deposit is
        // 7 days from the booking being scheduled (talent being accepted). The amount for a deposit
        // is based on the "deposit_payment" system setting which is a percentage of the total.
        // however if the due date falls within this period the due date will be set to now instead of outside the event timeline
        $depositDueDate = !is_null($this->utc_date) && Carbon::parse($this->utc_date)->lte(now()->addDays(7)) ? now() : now()->addDays(7);

        if ($depositDueDate->lte(now())) {
            $depositDueDate = now();
        }

        // If the final due date is before the deposit due date, make the deposit due at the same time as the final
        if ($this->calculateFinalPaymentDueDate()->lte($depositDueDate)) {
            $depositDueDate = $this->calculateFinalPaymentDueDate();
        }

        return $depositDueDate;
    }

    /**
     * Clear fields when opportunity type 'online' for the booking from request data.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function updateForOpportunityType(Request $request): void
    {
      if ($request->appearance_type === 'online') {
        $this->street_address = null;
        $this->suburb = null;
        $this->state = null;
        $this->country = null;
        $this->postcode = null;
        $this->additional_location_detail = null;

        $this->date = null;
        $this->arrival_time = null;
        $this->athlete_start_time = null;
        $this->duration = null;
        $this->date_flexible_or_unconfirmed = 0;
        $this->time_flexible_or_unconfirmed = 0;
        $this->date_options = null;
      }

        if ($request->appearance_type === Booking::APPEARANCE_ONLINE || $request->appearance_type === Booking::APPEARANCE_BOTH) {
        $oldCompleteByDate = $this->complete_by_date;

          $this->complete_by_date =  $request->filled('complete_by_date')
          ? Carbon::createFromFormat('Y-m-d', $request->input('complete_by_date'))->format('Y-m-d')
            : $this->complete_by_date;

          if ($this->complete_by_date !== $oldCompleteByDate && $this->typeIsRequest()) {
            $this->save();
            event(new BookingRequestCompleteByDateChanged($this));
        }
      }
    }

    /**
     * Update the booking from request data.
     *
     * This does not fill the sensitive fields.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function updateFromRequestPayload(Request $request, $save = true): void
    {
        $this->fill($request->except($this->sensitive));

        $this->updateDateAndTimezone($request);

        $this->updateForOpportunityType($request);

        if ($save) {
            $this->save();
        }
    }

    /**
     * Load the default relations that are used in several areas.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Pickstar\Booking\Booking
     */
    public function loadDefaultRelations(Request $request): Booking
    {
        $this->load([
            'client',
            'opportunity',
            'onlineOpportunity',
            'payments.transaction',
            'threads' => function ($query) use ($request) {
                $query->onlyMyThreads($request->user());
                $query->with('booking', 'participants', 'clients', 'talents');
            },
            'talent' => function ($query) use ($request) {
                $query->onlyMyselfIfTalent($request->user());

                $query->onlyManagedIfManager($request->user());
            }
        ]);

        if ($request->user()->isAdmin()) {
            $this->load([
                'adminOpportunity'
            ]);
        }

        return $this;
    }

    /**
     *  Get the billboard expiry date.
     *
     *  Return event date if event date is less than expiry date.
     *  If event date is null (online event) return billboard expiry date.
     *
     * @param billboard_expiry_date $billboard_expiry_date
     *
     * @return string
     */
    public function getBillboardExpiryDateAttribute($billboard_expiry_date)
    {

        if ($this->date !== null && (Carbon::parse($this->date) < Carbon::parse($billboard_expiry_date))) {
            return Carbon::parse($this->date)->toDateTimeString();
        }

        return $billboard_expiry_date;

    }

    public function getHumanReadableWithdrawReasonAttribute()
    {
        $reasonsLookup = [
            'budget' => 'Budget Issues',
            'applicants' => 'No Suitable Applicants',
            'talent_uninterested' => 'Talent Not Interested',
            'client_uninterested' => 'Client Not Interested In Applied Talent',
            'not_proceeding' => 'Opportunity Not Proceeding',
            'elsewhere' => 'Talent Sourced Elsewhere',
            'pickstar' => 'PickStar Withdrew',
            'call' => 'Did Not Return Calls',
            'info' => 'Only Wanted Information',
            'expired' => 'Expired',
            'other' => 'Other'
        ];
        return $reasonsLookup[$this->withdraw_reason];
    }

    /**
     * @return Model|HasMany|\Illuminate\Database\Query\Builder|object|null
     */
    public function getLatestNoteAttribute()
    {
        return $this->notes()->latest()->first();
    }

    public function getLocationIdAttribute()
    {
        $location = Location::where('shortname', $this->state)->first();
        return ArrayHelper::getValue($location, 'id');
    }

    public function calculateTalentApplicationExpiryDate()
    {
        if ($this->date && !$this->appearanceIsOnline() && now()->addDays(config('pickstar.talent_application_expiry_days'))->startOfDay()->addSecond()->format('Y-m-d H:i:s') > Carbon::parse($this->date)->addSecond()->format('Y-m-d H:i:s')) {
            return Carbon::parse($this->date)->addSecond()->format('Y-m-d H:i:s');
        } else {
            return now()->addDays(config('pickstar.talent_application_expiry_days'))->startOfDay()->addSecond()->format('Y-m-d H:i:s');
        }
    }

    /**
     * Duplicate the booking.
     *
     * Duplicated booking becomes a vetting request.
     *
     * Unsets most relations and reverts some columns to defaults, and passes the unsaved object back to the controller
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Pickstar\Booking\Booking
     */
    public function duplicate(Request $request = null): Booking
    {
        /* @var Booking $booking */

        $booking = $this->replicateWithDefaults();

        if($request !== null){
            // Update the replicated booking from the request.  Note, this creates the record in DB
            $booking->updateFromRequestPayload($request);

            // Associate the requested shortlist with the new booking.
            foreach ($request->input('shortlist') as $index => $talent) {
                $booking->talent()->attach($talent['id'], [
                    'priority' => ++$index,
                    'shortlisted' => true,
                    'status' => TalentPivot::STATUS_PENDING
                ]);
            }
        }else {
            $booking->save();
        }

        $booking->load([
            'client',
            'opportunity',
            'onlineOpportunity',
            'adminOpportunity',
            'talent',
            'talentNotified'
        ]);

        return $booking;
    }

    /**
     * Replicates the booking model without any connected data.
     *
     * This effectively extends Laravel's base replicate function.
     *
     * @return \Pickstar\Booking\Booking
     */
    public function replicateWithDefaults(): Booking
    {
        $booking = $this->replicate();

        // Remove some relation associations. Things like "notifications" will be created when we
        // save the replicated model.
        $booking->notification()->dissociate();

        $booking->unsetRelation('payments');
        $booking->unsetRelation('talent');

        // Null out some fields that will need to be set again after going through the processes.
        $booking->fill([
            'type' => Booking::TYPE_REQUEST,
            'status' => Booking::STATUS_VETTING,
            'billboard_expiry_date' => null,
            'vetted_date' => null,
            'commission_rate' => null,
            'scheduled_date' => null,
            'completed_date' => null,
            'client_accepted_terms' => false,
            'client_accepted_terms_date' => null,
            'date_options' => $booking->date_options ?? null
        ]);

        return $booking;
    }

    /**
     * Generate a Bitly URL to the booking requests page.
     *
     * @return string
     */
    public function generateRequestBitlyUrl()
    {
        return $this->generateBitlyUrl('request');
    }

    /**
     * Generate a Bitly URL to the booking requests page.
     *
     * @return string
     */
    public function generateVettingRequestBitlyUrl()
    {
        return $this->generateBitlyUrl('vetting-request');
    }

    /**
     * Generate a Bitly URL to the bookings page.
     *
     * @param string $type
     *
     * @return string
     */
    public function generateBitlyUrl($type = 'booking'): string
    {
        $path = null;

        switch ($type) {
            case 'booking':
                $path = 'bookings';
                break;
            case 'request':
                $path = 'requests';
                break;
            case 'vetting-request':
                $path = 'request-vetting';
                break;
        }

//        try {
            $cache = Cache::store();

            if ($cache instanceof TaggableStore) {
                $cache = $cache->tags(['bitly', $type]);
            }

            return $cache->rememberForever(sprintf('bitly:%s:%s', $type, $this->id), function () use ($path) {
                return Bitly::getUrl(
                    url()->toWebApp(sprintf('%s/%s', $path, $this->id))
                );
            });
  //      } catch (Exception $exception) {
 //           report($exception);

   //         return sprintf('Booking ID: %s', $this->id);
//        }
    }

    /**
     * Constrain the query with the given scope.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \Illuminate\Database\Eloquent\Scope $scope
     *
     * @return void
     */
    public function scopeConstrainWith($query, Scope $scope): void
    {
        $scope->apply($query, $this);
    }

    /**
     * Get a contextual status for a given user.
     *
     * The contextual status considers the users role and other various statuses which may
     * result in a status that is different from that of a booking. For example, a booking
     * is considered cancelled for a talent if they have cancelled their attendance.
     *
     * @param \Pickstar\User\User $user
     *
     * @return string
     */
    public function contextualStatusFor(User $user): string
    {
        if ($user->isAdmin() || $user->isClient()) {
            return $this->status;
        }

        if ($user->isTalent()) {
            // If the talent relation is not loaded on the booking or we don't find the current user
            // in the talent relation we'll return the bookings status, otherwise we'll look
            // for contextual statuses.
            if (
                $this->relationLoaded('talent') &&
                $talent = $this->talent->firstWhere('id', $user->id)
            ) {
                // Contextual status is considered cancelled if the booking itself is cancelled or the talent
                // has cancelled.
                if (
                    $this->status === static::STATUS_CANCELLED ||
                    $talent->response->status === TalentPivot::STATUS_CANCELLED
                ) {
                    return static::STATUS_CANCELLED;
                }

                // Contextual status is considered unsuccessful if the booking type is a booking, the talent
                // has applied and the client has not accepted.
                if (
                    $this->typeIsBooking() &&
                    $talent->response->status === TalentPivot::STATUS_APPLIED &&
                    $talent->response->client_accepted === false
                ) {
                    return TalentPivot::STATUS_UNSUCCESSFUL;
                }

                if (
                    $this->typeIsBooking() &&
                    $talent->response->status === TalentPivot::STATUS_APPLIED &&
                    $talent->response->client_accepted === true &&
                    $this->status === static::STATUS_SCHEDULED
                ) {
                    return static::STATUS_PENDING_PAYMENT;
                }

                if (
                    $this->typeIsBooking() &&
                    $talent->response->status === TalentPivot::STATUS_APPLIED &&
                    $talent->response->client_accepted === true &&
                    $this->status === static::STATUS_HOLD
                ) {
                    return static::STATUS_CANCELLED;
                }

                // Contextual status is considered declined if the booking is a request and the talent declined.
                if (
                    $this->typeIsRequest() &&
                    $talent->response->status === TalentPivot::STATUS_DECLINED
                ) {
                    return TalentPivot::STATUS_DECLINED;
                }

                // Contextual status is considered applied if the booking is a request and the talent declined.
                if (
                    $this->typeIsRequest() &&
                    $talent->response->status === TalentPivot::STATUS_APPLIED && $this->status === static::STATUS_WITHDRAWN
                ) {
                    return static::STATUS_WITHDRAWN;
                }

                // Contextual status is considered applied if the booking is a request and the talent declined.
                if (
                    $this->typeIsRequest() &&
                    $talent->response->status === TalentPivot::STATUS_APPLIED
                ) {
                    return TalentPivot::STATUS_APPLIED;
                }
            }
        } elseif ($user->isManager()) {
            if ($this->relationLoaded('talent')) {
                // Contextual status is considered cancelled if booking is cancelled or if every one of the
                // managers talent have cancelled.
                if ($this->status === static::STATUS_CANCELLED) {
                    return static::STATUS_CANCELLED;
                }

                // Contextual status is considered unsuccessful if every one of the managers talent applied
                // but were not accepted by the client.
                if (
                    $this->typeIsBooking() &&
                    $this->talent->every(function ($talent) {
                        return $talent->response->status === TalentPivot::STATUS_APPLIED &&
                            $talent->response->client_accepted === false;
                    })
                ) {
                    return TalentPivot::STATUS_UNSUCCESSFUL;
                }
            }
        }

        // Default to stored booking status.
        return $this->status;
    }

    /**
     * Fill date_options attribute, and notify talent to reapply if
     * date_options didn't previously exist.
     *
     * @param $dateOptions
     */
    public function updateDateOptions($dateOptions)
    {
        $oldDateOptions = $this->date_options;

        // In case user selects same dates;
        $this->date_options = $dateOptions;

        // Only notify talents to reapply if request changes from null or single date to multiple dates.
        if ($oldDateOptions === null && $this->typeIsRequest()) {
            $this->date = null;
            $this->save();
            $this->updateBookingRequestEventDateChanged();
        }
    }

    /**
     * Fire BookingRequestEventDateChanged event, and reset all
     * applied talents' relevant response attributes.
     */
    public function updateBookingRequestEventDateChanged()
    {
        event(new BookingRequestEventDateChanged($this));

        // TODO - figure out why this block is causing issues with auditable - seems to be a pivot thing
        TalentPivot::disableAuditing();
        foreach ($this->talentApplied as $talent) {
            $this->talent()->syncWithoutDetaching([
                $talent->id => [
                    'status' => TalentPivot::STATUS_PENDING,
                    'client_accepted' => false,
                    'applied_date' => null,
                    'expired' => false,
                    'expiry_date' => null,
                    'date_applied_for' => null
                ]
            ]);
        }
        TalentPivot::enableAuditing();
    }

    public function closeTalentThreads($user_id)
    {
        foreach ($this->threads()->whereHas('participants', function ($query) use ($user_id) {
                    $query->where('user_id', $user_id);
                })->get() as $thread) {
            $thread->fill(['active' => false])->save();
        }
    }

    public function markBookingAsComplete()
    {
        $this->fill(['status' => Booking::STATUS_COMPLETED, 'completed_date' => now()])->save();
        foreach ($this->threads as $thread) {
            $thread->fill(['active' => false])->save();
        }

        event(new BookingCompleted($this));
    }

    public function markTalentHasComplete(TalentPivot $response, $type)
    {
        switch($type) {
            case Booking::APPEARANCE_IN_PERSON:
                $completed_date = 'in_person_completed_date';
                break;
            case Booking::APPEARANCE_ONLINE:
                $completed_date = 'online_completed_date';
                break;
            default:
                throw new Exception('Error in marking booking as complete due to invalid event type');
                break;
        }

        $response->fill([$completed_date => now()])->save();

        if ($this->talentNotCompleted()->count() === 0) {
            $this->markBookingAsComplete();
        }
    }

    public function talentNotCompleted()
    {
        return $this->talentNotCompletedInPerson()->merge($this->talentNotCompletedOnline());
    }

    public function talentNotCompletedOnline()
    {
        if ($this->appearanceIsOnline()) {
            return $this->talentAttending()
                ->where('online_completed_date', null)
                ->get();
        }
        // Return empty collection since it may be used to merge with other collections.
        return new Collection();
    }

    public function talentNotCompletedInPerson()
    {
        if ($this->appearanceIsInPerson()) {
            return $this->talentAttending()
                ->where('in_person_completed_date', null)
                ->get();
        }
        // Return empty collection since it may be used to merge with other collections.
        return new Collection();
    }

    public function hasInPersonEvent() {
        return $this->appearance_type === Booking::APPEARANCE_IN_PERSON || $this->appearance_type === Booking::APPEARANCE_BOTH;
    }

    public function hasOnlineEvent() {
        return $this->appearance_type === Booking::APPEARANCE_ONLINE || $this->appearance_type === Booking::APPEARANCE_BOTH;
    }

    public function talentAttached($talent) {
        return $this->talent->pluck('id')->contains($talent->id);
    }

    /**
     * Get the full string for withdraw reason.
     *
     * @return string|null
     */
    public function getFullWithdrawReasonStringAttribute() {
        $reason = null;
        switch($this->withdraw_reason) {
            case 'talent_not_required':
                $reason = 'Talent No Longer Required';
                break;
            case 'budget':
                $reason = 'Budget Issues';
                break;
            case 'not_proceeding':
                $reason = 'Opportunity Not Proceeding';
                break;
            default:
                break;
        }
        return $reason;
    }

    /**
     * Returns the full name of the admin assigned to this booking/request.
     */
    public function getAssignedAdminNameAttribute() {
        $admin = User::find($this->assigned_admin);

        return $admin !== null ? $admin->name : null;
    }
}

<?php

namespace Pickstar\Events;

use Pickstar\Booking\Booking;

class BookingCompleted
{
    use Concerns\BookingEvent;

}

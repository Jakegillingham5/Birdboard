<?php

namespace Pickstar\Events;

class BookingRequestVetted implements Contracts\HasBookingInstance
{
    use Concerns\BookingEvent;

}

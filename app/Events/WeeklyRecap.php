<?php

namespace Pickstar\Events;

class WeeklyRecap implements Contracts\HasBookingInstance
{
    use Concerns\BookingEvent;
}

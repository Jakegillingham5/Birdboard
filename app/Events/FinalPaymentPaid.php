<?php

namespace Pickstar\Events;

class FinalPaymentPaid implements Contracts\HasBookingInstance
{
    use Concerns\BookingEvent;
}

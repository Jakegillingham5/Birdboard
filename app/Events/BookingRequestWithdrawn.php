<?php

namespace Pickstar\Events;

class BookingRequestWithdrawn implements Contracts\HasBookingInstance
{
    use Concerns\BookingEvent;
}

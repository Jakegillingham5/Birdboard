<?php
/**
 * Created by PhpStorm.
 * User: jpickstar
 * Date: 2019-04-29
 * Time: 09:22
 */

namespace Pickstar\Events;


use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Pickstar\User\ActiveCampaignUser;

class LeadRegistered
{
    use Dispatchable, SerializesModels;

    public $activeCampaignUser;

    public function __construct(ActiveCampaignUser $activeCampaignUser)
    {
        $this->activeCampaignUser = $activeCampaignUser;
    }
}
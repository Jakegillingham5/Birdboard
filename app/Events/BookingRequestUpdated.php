<?php

namespace Pickstar\Events;

class BookingRequestUpdated implements Contracts\HasBookingInstance
{
    use Concerns\BookingEvent;

}

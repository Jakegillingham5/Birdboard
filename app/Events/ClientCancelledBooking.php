<?php

namespace Pickstar\Events;

class ClientCancelledBooking implements Contracts\HasBookingInstance
{
    use Concerns\BookingEvent;
}

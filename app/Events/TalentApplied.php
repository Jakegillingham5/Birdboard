<?php

namespace Pickstar\Events;

use Pickstar\User\User;
use Pickstar\Booking\Booking;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class TalentApplied implements Contracts\HasBookingInstance
{
    use Dispatchable, SerializesModels;

    /**
     * Talent that applied.
     *
     * @var \Pickstar\User\User
     */
    public $talent;

    /**
     * Booking that the talent applied for.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * Talents comment on why they are suitable
     */
    public $comment;

    /**
     * Create a new message instance.
     *
     * @param \Pickstar\User\User $talent
     * @param \Pickstar\Booking\Booking $booking
     * @param $comment
     */
    public function __construct(User $talent, Booking $booking)
    {
        $this->talent = $talent;
        $this->booking = $booking;
        $this->comment = $booking->talentApplied()->withPivot('comment')->find($talent->id)->response->comment;
    }
}

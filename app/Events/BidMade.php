<?php

namespace Pickstar\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class BidMade implements Contracts\HasBookingInstance
{
    use Dispatchable, SerializesModels;

    public $name;
    public $email;
    public $company;
    public $bid;
    public $type;
    /**
     * Create a new event instance.
     *
     * @param \Pickstar\User\User $talent
     * @param \Pickstar\Booking\Booking $booking
     */
    public function __construct($name, $email, $company, $bid, $type)
    {
        $this->name = $name;
        $this->email = $email;
        $this->company = $company;
        $this->bid = $bid;
        $this->type = $type;
    }
}

<?php

namespace Pickstar\Events;

class BookingRequestEventDateChanged implements Contracts\HasBookingInstance
{
    use Concerns\BookingEvent;
}

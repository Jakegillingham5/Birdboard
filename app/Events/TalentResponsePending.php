<?php

namespace Pickstar\Events;

use Pickstar\User\User;
use Pickstar\Booking\Booking;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class TalentResponsePending
{
    use Dispatchable, SerializesModels;

    /**
     * Talent that had their response changed to pending.
     *
     * @var \Pickstar\User\User
     */
    public $talent;

    /**
     * Booking that talent has responded to.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * Create a new event instance.
     *
     * @param \Pickstar\User\User $talent
     * @param \Pickstar\Booking\Booking $booking
     */
    public function __construct(User $talent, Booking $booking)
    {
        $this->talent = $talent;
        $this->booking = $booking;
    }
}

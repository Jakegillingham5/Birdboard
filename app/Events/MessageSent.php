<?php

namespace Pickstar\Events;

use Pickstar\User\User;
use Pickstar\Message\Thread;
use Pickstar\Message\Message;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MessageSent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * User that sent the message
     *
     * @var \Pickstar\User\User
     */
    public $sender;

    /**
     * User that is receiving the message
     *
     * @var \Pickstar\User\User
     */
    public $recipient;

    /**
     * Message Details
     *
     * @var \Pickstar\Message\Message
     */
    public $message;

    /**
     * Thread that the message was sent in
     *
     * @var \Pickstar\Message\Thread
     */
    public $thread;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $sender, User $recipient, Message $message, Thread $thread)
    {
        $this->sender = $sender;
        $this->recipient = $recipient;
        $this->message = $message;
        $this->thread = $thread;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('chat.'.$this->recipient->id);
    }

    /**
     * The data that the message will be broadcast with
     *
     * @return array
     */
    public function broadcastWith()
    {
        return ['message' => new \Pickstar\Http\Resources\Message($this->message->load('thread')), 'unread_count' => $this->thread->userUnreadCount($this->recipient->id)];
    }
}

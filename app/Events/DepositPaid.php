<?php

namespace Pickstar\Events;

class DepositPaid implements Contracts\HasBookingInstance
{
    use Concerns\BookingEvent;
}

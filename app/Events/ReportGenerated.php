<?php

namespace Pickstar\Events;

use Pickstar\User\User;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ReportGenerated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets;

    /**
     * User instance.
     *
     * @var \Pickstar\User\User
     */
    protected $user;

    /**
     * Path hash of the report.
     *
     * @var string
     */
    public $reportPathHash;

    /**
     * Nice name of the report.
     *
     * @var string
     */
    public $reportNiceName;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, string $reportPathHash, string $reportNiceName = null)
    {
        $this->user = $user;
        $this->reportPathHash = $reportPathHash;
        $this->reportNiceName = $reportNiceName;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel(sprintf('user.%s.reports', $this->user->id));
    }
}

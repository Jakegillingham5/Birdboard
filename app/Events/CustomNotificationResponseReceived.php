<?php
/**
 * Created by PhpStorm.
 * User: jpickstar
 * Date: 2019-05-22
 * Time: 14:59
 */

namespace Pickstar\Events;


use Illuminate\Queue\SerializesModels;

class CustomNotificationResponseReceived
{
    use SerializesModels;

    public $data;

    /**
     * Create a new event instance.
     *
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }
}

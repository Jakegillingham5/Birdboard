<?php

namespace Pickstar\Events;

class CreateBookingThreads implements Contracts\HasBookingInstance
{
    use Concerns\BookingEvent;
}

<?php

namespace Pickstar\Events;

use Pickstar\User\User;
use Pickstar\Booking\Booking;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class TalentReviewing implements Contracts\HasBookingInstance
{
    use Dispatchable, SerializesModels;

    /**
     * Talent that applied.
     *
     * @var \Pickstar\User\User
     */
    public $talent;

    /**
     * Booking that the talent applied for.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * Create a new message instance.
     *
     * @param \Pickstar\User\User $talent
     * @param \Pickstar\Booking\Booking $booking
     */
    public function __construct(User $talent, Booking $booking)
    {
        $this->talent = $talent;
        $this->booking = $booking;
    }
}

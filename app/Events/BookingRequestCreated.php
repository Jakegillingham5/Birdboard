<?php

namespace Pickstar\Events;

class BookingRequestCreated implements Contracts\HasBookingInstance
{
    use Concerns\BookingEvent;

}

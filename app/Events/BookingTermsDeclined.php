<?php

namespace Pickstar\Events;

class BookingTermsDeclined implements Contracts\HasBookingInstance
{
    use Concerns\BookingEvent;
}

<?php

namespace Pickstar\Events;

class BookingRequestProgressedToBooking implements Contracts\HasBookingInstance
{
    use Concerns\BookingEvent;
}

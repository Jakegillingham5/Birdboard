<?php

namespace Pickstar\Events;

use Pickstar\User\User;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ReportFailed implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets;

    /**
     * User instance.
     *
     * @var \Pickstar\User\User
     */
    protected $user;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel(sprintf('user.%s.reports', $this->user->id));
    }
}

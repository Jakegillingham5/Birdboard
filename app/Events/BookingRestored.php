<?php

namespace Pickstar\Events;

class BookingRestored implements Contracts\HasBookingInstance
{
    use Concerns\BookingEvent;
}

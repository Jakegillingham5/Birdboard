<?php
/**
 * Created by PhpStorm.
 * User: jpickstar
 * Date: 2019-05-22
 * Time: 14:59
 */

namespace Pickstar\Events;


use Illuminate\Queue\SerializesModels;
use Pickstar\Marketing\Email;

class NewsletterRegistration
{
    use SerializesModels;

    /**
     * Email instance.  Uses a user variable as this is what the AC Listener uses.
     *
     * @var Email
     */
    public $user;

    /**
     * Create a new event instance.
     *
     * @param Email $email
     */
    public function __construct($email)
    {
        $this->user = $email;
    }
}
<?php

namespace Pickstar\Events;

use Pickstar\User\User;
use Pickstar\Talent\Endorsement;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class TalentEndorsementAdded
{
    use Dispatchable, SerializesModels;

    /**
     * Endorsement instance.
     *
     * @var array
     */
    public $endorsement;

    /**
     * Talent user instance.
     *
     * @var \Pickstar\User\User
     */
    public $talent;

    /**
     * Create a new event instance.
     *
     * @param Endorsement $endorsement
     * @param \Pickstar\User\User $talent
     *
     * @return void
     */
    public function __construct(Endorsement $endorsement, User $talent)
    {
        $this->endorsement = $endorsement;
        $this->talent = $talent;
    }
}

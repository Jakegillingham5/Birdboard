<?php

namespace Pickstar\Events\Concerns;

use Pickstar\Booking\Booking;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

trait BookingEvent
{
    use Dispatchable, SerializesModels;

    /**
     * Client connected to a booking
     *
     * @var \Pickstar\User\User
     */
    public $user;

    /**
     * @var Booking
     */
    public $booking;

    /**
     * Create a new event instance.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
        $this->user = $booking->client;
    }


}

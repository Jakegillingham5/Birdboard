<?php

namespace Pickstar\Events\Concerns;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

trait EsportsBase
{
    use Dispatchable, SerializesModels;

    /**
     * User that registered.
     *
     * @var \Pickstar\User\User
     */
    public $request_details;

    /**
     * Create a new event instance.
     *
     * @param \Pickstar\User\User $user
     */
    public function __construct($request_details)
    {
        $this->request_details = $request_details;
    }
}

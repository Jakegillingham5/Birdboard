<?php

namespace Pickstar\Events;

use Pickstar\User\User;
use Illuminate\Queue\SerializesModels;

class PaymentDetailsUpdated
{
    use SerializesModels;

    /**
     * User instance.
     *
     * @var \Pickstar\User\User
     */
    public $user;

    /**
     * Create a new event instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
}

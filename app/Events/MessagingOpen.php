<?php

namespace Pickstar\Events;

class MessagingOpen implements Contracts\HasBookingInstance
{
    use Concerns\BookingEvent;
}

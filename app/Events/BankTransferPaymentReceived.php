<?php

namespace Pickstar\Events;

use Pickstar\Payment\Payment;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class BankTransferPaymentReceived
{
    use Dispatchable, SerializesModels;

    /**
     * Payment instance.
     *
     * @var \Pickstar\Payment\Payment
     */
    public $payment;

    /**
     * Create a new payment received event instance.
     *
     * @param \Pickstar\Payment\Payment $payment
     */
    public function __construct(Payment $payment)
    {
        $this->payment = $payment;
    }
}

<?php

namespace Pickstar\Events;

use Pickstar\User\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class UserUpdated
{
    use Dispatchable, SerializesModels;

    /**
     * User that registered.
     *
     * @var \Pickstar\User\User
     */
    public $user;

    /**
     * Create a new event instance.
     *
     * @param \Pickstar\User\User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
}

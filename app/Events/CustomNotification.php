<?php

namespace Pickstar\Events;


use Illuminate\Queue\SerializesModels;
use Pickstar\User\User;

class CustomNotification
{
    use SerializesModels;

    public $user;

    public $data;

    public $channels = [];

    /**
     * Create a new notification instance.
     *
     * @param User $user
     * @param array $data
     * @param array $channels
     */
    public function __construct(User $user, array $data, array $channels)
    {
        $this->user = $user;
        $this->data = $data;
        $this->channels = $channels;
    }
}

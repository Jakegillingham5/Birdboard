<?php

namespace Pickstar\Events;

class BookingTermsAccepted implements Contracts\HasBookingInstance
{
    use Concerns\BookingEvent;
}

<?php


namespace Pickstar\Events;

class BookingRequestCompleteByDateChanged implements Contracts\HasBookingInstance
{
    use Concerns\BookingEvent;
}
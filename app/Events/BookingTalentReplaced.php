<?php

namespace Pickstar\Events;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Pickstar\Booking\Booking;
use Pickstar\User\User;

/**
 * Class BookingTalentReplaced
 * @package Pickstar\Events
 */
class BookingTalentReplaced
{
    use Dispatchable, SerializesModels;

    /**
     * @var Booking
     */
    public $booking;

    /***
     * @var User
     */
    public $replacement;

    /**
     * BookingTalentReplaced constructor.
     * @param Booking $booking
     * @param User $replacement
     */
    public function __construct(Booking $booking, User $replacement)
    {
        $this->booking = $booking;
        $this->replacement = $replacement;
    }
}

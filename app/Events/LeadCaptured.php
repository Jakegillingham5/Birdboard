<?php

namespace Pickstar\Events;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Queue\ShouldQueue;
use Pickstar\Marketing\Email;
use Pickstar\User\User;

class LeadCaptured implements ShouldQueue
{
    use Dispatchable, InteractsWithSockets;

    /**
     * @var User|Email
     */
    public $user;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }
}

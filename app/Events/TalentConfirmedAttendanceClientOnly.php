<?php

namespace Pickstar\Events;

use Pickstar\User\User;
use Pickstar\Booking\Booking;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class TalentConfirmedAttendanceClientOnly implements Contracts\HasBookingInstance
{
    use Dispatchable, SerializesModels;

    /**
     * Booking instance.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * Talent user instance.
     *
     * @var \Pickstar\User\User
     */
    public $talent;

    /*
     * Whether confirmation is for online or in-person
     *
     * @var eventType
     */
    public $eventType;

    /**
     * Create a new event instance.
     *
     * @param \Pickstar\Booking\Booking $booking
     * @param \Pickstar\User\User $talent
     * @param $eventType
     *
     * @return void
     */
    public function __construct(Booking $booking, User $talent, $eventType)
    {
        $this->eventType = $eventType;
        $this->booking = $booking;
        $this->talent = $talent;
    }
}

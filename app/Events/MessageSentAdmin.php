<?php

namespace Pickstar\Events;

use Pickstar\Message\Thread;
use Pickstar\User\User;
use Pickstar\Message\Message;
use Pickstar\Booking\Booking;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use SendGrid\Contacts\Recipient;

class MessageSentAdmin implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * User that sent the message
     *
     * @var \Pickstar\User\User
     */
    public $sender;

    /**
     * Message Details
     *
     * @var \Pickstar\Message\Message
     */
    public $message;

    /**
     * Thread that the message was sent in
     *
     * @var \Pickstar\Message\Thread
     */
    public $thread;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $sender, Message $message, Thread $thread)
    {
        $this->sender = $sender;
        $this->message = $message;
        $this->thread = $thread;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('chat.admin');
    }

    /**
     * The data that the message will be broadcast with
     *
     * @return array
     */
    public function broadcastWith()
    {
//        todo - find a way to pass through an unread count for the admin here
        return ['message' => new \Pickstar\Http\Resources\Message($this->message->load('user')), 'unread_count' => 0];
    }
}

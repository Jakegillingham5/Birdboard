<?php

namespace Pickstar\Events;

use Pickstar\User\User;
use Pickstar\Booking\Booking;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

class TalentAccepted
{
    use Dispatchable, SerializesModels;

    /**
     * Talent that was accepted.
     *
     * @var \Pickstar\User\User
     */
    public $user;

    /**
     * Booking that talent was accepted for.
     *
     * @var \Pickstar\Booking\Booking
     */
    public $booking;

    /**
     * Create a new event instance.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     */
    public function __construct(User $user, Booking $booking)
    {
        $this->user = $user;
        $this->booking = $booking;
    }
}

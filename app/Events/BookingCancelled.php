<?php

namespace Pickstar\Events;

class BookingCancelled implements Contracts\HasBookingInstance
{
    use Concerns\BookingEvent;
}

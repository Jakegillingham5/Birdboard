<?php

namespace Pickstar\Tag;

use Pickstar\Tag\Tag;
use Pickstar\User\User;
use Pickstar\Model\Concerns\Revisionable;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class AssociatedTagPivot extends Pivot
{
    use Revisionable;

    /**
     * Type-casted columns.
     *
     * @var array
     */
    protected $casts = [
        'protected' => 'bool'
    ];

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'associated_tags';

    /**
     * A talent tag pivot belongs to a talent.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent_tag(): BelongsTo
    {
        return $this->belongsTo(Tag::class, 'parent_tag_id');
    }

    /**
     * A talent pivot belongs to a tag.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function associated_tag(): BelongsTo
    {
        return $this->belongsTo(Tag::class, 'associated_tag_id');
    }

}

<?php

namespace Pickstar\Tag;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Collection;
use Pickstar\User\User;
use Pickstar\Booking\TalentTagPivot;
use Illuminate\Database\Eloquent\Model;
use Pickstar\Model\Concerns\Transformable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Tag extends Model
{
    use SoftDeletes,
        Transformable;

    /**
     * Table name.
     *
     * @var string
     */
    public $table = 'tags';

    /**
     * Enable timestamps.
     *
     * @var boolean
     */
    public $timestamps = true;

    /**
     * Available tag types.
     *
     * @var array
     */
    public $availableTypes = [
        'industry',
        'sport',
        'club',
        'sponsor',
        'charity',
        'general',
        'theme'
    ];

    /**
     * Fillable columns.
     *
     * @var array
     */
    protected $fillable = [
        'merged_tag_id',
        'name',
        'type',
        'deleted_at'
    ];

    /**
     * Hidden columns.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    /**
     * A tag can belong to many talent.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function talent(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'talent_tags', 'tag_id', 'talent_id')
            ->using(TalentTagPivot::class)
            ->as('tag_details')
            ->withPivot('retired');
    }

    /**
     * A tag can have many associated tags that it is the parent of
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function associatedTags(): BelongsToMany
    {
        return $this->belongsToMany(Tag::class, 'associated_tags', 'parent_tag_id', 'associated_tag_id')
            ->using(AssociatedTagPivot::class)
            ->as('associated_tags')
            ->withPivot('protected');
    }

    /**
     * A tag can have many parent tags that it is associated with
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function parentTags(): BelongsToMany
    {
        return $this->belongsToMany(Tag::class, 'associated_tags', 'associated_tag_id', 'parent_tag_id')
            ->using(AssociatedTagPivot::class)
            ->as('parent_tags')
            ->withPivot('protected');
    }

    public function aliases(): HasMany
    {
        return $this->hasMany(Tag::class, 'parent_id');
    }

    public function parent(): BelongsTo
    {
        return $this->belongsTo(Tag::class, 'parent_id', 'id');
    }

    /**
     * Scope to ignore tags that are aliases
     *
     * @param $query
     * @return mixed
     */
    public function scopeIgnoreAliases($query)
    {
        return $query->whereNull('parent_id');
    }

    /**
     * Scope tags that are aliases
     *
     * @param $query
     * @return mixed
     */
    public function scopeAliases($query)
    {
        return $query->whereNotNull('parent_id');
    }

    /**
     * Scope to ignore the industry tags.
     *
     * @param \Illuminate\Database\Eloquent\Query $query
     *
     * @return mixed
     */
    public function scopeIgnoreIndustry($query)
    {
        return $query->where('type', '!=', 'industry');
    }

    /**
     * Scope to ignore the general tags.
     *
     * @param \Illuminate\Database\Eloquent\Query $query
     *
     * @return mixed
     */
    public function scopeIgnoreGeneral($query)
    {
        return $query->where('type', '!=', 'general');
    }

    /**
     * Scope to the industry tags.
     *
     * @param \Illuminate\Database\Eloquent\Query $query
     *
     * @return mixed
     */
    public function scopeIndustry($query)
    {
        return $query->where('type', 'industry');
    }

    /**
     * Scope to the sport tags.
     *
     * @param \Illuminate\Database\Eloquent\Query $query
     *
     * @return mixed
     */
    public function scopeSport($query)
    {
        return $query->where('type', 'sport');
    }

    /**
     * Scope to the club tags.
     *
     * @param \Illuminate\Database\Eloquent\Query $query
     *
     * @return mixed
     */
    public function scopeClub($query)
    {
        return $query->where('type', 'club');
    }

    /**
     * Scope to the sponsor tags.
     *
     * @param \Illuminate\Database\Eloquent\Query $query
     *
     * @return mixed
     */
    public function scopeSponsor($query)
    {
        return $query->where('type', 'sponsor');
    }

    /**
     * Scope to the charity association tags.
     *
     * @param \Illuminate\Database\Eloquent\Query $query
     *
     * @return mixed
     */
    public function scopeCharity($query)
    {
        return $query->where('type', 'charity');
    }

    /**
     * Scope to the general tags.
     *
     * @param \Illuminate\Database\Eloquent\Query $query
     *
     * @return mixed
     */
    public function scopeGeneral($query)
    {
        return $query->where('type', 'general');
    }

    /**
     * Scope to the general tags.
     *
     * @param \Illuminate\Database\Eloquent\Query $query
     *
     * @return mixed
     */
    public function scopeTheme($query)
    {
        return $query->where('type', 'theme');
    }

    /**
     * Get the tags associated tags as a collection
     *
     * @return Collection
     */
    public function getAssociatedTagsAttribute()
    {
        return $this->associatedTags()->get();
    }

    /**
     * Get the tags parent tags as a collection
     *
     * @return Collection
     */
    public function getParentTagsAttribute()
    {
        return $this->parentTags()->get();
    }

    /**
     * Get the tags talent as a collection
     *
     * @return Collection
     */
    public function getTalentAttribute()
    {
        return $this->talent()->get();
    }

    /**
     * Get the tags aliases as a collection
     *
     * @return Collection
     */
    public function getAliasesAttribute()
    {
        return $this->aliases()->get();
    }

    /**
     * Get all of the associated tags and filter them to associated sponsors
     *
     * @return Collection
     */
    public function getAssociatedSponsorsAttribute()
    {
        return $this->associatedTags()->sponsor()->get()->merge($this->parentTags()->sponsor()->get());
    }

    /**
     * Get all of the associated tags and filter them to associated clubs
     *
     * @return Collection
     */
    public function getAssociatedClubsAttribute()
    {
        return $this->associatedTags()->club()->get()->merge($this->parentTags()->club()->get());
    }

    /**
     * Get all of the associated tags and filter them to associated sports
     *
     * @return Collection
     */
    public function getAssociatedSportsAttribute()
    {
        return $this->associatedTags()->sport()->get()->merge($this->parentTags()->sport()->get());
    }

    /**
     * Get all of the associated tags and filter them to associated charities
     *
     * @return Collection
     */
    public function getAssociatedCharitiesAttribute()
    {
        return $this->associatedTags()->charity()->get()->merge($this->parentTags()->charity()->get());
    }

    public function associateAssociatedTags(Collection $tags)
    {
        $tags = $tags->map(function ($tag) {
            return [
                'parent_tag_id' => $this->id,
                'associated_tag_id' => $tag['id'],
                'protected' => true
            ];
        });
        $this->associatedTags()->sync($tags);
    }

    public function associateParentTags(Collection $tags)
    {
        $tags = $tags->map(function ($tag) {
            return [
                'parent_tag_id' => $tag['id'],
                'associated_tag_id' => $this->id,
                'protected' => true
            ];
        });
        $this->parentTags()->sync($tags);
    }
}

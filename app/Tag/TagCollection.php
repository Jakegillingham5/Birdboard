<?php

namespace Pickstar\Tag;

use Illuminate\Database\Eloquent\Collection;

class TagCollection extends Collection
{
    /**
     * Gets and flattens associated tags into an un-nested  unique collection.
     *
     * @param bool $unique If true, filters out duplicate tags
     *
     * @return TagCollection
     */
    public function withAssociated(bool $unique = true)
    {
        $tags = new Collection();
        $this->map(function ($tag) use ($tags) {
            if (!$tag->pivot->retired && $tag->associatedTags()) {
                return $tags->push($tag->associatedTags()->get());
            }
        }, new static);

        $tags = $tags->flatten()->unique('name');

        return $tags;
    }
}

<?php

namespace Pickstar\Http\Resources;

use Pickstar\User\User;
use Illuminate\Http\Resources\Json\JsonResource;

class Client extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'name' => $this->name,
            'company' => $this->company,
            'email' => $this->email,
            'phone' => $this->phone,
            'position' => $this->position,
            'commission_rate' => $this->commission_rate,
            'twitter_provider_id' => $this->twitter_provider_id,
            'facebook_provider_id' => $this->facebook_provider_id,
            'status' => $this->status,
            'email_verified' => $this->email_verified,
            'push_notifications' => $this->push_notifications,
            'can_set_esports' => $this->can_set_esports,
        ];
    }
}

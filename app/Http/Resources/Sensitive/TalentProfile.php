<?php

namespace Pickstar\Http\Resources\Sensitive;

use Illuminate\Http\Resources\Json\JsonResource;

class TalentProfile extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'profile_image_url' => $this->profile_image_url,
            'highest_followers' => $this->highest_followers,
            'highest_followers_icon' => $this->highest_followers_icon,
            'tagline' => $this->tagline,
        ];
    }
}

<?php

namespace Pickstar\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Thread extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = $request->user();

        $pinned = false;
        if ($this->participants()->find($user->id)) {
            $pinned = $this->participants()->find($user->id)->thread_participation->pinned;
        }

        return [
            'id' => $this->id,
            'unread_count' => (int) $this->unread_count,
            'activated' => $this->userThreadActivated($user->id),
            'active' => $this->active,
            'pinned' => $pinned,
            'typing' => false,
            'booking' => $this->whenLoaded('booking', function () {
                return new Booking($this->booking->load('payments', 'client'));
            }),
            'terms_accepted' => $this->whenLoaded('participants', function () use ($user) {
                if ($this->participants->firstWhere('id', $user->id)) {
                    return $this->participants->firstWhere('id', $user->id)->thread_participation->terms_accepted;
                }
                return false;
            }),
            'clients' => $this->whenLoaded('clients', function () {
                return ThreadUser::collection($this->clients);
            }),
            'talents' => $this->whenLoaded('talents', function () {
                return ThreadUser::collection($this->talents);
            }),
            'participants' => $this->whenLoaded('participants', function () {
                return ThreadUser::collection($this->participants->sortByDesc(function ($participant) {
                    return $participant->latestThreadMessageDate($this->id);
                }));
            }),
            'messages' => $this->whenLoaded('messages', function () {
                return Message::collection($this->messages);
            }),
            'latest_message' => $this->latest_message,
        ];
    }
}

<?php

namespace Pickstar\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TalentProfile extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'profile_image_url' => $this->user->profile_image_url,
            'highest_followers' => $this->highest_followers,
            'highest_followers_icon' => $this->highest_followers_icon,
            'gender' => $this->gender,
            'manager' => $this->manager,
            'agency' => $this->agency,
            'tagline' => $this->tagline,
            'description' => $this->description,
            'facebook_link' => $this->facebook_link,
            'twitter_link' => $this->twitter_link,
            'instagram_link' => $this->instgram_link,
            'linkedin_link' => $this->linkedin_link,
            'facebook_follower_count' => $this->facebook_follower_count,
            'twitter_follower_count' => $this->twitter_follower_count,
            'instagram_follower_count' => $this->instagram_follower_count,
            'public' => $this->public,
            'manager_email' => $this->manager_email,
            'notification_preference' => $this->notification_preference,
        ];
    }
}

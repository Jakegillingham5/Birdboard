<?php

namespace Pickstar\Http\Resources;

use Pickstar\Booking\Booking;
use Illuminate\Http\Resources\Json\JsonResource;

class SystemNotification extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = $request->user();

        $data = $this->data;

        $options = [
            \Pickstar\Message\SystemNotification::LINK_REQUESTS_SHOW,
            \Pickstar\Message\SystemNotification::LINK_VETTING_SHOW,
            \Pickstar\Message\SystemNotification::LINK_BOOKINGS_SHOW
        ];
        if (array_search($data['link_type'], $options)) {
            $booking = Booking::everything()->find($this->data['link_id']);
            switch ($booking->type) {
                case Booking::TYPE_BOOKING:
                    $data['link_type'] = \Pickstar\Message\SystemNotification::LINK_BOOKINGS_SHOW;
                    break;
                case Booking::TYPE_REQUEST:
                    if ($request->user()->isAdmin() && $booking->status === Booking::STATUS_VETTING) {
                        $data['link_type'] = \Pickstar\Message\SystemNotification::LINK_VETTING_SHOW;
                        break;
                    }
                    $data['link_type'] = \Pickstar\Message\SystemNotification::LINK_REQUESTS_SHOW;
            }
        }
        return [
            'notif_type' => $data['notif_type'],
            'link_type' => $data['link_type'],
            'link_id' => $data['link_id'],
            'related_id' => $data['related_id'],
            'text' => $data['text'],
        ];
    }
}

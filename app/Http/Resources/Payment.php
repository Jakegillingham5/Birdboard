<?php

namespace Pickstar\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Payment extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = $request->user();

        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'booking_id' => $this->booking_id,
            'type' => $this->type,
            'status' => $this->status,
            'amount' => $this->amount,
            'due_date' => $this->due_date,
            'sent_date' => $this->sent_date,
            'paid_date' => $this->paid_date,
            'total_inclusive' => $this->total_inclusive,
            'method' => $this->method,
            'transaction' => Transaction::make($this->transaction),
        ];
    }
}

<?php

namespace Pickstar\Http\Resources;

use Pickstar\User\User;
use Illuminate\Http\Resources\Json\JsonResource;

class Talent extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'slug' => $this->slug,
            'sport_club' => $this->sport_club,
            'is_featured' => $this->is_featured,
            'location_id' => $this->location_id,
            'photos_count' => $this->photos_count,
            'profile_image_present' => $this->profile_image_present,
            $this->when(
                $this->userCanSeePhoneAndEmail($request->user()),
                $this->merge([
                    'email' => $this->email,
                    'phone' => $this->phone,
                    'response' => new TalentResponse($this->whenLoaded('response')),
                    'profile' => new TalentProfile($this->whenLoaded('profile')),
                ]),
                $this->merge([
                    'response' => new Sensitive\TalentResponse($this->whenLoaded('response')),
                    'profile' => new Sensitive\TalentProfile($this->whenLoaded('profile')),
                ])
            ),
        ];
    }

    protected function userCanSeePhoneAndEmail(?User $user): bool
    {
        if ($user && ($user->is($this->resource) || $user->isAdmin() || $this->resource->isManagedBy($user))) {
            return true;
        }

        return false;
    }
}

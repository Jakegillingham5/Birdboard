<?php

namespace Pickstar\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class TalentResponse extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'applied_date' => $this->applied_date,
            'attendance_confirmed' => $this->attendance_confirmed,
            'client_accepted' => $this->client_accepted,
            'comment' => $this->comment,
            'created_at' => $this->created_at,
            'expired' => $this->expired,
            'expiry_date' => $this->expiry_date,
            'priority' => $this->priority,
            'shortlisted' => $this->shortlisted,
            'status' => $this->status,
            'talent_id' => $this->talent_id,
            'talent_paid_notif_sent' => $this->talent_paid_notif_sent,
            'updated_at' => $this->updated_at,
            'date_applied_for' => $this->date_applied_for ? Carbon::parse($this->date_applied_for)->format('Y-m-d') : null,
            'online_completed_date' => $this->online_completed_date,
            'in_person_completed_date' => $this->in_person_completed_date,
            'xero_bill_reference' => $this->xero_bill_reference,
            'xero_deposit_invoice_reference' => $this->xero_deposit_invoice_reference,
            'xero_final_invoice_reference' => $this->xero_final_invoice_reference,
            'decline_reason' => $this->decline_reason,
            'online_participation_confirmed' => $this->online_participation_confirmed,
        ];
    }
}

<?php

namespace Pickstar\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Message extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // the reason we have duplicates e.g. id and _id, created_at and createdAt
        // is for mobile app vs web app, the package we use (GiftedChat) expects
        // _id etc, and it is less processing for the client if we just do it here
        return [
            'created_at' => $this->created_at,
            'createdAt' => $this->created_at,
            'deleted_at' => $this->deleted_at,
            'id' => $this->id,
            '_id' => $this->id,
            'sender_image' => $this->sender_image,
            'text' => $this->text,
            'thread_id' => $this->thread_id,
            'booking_id' => $this->thread->booking->id,
            'updated_at' => $this->updated_at,
            'user_id' => $this->user_id,
            'sent' => true,
            'received' => true,
            'user' => $this->whenLoaded('user', function () {
                return new ThreadUser($this->user);
            }),
        ];
    }
}

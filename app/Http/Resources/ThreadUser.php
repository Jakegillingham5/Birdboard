<?php

namespace Pickstar\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ThreadUser extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // the reason we have duplicates e.g. id and _id
        // is for mobile app vs web app, the package we use (GiftedChat) expects
        // _id etc, and it is less processing for the client if we just do it here
        return [
            'id' => $this->user_id ? $this->user_id : $this->id,
            '_id' => $this->user_id ? $this->user_id : $this->id,
            'name' => $this->name,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'role' => $this->role,
            'profile_image_hash' => $this->profile_image_hash,
            'profile_image_url' => $this->profile_image_url,
            'avatar' => $this->profile_image_url,
            'message_id' => $this->thread_participation ? $this->thread_participation->message_id : true,
            'terms_accepted' => $this->thread_participation ? $this->thread_participation->terms_accepted : true,
            'pinned' => $this->thread_participation ? $this->thread_participation->pinned : false,
            'thread_id' => $this->thread_participation ? $this->thread_participation->thread_id : null
        ];
    }
}

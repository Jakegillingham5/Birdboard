<?php

namespace Pickstar\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Pickstar\User\User;

class Booking extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = $request->user();

        return [
            'id' => $this->id,
            'billboard_expiry_date' => $this->billboard_expiry_date,
            'vetted_date' => optional($this->vetted_date)->toDateTimeString(),
            'opportunity_id' => $this->opportunity_id,
            'online_opportunity_id' => $this->online_opportunity_id,
            'booking_notifications_id' => $this->booking_notifications_id,
            'type' => $this->type,
            'status' => $this->status,
            'contextual_status' => $this->contextualStatusFor($user),
            'appearance_type' => $this->appearance_type,
            'name' => $this->name,
            'description' => $this->description,
            'requirements' => $this->requirements,
            'duration' => $this->duration,
            'required_stars' => $this->required_stars,
            'establishment' => $this->establishment,
            'additional_location_detail' => $this->additional_location_detail,
            'street_address' => $this->street_address,
            'suburb' => $this->suburb,
            'state' => $this->state,
            'country' => $this->country,
            'postcode' => $this->postcode,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'complete_by_date' => $this->complete_by_date,
            'complete_by_date_formatted' => $this->complete_by_date_formatted,//
            'date_options' => $this->date_options,
            'date' => $this->date ? Carbon::parse($this->date)->format('Y-m-d') : null,
            'has_date_options_only' => $this->has_date_options_only,
            'tz_date' => $this->tz_date,
            'utc_date' => optional($this->utc_date)->toDateTimeString(),
            'timezone' => $this->timezone,
            'arrival_time' => $this->arrival_time,
            'athlete_start_time' => $this->athlete_start_time,
            'others_can_apply' => $this->others_can_apply,
            'date_flexible_or_unconfirmed' => $this->date_flexible_or_unconfirmed,
            'time_flexible_or_unconfirmed' => $this->time_flexible_or_unconfirmed,
            'budget_not_confirmed' => $this->budget_not_confirmed,
            'expenses_covered' => $this->expenses_covered,
            'promoted_publicly' => $this->promoted_publicly,
            'social_media_content' => $this->social_media_content,
            'scheduled_date' => $this->scheduled_date,
            'completed_date' => $this->completed_date,
            'talent_expire' => $this->talent_expire,
            'opportunity_name' => $this->opportunity_name,
            'utc_end_date' => $this->utc_end_date,
            'latest_note' => $this->latest_note,
            'request_form_version' => $this->request_form_version,
            'talent_can_comment' => $this->talent_can_comment,
            'expires' => $this->expires,

            // Admin and Client only fields.
            $this->mergeWhen($user->isAdmin() || $user->isClient(), [
                'client_id' => $this->client_id,
                'budget' => $this->budget,
                'budget_per_star' => $this->budget_per_star,
                'commission_rate' => $this->commission_rate,
                'commission_dollar' => $this->commission_dollar,
                'commission_type' => $this->commission_type,
                'client_accepted_terms' => $this->client_accepted_terms,
                'client_accepted_terms_date' => optional($this->client_accepted_terms_date)->toDateTimeString(),
                'coupon' => $this->coupon,
                'withdraw_reason' => $this->withdraw_reason,
            ]),

            // Admin only fields.
            $this->mergeWhen($user->isAdmin(), [
                'talent_response_count' => $this->talent_response_count,
                'market_segment' => $this->market_segment,
                'admin_opportunity_id' => $this->admin_opportunity_id,
                'budget_per_star_as_user_rate' => $this->budget_per_star_as_user_rate,
                'created_at' => optional($this->created_at)->toDateTimeString(),
                'talent_notified' => Talent::collection($this->whenLoaded('talentNotified')),
                'managers_notified' => $this->whenLoaded('managersNotified'),
                'admin_users' => User::select(['first_name', 'last_name', 'id'])->where('role', 'admin')->get(),
                'assigned_admin' => $this->assigned_admin,
                'assigned_admin_name' => $this->assigned_admin_name,
                'tags' => $this->tags,
            ]),

            $this->mergeWhen($user->isManager(), [
                'tags' => $this->tags,
            ]),

            // Talent and manager only fields.
            $this->mergeWhen($user->isTalent() || $user->isManager(), [
                'budget_per_star_as_user_rate' => $this->budget_per_star_as_user_rate
            ]),

            // Role sensitive relations.
            'client' => $this->whenLoaded('client', function () use ($user) {
                return $this->when(
                    $user->can('see-client', $this->resource),
                    new Client($this->client),
                    new Sensitive\Client($this->client)
                );
            }),
            'payments' => $this->whenLoaded('payments', function () use ($user) {
                return $this->when(
                    $user->can('see-payments', $this->resource),
                    Payment::collection($this->payments),
                    Sensitive\Payment::collection($this->payments)
                );
            }),
            'threads' => $this->whenLoaded('threads', function () use ($user) {
                return $this->when(
                    $user->can('see-threads', $this->resource),
                    Thread::collection($this->threads),
                    Sensitive\Thread::collection($this->threads)
                );
            }),
            'unread_count' => $this->userUnreadCount($user->id),
            'talent' => Talent::collection($this->whenLoaded('talent')),
            'opportunity' => $this->whenLoaded('opportunity'),
            'online_opportunity' => $this->whenLoaded('onlineOpportunity'),
        ];
    }
}

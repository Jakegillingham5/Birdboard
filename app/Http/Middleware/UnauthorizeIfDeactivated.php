<?php

namespace Pickstar\Http\Middleware;

use Closure;
use Exception;

class UnauthorizeIfDeactivated
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() && $request->user()->isDeactivated()) {
            auth('web')->logout();

            // Attempts to blacklist the API JWT, capture any failures but continue as normal.
            try {
                auth('api')->logout();
            } catch (Exception $exception) {
                report($exception);
            }

            if ($request->expectsJson()) {
                return response()->json(['message' => 'Your account has been deactivated.'], 401);
            } else {
                return redirect()->route('home');
            }
        }

        return $next($request);
    }
}

<?php

namespace Pickstar\Http\Middleware;

use Closure;
use Tymon\JWTAuth\JWT;
use Illuminate\Contracts\Auth\Factory;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class RefreshTokens
{
    /**
     * JWT manager instance.
     *
     * @var \Tymon\JWTAuth\JWT
     */
    protected $jwt;

    /**
     * Create a new refresh token middleware instance.
     *
     * @param \Tymon\JWTAuth\JWT $jwt
     */
    public function __construct(JWT $jwt, Factory $auth)
    {
        $this->jwt = $jwt;
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     *
     * @throws \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if (
            (app()->environment('testing') && !$request->headers->has('authorization')) ||
            $this->auth->getDefaultDriver() == 'oauth'
        ) {
            return $response;
        }

        try {
            $token = $this->jwt->parseToken()->refresh();
        } catch (JWTException $exception) {
            throw new UnauthorizedHttpException('jwt', $exception->getMessage(), $exception, $exception->getCode());
        }

        $response->header('Authorization', sprintf('Bearer %s', $token));

        return $response;
    }
}

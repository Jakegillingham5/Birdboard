<?php
/**
 * Created by PhpStorm.
 * User: jpickstar
 * Date: 2019-03-08
 * Time: 12:04
 */

namespace Pickstar\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Pickstar\Providers\UTMServiceProvider;

class UTMTracker
{

    public function handle(Request $request, Closure $next)
    {
        UTMServiceProvider::storeUtm($request);

        return $next($request);
    }
}
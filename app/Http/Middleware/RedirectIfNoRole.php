<?php

namespace Pickstar\Http\Middleware;

use Closure;
use Illuminate\Routing\Router;

class RedirectIfNoRole
{
    /**
     * Router instance.
     *
     * @var \Illuminate\Routing\Router
     */
    protected $router;

    /**
     * Create a new redirect if no role instance.
     *
     * @param \Illuminate\Routing\Router $router
     *
     * @return void
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * Handle redirect to registeration role select if no role.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() && $request->user()->hasNoRole() && $this->notVisitingAllowedRoute()) {
            return redirect()->route('register.social');
        }

        return $next($request);
    }

    /**
     * Determine if current route is not the logout or the register type routes.
     *
     * @return bool
     */
    protected function notVisitingAllowedRoute()
    {
        $allowedNames = ['register.social', 'register.social.store', 'social.complete', 'logout'];

        return ! in_array($this->router->currentRouteName(), $allowedNames);
    }
}

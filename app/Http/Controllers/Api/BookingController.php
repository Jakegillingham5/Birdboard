<?php

namespace Pickstar\Http\Controllers\Api;

use Illuminate\Http\Request;
use Pickstar\Booking\Booking;
use Illuminate\Http\JsonResponse;
use Pickstar\Booking\TalentPivot;
use Pickstar\Events\BookingCancelled;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Events\ClientCancelledBooking;
use Pickstar\Events\TalentCancelledBooking;
use Pickstar\Http\Concerns\DeniesForBookings;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Http\Transformers\BookingTransformer;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Pickstar\Http\Resources\Booking as BookingResource;
use Pickstar\Http\Requests\Api\Booking\UpdateBookingRequest;

class BookingController extends Controller
{
    use DeniesForBookings,
        DeniesHttpRequests;

    /**
     * Get a list of bookings based on the user role.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Resources\Json\ResourceCollection
     */
    public function index(Request $request): ResourceCollection
    {
        $user = $request->user();

        $pageSize = $request->query('page_size', 25);

        $bookings = Booking::select('bookings.*')
            ->with(['client', 'threads', 'payments', 'onlineOpportunity', 'opportunity', 'talent' => function ($query) use ($user) {
                $query->onlyMyselfIfTalent($user);

                $query->onlyManagedIfManager($user);
            }])
            ->when($user->isClient(), function ($query) use ($user) {
                $query->where('client_id', $user->id);
            })
            ->when($user->isTalent() || $user->isManager(), function ($query) use ($user, $request) {
                $query->join('booking_talent', function ($join) use ($user) {
                    $join->on('booking_talent.booking_id', '=', 'bookings.id')
                    ->when($user->isTalent(), function ($query) use ($user) {
                            $query->where('booking_talent.talent_id', $user->id);
                        })
                        ->when($user->isManager(), function ($query) use ($user) {
                            $query->whereIn('booking_talent.talent_id', $user->managedTalent->pluck('id'));
                        });
                });

                $query->haveBeenAcceptedWithCancelled();
            })
            ->when($user->isAdmin(), function ($query) use ($user, $request) {
                $query->when($request->query('clientId'), function ($query, $client) {
                    $query->where('bookings.client_id', $client);
                });

                $query->when($request->query('managerId'), function ($query, $management) {
                    $query->whereHas('talentAttending', function ($query) use ($management) {
                        $query->where('management_id', $management);
                    });
                });
            })
            ->applyFiltersAndSorting($request, [
                'customStatusFilterCallback' => function ($query) use ($request, $user) {
                    $query->when($request->query('status', 'active') === 'active', function ($query) use ($user) {
                        $query->whereIn('bookings.status', [
                            Booking::STATUS_SCHEDULED,
                            Booking::STATUS_BOOKED,
                            Booking::STATUS_HOLD
                        ]);

                        $query->when($user->isTalent() || $user->isManager(), function ($query) {
                            $query->where('booking_talent.status', TalentPivot::STATUS_APPLIED);
                        });
                    });

                    $query->when($request->query('status') === 'event', function ($query) use ($user) {
                        $query->whereIn('bookings.status', [
                                Booking::STATUS_SCHEDULED,
                                Booking::STATUS_BOOKED,
                                Booking::STATUS_HOLD
                        ]);

                        $query->when($user->isTalent() || $user->isManager(), function ($query) {
                            $query->where('booking_talent.status', TalentPivot::STATUS_APPLIED);
                        });

                        $query->whereIn('bookings.appearance_type', [
                                Booking::APPEARANCE_BOTH,
                                Booking::APPEARANCE_IN_PERSON
                        ]);
                    });

                    $query->when($request->query('status') === 'campaign', function ($query) use ($user) {
                        $query->whereIn('bookings.status', [
                                Booking::STATUS_SCHEDULED,
                                Booking::STATUS_BOOKED,
                                Booking::STATUS_HOLD
                        ]);

                        $query->when($user->isTalent() || $user->isManager(), function ($query) {
                            $query->where('booking_talent.status', TalentPivot::STATUS_APPLIED);
                        });

                        $query->whereIn('bookings.appearance_type', [
                                Booking::APPEARANCE_BOTH,
                                Booking::APPEARANCE_ONLINE
                        ]);
                    });

                    $query->when($request->query('status') === 'archived', function ($query) use ($user) {
                        $query->where('bookings.status', Booking::STATUS_CANCELLED);

                        $query->when($user->isTalent() || $user->isManager(), function ($query) use ($user) {
                            $query->orWhere('booking_talent.status', TalentPivot::STATUS_CANCELLED);
                        });

                        $query->when(!$user->isAdmin(), function ($query) use ($user) {
                            $query->orWhere('bookings.status', Booking::STATUS_COMPLETED);
                        });
                    });

                    $query->when($request->query('status') === 'completed-event', function ($query) use ($user) {
                        $query->where('bookings.status', Booking::STATUS_COMPLETED);

                        $query->whereIn('bookings.appearance_type', [
                                Booking::APPEARANCE_BOTH,
                                Booking::APPEARANCE_IN_PERSON
                        ]);
                    });
                    $query->when($request->query('status') === 'completed-campaign', function ($query) use ($user) {
                        $query->where('bookings.status', Booking::STATUS_COMPLETED);

                        $query->whereIn('bookings.appearance_type', [
                                Booking::APPEARANCE_BOTH,
                                Booking::APPEARANCE_ONLINE
                        ]);
                    });
                },
                'defaultSortingCallback' => function ($query) {
                    // Default the order by the "online" appearance type so these appear at the top of the list,
                    // that's because they don't have a date. This is only when sorting by date and not by
                    // the price.
                    $query->orderByRaw('FIELD(bookings.appearance_type, ?) DESC', Booking::APPEARANCE_ONLINE);

                    $query->oldest('tz_date');
                },
                'customDateSortingCallback' => function ($query, $request) {
                    $query->orderByRaw('FIELD(bookings.appearance_type, ?) DESC', Booking::APPEARANCE_ONLINE)
                        ->when($request->query('order') === 'asc', function ($query) {
                            $query->oldest('tz_date');
                        }, function ($query) {
                            $query->latest('tz_date');
                        });
                },
                'customCreatedAtSortingCallback' => function ($query, $request) {
                    $query->when($request->query('order') === 'asc', function ($query) {
                        $query->oldest('created_at');
                    }, function ($query) {
                        $query->latest('created_at');
                    });
                },
                'customCompletedDateSortingCallback' => function ($query, $request) {
                    $query->when($request->query('order') === 'asc', function ($query) {
                        $query->oldest('completed_date');
                    }, function ($query) {
                        $query->latest('completed_date');
                    });
                },
                'customScheduledDateSortingCallback' => function ($query, $request) {
                    $query->when($request->query('order') === 'asc', function ($query) {
                        $query->oldest('scheduled_date');
                    }, function ($query) {
                        $query->latest('scheduled_date');
                    });
                },
                'sortingDateColumn' => 'tz_date',
                'filteringDateColumn' => 'tz_date'
            ])
            ->groupBy('bookings.id')
            ->paginate($pageSize);

        return BookingResource::collection($bookings);
    }

    /**
     * Show a single booking.
     *
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, Booking $booking): BookingResource
    {
        $this->authorize('view', $booking);

        $booking->loadDefaultRelations($request);

        return new BookingResource($booking);
    }

    /**
     * Update a booking.
     *
     * @param \Pickstar\Http\Requests\Api\Booking\UpdateBookingRequest $request
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateBookingRequest $request, Booking $booking): BookingResource
    {
        $booking->updateFromRequestPayload($request);

        $booking->updatePaymentsIfBudgetChanged();

        return new BookingResource($booking);
    }

    /**
     * Cancel a booking.
     *
     * Depending on who is performing this action will determine how the booking is cancelled.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, Booking $booking): BookingResource
    {
        $this->authorize('cancel', $booking);

        $this->denyIfBookingIsCompleted($booking);

        $this->denyIfBookingIsCancelled($booking);

        // If user is an admin then we can mark the booking as cancelled and fire off the cancelled
        // event which will dispatch any notifications necessary.
        if ($request->user()->isAdmin()) {
            $booking->fill(['status' => Booking::STATUS_CANCELLED])->save();
            foreach ($booking->threads as $thread) {
                $thread->fill(['active' => false])->save();
            }

            event(new BookingCancelled($booking));
        }

        // If user is a talent then we mark their response as cancelled. This will cause the booking
        // to show as cancelled for this talent only. Fires off the talent cancelled event which
        // will dispatch any notifications necessary.
        if ($request->user()->isTalent()) {
            $booking->talent()->updateExistingPivot($request->user()->id, [
                'status' => TalentPivot::STATUS_CANCELLED
            ]);
            $threads = $booking->threads()->whereHas('participants', function ($query) use ($request) {
                $query->where('user_id', $request->user()->id);
            })->get();
            foreach ($threads as $thread) {
                $thread->fill(['active' => false])->save();
            }

            event(new TalentCancelledBooking($request->user(), $booking));
        }

        // If user is a client then we mark the booking as "on hold". This means an admin will need
        // to action the cancellation by either cancelling the booking or restoring it. Fires off
        // the client cancelled event which will dispatch any notifications necessary.
        if ($request->user()->isClient()) {
            $this->denyIfBookingIsOnHold($booking);

            $booking->fill([
                'status' => Booking::STATUS_HOLD,
                'withdraw_reason' => $request->input('reason'),
                ])->save();
            foreach ($booking->threads as $thread) {
                $thread->fill(['active' => false])->save();
            }

            event(new ClientCancelledBooking($booking));
        }

        $booking->loadDefaultRelations($request);

        return new BookingResource($booking);
    }
}

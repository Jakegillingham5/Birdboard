<?php

namespace Pickstar\Http\Controllers\Api;

use Pickstar\Booking\Booking;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Services\RecommendationService;
use Illuminate\Http\Request;
use Pickstar\Http\Controllers\Controller;

class RecommendationsController extends Controller
{

    use DeniesHttpRequests;

    /**
     * Get all talent that meet the given criteria
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Support\Collection
     */
    public function index(Request $request, Booking $booking, RecommendationService $recommendationService)
    {
        $user = $request->user();

        if(!$user->isAdmin()) {
            $this->deny('Recommendations can only be viewed by administrators');
        }

        return response()->json($recommendationService->getStars($request, $booking, $user));
    }
}

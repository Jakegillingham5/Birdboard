<?php

namespace Pickstar\Http\Controllers\Api;

use Illuminate\Http\Request;
use Pickstar\Http\Controllers\Controller;

class AuthorizeController extends Controller
{
    public function index(Request $request)
    {
        return response()->json(
            $request->user()->append('pinned_threads')->load('profile')
        );
    }
}

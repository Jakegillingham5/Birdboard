<?php

namespace Pickstar\Http\Controllers\Api;

use Exception;
use Pickstar\Events\UserUpdated;
use Pickstar\User\User;
use Illuminate\Http\Request;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForUsers;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Http\Transformers\UserTransformer;
use Pickstar\Http\Requests\Api\UpdateUserRequest;

class UserController extends Controller
{
    use DeniesForUsers,
        DeniesHttpRequests;

    /**
     * Show the logged in user.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return response()->json($request->user()->append('email_hash'));
    }

    /**
     * Update a user.
     *
     * Always uses the existing value as a default. This prevents accidental nulling out of a column
     * if the request does not contain the given key. Expects Vue to send an empty value or null
     * to null out the column.
     *
     * @param \Pickstar\Http\Requests\Api\UpdateUserRequest $request
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $user->fill([
            'first_name' => $request->input('first_name', $user->first_name),
            'last_name' => $request->input('last_name', $user->last_name),
            'email' => $request->input('email', $user->email),
            'phone' => $request->input('phone', $user->phone),
            'company' => $request->input('company', $user->company),
            'position' => $request->input('position', $user->position),
            'can_set_esports' => $request->input('can_set_esports', $user->can_set_esports),
            'invoice_to' => $request->input('invoice_to', $user->invoice_to),
        ]);

        if ($user->isStaffManager()) {
            $user->company = $user->parent->company;
        }

        if ($user->isTalent()) {
            $user->profile->updateFromRequest($request);

            $user->updateTalentManagementFromRequest($request);
        }

        if (
            $request->user()->isAdmin() &&
            ($user->isMasterManager() || $user->isTalent())
        ) {
            $user->commission_rate = $request->input('commission_rate', $user->commission_rate);
            $user->commission_dollar = $request->input('commission_dollar', $user->commission_dollar);
            $user->commission_type = $request->input('commission_type', $user->commission_type);
        }

        $user->save();

        if (
            $user->isMasterManager() &&
            $user->wasChanged('company')
        ) {
            $user->staff()->update(['company' => $user->company]);
        }

        event(new UserUpdated($user));

        return response()->json($user->load('profile'));
    }

    /**
     * Delete a user.
     *
     * This performs a soft delete so users can be restored at a later time.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, User $user)
    {
        $this->authorize('delete', $user);

        $this->denyIfAdminDeletingThemselves($request->user(), $user);

        $this->denyIfUserHasPendingBookings($user);

        $user->delete();

        return response()->json($user);
    }

    /**
     * Show a user.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, User $user)
    {
        $this->authorize('view', $user);

        $user->load('profile', 'locations', 'opportunities', 'management');

        if ($user->isMasterManager()) {
            $user->append('management_talent_count');
            $user->append('management_request_count');
            $user->append('management_booking_count');
        }

        return response()->json(
            $user->transform([new UserTransformer, 'transform'], [$request->user()])
        );
    }
}

<?php

namespace Pickstar\Http\Controllers\Api;

use Pickstar\User\User;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Pickstar\Http\Controllers\Controller;

class FeaturedStarsController extends Controller
{
    public function index(Request $request): JsonResponse
    {
        $featured = User::talent()
            ->featured()
            ->with('profile', 'sportTags', 'clubTags')
            ->inRandomOrder()
            ->whereHas('profile', function ($query) {
                $query->public();
            })
            ->take(8)
            ->get();

        $featured->each(function ($user) {
            $user->profile->setVisible(['gender', 'tagline', 'description', 'highest_followers', 'highest_followers_icon']);
            $user->setVisible(['profile_image_url']);
            $user->addHidden('phone', 'company', 'status', 'email');
        });
        $featured->each->append('sport_club');
        $featured->each->addHidden('sportTags', 'clubTags');

        return response()->json($featured);
    }
}

<?php

namespace Pickstar\Http\Controllers\Api\Talent;

use Pickstar\User\User;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Pickstar\Http\Controllers\Controller;

class CountController extends Controller
{
    /**
     * Get the count of talents for different criteria.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $this->authorize('index', User::class);

        $talent = User::talent()
            ->when($request->user()->isAdmin() && $request->filled('managerId'), function ($query) use ($request) {
                $query->where('management_id', $request->get('managerId'));
            });

        $all = (clone $talent)->count();
        $active = (clone $talent)->active()->count();
        $unverified = (clone $talent)->pendingVetting()->count();

        return response()->json([
            'all' => $all,
            'active' => $active,
            'unverified' => $unverified
        ]);
    }
}

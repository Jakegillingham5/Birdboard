<?php

namespace Pickstar\Http\Controllers\Api\Talent;

use Pickstar\User\User;
use Illuminate\Http\Request;
use Pickstar\Location\Location;
use Pickstar\Http\Controllers\Controller;

class LocationController extends Controller
{
    /**
     * Update a talents available locations.
     *
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, User $user)
    {
        $this->authorize('update', $user);

        $user->locations()->sync($request->input('locations'));

        if ($request->filled('basedIn')) {
            $location = Location::find($request->get('basedIn'));
            $user->basedIn()->associate($location)->save();
        }

        return response()->json($user);
    }
}

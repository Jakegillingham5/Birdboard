<?php

namespace Pickstar\Http\Controllers\Api\Talent;

use Pickstar\User\User;
use Illuminate\Http\Request;
use Pickstar\Helpers\SearchHelpers;
use Pickstar\Http\Controllers\Controller;

class ListController extends Controller
{
    /**
     * Get all talent that meet the given criteria.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Support\Collection
     */
    public function index(Request $request)
    {
        $users = User::talent()
            ->active()
            ->select('id', 'first_name', 'last_name');

        // When non-admins attempt to fetch the talent list we only return public talent by default.
        // Admins have the ability to query with the "visibility" flag set as anything but "public"
        // to see hidden talent in this response.
        if (
            ! $request->user() ||
            ! $request->user()->isAdmin() ||
            $request->query('visibility', 'public') === 'public'
        ) {
            $users->whereHas('profile', function ($query) {
                $query->public();
            });
        }

        if ($request->filled('name')) {
            $sanitizedString = SearchHelpers::sanitizeSearchString($request->query('name'));
            $users->where(\DB::raw('concat(first_name, " ", last_name)'), 'like', '%' . $sanitizedString . '%');
        }

        return response()->json($users->paginate(50));
    }
}

<?php

namespace Pickstar\Http\Controllers\Api\Talent;

use Pickstar\User\User;
use Pickstar\Talent\Endorsement;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Events\TalentEndorsementAdded;
use Pickstar\Http\Requests\Api\Talent\StoreEndorsementRequest;
use Pickstar\Http\Requests\Api\Talent\UpdateEndorsementRequest;

class EndorsementController extends Controller
{
    /**
     * Get the talents endorsements.
     *
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(User $user)
    {
        $this->authorize('list', Endorsement::class);

        return response()->json($user->endorsements);
    }

    /**
     * Store a new endorsement against a talent.
     *
     * @param \Pickstar\Http\Requests\Api\Talent\UpdateEndorsementRequest $request
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreEndorsementRequest $request, User $user)
    {
        $this->authorize('update', Endorsement::class);

        $endorsement = $user->endorsements()->create($request->only([
            'company_name',
            'date_endorsed',
            'description'
        ]));

        event(new TalentEndorsementAdded($endorsement, $user));

        return response()->json($endorsement);
    }

    /**
     * Update an existing endorsement.
     *
     * @param \Pickstar\Http\Requests\Api\Talent\UpdateEndorsementRequest $request
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Endorsement\Endorsement $endorsement
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateEndorsementRequest $request, User $user, Endorsement $endorsement)
    {
        $endorsement->fill($request->only([
            'company_name',
            'date_endorsed',
            'description'
        ]))->save();

        return response()->json($endorsement);
    }

    /**
     * Delete an existing endorsement.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Endorsement\Endorsement $endorsement
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(User $user, Endorsement $endorsement)
    {
        $this->authorize('delete', $endorsement);

        $endorsement->delete();

        return response(null, 204);
    }
}

<?php

namespace Pickstar\Http\Controllers\Api\Talent;

use Pickstar\Events\TalentProfileUpdated;
use Pickstar\User\User;
use Illuminate\Http\Request;
use Pickstar\Opportunity\Opportunity;
use Pickstar\Http\Controllers\Controller;

class ProfileController extends Controller
{
    /**
     * Storing a talents profile data
     *
     * @param User $user
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(User $user, Request $request)
    {
        $this->authorize('update', $user);

        if ($request->filled('locations')) {
            $user->locations()->sync($request->get('locations'));
        }

        $user->syncTags($request->only('sport_tags', 'club_tags', 'sponsor_tags', 'charity_tags', 'theme_tags'));

        $opportunities = Opportunity::whereIn('id', $request->get('opportunities'))->get();

        $request->user()->opportunities()->sync($opportunities->pluck('id'));

        event(new TalentProfileUpdated($user));

        return response()->json($user);
    }
}

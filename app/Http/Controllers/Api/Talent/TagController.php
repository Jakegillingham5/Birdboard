<?php

namespace Pickstar\Http\Controllers\Api\Talent;

use Pickstar\User\User;
use Illuminate\Http\Request;
use Pickstar\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class TagController extends Controller
{
    /**
     * Get all of a talents tags.
     *
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(User $user)
    {
        $this->authorize('manage', $user);

        if (! $user->isTalent()) {
            throw new BadRequestHttpException('Cannot get tags for user that is not a talent.');
        }

        return response()->json(
            collect([
                'charity' => [],
                'sponsor' => [],
                'sport' => [],
                'club' => [],
                'theme' => []
            ])
            ->merge($user->tags->groupBy('type'))
        );
    }

    /**
     * Update the tags for a talent.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, User $user)
    {
        $this->authorize('update', $user);

        if (! $user->isTalent()) {
            throw new BadRequestHttpException('Cannot update tags for user that is not a talent.');
        }

        foreach (['charity', 'sponsor', 'sport', 'club', 'theme'] as $type) {
            if ($request->filled("tags.{$type}")) {
                $user->syncTagsByType($type, $request->input("tags.{$type}"));
            }
        }

        return response()->json(
            collect([
                'charity' => [],
                'sponsor' => [],
                'sport' => [],
                'club' => [],
                'theme' => []
            ])
            ->merge($user->tags->groupBy('type'))
        );
    }
}

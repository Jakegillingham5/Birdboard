<?php

namespace Pickstar\Http\Controllers\Api\Talent;

use Exception;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Requests\Api\Talent\UpdateMarketingPhotoRequest;
use Pickstar\Talent\TalentPhoto;
use Pickstar\User\User;

/**
 * Class MarketingPhotoController
 *
 * Handles requests regarding talent marketing photos
 *
 * @package Pickstar\Http\Controllers\Api\Talent
 */
class MarketingPhotoController extends Controller
{
    /**
     * Get list of talent's marketing photos
     *
     * @param UpdateMarketingPhotoRequest $request
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(UpdateMarketingPhotoRequest $request, User $user)
    {
        return response()->json($user->profile->photos);
    }

    /**
     * Non-client user can add photo to the profile of talent in question if they are authorised
     *
     * @param UpdateMarketingPhotoRequest $request
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function update(UpdateMarketingPhotoRequest $request, User $user)
    {
        $newPhotoFile = $request->file('newPhotoFile');

        $newPhoto = TalentPhoto::addMarketingPhoto($newPhotoFile, $user);

        return response()->json($newPhoto);
    }

    /**
     * Non-client user can delete photo to the profile of the talent in question if they are authorised
     *
     * @param UpdateMarketingPhotoRequest $request
     * @param User $user
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function destroy (UpdateMarketingPhotoRequest $request, User $user)
    {
        $photoHash = basename($request->get('photoUrl'));
        $user->profile->photos->where('photo_hash', $photoHash)->first()->handleDeleteMarketingPhoto();
        return response('File Successfully Deleted');
    }
}

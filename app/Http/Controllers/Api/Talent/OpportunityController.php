<?php

namespace Pickstar\Http\Controllers\Api\Talent;

use Pickstar\User\User;
use Illuminate\Http\Request;
use Pickstar\Http\Controllers\Controller;

class OpportunityController extends Controller
{
    /**
     * Update the types of opportunities a talent is available for.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, User $user)
    {
        $this->authorize('update', $user);

        $user->opportunities()->sync($request->input('opportunities'));

        return response()->json($user->opportunities);
    }
}

<?php

namespace Pickstar\Http\Controllers\Api\Booking;

use Carbon\Carbon;
use Carbon\CarbonTimeZone;
use Illuminate\Http\Request;
use Pickstar\Booking\Booking;
use Illuminate\Http\JsonResponse;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForBookings;
use Pickstar\Http\Resources\Booking as BookingResource;
use Pickstar\Http\Requests\Api\Booking\UpdateVettingBookingRequestRequest;

class VettingRequestController extends Controller
{
    use DeniesForBookings;

    /**
     * List all booking requests that require vetting.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $this->authorize('listVettingRequests', Booking::class);

        $bookings = Booking::vetting()
            ->with('opportunity', 'client')
            ->applyFiltersAndSorting($request, [
                'sortingDateColumn' => 'created_at',
                'filteringDateColumn' => 'created_at',
                'defaultSortingCallback' => function ($query) {
                    $query->oldest('created_at');
                }]);
        $bookings = $bookings->paginate(25);

        return BookingResource::collection($bookings);
    }

    /**
     * Show a booking request that requires vetting.
     *
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Booking $booking)
    {
        $this->authorize('viewVettingRequest', $booking);

        $booking->load('client', 'talent', 'opportunity', 'onlineOpportunity');

        return new BookingResource($booking);
    }

    /**
     * Update a vetting booking request.
     *
     * @param \Pickstar\Http\Requests\Api\Booking\UpdateVettingBookingRequestRequest $request
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateVettingBookingRequestRequest $request, Booking $booking)
    {
        $booking->updateFromRequestPayload($request);

        return new BookingResource($booking);
    }
}

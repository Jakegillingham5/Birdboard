<?php

namespace Pickstar\Http\Controllers\Api\Booking;

use Pickstar\User\User;
use Illuminate\Support\Str;
use Pickstar\Model\Revision;
use Pickstar\Payment\Payment;
use Pickstar\Booking\Booking;
use Illuminate\Http\JsonResponse;
use Pickstar\Booking\TalentPivot;
use Pickstar\Http\Controllers\Controller;

class HistoryController extends Controller
{
    /**
     * Show history for a given booking - legacy - using revisions (venturecraft).
     *
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function showLegacy(Booking $booking): JsonResponse
    {
        $this->authorize('history', $booking);

        $booking->load([
            'revisions.responsible',
            'revisions.revisionable',
            'payments.revisions.responsible',
            'payments.revisions.revisionable'
        ]);

        $history = $booking->revisions
            ->merge($booking->payments->pluck('revisions')->flatten())
            ->merge(
                Revision::with('revisionable.talent')
                    ->where('revisionable_type', TalentPivot::class)
                    ->whereIn('revisionable_id', $booking->talent->pluck('response.id'))
                    ->get()
                    ->flatten()
            )
            ->sortByDesc('id')
            ->values();

        return response()->json($history->transform(function ($record) {
            $from = $record->old_value;
            $to = $record->new_value;

            // Social content can contain HTML so we'll strip it all out and keep everything to a single line.
            if ($record->key === 'social_media_content') {
                $from = trim(preg_replace('/\s+/', ' ', strip_tags(str_replace('<', ' <', str_replace('&nbsp;', '', $from)))));
                $to = trim(preg_replace('/\s+/', ' ', strip_tags(str_replace('<', ' <', str_replace('&nbsp;', '', $to)))));
            }

            // Transform all status keys so the value is capitalized and spaced on an underscore.
            if ($record->key === 'status') {
                $from = ucwords(str_replace('_', ' ', $from));
                $to = ucwords(str_replace('_', ' ', $to));
            }

            // Include the related revisionable instance (this is the "booking" or "payment", for example) in the
            // record IF it matches one of the record tables.
            $related = null;

            if (in_array(Str::singular($record->tableAffected()), ['payment', 'booking_talent'])) {
                $related = $record->revisionable;
            }

            return [
                'field' => $record->fieldName(),
                'key' => $record->key,
                'what' => Str::singular($record->tableAffected()),
                'responsible' => $record->responsible ? $record->responsible->name : null,
                'from' => $from,
                'to' => $to,
                'date' => $record->created_at->format('d/m/Y @ H:i:s'),
                'related' => $related
            ];
        }));
    }

    /**
     * Show history for a given booking
     *
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Booking $booking): JsonResponse
    {
        $this->authorize('history', $booking);

        $booking->load([
            'audits',
            'payments.audits',
            'talentAudits'
        ]);

        $talentAudits = $booking->talentAudits->flatten();
        if ($talentAudits) {
            foreach ($talentAudits as &$audit) {
                $audit->old_values = json_decode($audit->old_values);
                $audit->new_values = json_decode($audit->new_values);
            }
        }
        $history = $booking->audits
            ->merge($booking->payments->pluck('audits')->flatten())
            ->merge($talentAudits)
            ->sortByDesc('id')
            ->values();

        return response()->json($history->transform(function ($record) {
            $from = (array) $record->old_values;
            $to = (array) $record->new_values;
            if (count($from)) {
                foreach ($from as $key => &$f) {
                    if ($f !== null) {
                        // Social content can contain HTML so we'll strip it all out and keep everything to a single line.
                        if ($key === 'social_media_content') {
                            $f = trim(preg_replace('/\s+/', ' ', strip_tags(str_replace('<', ' <', str_replace('&nbsp;', '', $f)))));
                        }

                        $f = ucwords(str_replace('_', ' ', $f));
                    }
                }
            }

            if (count($to)) {
                foreach ($to as $key => &$t) {
                    if ($t !== null) {
                        // Social content can contain HTML so we'll strip it all out and keep everything to a single line.
                        if ($key === 'social_media_content') {
                            $t = trim(preg_replace('/\s+/', ' ', strip_tags(str_replace('<', ' <', str_replace('&nbsp;', '', $t)))));
                        }

                        $t = ucwords(str_replace('_', ' ', $t));
                    }
                }
            }

            // Include the related revisionable instance (this is the "booking" or "payment", for example) in the
            // record IF it matches one of the record tables.
            $table = Str::singular((new $record->auditable_type)->getTable());
            $related = null;
            if ($table === 'payment') {
                $related = Payment::find($record->auditable_id);
            } elseif ($table === 'booking') {
                $related = Booking::find($record->auditable_id);
            }

            return [
                'what' => ucwords($table),
                'responsible' => User::find($record->user_id) ? User::find($record->user_id)->name : null,
                'from' => $from,
                'to' => $to,
                'date' => $record->created_at->format('d/m/Y @ H:i:s'),
                'related' => $related,
                'action' => $record->event
            ];
        }));

        return response()->json($history, 200);
    }
}

<?php

namespace Pickstar\Http\Controllers\Api\Booking;

use Pickstar\User\User;
use Illuminate\Http\Request;
use Pickstar\Booking\Booking;
use Illuminate\Http\JsonResponse;
use Pickstar\Booking\TalentPivot;
use Pickstar\Events\TalentApplied;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForBookings;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Http\Resources\Talent as TalentResource;
use Pickstar\Notifications\Talent\BookingRequestToBeReviewed;

class AddAdditionalTalentController extends Controller
{
    use DeniesForBookings,
        DeniesHttpRequests;

    /**
     * Add additional talent to a booking request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Booking $booking)
    {
        $this->authorize('addAdditionalTalent', $booking);

        $this->denyIfBookingIsNotPending($booking);

        if ($request->user()->isManager()) {
            $this->denyIfBookingIsNotPublic($booking);
        }

        $payload = $request->input('talent');

        // Get list of talent id from payload
        $talent_ids = empty($payload) ? [] : array_column($payload, 'id');

        // Find all the added talent. Filter those that are already on the booking as a
        // precautionary step. For managers, filter out those that are not managed by
        // the manager user so they can only add their own talent.
        $talent = User::talent()
            ->active()
            ->when($request->user()->isManager(), function ($query) use ($request) {
                $query->managedBy($request->user());
            })
            ->findMany($talent_ids)
            ->filter(function ($talent) use ($booking) {
                return !$booking->talent->contains($talent);
            });

        if ($request->user()->isManager()) {
            foreach ($talent as $t) {
                $this->denyIfTalentDoesNotHaveTag($booking, $t);
            }
        }

        // Add talent to the request as either hidden or applied depending on who is
        // adding the talent.
        $booking->talent()
            ->syncWithoutDetaching(
                $talent->mapWithKeys(function ($talent) use ($request, $booking, $payload) {
                    if ($request->user()->isAdmin()) {
                        return [
                            $talent->id => [
                                // Changing the status to 'applied' will break date_applied_for usage.
                                // This is because the talent response field requires a date_applied_for.
                                // Please consult author before making changes.
                                'status' => TalentPivot::STATUS_HIDDEN,
                                'applied_date' => null,
                                'expiry_date' => null
                            ]
                        ];
                    } else {
                        // Check to see if talent has selected a date if date options are available.
                        $date_applied_for = $booking->has_date_options_only ? $this->extractDate($payload, $talent->id) : $booking->date;

                        if ($booking->has_date_options_only && ($date_applied_for === null || !in_array($date_applied_for, $booking->date_options))) {
                            abort(500, 'Unable to proceed, error when recording applied date.');
                        }

                        if ($booking->talent_expire) {
                            return [
                                $talent->id => [
                                    'status' => TalentPivot::STATUS_APPLIED,
                                    'applied_date' => now(),
                                    'date_applied_for' => $date_applied_for,
                                    'expiry_date' => $booking->calculateTalentApplicationExpiryDate()
                                ]
                            ];
                        } else {
                            return [
                                $talent->id => [
                                    'status' => TalentPivot::STATUS_APPLIED,
                                    'date_applied_for' => $date_applied_for,
                                    'applied_date' => now()
                                ]
                            ];
                        }
                    }
                })->toArray()
            );

        // If user is a manager fire the talent applied event for each of the talent that
        // have been added.
        if ($request->user()->isManager()) {
            $talent->each(function ($talent) use ($booking, $request) {
                event(new TalentApplied($talent, $booking));
            });
        }

        return TalentResource::collection($booking->talentManagedBy($request->user())->get());
    }

    /**
     * Helper function to get date by id from nested array
     *
     * @param $array
     * @param $id
     * @return mixed
     */
    protected function extractDate($array, $id)
    {
        foreach($array as $item) {
            if ($item['id'] === $id) {
                return array_key_exists('date', $item) ? $item['date'] : null;
            }
        }
    }
}

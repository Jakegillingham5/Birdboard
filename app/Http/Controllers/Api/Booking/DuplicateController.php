<?php

namespace Pickstar\Http\Controllers\Api\Booking;

use Exception;
use Pickstar\Booking\Booking;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Events\BookingRequestCreated;
use Pickstar\Http\Resources\Booking as BookingResource;
use Pickstar\Http\Requests\Api\Booking\StoreBookingRequestRequest;
use Pickstar\Marketing\Source;

class DuplicateController extends Controller
{
    /**
     * Duplicate a booking request.
     *
     * @param \Pickstar\Booking\Booking $booking
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreBookingRequestRequest $request, Booking $booking)
    {
        $this->authorize('duplicate', $booking);

        try {
            DB::beginTransaction();

            $newBooking = $booking->duplicate($request);

            // Replicate the marketing source of the original booking if it's not null
            if ($booking->marketingSource !== null) {

                $marketingSourceReplicate = $booking->marketingSource->replicate();

                // Edit replicate marketing source to reflect new booking details
                $marketingSourceReplicate->fill([
                    'request_id' => $newBooking->id,
                    'campaign_name' => 'duplicate',
                    'existing_user' => true
                ]);

                $marketingSourceReplicate->save();
            }

            DB::commit();

            event(new BookingRequestCreated($newBooking));

            return new BookingResource($newBooking);
        } catch (Exception $exception) {
            report($exception);

            DB::rollBack();

            abort(500, 'Something went wrong when duplicating the booking.');
        }
    }
}

<?php

namespace Pickstar\Http\Controllers\Api\Booking;

use Pickstar\User\User;
use Pickstar\Booking\Booking;
use Illuminate\Http\Response;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForBookings;
use Pickstar\Http\Concerns\DeniesHttpRequests;

class DeleteShortlistedTalentController extends Controller
{
    use DeniesForBookings,
        DeniesHttpRequests;

    /**
     * Removes a shortlisted talent from the booking request.
     *
     * @param \Pickstar\Booking\Booking $booking
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Booking $booking, User $user)
    {
        $this->authorize('removeShortlistedTalent', $booking);

        // Make sure the talent is shortlisted on this booking request.
        if (! $talent = $booking->talentShortlisted()->find($user->id)) {
            $this->deny('Unable to perform action on talent that is not shortlisted on this booking request.');
        }

        $booking->talent()->detach($user);

        return response()->noContent();
    }
}

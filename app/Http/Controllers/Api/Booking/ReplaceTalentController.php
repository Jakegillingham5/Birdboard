<?php

namespace Pickstar\Http\Controllers\Api\Booking;

use Pickstar\Http\Resources\Talent;
use Pickstar\User\User;
use Pickstar\Booking\Booking;
use Pickstar\Booking\TalentPivot;
use Pickstar\Events\BookingTalentReplaced;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForBookings;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Http\Resources\Booking as BookingResource;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Pickstar\Http\Requests\Api\Booking\ReplaceTalentRequest;

class ReplaceTalentController extends Controller
{
    use DeniesForBookings,
        DeniesHttpRequests;

    /**
     * Replace/remove a talent from a bookin.
     *
     * @param \Pickstar\Http\Requests\Api\Booking\ReplaceTalentRequest $request
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ReplaceTalentRequest $request, Booking $booking)
    {
        $this->denyIfBookingIsCompleted($booking);

        $this->denyIfBookingIsCancelled($booking);

        try {
            $talent = $booking->talent()
                ->haveBeenAccepted()
                ->findOrFail($request->input('talent'));
        } catch (ModelNotFoundException $exception) {
            $this->deny('Unable to replace a talent that is not attending booking.');
        }

        // When removing a talent we maintain the record and simply update their status to removed. This
        // is for historical purposes and for future development. We only reject this if they are
        // trying to remove the last talent of a booking.
        if ($request->input('replacement') === 'remove') {
            if ($booking->talentAttending()->count() === 1) {
                $this->deny('Unable to remove the last talent from the booking.');
            }

            $talent->response->fill(['status' => TalentPivot::STATUS_REMOVED])->save();

            // After removing talent we need to update the payment amounts to reflect the changes to the booking
            // as it will now technically cost the client less.
            $booking->updatePayments();
        } else {
            try {
                // When fetching the replacement user we make sure that they are not already accepted for booking
                // we wish to add them to. This is an extra precaution.
                $replacement = User::talent()
                    ->whereDoesntHave('talentBookings', function ($query) use ($booking) {
                        $query->haveBeenAcceptedFor($booking);
                    })
                    ->findOrFail($request->input('replacement'));

                // Set the status on the original talent to "replaced" so we can see that the talent was initially
                // attending this booking but was replaced.
                $talent->response->fill(['status' => TalentPivot::STATUS_REPLACED])->save();

                // Attach the replacement talent to the booking and set them as both applied and accepted.
                // TODO - figure out why this block is causing issues with auditable - seems to be a pivot thing
                TalentPivot::disableAuditing();
                $booking->talent()->syncWithoutDetaching([
                    $replacement->id => [
                        'status' => TalentPivot::STATUS_APPLIED,
                        'client_accepted' => true
                    ]
                ]);
                TalentPivot::enableAuditing();

                // Notify the replacement talent of booking

                event(new BookingTalentReplaced($booking, $replacement));

            } catch (ModelNotFoundException $exception) {
                $this->deny('Unable to replace a talent with a user that is not a valid replacement.');
            }
        }

        $booking->loadDefaultRelations($request);

        return new BookingResource($booking);
    }
}

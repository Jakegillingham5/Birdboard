<?php

namespace Pickstar\Http\Controllers\Api\Booking;

use Illuminate\Http\Request;
use Pickstar\Booking\Booking;
use Illuminate\Http\JsonResponse;
use Pickstar\Events\CreateBookingThreads;
use Pickstar\Events\MessagingOpen;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Transformers\BookingTransformer;
use Pickstar\Http\Resources\Booking as BookingResource;

class MarkBookedUnpaidController extends Controller
{
    /**
     * Mark a booking as booked even though it is unpaid
     *
     * This is used for cases when admins want to allow a
     * talent to receive all of the notifications for a booking
     * when the client has not paid yet.
     *
     * @param \Illuminate\Http\Request;
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, Booking $booking)
    {
        $this->authorize('markBookedUnpaid', $booking);

        if ($booking->status === 'scheduled') {
            $booking->fill([
                    'status' => Booking::STATUS_BOOKED
            ])->save();

            event(new CreateBookingThreads($booking));
            event(new MessagingOpen($booking));
            $booking->notification->fill(['messaging_open' => true])->save();
            $booking->loadDefaultRelations($request);

            return new BookingResource($booking);
        } else {
            abort(403);
        }
    }
}

<?php

namespace Pickstar\Http\Controllers\Api\Booking;

use Illuminate\Http\Response;
use Pickstar\Notifications\System\TalentAttemptToCancel;
use Pickstar\User\AdminUser;
use Illuminate\Http\Request;
use Pickstar\Booking\Booking;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForBookings;
use Pickstar\Http\Concerns\DeniesHttpRequests;

class NotifyAdminAttemptToCancelController extends Controller
{
    use DeniesForBookings,
            DeniesHttpRequests;

    /**
     * Notify admin that a talent tried to cancel a booking that they are booked for
     *
     * @param \Illuminate\Http\Request $request
     * @param \Pickstar\Booking\Booking $booking
     * @param \Pickstar\User\User $talent
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Booking $booking): Response
    {
        $this->authorize('cancel', $booking);

        (new AdminUser)->notify(new TalentAttemptToCancel($booking, $request->user()));

        return response(null, 204);
    }
}

<?php

namespace Pickstar\Http\Controllers\Api\Booking;

use Pickstar\User\User;
use Illuminate\Http\Request;
use Pickstar\Booking\Booking;
use Illuminate\Http\JsonResponse;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForBookings;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Notifications\Manager\BookingRequestToBeReviewed;

class SendManagerNotificationController extends Controller
{
    use DeniesForBookings,
        DeniesHttpRequests;

    /**
     * Send managers a notification about a booking request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, Booking $booking): JsonResponse
    {
        $this->authorize('notifyManagers', $booking);

        $this->denyIfBookingIsNotPending($booking);

        $this->denyIfBookingIsNotPublic($booking);

        // Find all the added talent and filter out those that have already been notified. This
        // shouldn't happen but it's just a precautionary step to prevent any hackery.
        $managers = User::managers()
            ->active()
            ->findMany($request->input('managers'))
            ->filter(function ($manager) use ($booking) {
                return ! $booking->managersNotified->contains($manager);
            });

        // Now we can send the notification to each of the talent and add them to the notifications
        // table so we don't notify them again.
        $managers->each(function ($manager) use ($booking) {
            $manager->notify(new BookingRequestToBeReviewed($booking));
        });

        $booking->managersNotified()
            ->syncWithoutDetaching($managers->pluck('id'));

        return response()->json($booking->managersNotified()->get());
    }
}

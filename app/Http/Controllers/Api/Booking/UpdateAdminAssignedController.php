<?php

namespace Pickstar\Http\Controllers\Api\Booking;

use Illuminate\Http\Request;
use Pickstar\Booking\Booking;
use Illuminate\Http\JsonResponse;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Resources\Booking as BookingResource;
use Pickstar\User\User;

class UpdateAdminAssignedController extends Controller
{
    /**
     * Update the admin assigned to the request or booking.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Booking $booking)
    {
        $this->authorize('update', $booking);

        $userID = $request->input('userID');

        $assignedAdmin = User::where('role', 'admin')->where('id', $userID)->first();

        if ($assignedAdmin !== null) {
            $booking->fill(['assigned_admin' => $assignedAdmin->id ])->save();
        } else {
            abort(500, 'Unable to assign request/booking to non-admin user.');
        }

        return new BookingResource($booking);
    }
}

<?php

namespace Pickstar\Http\Controllers\Api\Booking;

use Pickstar\User\User;
use Illuminate\Http\Request;
use Pickstar\Booking\Booking;
use Pickstar\Booking\TalentPivot;
use Pickstar\Events\TalentApplied;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForBookings;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Http\Resources\Talent as TalentResource;
use Pickstar\Http\Requests\Api\Booking\ApplyRequestRequest;

class ApplyRequestController extends Controller
{
    use DeniesForBookings,
        DeniesHttpRequests;

    /**
     * Apply for a booking as a talent.
     *
     * @param ApplyRequestRequest $request
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(ApplyRequestRequest $request, Booking $booking)
    {
        $this->authorize('respondRequest', $booking);

        $this->denyIfBookingIsNotPending($booking);

        $this->denyIfBookingIsExpired($booking);

        $this->denyIfTalentDoesNotHaveTag($booking, $request->user());

        $this->denyIfTalentHasAlreadyApplied($booking, $request->user());

        if ($request->get('comment')) {
            $this->denyIfTalentCannotComment($booking);
        }

        $attributes = [
            'status' => TalentPivot::STATUS_APPLIED,
            'applied_date' => now(),
            'expiry_date' => $booking->talent_expire
                ? $booking->calculateTalentApplicationExpiryDate()
                : null,
            'comment' => $request->get('comment')
        ];

        // Set date_applied_for
        // If has_date_options_only then talent must select a date
        $attributes['date_applied_for'] = $booking->has_date_options_only ? $request->input('dateAppliedFor') : $booking->date;
        if ($booking->has_date_options_only && $attributes['date_applied_for'] === null) {
            abort(500, 'Unable to proceed, error when recording applied date.');
        }

        if ($talent = $booking->talent()->find($request->user()->id)) {
            $talent->response->fill($attributes)->save();
        } else {
            $booking->talent()->attach($request->user(), $attributes);
        }

        event(new TalentApplied($request->user(), $booking));

        return new TalentResource($booking->talent()->find($request->user()->id));
    }

    /**
     * Change application comment
     *
     * @param \Illuminate\Http\Request $request
     * @param \Pickstar\Booking\Booking $booking
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Booking $booking, User $user)
    {
        $this->authorize('respondRequest', $booking);

        $this->denyIfBookingIsNotPending($booking);

        $this->denyIfBookingIsExpired($booking);

        if ($talent = $booking->talent()->find($user->id)) {
            $talent->response->fill([
                'comment' => $request->get('comment')
            ])->save();
        }

        return response()->json($booking->talent()->find($user->id));
    }
}

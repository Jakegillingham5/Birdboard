<?php

namespace Pickstar\Http\Controllers\Api\Booking;

use Pickstar\User\User;
use Illuminate\Http\Request;
use Pickstar\Booking\Booking;
use Pickstar\Booking\TalentPivot;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForUsers;
use Pickstar\Http\Concerns\DeniesForBookings;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Http\Resources\Talent as TalentResource;

class AcceptTalentController extends Controller
{
    use DeniesForUsers,
        DeniesForBookings,
        DeniesHttpRequests;

    /**
     * Accept a talent for a booking request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Pickstar\Booking\Booking $booking
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Booking $booking, User $user)
    {
        $this->authorize('acceptTalent', $booking);

        $this->denyIfBookingIsNotPending($booking);

        // Make sure that this is a talent that actually exists as part of the booking.
        // We simply grab the talent including their pivot response from the
        // bookings talent.
        if (! $talent = $booking->talent()->find($user->id)) {
            $this->deny('Unable to perform action on talent that has not responded to booking request.');
        }

        if ($request->user()->isClient()) {
            // Make sure that the talent has a response status of "applied" before being
            // able to accept them. This is to prevent prematurely accepting a talent
            // before they themselves have had a chance to respond.
            if ($talent->response->status !== TalentPivot::STATUS_APPLIED) {
                $this->deny('Unable to perform action on talent that has not applied to booking request.');
            }
        } elseif ($request->user()->isAdmin()) {
            // If user is an admin then we'll forcibly set the talents response status
            // to applied. This lets admins force accept a talent.
            $talent->response->fill(['status' => TalentPivot::STATUS_APPLIED]);
        }

        // If booking date doesn't exist, then set it to the date that the talent selected
        // from the date_options.
        $dateAppliedFor = $talent->response->date_applied_for !== null
            ? $talent->response->date_applied_for->format('Y-m-d')
            : null;

        if ($booking->has_date_options_only) {
            // Ensure that the talent's date_applied_for is actually one of the date_options
            if (in_array($dateAppliedFor, $booking->date_options)) {
                $request->merge([
                    'date' => $dateAppliedFor,
                    'state' => $booking->state,
                ]);
                $booking->updateDateAndTimezone($request);
                // Upon booking a talent, the date is no longer flexible
                $booking->date_flexible_or_unconfirmed = 0;
                $booking->save();
            } else {
                abort(500, 'Unable to complete talent booking, error when recording applied date.');
            }
        }

        $talent->response->fill(['client_accepted' => true])->save();

        return new TalentResource($talent);
    }
}

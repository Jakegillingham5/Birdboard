<?php

namespace Pickstar\Http\Controllers\Api\Booking;

use Carbon\Carbon;
use Pickstar\User\User;
use Pickstar\Booking\Booking;
use Pickstar\Booking\TalentPivot;
use Pickstar\Events\TalentApplied;
use Pickstar\Events\TalentReviewing;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Events\TalentResponsePending;
use Pickstar\Http\Concerns\DeniesForUsers;
use Pickstar\Events\AppliedTalentDeclining;
use Pickstar\Http\Concerns\DeniesForBookings;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Http\Resources\Talent as TalentResource;
use Pickstar\Http\Requests\Api\Booking\UpdateTalentResponseRequest;

class UpdateTalentResponseController extends Controller
{
    use DeniesForUsers,
        DeniesForBookings,
        DeniesHttpRequests;

    /**
     * Update a talents response status for a given booking request.
     *
     * @param \Pickstar\Http\Requests\Api\Booking\UpdateTalentResponseRequest $request
     * @param \Pickstar\Booking\Booking $booking
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateTalentResponseRequest $request, Booking $booking, User $user)
    {
        $this->denyIfBookingIsNotPending($booking);

        // Make sure that this is a talent that actually exists as part of the booking is
        // managed by the current user if applicable.
        if (!$talent = $booking->talentManagedBy($request->user())->find($user->id)) {
            $this->deny('Unable to perform action on invalid talent.');
        }

        // Make sure that if the current talent response status is not hidden that they
        // are not trying to set it to hidden. This is to prevent talent responses
        // from being hidden (and thus the request being hidden) from talents
        // after they themselves have applied for a booking request.
        if ($talent->response->status !== TalentPivot::STATUS_HIDDEN && $request->get('status') === TalentPivot::STATUS_HIDDEN) {
            $this->deny('Unable to hide a talent response that is no longer hidden.');
        }

        if ($request->get('status') === TalentPivot::STATUS_REVIEWING &&
            $request->user()->isManager() &&
            !($talent->response->status === TalentPivot::STATUS_REVIEWING || $talent->response->status === TalentPivot::STATUS_PENDING)) {
                $this->deny('Unable to update talent response to status reviewing');
        }

        $previous_status = null;
        if ($talent->response->status) {
            $previous_status = $talent->response->status;
            if ($request->filled('previousStatus')) {
                $previous_status = $request->get('previousStatus');
            }
        }

        if ($request->user()->isAdmin()) {
            $declineReason = TalentPivot::DECLINE_PICKSTAR;
        } elseif ($request->user()->isManager() && !$request->filled('decline_reason')) {
            $declineReason = TalentPivot::DECLINE_MANAGER;
        } else {
            $declineReason = $request->get('decline_reason');
        }

        // Update the status to the one given in the request. Also set the client accepted
        // flag to false, this is so that if the talent had previously been accepted by
        // the client the admin still has the ability to decline the response on behalf
        // of the talent and remove the clients acceptance.
        $talent->response->fill([
            'status' => $request->get('status'),
            'client_accepted' => false,
            'decline_reason' => $declineReason,
            'date_applied_for' => null,
        ])->save();

        if ($talent->response->status === TalentPivot::STATUS_APPLIED && $booking->talent_expire) {
            $talent->response->fill([
                'response.expired' => false,
                'response.expiry_date' => $booking->calculateTalentApplicationExpiryDate()
            ])->save();
        }

        // If the talent response status is changed to pending we fire an event. This is
        // used to alert the talent that their response has been changed to pending.
        if ($talent->response->status === TalentPivot::STATUS_PENDING && $previous_status !== TalentPivot::STATUS_APPLIED) {
            event(new TalentResponsePending($talent, $booking));
        }

        // If the talent response status is changed to applied we fire an event. This is
        // used to alert the talent that their response has been changed to applied.
        if ($talent->response->status === TalentPivot::STATUS_APPLIED) {

            $dateAppliedFor = $booking->has_date_options_only ? $request->input('dateAppliedFor') : $booking->date;
            if ($booking->has_date_options_only && $dateAppliedFor === null) {
                // Talent must select a date if date_options are available.
                abort(500, 'Unable to proceed, error when recording applied date.');
            }
            $talent->response->fill(['date_applied_for' => $dateAppliedFor])->save();

            event(new TalentApplied($talent, $booking));
        }

        if ($talent->response->status === TalentPivot::STATUS_REVIEWING) {
            event(new TalentReviewing($talent, $booking));
        }

        // If the talent response status is changed from applied to declined we fire an event. This is
        // used to alert the client and admins that an applied talent has now declined
        if ($talent->response->status === TalentPivot::STATUS_DECLINED && $previous_status === TalentPivot::STATUS_APPLIED) {
            event(new AppliedTalentDeclining($talent, $booking));
        }

        return new TalentResource($booking->talent()->find($talent->id));
    }
}

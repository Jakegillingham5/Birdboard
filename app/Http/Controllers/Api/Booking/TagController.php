<?php

namespace Pickstar\Http\Controllers\Api\Booking;

use Illuminate\Http\Request;
use Pickstar\Booking\Booking;
use Pickstar\Events\BookingRestored;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForBookings;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Http\Transformers\BookingTransformer;
use Pickstar\Http\Resources\Booking as BookingResource;

class TagController extends Controller
{
    use DeniesForBookings,
        DeniesHttpRequests;

    /**
     * Update the bookings tags based on the request data
     *
     * @param Request $request
     * @param Booking $booking
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Request $request, Booking $booking)
    {
        $this->authorize('manageBookingTags', $booking);

        $booking->tags()->sync($request->input('tags'));

        return response()->json($booking->tags, 200);
    }
}

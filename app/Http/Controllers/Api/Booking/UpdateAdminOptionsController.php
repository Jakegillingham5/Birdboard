<?php

namespace Pickstar\Http\Controllers\Api\Booking;

use Illuminate\Http\Request;
use Pickstar\Booking\Booking;
use Illuminate\Http\JsonResponse;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Resources\Booking as BookingResource;

class UpdateAdminOptionsController extends Controller
{
    /**
     * Update the admin options on a booking.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Booking $booking)
    {
        $this->authorize('update', $booking);

        $booking->fill([
            'commission_rate' => $request->input('commission_rate'),
            'commission_dollar' => $request->input('commission_dollar'),
            'commission_type' => $request->input('commission_type'),
            'talent_can_comment' => $request->input('talent_can_comment'),
            'talent_expire' => $request->input('talent_expire'),
            'expires' => $request->input('expires'),
            'billboard_expiry_date' => $request->input('billboard_expiry_date')
        ])->save();

        return new BookingResource($booking);
    }
}

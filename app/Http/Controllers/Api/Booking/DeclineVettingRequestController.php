<?php

namespace Pickstar\Http\Controllers\Api\Booking;

use Illuminate\Http\Request;
use Pickstar\Booking\Booking;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForBookings;
use Pickstar\Http\Resources\Booking as BookingResource;

class DeclineVettingRequestController extends Controller
{
    use DeniesForBookings;

    /**
     * Decline a vetting booking request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Booking $booking)
    {
        $this->authorize('declineVettingRequest', $booking);

        $booking->fill([
            'status' => Booking::STATUS_DECLINED,
            'vetted_date' => now(),
            'billboard_expiry_date' => null,
            'withdraw_reason' => $request->get('reason')
        ])->save();

        return new BookingResource($booking);
    }
}

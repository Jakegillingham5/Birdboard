<?php

namespace Pickstar\Http\Controllers\Api\Booking;

use DB;
use Exception;
use Illuminate\Http\Request;
use Pickstar\Booking\Booking;
use Illuminate\Http\JsonResponse;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForBookings;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Http\Resources\Booking as BookingResource;
use Pickstar\Http\Requests\Api\Booking\ReactivateBookingRequest;

class ReactivateRequestController extends Controller
{
    use DeniesForBookings,
        DeniesHttpRequests;

    /**
     * Reactivate a booking request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(ReactivateBookingRequest $request, Booking $booking)
    {
        $this->authorize('reactivateRequest', $booking);

        $this->denyIfBookingIsNotWithdrawnOrDeclined($booking);

        if ($booking->status === Booking::STATUS_DECLINED) {
            $booking->fill([
                'status' => Booking::STATUS_VETTING,
                'withdraw_reason' => null,
            ])->save();
        } elseif ($booking->status === Booking::STATUS_WITHDRAWN) {
            $booking->fill([
                'status' => Booking::STATUS_PENDING,
                'withdraw_reason' => null,
            ])->save();
        }

        return new BookingResource($booking);
    }
}

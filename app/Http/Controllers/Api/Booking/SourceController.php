<?php

namespace Pickstar\Http\Controllers\Api\Booking;

use Illuminate\Http\JsonResponse;
use Pickstar\Booking\Booking;
use Pickstar\Events\BookingRequestUpdated;
use Pickstar\Marketing\Source;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Requests\Api\Booking\UpdateSourceRequest;

class SourceController extends Controller
{
    /**
     * Get a booking's source
     *
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Booking $booking):JsonResponse
    {
        $this->authorize('getSource', Source::class);

        return response()->json($booking->marketingSource()->get()->first());
    }

    /**
     * Update a booking's source
     *
     * @param \Pickstar\Http\Requests\Api\Booking\UpdateSourceRequest $request
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateSourceRequest $request, Booking $booking):JsonResponse
    {
        $this->authorize('updateSource', Source::class);
        $this->authorize('update', $booking);

        if($booking->marketingSource !== null) {
            $booking->marketingSource->fill([
                'email' => $request->get('email'),
                'campaign_admin_source' => $request->get('campaign_admin_source')
            ])->save();
        } else {
            $booking->marketingSource()->firstOrCreate([
                'email' => $request->get('email'),
                'campaign_admin_source' => $request->get('campaign_admin_source'),
                'campaign_source' => null,
                'campaign_name' => null,
                'campaign_medium' => null,
                'campaign_content' => null,
                'campaign_term' => null,
            ])->save();
        }

        $booking->fill([
            'market_segment' => $request->get('market_segment'),
            'admin_opportunity_id' => $request->get('admin_opportunity_id')
        ])->save();

        event(new BookingRequestUpdated($booking));

        return response()->json($booking->marketingSource()->get()->first());
    }

}

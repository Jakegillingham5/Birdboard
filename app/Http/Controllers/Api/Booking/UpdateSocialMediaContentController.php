<?php

namespace Pickstar\Http\Controllers\Api\Booking;

use Illuminate\Http\Request;
use Pickstar\Booking\Booking;
use Illuminate\Http\JsonResponse;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForBookings;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Http\Resources\Booking as BookingResource;

class UpdateSocialMediaContentController extends Controller
{
    use DeniesForBookings,
        DeniesHttpRequests;

    /**
     * Update the social media content on a booking.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Booking $booking)
    {
        $this->authorize('updateSocialMediaContent', $booking);

        $this->denyIfBookingIsNotOnline($booking);

        $this->denyIfBookingIsCompleted($booking);

        $booking->fill(['social_media_content' => $request->input('social_media_content')])->save();

        return new BookingResource($booking);
    }
}

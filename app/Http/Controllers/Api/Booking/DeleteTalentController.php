<?php

namespace Pickstar\Http\Controllers\Api\Booking;

use Pickstar\User\User;
use Pickstar\Booking\Booking;
use Illuminate\Http\Response;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForBookings;
use Pickstar\Http\Concerns\DeniesHttpRequests;

class DeleteTalentController extends Controller
{
    use DeniesForBookings,
        DeniesHttpRequests;

    /**
     * Removes a talents association to the booking request by detaching.
     *
     * @param \Pickstar\Booking\Booking $booking
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Booking $booking, User $user)
    {
        $this->authorize('removeTalent', $booking);

        $this->denyIfBookingIsNotPending($booking);

        // Make sure that this is a talent that actually exists as part of the booking.
        // We simply grab the talent including their pivot response from the
        // bookings talent.
        if (! $talent = $booking->talent()->find($user->id)) {
            $this->deny('Unable to perform action on talent that has not responded to booking request.');
        }

        // Make sure that if the talent is shortlisted that we prevent them from
        // being removed. Only talent that apply after the fact may be removed
        // from the list.
        if ($talent->response->shortlisted) {
            $this->deny('Unable to remove a shortlisted talent from a booking request.');
        }

        $booking->talent()->detach($user->id);

        return response()->noContent();
    }
}

<?php

namespace Pickstar\Http\Controllers\Api\Booking;

use Pickstar\User\User;
use Illuminate\Http\Request;
use Pickstar\Booking\Booking;
use Illuminate\Http\JsonResponse;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForBookings;
use Pickstar\Events\TalentConfirmedAttendance;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Http\Resources\Talent as TalentResource;
use Pickstar\Events\TalentConfirmedAttendanceClientOnly;

class ConfirmTalentAttendanceController extends Controller
{
    use DeniesForBookings,
        DeniesHttpRequests;

    /**
     * Confirm the attendance of a talent.
     *
     * @param \Pickstar\Booking\Booking $booking
     * @param \Pickstar\User\User $talent
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Booking $booking, User $talent)
    {
        $this->authorize('confirmAttendance', $booking);

        $this->authorize('update', $talent);

        // If event type is in_person, only allow confirmation 24 hours before event
        $eventType = $request->input('type');

        switch($eventType) {
            case Booking::APPEARANCE_IN_PERSON:
                $this->denyIfCannotConfirmAttendanceBasedOnDate($booking);
                $attributes = ['attendance_confirmed' => true];
                break;
            case Booking::APPEARANCE_ONLINE:
                $attributes = ['online_participation_confirmed' => true];
                break;
            default:
                $this->deny('Unable to confirm attendance due to invalid event type.');
                break;
        }

        $this->denyIfTalentNotAttendingBooking($booking, $talent);

        $this->denyIfBookingIsCompleted($booking);

        $this->denyIfBookingIsNotBooked($booking);

        $booking->talent()
            ->findOrFail($talent->id)
            ->response
            ->fill($attributes)
            ->save();

        // Only dispatch talent confirmed attendance event if the request user is not an admin.
        if (! $request->user()->isAdmin()) {
            TalentConfirmedAttendance::dispatch($booking, $talent, $eventType);
        } else {
            TalentConfirmedAttendanceClientOnly::dispatch($booking, $talent, $eventType);
        }

        return new TalentResource($booking->talent()->find($talent->id));
    }
}

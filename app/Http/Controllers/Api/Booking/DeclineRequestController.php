<?php

namespace Pickstar\Http\Controllers\Api\Booking;

use Pickstar\Booking\Booking;
use Pickstar\Booking\TalentPivot;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Events\AppliedTalentDeclining;
use Pickstar\Http\Concerns\DeniesForBookings;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Http\Resources\Talent as TalentResource;
use Pickstar\Http\Requests\Api\Booking\DeclineBookingRequest;

class DeclineRequestController extends Controller
{
    use DeniesForBookings,
        DeniesHttpRequests;

    /**
     * Decline for a booking as a talent.
     *
     * @param \Pickstar\Http\Requests\Api\Booking\DeclineBookingRequest $request
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(DeclineBookingRequest $request, Booking $booking)
    {
        $this->denyIfBookingIsNotPending($booking);

        $this->denyIfBookingIsExpired($booking);

        $previous_status = null;
        if ($talent = $booking->talent()->find($request->user()->id)) {
            $previous_status = $booking->talent()->find($request->user()->id)->response->status;
        }

        $attributes = [
            'status' => TalentPivot::STATUS_DECLINED,
            'decline_reason' => $request->get('reason'),
            'applied_date' => null,
        ];

        if ($talent) {
            $talent->response->fill($attributes)->save();
        } else {
            $booking->talent()->attach($request->user(), $attributes);
        }

        if ($previous_status === TalentPivot::STATUS_APPLIED) {
            event(new AppliedTalentDeclining($booking->talent()->find($request->user()->id), $booking));
        }

        return new TalentResource($booking->talent()->find($request->user()->id));
    }
}

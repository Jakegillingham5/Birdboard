<?php

namespace Pickstar\Http\Controllers\Api\Booking;

use Pickstar\Booking\Note;
use Illuminate\Http\Request;
use Pickstar\Booking\Booking;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Requests\Api\Booking\StoreBookingNoteRequest;

class NotesController extends Controller
{
    /**
     * Get a booking's notes
     *
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Booking $booking)
    {
        $this->authorize('getNotes', Note::class);

        return response()->json($booking->notes()->latest()->get());
    }

    /**
     * Store a note against a booking
     *
     * @param \Pickstar\Http\Requests\Api\Booking\StoreBookingNoteRequest $request
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreBookingNoteRequest $request, Booking $booking)
    {
        $this->authorize('storeNote', Note::class);

        $note = $booking->notes()->create([
                'user_id' => $request->get('user_id'),
                'text' => $request->get('text')
        ]);

        return response()->json($note);
    }

    public function show(Booking $booking)
    {
        $this->authorize('getNotes', Note::class);

        return response()->json($booking->notes()->latest()->get());
    }

    /**
     * Delete an existing note.
     *
     * @param \Pickstar\Booking\Booking $booking
     * @param \Pickstar\Booking\Note $note
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Booking $booking, Note $note)
    {
        $this->authorize('deleteNote', $note);

        $note->delete();

        return response(null, 204);
    }
}

<?php

namespace Pickstar\Http\Controllers\Api\Booking;

use Pickstar\User\User;
use Pickstar\Booking\Booking;
use Pickstar\Booking\TalentPivot;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForBookings;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Http\Resources\Talent as TalentResource;
use Pickstar\Http\Requests\Api\Booking\ReactivateTalentRequest;

class ReactivateTalentController extends Controller
{
    use DeniesForBookings,
        DeniesHttpRequests;

    /**
     * Reactivate a talent from on a booking.
     *
     * @param \Pickstar\Http\Requests\Api\Booking\ReactivateTalentRequest $request
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ReactivateTalentRequest $request, Booking $booking)
    {
        $this->denyIfBookingTypeIsBooking($booking);

        //The talent to reactivate
        $talent_id = $request->get('talent_id');
        if (! $talent = $booking->talent()->find($talent_id)) {
            $this->deny('Unable to perform action on talent that has not responded to booking request.');
        }

        if ($talent->response->status !== TalentPivot::STATUS_APPLIED) {
            $this->deny('Unable to perform this action on a talent that has not applied for the booking request');
        }

        //try to reactivate them
        if ($booking->talent_expire) {
            $talent->response->fill([
                'expired' => false,
                'expiry_date' => $booking->calculateTalentApplicationExpiryDate()
            ])->save();
        }

        //If talent do not expire for this booking, when we reactivate them, we set it to no expiry date
        if (!$booking->talent_expire) {
            $talent->response->fill([
                'expired' => false,
                'expiry_date' => null
            ])->save();
        }

        return new TalentResource($booking->talent()->find($talent_id));
    }
}

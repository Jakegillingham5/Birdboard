<?php

namespace Pickstar\Http\Controllers\Api\Booking;

use Pickstar\Booking\Booking;
use Pickstar\Setting\Setting;
use Pickstar\Events\BookingRequestVetted;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForBookings;
use Pickstar\Http\Resources\Booking as BookingResource;

class ApproveVettingRequestController extends Controller
{
    use DeniesForBookings;

    /**
     * Approve a vetting booking request.
     *
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Booking $booking)
    {
        $this->authorize('approveVettingRequest', $booking);

        $booking->fill([
            'status' => Booking::STATUS_PENDING,
            'vetted_date' => now(),
            'billboard_expiry_date' => now()->addDays(Setting::billboardPostLength())
        ])->save();

        event(new BookingRequestVetted($booking));

        return new BookingResource($booking);
    }
}

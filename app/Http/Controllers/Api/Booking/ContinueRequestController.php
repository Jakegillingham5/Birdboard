<?php

namespace Pickstar\Http\Controllers\Api\Booking;

use Pickstar\User\User;
use Illuminate\Http\Request;
use Pickstar\Booking\Booking;
use Pickstar\Message\Thread;
use Pickstar\Events\TalentAccepted;
use Pickstar\Events\BookingTermsAccepted;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForBookings;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Http\Transformers\BookingTransformer;
use Pickstar\Events\BookingRequestProgressedToBooking;
use Pickstar\Http\Resources\Booking as BookingResource;

class ContinueRequestController extends Controller
{
    use DeniesForBookings,
        DeniesHttpRequests;

    /**
     * Continue a booking request through to the booking state with less than
     * the required number of stars.
     *
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return BookingResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Request $request, Booking $booking)
    {
        $this->authorize('continueRequest', $booking);

        $this->denyIfBookingIsNotPending($booking);

        // Make sure that if we have no talent accepted by the client that we deny them
        // from continuing the booking. Need at least one talent.
        if ($booking->talent()->wherePivot('client_accepted', true)->count() === 0) {
            $this->deny('Unable to continue with booking with no accepted stars.');
        }

        $booking->fill([
            'type' => Booking::TYPE_BOOKING,
            'status' => Booking::STATUS_SCHEDULED,
            'date_options' => null,
        ])->save();

        event(new BookingRequestProgressedToBooking($booking));
        foreach ($booking->talent()->where('client_accepted', true)->get() as $talent) {
            // Fire the talent accepted event which will control whether the booking request
            // becomes a booking based on the amount of talent accepted for it. The reason
            // for passing our talent instead of our user is because it contains the
            // pivot record as well.
            event(new TalentAccepted($talent, $booking));
        }

        return new BookingResource($booking);
    }

    /**
     * Continue a booking request and leave open for further bookings=
     *
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return BookingResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function continue(Booking $booking)
    {
        $this->authorize('continueRequest', $booking);
        $this->denyIfBookingIsNotPending($booking);

        // Make sure that if we have no talent accepted by the client that we deny them
        // from continuing the booking. Need at least one talent.
        if ($booking->talent()->wherePivot('client_accepted', true)->count() === 0) {
            $this->deny('Unable to continue with booking with no accepted stars.');
        }

        $talents        = $booking->talent()->wherePivot('client_accepted', true)->get();
        $new            = $booking->duplicate();
        $new->fill([
            'talent_required'           => count($talents),
            'type'                      => Booking::TYPE_BOOKING,
            'status'                    => Booking::STATUS_SCHEDULED,
            'client_accepted_terms'     => true,
            'client_accepted_terms_date' => now(),
            'date_options' => null,
        ])->save();

        foreach($talents as $talent){
            $talent->response->booking_id = $new->id;
            $talent->response->save();
        }

        $booking->client_accepted_terms = 0;
        $booking->required_stars -= count($talents);

        $booking->save();

        event(new BookingRequestProgressedToBooking($new));
        foreach ($new->talent()->where('client_accepted', true)->get() as $talent) {
            // Fire the talent accepted event which will control whether the booking request
            // becomes a booking based on the amount of talent accepted for it. The reason
            // for passing our talent instead of our user is because it contains the
            // pivot record as well.
            event(new TalentAccepted($talent, $new));
        }

        return new BookingResource($new);
    }
}

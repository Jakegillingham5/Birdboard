<?php

namespace Pickstar\Http\Controllers\Api\Booking;

use Illuminate\Http\Request;
use Pickstar\Booking\Booking;
use Pickstar\Events\BookingRestored;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForBookings;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Http\Transformers\BookingTransformer;
use Pickstar\Http\Resources\Booking as BookingResource;

class RestoreController extends Controller
{
    use DeniesForBookings,
        DeniesHttpRequests;

    /**
     * Restore a booking that is "on hold" from being cancelled by a client.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Booking $booking)
    {
        $this->authorize('restore', $booking);

        $this->denyIfBookingIsNotOnHold($booking);

        if ($booking->client_accepted_terms && $booking->payments()->deposit()->paid()->exists()) {
            $booking->fill(['status' => Booking::STATUS_BOOKED])->save();
        } else {
            $booking->fill(['status' => Booking::STATUS_SCHEDULED])->save();
        }

        event(new BookingRestored($booking));

        $booking->loadDefaultRelations($request);

        return new BookingResource($booking);
    }
}

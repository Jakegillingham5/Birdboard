<?php

namespace Pickstar\Http\Controllers\Api\Booking;

use Pickstar\User\User;
use Illuminate\Http\Response;
use Pickstar\Booking\Booking;
use Pickstar\Booking\TalentPivot;
use Pickstar\Events\BookingCompleted;
use Pickstar\Events\TalentPaymentSent;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForBookings;
use Pickstar\Http\Concerns\DeniesHttpRequests;

class NotifyTalentPaymentSentController extends Controller
{
    use DeniesForBookings,
        DeniesHttpRequests;

    /**
     * Notify a talent that their payment for a booking has been sent
     *
     * @param \Pickstar\Booking\Booking $booking
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Booking $booking, User $user) : Response
    {
        $this->authorize('notifyTalentPaid', User::class);

        $this->denyIfBookingTypeIsRequest($booking);

        $this->denyIfBookingIsNotCompleted($booking);

        if ($booking->talent()->find($user->id)->response->talent_paid_notif_sent) {
            $this->deny('Unable to send notification to talent already notified.');
        }

        event(new TalentPaymentSent($user, $booking));

        // TODO -figure out why this block is failing for auditing
        TalentPivot::disableAuditing();
        $booking->talent()->syncWithoutDetaching([
            $user->id => [
                'talent_paid_notif_sent' => true
            ]
        ]);
        TalentPivot::enableAuditing();

        return response(null, 204);
    }
}

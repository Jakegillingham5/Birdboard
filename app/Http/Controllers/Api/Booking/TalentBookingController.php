<?php

namespace Pickstar\Http\Controllers\Api\Booking;

use Pickstar\User\User;
use Illuminate\Http\Request;
use Pickstar\Booking\Booking;
use Illuminate\Http\JsonResponse;
use Pickstar\Booking\TalentPivot;
use Pickstar\Events\BookingCancelled;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Events\ClientCancelledBooking;
use Pickstar\Events\TalentCancelledBooking;
use Pickstar\Http\Concerns\DeniesForBookings;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Http\Transformers\BookingTransformer;
use Pickstar\Http\Resources\Booking as BookingResource;
use Pickstar\Http\Requests\Api\Booking\UpdateBookingRequest;

class TalentBookingController extends Controller
{
    use DeniesForBookings,
        DeniesHttpRequests;

    /**
     * Get a list of a talent's bookings.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, User $user)
    {
        $this->authorize('talentBookings', $request->user);

        $pageSize = $request->query('page_size', 5);

        $bookings = Booking::select('bookings.*')
            ->when($user->isTalent(), function ($query) use ($user, $request) {
                $query->join('booking_talent', function ($join) use ($user) {
                    $join->on('booking_talent.booking_id', '=', 'bookings.id')
                        ->when($user->isTalent(), function ($query) use ($user) {
                            $query->where('booking_talent.talent_id', $user->id);
                        });
                });

                $query->haveBeenAcceptedWithCancelled();
            })
            ->applyFiltersAndSorting($request, [
                'customStatusFilterCallback' => function ($query) use ($request, $user) {
                    $query->when($request->query('status', 'active') === 'active', function ($query) use ($user) {
                        $query->whereIn('bookings.status', [
                            Booking::STATUS_SCHEDULED,
                            Booking::STATUS_BOOKED,
                            Booking::STATUS_HOLD
                        ]);

                        $query->when($user->isTalent() || $user->isManager(), function ($query) {
                            $query->where('booking_talent.status', TalentPivot::STATUS_APPLIED);
                        });
                    });

                    $query->when($request->query('status') === 'event', function ($query) use ($user) {
                        $query->whereIn('bookings.status', [
                            Booking::STATUS_SCHEDULED,
                            Booking::STATUS_BOOKED,
                            Booking::STATUS_HOLD
                        ]);

                        $query->when($user->isTalent() || $user->isManager(), function ($query) {
                            $query->where('booking_talent.status', TalentPivot::STATUS_APPLIED);
                        });

                        $query->whereIn('bookings.appearance_type', [
                            Booking::APPEARANCE_BOTH,
                            Booking::APPEARANCE_IN_PERSON
                        ]);
                    });

                    $query->when($request->query('status') === 'campaign', function ($query) use ($user) {
                        $query->whereIn('bookings.status', [
                            Booking::STATUS_SCHEDULED,
                            Booking::STATUS_BOOKED,
                            Booking::STATUS_HOLD
                        ]);

                        $query->when($user->isTalent() || $user->isManager(), function ($query) {
                            $query->where('booking_talent.status', TalentPivot::STATUS_APPLIED);
                        });

                        $query->whereIn('bookings.appearance_type', [
                            Booking::APPEARANCE_BOTH,
                            Booking::APPEARANCE_ONLINE
                        ]);
                    });

                    $query->when($request->query('status') === 'archived', function ($query) use ($user) {
                        $query->where('bookings.status', Booking::STATUS_CANCELLED);

                        $query->when($user->isTalent() || $user->isManager(), function ($query) use ($user) {
                            $query->orWhere('booking_talent.status', TalentPivot::STATUS_CANCELLED);
                        });

                        $query->when(!$user->isAdmin(), function ($query) use ($user) {
                            $query->orWhere('bookings.status', Booking::STATUS_COMPLETED);
                        });
                    });

                    $query->when($request->query('status') === 'completed-event', function ($query) use ($user) {
                        $query->where('bookings.status', Booking::STATUS_COMPLETED);

                        $query->whereIn('bookings.appearance_type', [
                            Booking::APPEARANCE_BOTH,
                            Booking::APPEARANCE_IN_PERSON
                        ]);
                    });
                    $query->when($request->query('status') === 'completed-campaign', function ($query) use ($user) {
                        $query->where('bookings.status', Booking::STATUS_COMPLETED);

                        $query->whereIn('bookings.appearance_type', [
                            Booking::APPEARANCE_BOTH,
                            Booking::APPEARANCE_ONLINE
                        ]);
                    });
                },
                'defaultSortingCallback' => function ($query) {
                    // Default the order by the "online" appearance type so these appear at the top of the list,
                    // that's because they don't have a date. This is only when sorting by date and not by
                    // the price.
                    $query->orderByRaw('FIELD(bookings.appearance_type, ?) DESC', Booking::APPEARANCE_ONLINE);

                    $query->oldest('tz_date');
                },
                'customDateSortingCallback' => function ($query, $request) {
                    $query->orderByRaw('FIELD(bookings.appearance_type, ?) DESC', Booking::APPEARANCE_ONLINE)
                        ->when($request->query('order') === 'asc', function ($query) {
                            $query->oldest('tz_date');
                        }, function ($query) {
                            $query->latest('tz_date');
                        });
                },
                'customCreatedAtSortingCallback' => function ($query, $request) {
                    $query->when($request->query('order') === 'asc', function ($query) {
                        $query->oldest('created_at');
                    }, function ($query) {
                        $query->latest('created_at');
                    });
                },
                'sortingDateColumn' => 'tz_date',
                'filteringDateColumn' => 'tz_date'
            ])
            ->groupBy('bookings.id')
            ->paginate($pageSize);

        return BookingResource::collection($bookings);
    }
}

<?php

namespace Pickstar\Http\Controllers\Api\Booking;

use Pickstar\User\User;
use Illuminate\Http\Request;
use Pickstar\Booking\Booking;
use Illuminate\Http\JsonResponse;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForBookings;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Http\Resources\Talent as TalentResource;
use Pickstar\Notifications\Talent\BookingRequestToBeReviewed;

class SendTalentNotificationController extends Controller
{
    use DeniesForBookings,
        DeniesHttpRequests;

    /**
     * Send talent a notification about a booking request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, Booking $booking)
    {
        $this->authorize('notifyTalent', $booking);

        $this->denyIfBookingIsNotPending($booking);

        //allow on all requests now   $this->denyIfBookingIsNotPublic($booking);

        // Find all the added talent and filter out those that have already been notified. This
        // shouldn't happen but it's just a precautionary step to prevent any hackery.
        $talent = User::talent()
            ->active()
            ->findMany($request->input('talent'))
            ->filter(function ($talent) use ($booking) {
                return ! $booking->talentNotified->contains($talent);
            });

        // Now we can send the notification to each of the talent and add them to the notifications
        // table so we don't notify them again.
        $talent->each(function ($talent) use ($booking) {
            $talent->notify(new BookingRequestToBeReviewed($booking));
        });

        $booking->talentNotified()
            ->syncWithoutDetaching($talent->pluck('id'));

        return TalentResource::collection($booking->talentNotified()->get());
    }
}

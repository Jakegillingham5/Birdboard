<?php

namespace Pickstar\Http\Controllers\Api\Booking;

use DB;
use Exception;
use Pickstar\Events\LeadCaptured;
use Pickstar\User\User;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Pickstar\Booking\Booking;
use Pickstar\Marketing\Source;
use Illuminate\Http\JsonResponse;
use Pickstar\Booking\TalentPivot;
use Pickstar\Events\UserRegistered;
use Illuminate\Support\Facades\Auth;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Events\BookingRequestCreated;
use Pickstar\Events\BookingRequestWithdrawn;
use Pickstar\Http\Concerns\DeniesForBookings;
use Pickstar\Registration\ClientRegistration;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Booking\Scopes\AdminRequestsScope;
use Pickstar\Booking\Scopes\ClientRequestsScope;
use Pickstar\Booking\Scopes\TalentRequestsScope;
use Pickstar\Booking\Scopes\ManagerRequestsScope;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Pickstar\Http\Resources\Booking as BookingResource;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Pickstar\Http\Transformers\BookingRequestTransformer;
use Pickstar\Http\Requests\Api\Booking\WithdrawBookingRequest;
use Pickstar\Http\Requests\Api\Booking\StoreBookingRequestRequest;
use Pickstar\Http\Requests\Api\Booking\UpdateBookingRequestRequest;

class RequestController extends Controller
{
    use DeniesForBookings,
        DeniesHttpRequests;

    /**
     * Get a list of booking requests based on the user role.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request): ResourceCollection
    {
        $this->authorize('listRequests', Booking::class);

        $user = $request->user();

        $pageSize = $request->query('page_size', 25);

        $bookings = Booking::select('bookings.*')
            ->withoutGlobalScope('bookings')
            ->with('opportunity', 'onlineOpportunity', 'client')
            ->when($user->isAdmin(), function ($query) use ($request) {
                $query->constrainWith(new AdminRequestsScope($request));
            })
            ->when($user->isClient(), function ($query) use ($request) {
                $query->constrainWith(new ClientRequestsScope($request));
            })
            ->when($user->isTalent(), function ($query) use ($request) {
                $query->constrainWith(new TalentRequestsScope($request));
            })
            ->when($user->isManager(), function ($query) use ($request) {
                $query->constrainWith(new ManagerRequestsScope($request));
            })
            ->applyFiltersAndSorting($request, [
                'defaultSortingCallback' => function ($query) use ($request) {
                    $query->orderBy('date', 'asc');
                },
                'customDateSortingCallback' => function ($query, $request) {
                    $query->orderByRaw('FIELD(bookings.appearance_type, ?) DESC', Booking::APPEARANCE_ONLINE)
                        ->when($request->query('order') === 'asc', function ($query) {
                            $query->oldest('tz_date');
                        }, function ($query) {
                            $query->latest('tz_date');
                        });
                },
                'customCreatedAtSortingCallback' => function ($query, $request) {
                    $query->when($request->query('order') === 'asc', function ($query) {
                        $query->oldest('created_at');
                    }, function ($query) {
                        $query->latest('created_at');
                    });
                },
                'sortingCreatedAtColumn' => 'created_at',
                'sortingDateColumn' => 'date',
                'filteringDateColumn' => 'tz_date'
            ])
            ->groupBy('bookings.id')
            ->paginate($pageSize);

        return BookingResource::collection($bookings);
    }

    /**
     * Update a booking request.
     *
     * @param \Pickstar\Http\Requests\Api\Booking\UpdateBookingRequestRequest $request
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateBookingRequestRequest $request, Booking $booking): BookingResource
    {
        $this->denyIfBookingIsDeclined($booking);

        $booking->updateFromRequestPayload($request);

        return new BookingResource($booking);
    }

    /**
     * Show a single booking request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, Booking $booking): BookingResource
    {
        $this->authorize('viewRequest', $booking);

        $booking->load([
            'client',
            'opportunity',
            'onlineOpportunity',
            'talent' => function ($query) use ($request) {
                // If talent only themselves in the response. Don't want talent knowing who else is attending
                // at this stage in the game.
                $query->onlyMyselfIfTalent($request->user());

                // If client only show the applied or shortlisted talent. Don't want the client to see the
                // talent that have declined or the admin has added as "hidden".
                $query->onlyAppliedOrShortlistedIfClient($request->user());

                // If manager only show the managed talent.
                $query->onlyManagedIfManager($request->user());
            },
            'talentNotified',
            'managersNotified'
        ]);

        return new BookingResource($booking);
    }

    /**
     * Create a new booking request.
     *
     * @param \Pickstar\Http\Requests\Api\Booking\StoreBookingRequestRequest $request
     * @param \Pickstar\Registration\ClientRegistration $registration
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreBookingRequestRequest $request, ClientRegistration $registration): JsonResponse
    {
        try {
            DB::beginTransaction();

            // Either grab the request user from the request instance or attempt to create a new user
            // from the request payload via the client registration helper.
            $user = $request->user() ??
                $registration->handle($request->only('first_name', 'last_name', 'company', 'email', 'phone', 'password'));

            // By default we assume that the client is the user.
            $client = $user;

            // If a client has been passed along with the request then we need to attempt to find
            // that user, as an admin can submit a request on behalf of a client.
            if ($request->filled('client')) {
                if ($request->user() && $request->user()->isAdmin()) {
                    try {
                        $client = User::onlyClients()->findOrFail($request->get('client'));
                    } catch (ModelNotFoundException $exception) {
                        return response()->json(['message' => 'Unable to create booking request for invalid client.'], 400);
                    }
                }
            } elseif ($request->user() && $request->user()->isAdmin()) {
                return response()->json(['message' => 'Unable to create booking request as an administrator unless creating for a client.'], 400);
            }

            // Ensure that client has agreed to terms and conditions
            $termsChecked = $request->input('client_agreed_terms_on_request_creation');
            if (!$termsChecked) {
                return response()->json(['message' => 'Unable to create booking request since terms have not been accepted.'], 400);
            }

            // New up the booking instance. We can then update the booking from the request payload,
            // passing in "false" indicates we do not want to save the booking yet. We'll then
            // associate the client and fill in some other details that cannot be adjusted
            // by the person creating the request.
            $booking = new Booking;

            $booking->updateFromRequestPayload($request, false);

            $booking->client()->associate($client);

            $booking->fill([
                'type' => Booking::TYPE_REQUEST,
                'status' => Booking::STATUS_VETTING,
                'others_can_apply' => true,
                'request_form_version' => $request->input('request_form_version'),
                'initial_budget_per_star' => $request->input('budget_per_star'),
                'initial_required_stars' => $request->input('required_stars'),
                'client_agreed_terms_on_request_creation' => $termsChecked,
                'client_agreed_terms_on_request_creation_date' => now(),
            ]);

            $booking->save();

            // If we have a shortlist then we can attach each of the talents to the booking.
            // we will create any shortlist item as a user that is not accessible
            if ($request->filled('shortlist')) {
                foreach ($request->get('shortlist') as $index => $shortlistItem) {
                    $id = $shortlistItem['id'];

                    // Need to create unknown talent if their ID begins with an S.
                    if (starts_with($id, 'S')) {
                        $nameParts = explode(' ', $shortlistItem['name']);

                        if (count($nameParts) > 2) {
                            $firstName = implode(' ', array_slice($nameParts, 0, -1));
                            $lastName = Arr::last($nameParts);
                        } elseif (count($nameParts) === 2) {
                            $firstName = Arr::first($nameParts);
                            $lastName = Arr::last($nameParts);
                        } else {
                            $firstName = Arr::first($nameParts);
                            $lastName = null;
                        }

                        $unknownTalent = User::create([
                            'first_name' => $firstName,
                            'last_name' => $lastName,
                            'role' => User::ROLE_UNKNOWN,
                            'status' => User::STATUS_ACTIVE
                        ]);

                        $unknownTalent->profile()->create([]);

                        $id = $unknownTalent->id;
                    }

                    $booking->talent()->attach($id, [
                        'priority' => ++$index,
                        'shortlisted' => true,
                        'status' => TalentPivot::STATUS_PENDING
                    ]);
                }
            }

            $booking->load('talent');

            $admin_source = $request->input('admin_source');

            if ($client->wasRecentlyCreated) {
                Auth::setUser($client);

                Source::insertOrUpdateWithGA($request->get('email'), $admin_source);
                Source::updateSourceTableWhenCreatedRecently($request->get('email'), $booking->id, $user->id);
                event(new UserRegistered($client));
            } else {
                Source::insertOrUpdateWithGA($user->email, $admin_source, $booking->id, $user->id, true);
            }


            // All is well! Commit it.
            DB::commit();

            event(new BookingRequestCreated($booking));

            return (new BookingResource($booking))
                ->response()
                ->header('Authorization', sprintf('Bearer %s', $client->createJwtToken()));
        } catch (Exception $exception) {
            report($exception);

            DB::rollBack();

            abort(500, 'Something went wrong when creating the booking.');
        }
    }

    /**
     * Withdraw a booking request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(WithdrawBookingRequest $request, Booking $booking): BookingResource
    {
        $this->authorize('withdrawRequest', $booking);

        $this->denyIfBookingIsWithdrawn($booking);

        $booking->fill([
            'status' => Booking::STATUS_WITHDRAWN,
            'withdraw_reason' => $request->get('reason'),
        ])->save();

        if ($request->user()->isClient()) {
            BookingRequestWithdrawn::dispatch($booking);
        }

        return new BookingResource($booking);
    }
}

<?php

namespace Pickstar\Http\Controllers\Api\Booking;

use Illuminate\Support\Facades\Request;
use Exception;
use Pickstar\Booking\Booking;
use Illuminate\Http\JsonResponse;
use Pickstar\Events\BookingCompleted;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForBookings;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Http\Resources\Booking as BookingResource;

class MarkCompleteController extends Controller
{
    use DeniesForBookings,
        DeniesHttpRequests;

    /**
     * Mark a booking as complete.
     *
     * This can only occur on the day of the booking and only by admins and talents that attended
     * the booking. Attending talents have to have confirmed their attendance.
     *
     * @param \Pickstar\Booking\Booking $booking
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Booking $booking)
    {
        $user = auth()->user();
        $eventType = Request::input('type');

        switch($eventType) {
            case Booking::APPEARANCE_IN_PERSON:
                $this->authorize('completeInPerson', $booking);
                $this->denyIfNotDayOfBooking($booking);
                break;
            case Booking::APPEARANCE_ONLINE:
                $this->authorize('completeOnline', $booking);
                break;
            default:
                if (!$user->isAdmin()) {
                    throw new Exception('Error in marking booking as complete due to invalid event type');
                }
        }

        $this->denyIfBookingIsNotBooked($booking);

        try {
            if ($user->isAdmin()) {
                $booking->markBookingAsComplete();

            } else if ($user->isTalent()) {
                $response = $booking->talentAttending()->where('users.id', $user->id)->first()->response;
                $booking->markTalentHasComplete($response, $eventType);
                $booking->closeTalentThreads($user->id);
            }
        } catch (Exception $e) {
            abort(500, 'Error in marking booking as complete due to invalid event type.');
        }

        return new BookingResource($booking->refresh());
    }
}

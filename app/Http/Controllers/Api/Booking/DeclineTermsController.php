<?php

namespace Pickstar\Http\Controllers\Api\Booking;

use Pickstar\Booking\Booking;
use Illuminate\Http\JsonResponse;
use Pickstar\Events\BookingTermsDeclined;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForBookings;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Http\Transformers\BookingTransformer;
use Pickstar\Http\Resources\Booking as BookingResource;
use Pickstar\Http\Requests\Api\Booking\AcceptBookingTermsRequest;

class DeclineTermsController extends Controller
{
    use DeniesForBookings,
        DeniesHttpRequests;

    /**
     * Update a bookings terms to be accepted if they are not already.
     *
     * @param \Pickstar\Http\Requests\Api\Booking\AcceptBookingTermsRequest $request
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(AcceptBookingTermsRequest $request, Booking $booking)
    {
        $this->denyIfBookingTermsAlreadyAccepted($booking);

        $this->denyIfBookingTermsDeniedEmailSent($booking);

        $booking->notification->client_declined_terms = true;

        $booking->notification->save();

        event(new BookingTermsDeclined($booking));

        return new BookingResource($booking);
    }
}

<?php

namespace Pickstar\Http\Controllers\Api;

use Illuminate\Http\Request;
use Pickstar\Booking\Booking;
use Illuminate\Http\JsonResponse;
use Pickstar\Booking\TalentPivot;
use Pickstar\Opportunity\Opportunity;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Transformers\BookingRequestTransformer;

class BillboardController extends Controller
{
    /**
     * List bookings available on the billboard.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $this->authorize('listBillboard', Booking::class);

        $user = $request->user();

        $bookings = Booking::
            when($user->isTalent(), function ($query) use ($user) {
                $query->whereHas('tags', function ($query) use ($user) {
                    $query->whereIn('tag_id', $user->tags->pluck('id'));
                });
                $query->orWhereDoesntHave('tags');
                $query->orWhereHas('talent', function ($query) use ($user) {
                    $query->where('booking_talent.talent_id', $user->id);
                });
            })
            ->requests()
            ->withoutVetting()
            ->onBillboard()
            ->isNotExpired()
            ->with('opportunity', 'onlineOpportunity', 'client')
            ->where('status', Booking::STATUS_PENDING)

            // If user is an admin then we'll add a talent response count to the query so
            // we know how many talent have applied for the request.
            ->when($user->isAdmin(), function ($query) {
                $query->withCount(['talent as talent_response_count' => function ($query) {
                    $query->where('booking_talent.status', TalentPivot::STATUS_APPLIED);
                }]);
            })

            // If user a talent we'll include the talent responses but only include the
            // logged in users response so we cannot see others responses.
            ->when($user->isTalent() || $user->isManager(), function ($query) use ($user) {
                $query->with(['talent' => function ($query) use ($user) {
                    $query->onlyMyselfIfTalent($user);

                    $query->onlyManagedIfManager($user);
                }]);
            })

            // Filter by locations.
            ->when($request->query('locations'), function ($query, $locations) {
                $query->where(function ($query) use ($locations) {
                    $query->whereIn('state', (array) $locations)->orWhereNull('state');
                });

                $query->orderByRaw('bookings.appearance_type <> ? DESC, bookings.state IS NOT NULL DESC', [
                    Booking::APPEARANCE_ONLINE,
                ]);
            })

            // Filter by opportunity types.
            ->when($request->query('opportunities'), function ($query, $opportunities) {
                $opportunities = Opportunity::findMany((array) $opportunities);

                $query->where(function ($query) use ($opportunities) {
                    $query->whereIn('opportunity_id', $opportunities->where('type', Opportunity::TYPE_IN_PERSON)->pluck('id'))
                        ->orWhereIn('online_opportunity_id', $opportunities->where('type', Opportunity::TYPE_ONLINE)->pluck('id'));
                });
            })

            // Filter by response type for managers and talent.
            ->when($request->filled('response'), function ($query) use ($user, $request) {
                $responses = (array) $request->query('response', []);

                if (empty($responses)) {
                    return;
                }

                $query->where(function ($query) use ($user, $responses) {
                    foreach ($responses as $response) {
                        switch ($response) {
                            case 'none':
                                $query->when($user->isTalent(), function ($query) use ($user) {
                                    $query->orWhereDoesntHave('talent', function ($query) use ($user) {
                                        $query->where('booking_talent.talent_id', $user->id);
                                    });

                                    $query->orWhereHas('talent', function ($query) use ($user) {
                                        $query->where('booking_talent.talent_id', $user->id)
                                            ->where('booking_talent.status', TalentPivot::STATUS_PENDING);
                                    });
                                });

                                $query->when($user->isManager(), function ($query) use ($user) {
                                    $query->orWhereDoesntHave('talent', function ($query) use ($user) {
                                        $query->managedBy($user);
                                    });

                                    $query->orWhereHas('talent', function ($query) use ($user) {
                                        $query->managedBy($user)
                                            ->where('booking_talent.status', TalentPivot::STATUS_PENDING);
                                    });
                                });
                                break;
                            case 'applied':
                            case 'declined':
                                $query->orWhereHas('talent', function ($query) use ($user, $response) {
                                    $query->when($user->isTalent(), function ($query) use ($user) {
                                        $query->where('booking_talent.talent_id', $user->id);
                                    }, function ($query) use ($user) {
                                        $query->managedBy($user);
                                    });

                                    if ($response === 'applied') {
                                        $query->where('booking_talent.status', TalentPivot::STATUS_APPLIED);
                                    }

                                    if ($response === 'declined') {
                                        $query->where('booking_talent.status', TalentPivot::STATUS_DECLINED);
                                    }
                                });
                                break;
                        }
                    }
                });
            });

        // If the given sort is by "price" then we will sort the results by the
        // "budget_per_star" column in the desired direction.
        if ($request->filled('sort') && $request->query('sort') === 'price') {
            if ($request->query('order') === 'asc') {
                $bookings->orderBy('budget_per_star', 'asc');
            } else {
                $bookings->orderBy('budget_per_star', 'desc');
            }

        // If the given sort is by "date" then we will sort the results by the
        // "date" column in the desired direction.
        } elseif ($request->filled('sort') && $request->query('sort') === 'date') {
            if ($request->query('order') === 'asc') {
                $bookings->orderBy('date', 'asc');
            } else {
                $bookings->orderBy('date', 'desc');
            }
        } elseif ($request->filled('sort') && $request->query('sort') === 'created_at') {
            if ($request->query('order') === 'asc') {
                $bookings->orderBy('created_at', 'asc');
            } else {
                $bookings->orderBy('created_at', 'desc');
            }
        // By default we sort by the latest "date".
        } else {
            $bookings->latest('date');
        }

        $pageSize = $request->query('page_size', 25);
        $bookings = $bookings->paginate($pageSize);

        $bookings->each(function ($booking) use ($request) {
            $booking->transform([new BookingRequestTransformer, 'transform'], [$request->user()]);
        });

        return response()->json($bookings);
    }
}

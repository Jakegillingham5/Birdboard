<?php

namespace Pickstar\Http\Controllers\Api\Report;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Pickstar\Jobs\Report\RequestReport;
use Pickstar\Http\Controllers\Controller;

class RequestReportController extends Controller
{
    /**
     * Export booking requests report.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        if (!auth()->user()->isAdmin()) {
            return response()->json([], 403);
        }

        RequestReport::dispatch(
            $request->user(),
            $request->input()
        );

        return response()->json([], 204);
    }
}

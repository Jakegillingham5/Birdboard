<?php

namespace Pickstar\Http\Controllers\Api\Report;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Pickstar\Jobs\Report\ManagerReport;
use Pickstar\Http\Controllers\Controller;

class ManagerReportController extends Controller
{
    /**
     * Export client report.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        if (!auth()->user()->isAdmin()) {
            return response()->json([], 403);
        }

        ManagerReport::dispatch($request->user());

        return response()->json([], 204);
    }
}

<?php

namespace Pickstar\Http\Controllers\Api\Report;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Jobs\Report\DeclineReasonReport;

class DeclineReasonReportController extends Controller
{
    /**
     * Export decline reason report.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $this->authorize('run.decline-reason-report');

        DeclineReasonReport::dispatch($request->user(), $request->input());

        return response()->json([], 204);
    }
}

<?php

namespace Pickstar\Http\Controllers\Api\Report;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Pickstar\Jobs\Report\MarketingReport;
use Pickstar\Http\Controllers\Controller;

class MarketingReportController extends Controller
{
    /**
     * Export marketing report.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        if (!auth()->user()->isAdmin()) {
            return response()->json([], 403);
        }

        MarketingReport::dispatch(
            $request->user(),
            $request->input()
        );

        return response()->json([], 204);
    }
}

<?php

namespace Pickstar\Http\Controllers\Api;

use Pickstar\User\User;
use Pickstar\Notifications;
use Illuminate\Http\Request;
use Pickstar\User\AdminUser;
use Pickstar\Booking\Booking;
use Illuminate\Http\JsonResponse;
use Pickstar\Booking\TalentPivot;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForUsers;
use Pickstar\Http\Transformers\UserTransformer;
use Pickstar\Notifications\System\EmailVerification;
use Pickstar\Http\Requests\Api\System\StoreAdminRequest;

class AdminController extends Controller
{
    use DeniesForUsers;

    /**
     * Get all admin that meet the given criteria.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, User $user): JsonResponse
    {
        $this->authorize('listAdmins', $user);

        $user = $request->user();

        $admins = User::admin()
            ->withTrashed()
            ->orderBy('first_name', 'asc')
            ->orderBy('last_name', 'asc')
            ->paginate(25);

        return response()->json($admins);
    }

    /**
     * Create a new admin user.
     *
     * @param \Pickstar\Http\Requests\Api\System\StoreAdminRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreAdminRequest $request, User $user): JsonResponse
    {
        $defaultAttributes = [
            'role' => User::ROLE_ADMIN,
            'status' =>  User::STATUS_ACTIVE,
            'email_verified' => true
        ];

        $admin = new User($defaultAttributes + $request->only([
                'first_name',
                'last_name',
                'email'
            ]));

        $admin->save();

        return response()->json($admin, 201);
    }

    /**
     * Delete a admin.
     *
     * This performs a soft delete so admins can be restored at a later time.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, User $user)
    {
        $this->authorize('deleteAdmin', $user);

        $this->denyIfAdminDeletingThemselves($request->user(), $user);

        $user->delete();

        return response()->json($user);
    }
}

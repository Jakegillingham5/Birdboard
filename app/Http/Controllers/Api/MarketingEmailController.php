<?php

namespace Pickstar\Http\Controllers\Api;

use Pickstar\Events\LeadCaptured;
use Pickstar\Marketing\Email;
use Illuminate\Http\JsonResponse;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Requests\Api\StoreMarketingEmailRequest;
use Pickstar\Marketing\Source;

class MarketingEmailController extends Controller
{
    /**
     * Stores a given email.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreMarketingEmailRequest $request): JsonResponse
    {
        $email = Email::firstOrCreate(['email' => $request->get('email')],
            [
                'first_name'        => $request->get('first_name'),
                'last_name'         => $request->get('last_name'),
                'collection_form'   => $request->get('collection_form'),
                'collection_page'   => \Request::server('HTTP_REFERER')
            ]);

        Source::insertOrUpdateWithGA($request->get('email'));
        event(new LeadCaptured($email));

        return response()->json($email);
    }
}

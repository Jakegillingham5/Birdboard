<?php

namespace Pickstar\Http\Controllers\Api;

use Pickstar\Helpers\SearchHelpers;
use Pickstar\User\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Notifications\System\EmailVerification;
use Pickstar\Http\Requests\Api\Manager\StoreManagerRequest;

class ManagerController extends Controller
{
    /**
     * Get all managers that meet the given criteria.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $this->authorize('index', User::class);

        $managers = User::masterManagers();

        if ($request->filled('status')) {
            $status = $request->query('status');

            if ($status === 'active') {
                $managers->where(function ($query) {
                    $query->where('email_verified', true)
                        ->where('status', '<>', User::STATUS_PENDING);
                });
            } elseif ($status === 'unverified') {
                $managers->where(function ($query) {
                    $query->where('email_verified', false)
                        ->orWhere('status', User::STATUS_PENDING);
                });
            } elseif ($status === 'trash') {
                $managers->onlyTrashed();
            }
        }

        if ($request->filled('name')) {
            $sanitizedString = SearchHelpers::sanitizeSearchString($request->query('name'));
            $managers->where(\DB::raw('concat(first_name, " ", last_name)'), 'like', '%' . $sanitizedString. '%');
            $managers->orWhere('phone', 'like', '%' . $sanitizedString . '%');
        }

        if ($request->filled('company')) {
            $managers->where('company', 'LIKE', '%'.$request->input('company').'%');
        }

        switch ($request->query('sort', 'name')) {
            case 'name':
                if ($request->query('order', 'asc') === 'asc') {
                    $managers->orderBy('first_name', 'asc')
                        ->orderBy('last_name', 'asc');
                } else {
                    $managers->orderBy('first_name', 'desc')
                        ->orderBy('last_name', 'desc');
                }
                break;
            case 'date':
                if ($request->query('order', 'asc') === 'asc') {
                    $managers->oldest();
                } else {
                    $managers->latest();
                }
                break;
            case 'company':
                if ($request->query('order', 'asc') === 'asc') {
                    $managers->orderBy('company', 'asc');
                } else {
                    $managers->orderBy('company', 'desc');
                }
        }

        $managers = $managers->paginate(25);

        return response()->json($managers);
    }

    /**
     * Get an single manager.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, User $user): JsonResponse
    {
        $this->authorize('manage', $user);

        return response()->json($user);
    }

    /**
     * Create a new manager user.
     *
     * @param \Pickstar\Http\Requests\Api\Manager\StoreManagerRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreManagerRequest $request): JsonResponse
    {
        $defaultAttributes = [
            'role' => User::ROLE_MANAGER,
            'status' => User::STATUS_ACTIVE,
            'email_verification_token' => Str::random(32)
        ];

        $manager = User::create($defaultAttributes + $request->only([
            'first_name',
            'last_name',
            'email',
            'phone',
            'company',
        ]));

        $manager->notify(new EmailVerification);

        return response()->json($manager, 201);
    }
}

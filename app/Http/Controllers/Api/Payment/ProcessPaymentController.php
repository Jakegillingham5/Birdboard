<?php

namespace Pickstar\Http\Controllers\Api\Payment;

use Pickstar\Booking\Booking;
use Pickstar\Events\CreateBookingThreads;
use Pickstar\Payment\Payment;
use Pickstar\Events\DepositPaid;
use Illuminate\Http\JsonResponse;
use Pickstar\Payment\Transaction;
use Pickstar\Events\FinalPaymentPaid;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForBookings;
use Pickstar\Http\Concerns\DeniesForPayments;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Http\Requests\Api\Booking\ProcessBookingPaymentRequest;
use Pickstar\Services\PinService;

class ProcessPaymentController extends Controller
{
    use DeniesForPayments,
        DeniesForBookings,
        DeniesHttpRequests;

    /**
     * Process a payment for a booking.
     *
     * @param \Pickstar\Http\Requests\Api\Booking\ProcessBookingPaymentRequest $request
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(
        ProcessBookingPaymentRequest $request,
        Booking $booking
    ): JsonResponse {
        // Prepare the payments by first fetching them and then filtering based on whether or not
        // they are unpaid. Do not let them pay for an already paid payment again.
        $payments = collect($request->input('payments', []))
            ->map(function ($id) use ($booking) {
                return $booking->payments->find($id);
            })->filter(function ($payment) {
                return $payment->status === Payment::STATUS_SENT ||
                    $payment->status === Payment::STATUS_UNPAID;
            });

        $token_id = $request->post('token_id');
        if($token_id !== null) {
            /* @var PinService $service */
            try {
                $service = app()->make(PinService::class);
                $transactionTotal = $service->getTransactionAmount($token_id);
            } catch (\Exception $e) {
                report($e);
                //api call failed
                $transactionTotal  = gst($payments->sum('amount'));
                $transactionTotal += Payment::estimateFees($transactionTotal);
            }
        } else {
            $transactionTotal  = $payments->sum('amount');
        }

        $this->denyIfNoPaymentsToProcess($payments);

        $this->denyIfProcessingFinalPaymentBeforeDeposit($payments, $booking);

        // If the user is the client we want to deny only them from making a payment before the terms
        // have been accepted. We'll let this slide for admins though.
        if ($request->user()->isClient()) {
            $this->denyIfBookingTermsNotAccepted($booking);
        }

        $transaction = Transaction::create([
            'payment_transaction_id' => $token_id,
            'amount' => $transactionTotal,
            'method' => Transaction::METHOD_CREDIT_CARD,
        ]);

        // With the transaction created we can now associate it with all the payments that this transaction
        // has covered. Each payment gets its status updated to "paid" and the paid date set.
        $payments->each(function ($payment) use ($transaction, $booking) {
            static $paidDate;

            $payment->transaction()->associate($transaction);

            $payment->fill([
                'status' => Payment::STATUS_PAID,
                'paid_date' => $paidDate ?? $paidDate = now()
            ])->save();

            // Fire the "DepositPaid" event if the payment is a deposit.
            if ($payment->type === Payment::TYPE_DEPOSIT) {
                event(new DepositPaid($booking));
                event(new CreateBookingThreads($booking));
                $booking->notification->fill(['messaging_open' => true])->save();
            }

            if ($payment->type === Payment::TYPE_FINAL) {
                event(new FinalPaymentPaid($booking));
            }
        });

        return response()->json($booking->payments()->with('transaction')->get());
    }
}

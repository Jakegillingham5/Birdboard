<?php

namespace Pickstar\Http\Controllers\Api\Payment;

use Pickstar\Booking\Booking;
use Pickstar\Events\CreateBookingThreads;
use Pickstar\Payment\Payment;
use Pickstar\Events\DepositPaid;
use Illuminate\Http\JsonResponse;
use Pickstar\Payment\Transaction;
use Pickstar\Events\FinalPaymentPaid;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForPayments;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Events\BankTransferPaymentReceived;
use Pickstar\Http\Requests\Api\Booking\UpdatePaymentReceivedRequest;

class PaymentReceivedController extends Controller
{
    use DeniesForPayments,
        DeniesHttpRequests;

    /**
     * Mark payments as being received for a given booking.
     *
     * @param \Pickstar\Http\Requests\Api\Booking\UpdatePaymentReceivedRequest $request
     * @param \Pickstar\Booking\Bookyeping $booking
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdatePaymentReceivedRequest $request, Booking $booking): JsonResponse
    {
        // Prepare the payments by first fetching them and then filtering based on whether or not
        // they are unpaid. Do not let them pay for an already paid payment again.
        $payments = collect($request->input('payments', []))
            ->map(function ($id) use ($booking) {
                return $booking->payments->find($id);
            })->filter(function ($payment) {
                return $payment->status === Payment::STATUS_SENT ||
                    $payment->status === Payment::STATUS_UNPAID;
            });

        $this->denyIfNoPaymentsToProcess($payments);

        $this->denyIfProcessingFinalPaymentBeforeDeposit($payments, $booking);

        $transaction = Transaction::create([
            'amount' => gst($payments->sum('amount')),
            'method' => $request->input('payment_method'),
        ]);

        $payments->each(function ($payment) use ($transaction, $booking, $request) {
            static $paidDate;

            $payment->transaction()->associate($transaction);

            $payment->fill([
                'status' => Payment::STATUS_PAID,
                'paid_date' => $paidDate ?? $paidDate = now(),
                'method' => $request->input('payment_method'),
            ])->save();

            // Fire the "BankTransferPaymentReceived" event and, if the payment is a deposit, we'll also fire
            // the "DepositPaid" event. Both trigger alerts to different people.
            event(new BankTransferPaymentReceived($payment));

            if ($payment->type === Payment::TYPE_DEPOSIT) {
                event(new DepositPaid($booking));
                event(new CreateBookingThreads($booking));
                $booking->notification->fill(['messaging_open' => true])->save();
            }

            if ($payment->type === Payment::TYPE_FINAL) {
                event(new FinalPaymentPaid($booking));
            }
        });

        return response()->json($booking->payments()->with('transaction')->get());
    }
}

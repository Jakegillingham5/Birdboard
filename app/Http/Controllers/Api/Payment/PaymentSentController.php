<?php

namespace Pickstar\Http\Controllers\Api\Payment;

use Pickstar\Booking\Booking;
use Pickstar\Payment\Payment;
use Illuminate\Http\JsonResponse;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForPayments;
use Pickstar\Http\Concerns\DeniesForBookings;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Http\Requests\Api\Booking\UpdatePaymentSentRequest;

class PaymentSentController extends Controller
{
    use DeniesForPayments,
        DeniesForBookings,
        DeniesHttpRequests;

    /**
     * Mark payments as being sent for a given booking.
     *
     * @param \Pickstar\Http\Requests\Api\Booking\UpdatePaymentSentRequest $request
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdatePaymentSentRequest $request, Booking $booking): JsonResponse
    {
        // Prepare the payments by first fetching them and then filtering based on whether or not
        // they are unpaid. Do not let them pay for an already paid payment again.
        $payments   = collect($request->input('payments', []))
            ->map(function ($id) use ($booking) {
                return $booking->payments->find($id);
            })->filter(function ($payment) {
                return $payment->status === Payment::STATUS_UNPAID;
            });

        $this->denyIfNoPaymentsToProcess($payments);

        $this->denyIfBookingTermsNotAccepted($booking);

        $this->denyIfSendingFinalPaymentBeforeDeposit($payments, $booking);

        $payments->each(function ($payment) use ($request) {
            static $sentDate;

            $payment->fill([
                'status' => Payment::STATUS_SENT,
                'sent_date' => $sentDate ?? $sentDate = now(),
                'method' => $request->input('payment_method')
            ])->save();
        });

        return response()->json($booking->payments()->get());
    }
}

<?php

namespace Pickstar\Http\Controllers\Api\Payment;

use Carbon\Carbon;
use Illuminate\Http\Request;
use InvalidArgumentException;
use Pickstar\Booking\Booking;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Payment\Payment;
use Pickstar\User\User;

class PaymentController extends Controller
{
    /**
     * Get all payments that meet the given criteria
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Support\Collection
     */
    public function index(Request $request)
    {
        $this->authorize('list', Payment::class);

        $payments = Payment::with('booking.opportunity', 'booking.onlineOpportunity','transaction')
            ->whereHas('booking', function ($booking) {
                $booking->where('status', '<>', Booking::STATUS_CANCELLED);
            });

        // If user is a client only return their payments.
        if ($request->user()->isClient()) {
            $payments->where('user_id', $request->user()->id);

        // If user is an admin and we have a client ID then show the payments for that client.
        } elseif ($request->user()->isAdmin() && $request->filled('clientId')) {
            $payments->where('user_id', $request->get('clientId'));
        }

        if ($request->filled('amount_low')) {
            $payments->where('amount', '>=', ($request->get('amount_low') - Payment::TAX_RATE * $request->get('amount_low')));
        }

        if ($request->filled('amount_high')) {
            $payments->where('amount', '<=', $request->get('amount_high') - Payment::TAX_RATE * $request->get('amount_high'));
        }

        if ($request->filled('date_low')) {
            try {
                $payments->where('due_date', '>=', Carbon::createFromFormat('Y-m-d', $request->get('date_low'))->startOfDay());
            } catch (InvalidArgumentException $exception) {
                //
            }
        }

        if ($request->filled('date_high')) {
            try {
                $payments->where('due_date', '<=', Carbon::createFromFormat('Y-m-d', $request->get('date_high'))->endOfDay());
            } catch (InvalidArgumentException $exception) {
                //
            }
        }

        if ($request->filled('status')) {
            $statuses = (array) $request->get('status');

            $payments->where(function ($query) use ($statuses) {
                $query->whereIn('status', $statuses);

                // If filtering by overdue we need to include those that are past the due date as we don't
                // actually stamp the overdue status.
                if (in_array('overdue', $statuses)) {
                    $query->orWhere(function ($payment) {
                        $payment->where('status', '<>', Payment::STATUS_PAID)
                            ->where('due_date', '<=', now()->endOfDay());
                    });
                }
            });
        }

        if ($request->filled('locations')) {
            $payments->whereHas('booking', function ($query) use ($request) {
                $query->whereIn('state', (array) $request->get('locations'));
            });
        }

        if ($request->filled('opportunities')) {
            $payments->whereHas('booking', function ($query) use ($request) {
                $query->whereIn('opportunity_id', (array) $request->get('opportunities'))
                    ->orWhereIn('online_opportunity_id', (array) $request->get('opportunities'));
            });
        }

        $payments = $payments->orderBy('due_date', 'asc')->paginate(25);

        return response()->json($payments);
    }
}

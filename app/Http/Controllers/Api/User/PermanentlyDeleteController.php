<?php

namespace Pickstar\Http\Controllers\Api\User;

use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Pickstar\Booking\TalentNotificationPivot;
use Pickstar\Http\Concerns\DeniesForUsers;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Http\Controllers\Controller;
use Pickstar\User\User;

class PermanentlyDeleteController extends Controller
{
    use DeniesForUsers,
        DeniesHttpRequests;

    /**
     * Permanently delete a user.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, User $user): JsonResponse
    {
        $this->authorize('permanentlyDelete', $user);

        $this->denyIfAdminDeletingThemselves($request->user(), $user);

        $this->denyIfUserHasPendingBookings($user);

        if ($user->isTalent()) {
            $user->profile()->delete();
            $user->endorsements()->delete();
            $user->locations()->detach();
            $user->opportunities()->detach();
            $user->tags()->detach();
            $user->paymentDetails()->delete();
            // http://support.codium.com.au/a/tickets/8271
            TalentNotificationPivot::where('talent_id', $user->id)->delete();
        }

        try {
            $user->forceDelete();
        } catch (QueryException $e) {
            \Log::error($e);

            $message = 'Unable to permanently delete this account.';

            if ($user->isManager()) {
                $message .= ' Please unassociate talent from this manager.';
            }

            return response()->json([
                'message' => $message
            ], 400);
        }

        return response()->json('', 204);
    }
}

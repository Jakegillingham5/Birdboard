<?php

namespace Pickstar\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Pickstar\Http\Controllers\Controller;

class PushNotificationController extends Controller
{
    /**
     * Enable/disable push notifications for authenticated user.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request): JsonResponse
    {
        $request->user()
            ->fill(['push_notifications' => $request->input('enabled', $request->user()->push_notifications)])
            ->save();

        return response()->json($request->user());
    }
}

<?php

namespace Pickstar\Http\Controllers\Api\User;

use Pickstar\User\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForUsers;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Notifications\System\EmailVerification;

class VerificationController extends Controller
{
    use DeniesForUsers,
        DeniesHttpRequests;

    /**
     * Manually verify a users email.
     *
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(User $user)
    {
        $this->authorize('verify', $user);

        $this->denyIfUserAlreadyVerified($user);

        $user->fill([
            'email_verified' => true,
            'email_verification_token' => null
        ])->save();

        return response()->json($user);
    }

    /**
     * Trigger a resend of the verification email manually.
     *
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function resend(User $user)
    {
        $this->authorize('resendVerification', $user);

        $this->denyIfUserAlreadyVerified($user);

        $user->fill([
            'email_verified' => false,
            'email_verification_token' => Str::random(32)
        ])->save();

        $user->notify(new EmailVerification);

        return response()->json($user);
    }
}

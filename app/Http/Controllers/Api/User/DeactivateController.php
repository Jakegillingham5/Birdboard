<?php

namespace Pickstar\Http\Controllers\Api\User;

use Pickstar\User\User;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForUsers;
use Pickstar\Http\Concerns\DeniesHttpRequests;

class DeactivateController extends Controller
{
    use DeniesForUsers,
        DeniesHttpRequests;

    /**
     * Deactivate a user.
     *
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(User $user)
    {
        $this->authorize('deactivate', $user);

        $this->denyIfUserAlreadyDeactivated($user);

        $this->denyIfUserIsNotStaffManager($user);

        $user->fill(['status' => User::STATUS_DEACTIVATED])->save();

        return response()->json($user);
    }
}

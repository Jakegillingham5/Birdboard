<?php

namespace Pickstar\Http\Controllers\Api\User;

use Pickstar\User\User;
use Illuminate\Support\Facades\Password;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Requests\Api\ForgotPasswordRequest;
use Pickstar\Http\Requests\Api\UpdateUserPasswordRequest;

class PasswordResetController extends Controller
{
    public function store(ForgotPasswordRequest $request, User $user)
    {
        $response = Password::broker()->sendResetLink($request->only('email'));

        return response()->json(['success' => 'success']);
    }
}

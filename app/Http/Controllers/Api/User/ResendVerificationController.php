<?php

namespace Pickstar\Http\Controllers\Api\User;

use Pickstar\User\User;
use Illuminate\Support\Str;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForUsers;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Notifications\System\EmailVerification;

class ResendVerificationController extends Controller
{
    use DeniesForUsers,
        DeniesHttpRequests;

    /**
     * Trigger a resend of the verification email manually.
     *
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(User $user)
    {
        $this->authorize('resendVerification', $user);

        $this->denyIfUserAlreadyVerified($user);

        $user->fill([
            'email_verified' => false,
            'email_verification_token' => Str::random(32)
        ])->save();

        $user->notify(new EmailVerification);

        return response()->json($user);
    }
}

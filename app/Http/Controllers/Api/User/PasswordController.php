<?php

namespace Pickstar\Http\Controllers\Api\User;

use Pickstar\User\User;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Requests\Api\UpdateUserPasswordRequest;

class PasswordController extends Controller
{
    public function update(UpdateUserPasswordRequest $request, User $user)
    {
        $user->fill(['password' => $request->get('password')])->save();

        return response()->json($user);
    }
}

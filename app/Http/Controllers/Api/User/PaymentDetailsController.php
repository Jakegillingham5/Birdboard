<?php

namespace Pickstar\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use Pickstar\User\User;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Events\PaymentDetailsUpdated;
use Pickstar\Http\Concerns\DeniesForUsers;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Pickstar\Http\Requests\Api\Talent\UpdatePaymentDetailsRequest;

class PaymentDetailsController extends Controller
{
    use DeniesForUsers,
        DeniesHttpRequests;

    /**
     * Show a users payment details.
     *
     * @param Request
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, User $user)
    {
        $this->authorize('manage', $user);

        $this->denyIfUserIsNotRole($user, [User::ROLE_TALENT, User::ROLE_MANAGER]);

        if($request->user()->isAdmin()){
            $paymentDetails = $user->getBestPaymentDetails();
        } else {
            $paymentDetails = $user->paymentDetails;
        }

        if($paymentDetails === null) {
            $this->deny('The user does not have any payment details.');
        }

        return response()->json($paymentDetails);
    }

    /**
     * Store or update payment details for a user.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(UpdatePaymentDetailsRequest $request, User $user)
    {
        $this->denyIfUserIsNotRole($user, [User::ROLE_TALENT, User::ROLE_MANAGER]);

        $paymentDetails = $user->paymentDetails()->updateOrCreate([], $request->only([
            'bsb_number',
            'account_name',
            'account_number',
            'abn',
            'gst_registered'
        ]));

        if ($paymentDetails->wasChanged()) {
            event(new PaymentDetailsUpdated($user));
        }

        return response()->json($user->paymentDetails);
    }
}

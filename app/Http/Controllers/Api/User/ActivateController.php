<?php

namespace Pickstar\Http\Controllers\Api\User;

use Pickstar\User\User;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForUsers;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Notifications;

class ActivateController extends Controller
{
    use DeniesForUsers,
        DeniesHttpRequests;

    /**
     * Activate a user.
     *
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(User $user)
    {
        $this->authorize('activate', $user);

        $this->denyIfUserAlreadyActivated($user);

        $user->fill(['status' => User::STATUS_ACTIVE])->save();

        // If user is a talent then we'll notify them that their account was activated.
        if ($user->isTalent()) {
            $user->notify(new Notifications\Talent\AccountActivated);
        }
        if ($user->isManager()) {
            $user->notify(new Notifications\Manager\AccountActivated);
        }

        return response()->json($user);
    }
}

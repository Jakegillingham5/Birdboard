<?php

namespace Pickstar\Http\Controllers\Api\User;

use Pickstar\User\User;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForUsers;
use Pickstar\Http\Concerns\DeniesHttpRequests;

class RestoreController extends Controller
{
    use DeniesForUsers,
        DeniesHttpRequests;

    /**
     * Restore a deleted user account.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(User $user)
    {
        $this->authorize('restore', $user);

        $this->denyIfUserAlreadyRestored($user);

        $user->restore();

        return response()->json($user);
    }
}

<?php

namespace Pickstar\Http\Controllers\Api\User;

use Pickstar\User\User;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Requests\Api\UpdateProfileImageRequest;

class ProfileImageController extends Controller
{
    /**
     * Update a talent profile fields.
     *
     * @param \Pickstar\Http\Requests\Api\UpdateProfileRequest $request
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateProfileImageRequest $request, User $user)
    {
        $user->handleProfileImage($request->file('profile_image'), $request->get('profile_image_crop'));

        return response()->json($user);
    }
}

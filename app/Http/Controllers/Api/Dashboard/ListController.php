<?php

namespace Pickstar\Http\Controllers\Api\Dashboard;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Services\DashboardListService;

class ListController extends Controller
{
    /**
     * Get the dashboard list results based on the request user.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, DashboardListService $dashboardListService): JsonResponse
    {
        return response()->json($dashboardListService->getListsForUser($request->user()));
    }
}

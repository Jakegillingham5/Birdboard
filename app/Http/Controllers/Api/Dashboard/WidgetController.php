<?php

namespace Pickstar\Http\Controllers\Api\Dashboard;

use Pickstar\User\User;
use Illuminate\Http\Request;
use Pickstar\Booking\Booking;
use Pickstar\Payment\Payment;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Pickstar\Booking\TalentPivot;
use Pickstar\Http\Controllers\Controller;

class WidgetController extends Controller
{
    /**
     * Get the widget numbers for the dashboard.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $this->authorize('dashboardWidgets');

        // Bindings for requests widget query. Requests widget includes a count of the total number of requests
        // currently in the system. The amount is a simple sum of the all the budget per stars multiplied by
        // the required number of stars because at this point nothing has been solidified in terms of who
        // is attending so the amount gives an idea of how much potential money is in the pipeline.
        $bindings = [Booking::TYPE_REQUEST, Booking::STATUS_PENDING, Booking::STATUS_VETTING, now()];

        $requests = DB::selectOne(
            'SELECT
                COUNT(id) as `count`,
                SUM(budget_per_star * required_stars) as `total`
            FROM bookings
            WHERE type=?
                AND status IN (?, ?)
                AND (
                    `date` is null
                    OR
                    `date` > ?
                )',
            $bindings
        );

        // Bindings for bookings widget query. Bookings widget includes a count of the total number of upcoming
        // bookings plus the amount. Difference here is that the amount is based on the budget per star
        // multiplied by the total number of talent that are attending the event rather than the
        // desired number of stars, as by now these may be different.
        $bindings = [
            TalentPivot::STATUS_APPLIED,
            true,
            Booking::TYPE_BOOKING,
            Booking::STATUS_SCHEDULED,
            Booking::STATUS_BOOKED,
            Booking::STATUS_HOLD
        ];

        $bookings = DB::selectOne(
            'SELECT
                COUNT(id) as `count`,
                SUM(
                    budget_per_star *
                    (SELECT
                        COUNT(*)
                    FROM booking_talent
                    WHERE booking_talent.booking_id=bookings.id
                        AND booking_talent.status=?
                        AND booking_talent.client_accepted=?
                    )
                ) as `total`
            FROM bookings
            WHERE type=?
                AND status IN (?, ?, ?)',
            $bindings
        );

        // Payments widget includes a total count of paid payments and the total amount paid for the current
        // calendar month.
        $payments = Payment::paid()
            ->selectRaw('COUNT(id) as `count`')
            ->selectRaw('SUM(amount) as `total`')
            ->whereBetween('paid_date', [now()->startOfMonth(), now()->endOfMonth()])
            ->first();

        return response()->json([
            'booking_requests_count' => $requests->count,
            'booking_requests_total' => (float) $requests->total,
            'bookings_count' => $bookings->count,
            'bookings_total' => (float) $bookings->total,
            'payments_count' => $payments->count,
            'payments_total' => (float) $payments->total,
            'vetting_talent' => User::talent()->pendingVetting()->count(),
            'vetting_managers' => User::masterManagers()->pendingVetting()->count(),
        ]);
    }
}

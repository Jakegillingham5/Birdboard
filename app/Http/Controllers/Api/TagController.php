<?php

namespace Pickstar\Http\Controllers\Api;

use Pickstar\Tag\Tag;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Transformers\TagTransformer;
use Pickstar\Http\Requests\Api\StoreTagRequest;
use Pickstar\Http\Requests\Api\UpdateTagRequest;

class TagController extends Controller
{
    /**
     * Get all tags by type and where name matches a search term.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Pickstar\Tag\Tag
     *
     * @return \Illuminate\Support\Collection
     */
    public function index(Request $request, Tag $tags): JsonResponse
    {
        // If the request has a type and that type is a valid tag type then we'll filter
        // based on that type.
        if ($request->filled('type') && in_array($request->query('type'), $tags->availableTypes)) {
            $tags = $tags->{$request->query('type')}();
        }

        $tags = $tags->withCount('talent as occurances')
            ->where('name', 'like', "%{$request->query('term')}%");

        if ($request->get('aliases') !== null) {
            $tags = $tags->ignoreAliases();
        }

        if ($request->filled('sort')) {
            $order = strtolower($request->query('order', 'asc'));

            if (! in_array($order, ['asc', 'desc'])) {
                $order = 'asc';
            }

            switch ($request->query('sort')) {
                case 'name':
                    $tags->orderBy('name', $order);
                    break;
            }
        } else {
            $tags->latest();
        }

        $pageSize = $request->query('page_size', 50);
        $tags = $tags->paginate($pageSize);

        $tags->each(function ($tag) use ($request) {
            $tag->transform([new TagTransformer, 'transform'], [$request->user()]);
        });

        return response()->json($tags);
    }

    public function show (Request $request, Tag $tag) {
        $this->authorize('show', $tag);

        return $tag->append(['associated_sponsors', 'associated_clubs', 'associated_sports', 'associated_charities', 'parent_tags', 'talent', 'aliases']);
    }

    /**
     * Store a new tag with its name and type.
     *
     * @param \Pickstar\Http\Requests\Api\StoreTagRequest $request
     *
     * @return \Pickstar\Tag\Tag
     */
    public function store(StoreTagRequest $request): JsonResponse
    {
        $tag = Tag::create($request->only('name', 'type'));
        if ($request->get('alias') === true) {
            $tag->parent()->associate($request->get('parent_id'))->save();
        }

        return response()->json(
            Tag::withCount('talent as occurances')->find($tag->id)->transform([new TagTransformer, 'transform'], [$request->user()])
        );
    }

    /**
     * Delete a tag.
     *
     * This soft deletes the tag so it isn't permanently removed.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Pickstar\Tag\Tag $tag
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, Tag $tag): JsonResponse
    {
        $this->authorize('delete', $tag);

        $tag->delete();

        return response()->json([], 204);
    }

    /**
     * Update a tag.
     *
     * This is used to rename an existing tag.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Pickstar\Tag\Tag $tag
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateTagRequest $request, Tag $tag): JsonResponse
    {
        $tag->fill(['name' => $request->get('name')])->save();

        return response()->json($tag);
    }
}

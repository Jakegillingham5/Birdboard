<?php

namespace Pickstar\Http\Controllers\Api;

use Mail;
use Pickstar\Setting\Setting;
use Pickstar\Mail\System\TalentRequested;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Requests\Api\TalentRequestRequest;

class TalentRequestController extends Controller
{
    /**
     * Stores a given email
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(TalentRequestRequest $request)
    {
        $name = $request->get('name');
        $email = $request->get('email');
        $starName = $request->get('star_name');
        $description = $request->get('description');

        Mail::to(Setting::notificationEmail())
            ->send(new TalentRequested($name, $email, $starName, $description));

        return response()->json(500);
    }
}

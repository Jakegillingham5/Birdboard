<?php

namespace Pickstar\Http\Controllers\Api;

use Illuminate\Http\Request;
use Pickstar\Location\Location;
use Pickstar\Http\Controllers\Controller;

class LocationController extends Controller
{

    /**
     * Return only Australian states as locations.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $locations = Location::withoutAustralia()->where('name', 'like', '%'.$request->get('term').'%')->orderBy('name', 'asc')->get();

        return response()->json($locations);
    }
}

<?php

namespace Pickstar\Http\Controllers\Api;

use Mail;
use Pickstar\User\User;
use Illuminate\Http\Request;
use Pickstar\User\AdminUser;
use Pickstar\Marketing\Email;
use Pickstar\Mail\System\EmailShortlist;
use Pickstar\Events\EsportsEnquiryFan;
use Pickstar\Events\EsportsEnquiry;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Requests\Api\EsportsRequest;

class EsportsController extends Controller
{
    /**
     * Sends an email of the shortlist to another user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(EsportsRequest $request)
    {
        $request_details = [
            'email' => $request->get('email'),
            'selected_star' => $request->get('selected_star'),
            'time' => $request->get('time'),
            'description' => $request->get('description'),
            'budget' => $request->get('budget'),
            'phone' => $request->get('phone'),
            'name' => $request->get('name'),
            'path' => $request->get('path'),
            'package' => $request->get('package')
        ];

        $path = $request->get('path');
        event(new EsportsEnquiry($request_details));
        Mail::to($request->get('email'))
            ->queue(new \Pickstar\Mail\Client\EsportsEnquiry($request_details));

        return response('', 204);
    }
}

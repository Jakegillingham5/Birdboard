<?php

namespace Pickstar\Http\Controllers\Api;

use Mail;
use Pickstar\User\User;
use Illuminate\Http\Request;
use Pickstar\User\AdminUser;
use Pickstar\Marketing\Email;
use Pickstar\Mail\System\EmailShortlist;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Requests\Api\EmailShortlistRequest;

class EmailShortlistController extends Controller
{
    /**
     * Sends an email of the shortlist to another user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(EmailShortlistRequest $request)
    {
        $email = $request->get('email');

        $shortlist = User::talent()
            ->with('profile')
            ->whereIn('id', collect($request->get('shortlist'))->pluck('id'))
            ->get();

        Mail::to($email)
            ->bcc(system_setting('notification_email'))
            ->bcc(config('mail.cc_montesi'))
            ->queue(new EmailShortlist($email, $shortlist));

        return response('', 204);
    }
}

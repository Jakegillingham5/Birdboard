<?php

namespace Pickstar\Http\Controllers\Api;

use Psr\Http\Message\ServerRequestInterface;
use Pickstar\User\User;
use Nexmo\Client\Signature;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Nexmo\Message\InboundMessage;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Notifications\System\CustomNotificationResponseReceived;

class CustomNotificationController extends Controller
{
    use DeniesHttpRequests;

    /**
     * Handle sending a custom notification from the system
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        if (!$request->user()->isAdmin()) {
            $this->deny('Non-admin users cannot send custom notifications');
        }
        foreach ($request->input('users') as $user_id) {
            event(new \Pickstar\Events\CustomNotification(User::find($user_id), $request->input('data'), $request->input('channels')));
        }
        return response()->json('', 204);
    }

    public function receive(ServerRequestInterface $request) :JsonResponse
    {
        report(new \Exception('received inbound message'));
        $inbound = new InboundMessage($request);
        if($inbound->isValid()){
            $params = $inbound->getRequestData();
            report(new \Exception(json_encode($inbound->getRequestData())));
            report(new \Exception(json_encode($request->getQueryParams())));
            $signature = new Signature(
                $params,
                env('NEXMO_SIGNATURE_SECRET'),
                'md5hash'
            );
            report(new \Exception(json_encode($signature)));
            $validSig = $signature->check($params['sig']);
            report(new \Exception(json_encode($validSig)));

            if($validSig) {
                event(new CustomNotificationResponseReceived($params));
            }
        }

        return response()->json('', 204);
    }
}

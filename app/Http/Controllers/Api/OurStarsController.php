<?php

namespace Pickstar\Http\Controllers\Api;

use Pickstar\User\User;
use Illuminate\Http\Request;
use Pickstar\Talent\Profile;
use Pickstar\Services\OurStarsService;
use Pickstar\Http\Controllers\Controller;

class OurStarsController extends Controller
{
    /**
     * Get all talent that meet the given criteria
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Support\Collection
     */
    public function index(Request $request, OurStarsService $ourStarsService)
    {
        return response()->json($ourStarsService->getStars($request));
    }
}

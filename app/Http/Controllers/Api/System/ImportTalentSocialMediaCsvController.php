<?php

namespace Pickstar\Http\Controllers\Api\System;

use Exception;
use Illuminate\Http\JsonResponse;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Jobs\ImportTalentSocialMediaCsv;
use Pickstar\Http\Requests\Api\System\ImportTalentSocialMediaCsvRequest;

class ImportTalentSocialMediaCsvController extends Controller
{
    /**
     * Process a talent social media CSV import.
     *
     * @param \Pickstar\Http\Requests\Api\System\ImportTalentSocialMediaCsvRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ImportTalentSocialMediaCsvRequest $request): JsonResponse
    {
        try {
            return response()->json([
                'failed' => ImportTalentSocialMediaCsv::dispatchNow($request->file('csv'))
            ]);
        } catch (Exception $exception) {
            report($exception);

            return response()->json(['message' => 'There was a problem importing the CSV file.'], 500);
        }
    }
}

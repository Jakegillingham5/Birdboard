<?php

namespace Pickstar\Http\Controllers\Api\System;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Gate;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Jobs\ExportTalentSocialMediaCsv;

class ExportTalentSocialMediaCsvController extends Controller
{
    /**
     * Export the talent social media CSV file.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(): JsonResponse
    {
        if (Gate::denies('export-talent-social-media-csv')) {
            abort(403);
        }

        try {
            return response()->json([
                'file' => ExportTalentSocialMediaCsv::dispatchNow(),
                'name' => sprintf('Talent Social Media - %s.csv', now()->format('Y-m-d H-i-s'))
            ]);
        } catch (Exception $exception) {
            report($exception);

            return response()->json(['message' => 'There was a problem exporting the CSV file.'], 500);
        }
    }
}

<?php

namespace Pickstar\Http\Controllers\Api\Message;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Pickstar\Booking\Booking;
use Pickstar\Message\Thread;
use Pickstar\Message\Message;
use Illuminate\Http\JsonResponse;
use Pickstar\Helpers\SearchHelpers;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForUsers;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Http\Resources\Thread as ThreadResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Psy\Util\Json;

class BookingThreadController extends Controller
{
    use DeniesForUsers,
        DeniesHttpRequests;

    /**
     * Get all threads for a user
     *
     * @param Request $request
     * @param Booking $booking
     *
     * @return ResourceCollection
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index(Request $request, Booking $booking): ResourceCollection
    {
        $this->authorize('getThreads', Thread::class);

        $user = $request->user();

        $threads = $booking->threads()
            ->leftJoin('participants', 'participants.thread_id', '=', 'threads.id')
            ->leftJoin('messages', 'messages.thread_id', '=', 'threads.id')
            ->where('participants.user_id', DB::raw($user->id))
            ->selectRaw(
                'threads.*,
                 MAX(messages.created_at) AS latest_post_at,
                 count(case when `messages`.`id` > participants.message_id then 1 else null end) as unread_count')
            ->groupBy('threads.id')
            ->with(['participants' => function ($query) {
                $query->select(['user_id', 'first_name', 'last_name', 'role']);
            }])
            ->when(!$user->isAdmin(), function ($query) use ($user) {
                $query->active();
            })
            ->whereHas('participants', function ($query) use ($user) {
                $query->where('user_id', '=', $user->id);
            })
            ->orderBy('latest_post_at', 'desc')
            ->with('booking')
            ->get();

        return ThreadResource::collection($threads);
    }
}

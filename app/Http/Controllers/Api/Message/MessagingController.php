<?php

namespace Pickstar\Http\Controllers\Api\Message;

use Pickstar\User\User;
use Illuminate\Http\Request;
use Pickstar\Message\Thread;
use Pickstar\Message\Message;
use Pickstar\Events\MessageSent;
use Illuminate\Http\JsonResponse;
use Pickstar\Events\MessageSentAdmin;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForUsers;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Http\Requests\Api\SendMessageRequest;
use Pickstar\Http\Resources\Message as MessageResource;

class MessagingController extends Controller
{
    use DeniesForUsers,
        DeniesHttpRequests;

    /**
     * Send a message to all participants in a thread
     *
     * @param SendMessageRequest $request
     * @param Thread $thread
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index(SendMessageRequest $request, Thread $thread): JsonResponse
    {
        $this->authorize('sendMessage', $thread);
        $user = $request->user();

        $message = new Message([
            'text' => $request->get('text')
        ]);
        $message->user()->associate(User::find($user->id));
        $message->thread()->associate(Thread::find($thread->id));
        $message->save();

        // if the user is on the thread (they should be), set their most recently read message
        if ($thread->participants()->find($user->id)) {
            $thread->participants()->updateExistingPivot($user->id, ['message_id' => $message->id]);
        }

        foreach($thread->participants()->where('role', '!=', 'admin')->get() as $participant) {
            // don't broadcast the event to the user that sent the message
            if ($user->id !== $participant->id) {
                event(new MessageSent($user, $participant, $message, $thread));
            }
        }

        event(new MessageSentAdmin($user, $message, $thread));

        return response()->json(new MessageResource($message), 200);
    }

    /**
     * Soft delete a message
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Request $request): \Illuminate\Http\Response
    {
        $this->authorize('deleteMessage', Thread::class);

        $thread = Thread::find($request->get('thread_id'));
        $thread->messages()->find($request->get('message_id'))->delete();
        return response(new MessageResource($thread->messages()->withTrashed()->find($request->get('message_id'))), 200);
    }

    /**
     * Restore a deleted message
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function restore(Request $request): \Illuminate\Http\Response
    {
        $this->authorize('restoreMessage', Thread::class);

        $thread = Thread::find($request->get('thread_id'));
        $thread->messages()->withTrashed()->find($request->get('message_id'))->restore();
        return response(new MessageResource($thread->messages()->find($request->get('message_id'))), 200);
    }

    /**
     * Set the last read message for a user in a thread
     *
     * @param Request $request
     * @param \Pickstar\Message\Thread $thread
     *
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function read(Request $request, Thread $thread): JsonResponse
    {
        $this->authorize('readMessages', $thread);

        $user = $request->user();
        $unread_count = $thread->userUnreadCount($user->id);

        if ($thread->messages()->count()) {
            if ($thread->participants()->find($request->user()->id)) {
                $thread->participants()->updateExistingPivot($user->id, ['message_id' => $thread->messages()->latest()->first()->id]);
            }
        }
        return response()->json($unread_count, 200);
    }
}

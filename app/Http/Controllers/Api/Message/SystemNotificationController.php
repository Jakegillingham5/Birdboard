<?php

namespace Pickstar\Http\Controllers\Api\Message;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForUsers;
use Pickstar\Message\SystemNotification;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Pickstar\Http\Resources\SystemNotification as SystemNotificationResource;

class SystemNotificationController extends Controller
{
    use DeniesForUsers,
        DeniesHttpRequests;

    /**
     * Get a users notifications
     *
     * @param Request $request
     *
     * @return ResourceCollection
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index(Request $request): ResourceCollection
    {
        $this->authorize('getSystemNotifications', SystemNotification::class);

        $notifications = $request->user()->notifications()->latest()->paginate(25);
        return SystemNotificationResource::collection($notifications);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function unreadCount(Request $request): JsonResponse
    {
        $this->authorize('getSystemNotifications', SystemNotification::class);

        return response()->json($request->user()->unreadNotifications()->count(), 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function read(Request $request): \Illuminate\Http\Response
    {
        $this->authorize('readSystemNotifications', SystemNotification::class);

        $user = $request->user();
        $user->unreadNotifications->markAsRead();
        return response('', 204);
    }
}

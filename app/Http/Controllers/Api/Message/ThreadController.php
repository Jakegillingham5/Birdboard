<?php

namespace Pickstar\Http\Controllers\Api\Message;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Pickstar\Message\Thread;
use Pickstar\Message\Message;
use Illuminate\Http\JsonResponse;
use Pickstar\Helpers\SearchHelpers;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForUsers;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Http\Resources\Thread as ThreadResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Psy\Util\Json;

class ThreadController extends Controller
{
    use DeniesForUsers,
        DeniesHttpRequests;

    /**
     * Get a thread
     *
     * @param Request $request
     * @param Thread $thread
     * @return ThreadResource
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Request $request, Thread $thread): ThreadResource
    {
        $this->authorize('getThread', $thread);

        $user = $request->user();
        if ($user->isAdmin()) {
            $thread->load(['messages' => function ($query) {
                $query->withTrashed();
            }]);
        } else {
            $thread->load('messages');
        }
        $thread->load('booking', 'clients', 'talents', 'participants');
        return new ThreadResource($thread);
    }

    /**
     * Get all threads for a user
     *
     * @param Request $request
     *
     * @return ResourceCollection
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index(Request $request): ResourceCollection
    {
        $this->authorize('getThreads', Thread::class);

        $user = $request->user();

        $threads = Thread::leftJoin('messages', 'messages.thread_id', '=', 'threads.id')
            ->leftJoin('bookings', 'bookings.id', '=', 'threads.booking_id')
            ->leftJoin('participants', 'participants.thread_id', '=', 'threads.id')
            ->where('participants.user_id', DB::raw($user->id))
            ->selectRaw(
                'threads.*,
                 MAX(messages.created_at) AS latest_post_at,
                 count(case when `messages`.`id` > participants.message_id then 1 else null end) as unread_count')
            ->groupBy('threads.id')
            ->with(['participants' => function ($query) {
                $query->select(['user_id', 'first_name', 'last_name', 'role', 'profile_image_hash']);
            }])
            ->when($request->query('keywords'), function ($query, $keywords) {
                $query->leftJoin('users', 'participants.user_id', '=', 'users.id');
                $sanitizedString = SearchHelpers::sanitizeSearchString(trim($keywords));
                $query->where('bookings.name', 'like', '%' . $sanitizedString . '%');
                $query->orWhere('bookings.id', '=', $sanitizedString);
                $query->orWhere(\DB::raw('concat(users.first_name, " ", users.last_name)'), 'like', '%' . $sanitizedString . '%');
            })
            ->when($request->query('date_from'), function ($query) use ($request) {
                $query->where('bookings.date', '>=', Carbon::createFromFormat('Y-m-d', $request->query('date_from'))->startOfDay());
            })
            ->when($request->query('date_to'), function ($query) use ($request) {
                $query->where('bookings.date', '<=', Carbon::createFromFormat('Y-m-d', $request->query('date_to'))->endOfDay());
            })
            ->when($request->query('id'), function ($query) use ($request) {
                $query->where('bookings.id', '=', $request->query('id'));
            })
            ->when($request->query('activated'), function ($query) use ($request, $user) {
                $query->whereHas('participants', function ($query) use ($user, $request) {
                    $query->where('user_id', '=', $user->id);
                    $query->where('activated', (boolean) $request->query('activated'));
                });
                $query->orWhereHas('messages');
            })
            ->when(!$user->isAdmin(), function ($query) {
                $query->active();
            })
            ->when($request->query('status') === 'active', function ($query) {
                $query->active();
            })
            ->when($request->query('status') === 'archived', function ($query) use ($user) {
                if ($user->isAdmin()) {
                    $query->inactive();
                } else {
                    $query->active();
                }
            })
            ->when($request->query('status') === 'pinned', function ($query) use ($user) {
                if ($user->isAdmin()) {
                    $query->whereHas('participants', function ($query) use ($user) {
                        $query->where('user_id', '=', $user->id);
                        $query->where('pinned', true);
                    });
                } else {
                    $query->active();
                }
            })
            ->whereHas('participants', function ($query) use ($user) {
                $query->where('user_id', '=', $user->id);
            })
            ->orderBy($request->query('sort'), $request->query('order'))
            ->orderBy('id', 'asc')
            ->with('booking')
            ->paginate(10);

        return ThreadResource::collection($threads);
    }

    /**
     * Get the unread count for a user
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function unreadCount(Request $request) {
        $this->authorize('getThreads', Thread::class);

        $user = $request->user();

        $unread_count = Message::leftJoin('threads', 'threads.id', '=', 'messages.thread_id')
                                ->leftJoin('participants', 'threads.id', '=', 'participants.thread_id')
                                ->where('participants.user_id', '=', $user->id)
                                ->where('messages.id', '>', \DB::raw('participants.message_id'))
                                ->where('threads.active', true)
                                ->count();

        return response()->json($unread_count, 200);
    }

    /**
     * @param Request $request
     * @param Thread $thread
     * @return JsonResponse
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function close(Request $request, Thread $thread): JsonResponse
    {
        $this->authorize('closeThread', Thread::class);

        $thread->fill(['active' => false])->save();
        return response()->json([], 204);
    }

    /**
     * @param Request $request
     * @param Thread $thread
     * @return JsonResponse
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function reopen(Request $request, Thread $thread): JsonResponse
    {
        $this->authorize('reopenThread', Thread::class);

        $thread->fill(['active' => true])->save();
        return response()->json([], 204);
    }

    /**
     * Pin a thread
     *
     * @param Request $request
     * @param Thread $thread
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function pin(Request $request, Thread $thread): JsonResponse
    {
        $this->authorize('pinThread', Thread::class);

        $thread->participants()->updateExistingPivot($request->user()->id, [
            'pinned' => true
        ]);

        return response()->json([], 204);
    }

    /**
     * Unpin a thread
     *
     * @param Request $request
     * @param Thread $thread
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function unpin(Request $request, Thread $thread): JsonResponse
    {
        $this->authorize('unpinThread', Thread::class);

        $thread->participants()->updateExistingPivot($request->user()->id, [
            'pinned' => false
        ]);

        return response()->json([], 204);
    }

    /**
     * Activate a thread for a user
     *
     * @param Request $request
     * @param Thread $thread
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function activate(Request $request, Thread $thread): JsonResponse
    {
        $this->authorize('activateThread', $thread);

        $thread->participants()->updateExistingPivot($request->user()->id, [
            'activated' => true
        ]);

        return response()->json(new \Pickstar\Http\Resources\Thread($thread->load('booking')), 200);
    }

    /**
     * Deactivate a thread for a user
     *
     * @param Request $request
     * @param Thread $thread
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function deactivate(Request $request, Thread $thread): JsonResponse
    {
        $this->authorize('deactivateThread', $thread);

        $thread->participants()->updateExistingPivot($request->user()->id, [
            'activated' => false
        ]);

        return response()->json(new \Pickstar\Http\Resources\Thread($thread->load('booking')), 200);
    }
}

<?php

namespace Pickstar\Http\Controllers\Api\Message;

use Pickstar\Message\Thread;
use Illuminate\Support\Facades\Request;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForBookings;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Http\Resources\Thread as ThreadResource;

class AcceptTermsController extends Controller
{
    use DeniesForBookings,
        DeniesHttpRequests;

    /**
     * Update a bookings terms to be accepted if they are not already.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Pickstar\Message\Thread $thread
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(\Illuminate\Http\Request $request, Thread $thread)
    {
        $user = $request->user();

        $thread->participants()->updateExistingPivot($user->id, [
            'terms_accepted' => true
        ]);

        if ($user->isAdmin()) {
            $thread->load(['messages' => function ($query) {
                $query->withTrashed();
            }]);
        } else {
            $thread->load('messages');
        }
        $thread->load('booking', 'clients', 'talents', 'participants');

        return new ThreadResource($thread);
    }
}

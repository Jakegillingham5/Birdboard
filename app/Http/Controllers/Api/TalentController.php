<?php

namespace Pickstar\Http\Controllers\Api;

use Pickstar\User\User;
use Pickstar\Notifications;
use Illuminate\Http\Request;
use Pickstar\User\AdminUser;
use Pickstar\Booking\Booking;
use Illuminate\Http\JsonResponse;
use Pickstar\Booking\TalentPivot;
use Pickstar\Helpers\SearchHelpers;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Transformers\UserTransformer;
use Pickstar\Notifications\System\EmailVerification;
use Pickstar\Http\Requests\Api\Talent\StoreTalentRequest;

class TalentController extends Controller
{
    /**
     * Get all talent that meet the given criteria.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $this->authorize('listTalent', User::class);

        $user = $request->user();

        $talent = User::talent()
            ->with([
                'profile',
                'tags',
                'locations',
                'paymentDetails',
                'opportunities',
                'management',
            ])
            ->withCount([
                'talentBookings AS uncompleted_bookings' => function ($query) {
                    $query->whereIn('bookings.status', [Booking::STATUS_SCHEDULED, Booking::STATUS_BOOKED])
                        ->where('booking_talent.client_accepted', true)
                        ->where('booking_talent.status', TalentPivot::STATUS_APPLIED);
                },
                'talentBookingRequests AS requests_pending_response' => function ($query) {
                    $query->isNotExpired()
                        ->whereIn('booking_talent.status', [TalentPivot::STATUS_PENDING, TalentPivot::STATUS_APPLIED])
                        ->where('bookings.status', Booking::STATUS_PENDING);
                }
            ])
            ->when($user->isAdmin(), function ($query) use ($request) {
                $query->when($request->query('status'), function ($query, $status) {
                    if ($status === 'unverified') {
                        $query->where('status', User::STATUS_PENDING);
                    } elseif ($status === 'featured') {
                        $query->featured();
                    } elseif ($status === 'trash') {
                        $query->onlyTrashed();
                    }
                });

                $query->when($request->query('managerId'), function ($query, $management) {
                    $query->where('management_id', $management);
                });
            })
            ->when($request->query('status') === 'active', function ($query) {
                $query->where('status', User::STATUS_ACTIVE);
            })
            ->when($request->query('name'), function ($query, $name) {
                $sanitizedString = SearchHelpers::sanitizeSearchString($name);
                $query->where(\DB::raw('concat(first_name, " ", last_name)'), 'like', '%' . $sanitizedString . '%');
                $query->orWhere('phone', 'like', '%' . $sanitizedString . '%');
            })
            ->when($user->isManager(), function ($query) use ($request, $user) {
                $query->managedBy($user);
                if ($request->query('booking_id')) {
                    $booking = Booking::everything()->find($request->query('booking_id'));
                    if ($booking && $booking->tags->count()) {
                        $query->whereHas('tags', function ($query) use ($booking) {
                            $query->whereIn('id', $booking->tags->pluck('id'));
                        });
                    }
                }
            })
            ->orderBy('first_name', 'asc')
            ->orderBy('last_name', 'asc')
            ->paginate(25);

        $talent->each(function ($talent) use ($user) {
            $talent->transform([new UserTransformer, 'transform'], [$user]);
        });

        return response()->json($talent);
    }

    /**
     * Create a new talent user.
     *
     * @param \Pickstar\Http\Requests\Api\Talent\StoreTalentRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreTalentRequest $request): JsonResponse
    {
        $defaultAttributes = [
            'role' => User::ROLE_TALENT,
            'status' => $request->user()->isAdmin()
                ? User::STATUS_ACTIVE
                : User::STATUS_PENDING,
            'email_verified' => true
        ];

        $talent = new User($defaultAttributes + $request->only([
            'first_name',
            'last_name',
            'email',
            'phone'
        ]));

        if ($request->user()->isManager()) {
            $talent->management()->associate($request->user());
        }

        $talent->save();

        $talent->profile()->create([
            'gender' => $request->input('profile.gender'),
            'notification_preference' => $request->user()->isManager()
                ? true
                : false
        ]);

        if ($request->user()->isManager()) {
            (new AdminUser)->notify(new Notifications\System\TalentRegistered($talent));
        }

        return response()->json($talent, 201);
    }
}

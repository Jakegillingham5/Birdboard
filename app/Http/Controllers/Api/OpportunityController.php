<?php

namespace Pickstar\Http\Controllers\Api;

use Composer\Package\Package;
use Illuminate\Http\Request;
use Pickstar\Opportunity\Opportunity;
use Pickstar\Http\Controllers\Controller;

class OpportunityController extends Controller
{
    /**
     * Return opportunities.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $opportunities = Opportunity::select(['name', 'type', 'id']);

        if ($request->has('level')) {
            if ($request->query('level') === 'parent') {
                $opportunities->onlyParents();
            } else if ($request->query('level') === 'children') {
                $opportunities->onlyChildren();
            }

            if ($request->filled('parent_ids')) {
                $opportunities = $opportunities->whereHas('parents', function ($query) use ($request) {
                    $query->whereIn('opportunity_relations.parent_opportunity_id', $request->query('parent_ids'));
                });
            }
        } else {
            $opportunities = $opportunities->onlyParents()
                ->with(['children' => function ($query) {
                    $query->select(['opportunities.name', 'opportunities.id']);
                }]);
        }

        $opportunities->where('name', 'like', '%'.$request->get('term').'%');

        if ($request->filled('type')) {
            $opportunities->where('type', $request->get('type'));
        }

        return response()->json($opportunities->orderBy('name')->orderBy('id')->get());
    }
}

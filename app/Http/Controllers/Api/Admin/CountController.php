<?php

namespace Pickstar\Http\Controllers\Api\Admin;

use Pickstar\User\User;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Pickstar\Http\Controllers\Controller;

class CountController extends Controller
{
    /**
     * Get the count of admins for different criteria.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $this->authorize('index', User::class);

        $admins = User::admin()
                    ->withTrashed();

        $all = (clone $admins)->count();
        $active = (clone $admins)->notDeleted()->count();
        $trashed = (clone $admins)->deleted()->count();

        return response()->json([
            'all' => $all,
            'active' => $active,
            'trashed' => $trashed
        ]);
    }
}

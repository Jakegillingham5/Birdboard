<?php

namespace Pickstar\Http\Controllers\Api\Client;

use Pickstar\User\User;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Pickstar\Http\Controllers\Controller;

class CountController extends Controller
{
    /**
     * Get count of clients.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $this->authorize('index', User::class);

        $all = User::onlyClients()->count();
        $active = User::onlyClients()->where('email_verified', true)->count();
        $unverified = User::onlyClients()->where('email_verified', false)->count();

        return response()->json(compact('all', 'active', 'unverified'));
    }
}

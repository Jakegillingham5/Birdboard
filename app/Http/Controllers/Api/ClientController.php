<?php

namespace Pickstar\Http\Controllers\Api;

use Pickstar\User\User;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Pickstar\Helpers\SearchHelpers;
use Pickstar\Http\Controllers\Controller;

class ClientController extends Controller
{
    /**
     * Get all clients that meet the given criteria.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $this->authorize('index', User::class);

        $clients = User::onlyClients();

        if ($request->filled('status')) {
            $status = $request->get('status');

            if ($status === 'active') {
                $clients->where('email_verified', true);
            } elseif ($status === 'unverified') {
                $clients->where('email_verified', false);
            } elseif ($status === 'trash') {
                $clients->onlyTrashed();
            }
        }

        if ($request->filled('name')) {
            $sanitizedString = SearchHelpers::sanitizeSearchString($request->query('name'));
            $clients->where(\DB::raw('concat(first_name, " ", last_name)'), 'like', '%' . $sanitizedString . '%');
            $clients->orWhere('phone', 'like', '%' . $sanitizedString . '%');
        }

        $clients = $clients->orderBy('first_name', 'asc')
            ->orderBy('last_name', 'asc')
            ->orderBy('id', 'desc')
            ->paginate(25);

        return response()->json($clients);
    }

    /**
     * Get an single client.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, User $user): JsonResponse
    {
        $this->authorize('manage', $user);

        return response()->json($user);
    }
}

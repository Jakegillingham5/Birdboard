<?php

namespace Pickstar\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Pickstar\Booking\Booking;
use Illuminate\Http\JsonResponse;
use Pickstar\Booking\TalentPivot;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Transformers\BookingTransformer;
use Pickstar\Http\Resources\Booking as BookingResource;

class CalendarController extends Controller
{
    public function index(Request $request)
    {
        $this->authorize('list', Booking::class);

        $user = $request->user();

        $bookings = Booking::select('bookings.*')
            ->withoutVetting()
            ->inPerson()
            ->with([
                'opportunity',
                'onlineOpportunity',
                'client',
                'talent' => function ($query) use ($user) {
                    $query->onlyManagedIfManager($user);

                    $query->onlyMyselfIfTalent($user);

                    $query->onlyAppliedOrShortlistedIfClient($user);
                }
            ])
            ->whereBetween('utc_date', [
                Carbon::createFromTimestampUTC($request->query('from'))->startOfDay(),
                Carbon::createFromTimestampUTC($request->query('to'))->endOfDay()
            ])
            ->whereNotIn('bookings.status', [
                Booking::STATUS_CANCELLED,
                Booking::STATUS_WITHDRAWN
            ])
            ->when($user->isClient(), function ($query) use ($request, $user) {
                $query->where('bookings.client_id', $user->id);
            })
            ->when($user->isTalent() || $user->isManager(), function ($query) use ($request, $user) {
                $query->join('booking_talent', function ($join) use ($user) {
                    $join->on('booking_talent.booking_id', '=', 'bookings.id')
                        ->whereIn('booking_talent.status', [TalentPivot::STATUS_PENDING, TalentPivot::STATUS_APPLIED])
                        ->when($user->isTalent(), function ($query) use ($user) {
                            $query->where('booking_talent.talent_id', $user->id);
                        })
                        ->when($user->isManager(), function ($query) use ($user) {
                            $query->whereIn('booking_talent.talent_id', $user->managedTalent->pluck('id'));
                        });
                });

                $query->where(function ($query) {
                    $query->where(function ($query) {
                        $query->where('bookings.type', Booking::TYPE_BOOKING)
                            ->where('booking_talent.client_accepted', true);
                    })->orWhere(function ($query) {
                        $query->where('bookings.type', Booking::TYPE_REQUEST);
                    });
                });
            })
            ->when($request->filled('talent') && ($user->isAdmin() || $user->isManager()), function ($query) use ($request, $user) {
                $talent = (array) $request->query('talent');

                $query->whereHas('talent', function ($query) use ($talent, $user) {
                    $query->whereIn('users.id', $talent)
                        ->managedBy($user);
                });
            })
            ->groupBy('bookings.id')
            ->get();

        $bookings->each->append('opportunity_name', 'utc_end_date');

        return BookingResource::collection($bookings);
    }
}

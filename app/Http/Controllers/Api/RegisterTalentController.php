<?php

namespace Pickstar\Http\Controllers\Api;

use Pickstar\User\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Pickstar\Events\UserRegistered;
use Pickstar\Opportunity\Opportunity;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Requests\Register\TalentRegisterRequest;

class RegisterTalentController extends Controller
{
    /**
     * Endpoint for storing a new talent
     *
     * @param TalentRegisterRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(TalentRegisterRequest $request)
    {
        $user = User::create([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'role' => User::ROLE_TALENT,
            'email' => $request->get('email'),
            'password' => $request->get('password'),
            'phone' => $request->get('phone'),
            'email_verification_token' => Str::random(32),
            'status' => User::STATUS_PENDING
        ]);

        $user->profile()->create([
            'manager' => $request->get('manager'),
            'agency' => $request->get('agency'),
            'gender' => $request->get('gender'),
        ]);

        // Trigger new talent registration event
        event(new UserRegistered($user));

        return response()->json($user);
    }

    /**
     * Storing a talents profile data
     *
     * @param User $user
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateProfile(User $user, Request $request)
    {
        if ($request->filled('locations')) {
            $user->locations()->sync($request->get('locations'));
        }

        $user->syncTags($request->only('sport_tags', 'club_tags', 'sponsor_tags', 'charity_tags'));

        $opportunities = Opportunity::whereIn('id', $request->get('opportunities'))->get();

        $request->user()->opportunities()->sync($opportunities->pluck('id'));

    }
}

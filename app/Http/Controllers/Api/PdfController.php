<?php

namespace Pickstar\Http\Controllers\Api;

use Illuminate\Http\Request;
use Pickstar\Booking\Booking;
use Pickstar\Jobs\BookingPdf;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Jobs\TalentProfilePdf;
use Pickstar\User\User;

class PdfController extends Controller
{
    /**
     * Generate a booking PDF and return the nice name and file hash name.
     *
     * @param Pickstar\Booking\Booking $booking
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Booking $booking, Request $request): JsonResponse
    {
        $this->authorize('pdf', $booking);

        $user = $request->user();

        // Admin can get different versions of pdf
        if ($user->isAdmin()) {
            $user = new User(['role' => $request->get('role')]);
        }

        return response()->json([
            'file' => BookingPdf::dispatchNow($booking, $user),
            'name' => sprintf('PickStar Booking Request - %s.pdf', str_replace(["/", "\\"], "-", $booking->name)),
            'userType' => $user->role
        ]);
    }

    /**
     * Generate talent profile pdf and return file hash and name.
     *
     * @param User $user
     * @return JsonResponse
     */
    public function talentProfile(User $user): JsonResponse
    {
        if ($user->isTalent()) {
            return response()->json([
                'file' => TalentProfilePdf::dispatchNow($user),
                'name' => sprintf('PickStar Talent Profile - %s.pdf', str_replace(["/", "\\"], "-", $user->name)),
            ]);
        } else {
            abort(500, 'Unable to find requested talent profile.');
        }
    }
}

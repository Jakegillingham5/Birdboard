<?php

namespace Pickstar\Http\Controllers\Api;

use Illuminate\Http\Request;
use Pickstar\Setting\Setting;
use Illuminate\Support\Facades\Cache;
use Pickstar\Http\Controllers\Controller;

class SettingController extends Controller
{
    /**
     * Get all settings.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $this->authorize('index', Setting::class);

        $settings = Cache::rememberForever('settings', function () {
            return Setting::first();
        });

        return response()->json($settings);
    }

    /**
     * Update settings.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $this->authorize('update', Setting::class);

        $settings = Setting::first();

        $settings->fill($request->only(Setting::getSettingKeys()))->save();

        Cache::forget('settings');

        return response()->json($settings);
    }
}

<?php

namespace Pickstar\Http\Controllers\Api\Manager;

use Pickstar\User\User;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Pickstar\Http\Controllers\Controller;

class CountController extends Controller
{
    /**
     * Get count of managers.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $this->authorize('index', User::class);

        $all = User::masterManagers()->count();

        $active = User::masterManagers()
            ->where(function ($query) {
                $query->where('email_verified', true)
                    ->where('status', '<>', User::STATUS_PENDING);
            })
            ->count();

        $unverified = User::masterManagers()
            ->where(function ($query) {
                $query->where('email_verified', false)
                    ->orWhere('status', User::STATUS_PENDING);
            })
            ->count();

        return response()->json(compact('all', 'active', 'unverified'));
    }
}

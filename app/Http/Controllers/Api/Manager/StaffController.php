<?php

namespace Pickstar\Http\Controllers\Api\Manager;

use Pickstar\User\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesForUsers;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Notifications\System\EmailVerification;
use Pickstar\Http\Requests\Api\Manager\StoreStaffManagerRequest;

class StaffController extends Controller
{
    use DeniesForUsers,
        DeniesHttpRequests;

    /**
     * Get list of staff managers for a manager.
     *
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request, User $user): JsonResponse
    {
        $this->authorize('manage', $user);

        $this->denyIfUserIsNotRole($user, [User::ROLE_MANAGER]);

        $staff = $user->staff()
            ->when($request->query('status'), function ($query, $status) {
                if ($status === 'active') {
                    $query->active();
                } else {
                    $query->withTrashed();
                }
            })
            ->get();

        return response()->json($staff);
    }

    /**
     * Create a new staff manager for a manager.
     *
     * @param \Pickstar\Http\Requests\Api\Manager\StoreStaffManagerRequest $request
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreStaffManagerRequest $request, User $user): JsonResponse
    {
        $this->denyIfUserIsNotRole($user, [User::ROLE_MANAGER]);

        $this->denyIfUserPendingActivation($user);

        $defaultAttributes = [
            'role' => User::ROLE_MANAGER,
            'status' => User::STATUS_ACTIVE,
            'email_verification_token' => Str::random(32),
            'company' => $user->company
        ];

        $staffManager = $user->staff()->create($defaultAttributes + $request->only([
            'first_name',
            'last_name',
            'email',
            'phone',
        ]));

        $staffManager->notify(new EmailVerification);

        return response()->json($staffManager);
    }
}

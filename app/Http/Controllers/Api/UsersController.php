<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{

    public $successStatus = 200;

    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(){
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
            $success['token'] =  $user->createToken('CypressTesting')->accessToken;
            $success['user'] =  $user;
            return response()->json(['success' => $success], $this-> successStatus);
        }else{
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }
}

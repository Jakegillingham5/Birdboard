<?php

namespace Pickstar\Http\Controllers\Api;

use Illuminate\Http\Request;
use Pickstar\User\User;
use Pickstar\Http\Controllers\Controller;

class AdminUsersController extends Controller
{
    /**
     * Return admin users full name and id.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index(Request $request)
    {
        $user = $request->user();

        $this->authorize('index', $user);

        $adminUsers = User::where('role', 'admin')->orderBy('last_name')->get();

        $results = array();

        foreach($adminUsers as $admin) {
            $results[] = (object) ['name' => $admin->name, 'id' => $admin->id];
        }

        return response()->json($results);
    }
}

<?php

namespace Pickstar\Http\Controllers\Api\Tag;

use Illuminate\Http\Request;
use Pickstar\Http\Transformers\TagTransformer;
use Pickstar\Tag\Tag;
use Pickstar\User\User;
use Illuminate\Http\JsonResponse;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Http\Requests\Api\AssociateTagRequest;

class RemoveTalentController extends Controller
{
    use DeniesHttpRequests;

    /**
     * Associate many tags with one other tag
     *
     * @param Request $request
     * @param Pickstar\Tag\Tag $tag
     * @param Pickstar\User\User $talent
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request, Tag $tag, User $talent): JsonResponse
    {
        $this->authorize('removeTalent', $tag);

        $tag->talent()->detach($talent->id);

        return response()->json($tag->append(['associated_tags', 'parent_tags', 'talent']));
    }
}

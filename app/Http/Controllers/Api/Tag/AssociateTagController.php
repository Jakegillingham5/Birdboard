<?php

namespace Pickstar\Http\Controllers\Api\Tag;

use Illuminate\Http\Request;
use Pickstar\Http\Transformers\TagTransformer;
use Pickstar\Tag\Tag;
use Pickstar\User\User;
use Illuminate\Http\JsonResponse;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Http\Requests\Api\AssociateTagRequest;

class AssociateTagController extends Controller
{
    use DeniesHttpRequests;

    /**
     * Associate many tags with one other tag
     *
     * @param Pickstar\Http\Requests\Api\AssociateTagRequest $request
     * @param Pickstar\Tag\Tag $tag
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(AssociateTagRequest $request, Tag $tag): JsonResponse
    {
        switch ($tag->type) {
            case 'sport':
                $tags = collect($request->input('associated_clubs'));
                $tags = $tags->merge(collect($request->input('associated_sponsors')));
                $tags = $tags->merge(collect($request->input('associated_charities')));
                $tag->associateAssociatedTags($tags);
                break;
            case 'club':
                $tags = collect($request->input('associated_sports'));
                $tag->associateParentTags($tags);

                $tags = collect($request->input('associated_sponsors'));
                $tags = $tags->merge(collect($request->input('associated_charities')));
                $tag->associateAssociatedTags($tags);
                break;
            case 'sponsor':
                $tags = collect($request->input('associated_sports'));
                $tags = $tags->merge(collect($request->input('associated_clubs')));
                $tag->associateParentTags($tags);

                $tags = collect($request->input('associated_charities'));
                $tag->associateAssociatedTags($tags);
                break;
            case 'charity':
                $tags = collect($request->input('associated_sports'));
                $tags = $tags->merge(collect($request->input('associated_clubs')));
                $tags = $tags->merge(collect($request->input('associated_sponsors')));
                $tag->associateParentTags($tags);
                break;
        }

        return response()->json(null);
    }
}

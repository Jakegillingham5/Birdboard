<?php

namespace Pickstar\Http\Controllers\Api\Tag;

use Pickstar\Tag\Tag;
use Pickstar\User\User;
use Illuminate\Http\JsonResponse;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Concerns\DeniesHttpRequests;
use Pickstar\Http\Requests\Api\MergeTagsRequest;

class MergeController extends Controller
{
    use DeniesHttpRequests;

    /**
     * Merge two or more tags into a single tag.
     *
     * @param \Pickstar\Http\Requests\Api\MergeTagsRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(MergeTagsRequest $request): JsonResponse
    {
        $selectedTags = Tag::where('type', $request->input('type'))
            ->whereIn('id', $request->input('tags'))
            ->get();

        // Double check to prevent them from trying to merge less than 2 tags of the given type.
        if ($selectedTags->count() < 2) {
            $this->deny('Unable to perform merge when less than 2 tags of given type selected.');
        }

        $mergeTag = Tag::create($request->only('name', 'type'));

        User::talent()
            ->whereHas('tags', function ($query) use ($selectedTags) {
                $query->whereIn('id', $selectedTags->pluck('id'));
            })
            ->get()
            ->each(function ($talent) use ($mergeTag, $selectedTags) {

                //If the talent is retired from one of the tags that we are merging, they are retired from the new tag being made
                $contains = false;
                foreach($selectedTags as $selectedTag) {
                    if ($talent->tags->contains($selectedTag)) {
                        foreach($talent->tags as $tag) {
                            if($tag->pivot->retired === true && $selectedTag->id === $tag->id) {
                                $contains = true;
                            }
                        }
                    }
                };
                if ($contains === true ) {
                    $talent->tags()->attach($mergeTag, ['retired' => true]);
                } else {
                    $talent->tags()->attach($mergeTag, ['retired' => false]);
                }
            });

        // Spin through each of the selected tags and update their merged tag ID to the newly
        // created tag as well as setting their deleted at timestamp. This is so we are
        // able to see what tags have been merged and what they were merged into.
        $selectedTags->each(function ($tag) use ($mergeTag) {
            $tag->fill(['merged_tag_id' => $mergeTag->id, 'deleted_at' => now()])->save();

            $tag->talent()->detach();
        });

        return response()->json(Tag::withCount('talent as occurances')->find($mergeTag->id));
    }
}

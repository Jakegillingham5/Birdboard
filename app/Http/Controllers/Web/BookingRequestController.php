<?php

namespace Pickstar\Http\Controllers\Web;

use Pickstar\Marketing\Email;
use Pickstar\User\User;
use Tymon\JWTAuth\JWTAuth;
use Illuminate\Http\Request;
use Pickstar\Http\Controllers\Controller;

class BookingRequestController extends Controller
{
    /**
     * Show the booking request form.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $client = null;
        $version = 'default';

        if ($request->query('client') && optional($request->user())->isAdmin()) {
            $client = User::onlyClients()->find($request->query('client'));
        }

        return view('booking-request.index', compact('client', 'version'));
    }

    /**
     * Show the booking request form.
     *
     * @return \Illuminate\Http\Response
     */
    public function test(Request $request)
    {
        $client = null;
        $version = 'default';

        if ($request->query('client') && optional($request->user())->isAdmin()) {
            $client = User::onlyClients()->find($request->query('client'));
        }

        return view('booking-request.index', compact('client', 'version'));
    }

    /**
     * Redirect client to the booking after persisting a web session once a booking request
     * has been created.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Tymon\JWTAuth\JWTAuth $jwt
     *
     * @return mixed
     */
    public function store(Request $request, JWTAuth $jwt)
    {
        $jwt->setToken($request->get('_jwt'));

        if ($jwt->check() && $jwt->checkSubjectModel(User::class)) {
            $user = User::find($jwt->getClaim('sub'));

            auth('web')->login($user);

            // Attempt to find the booking by the ID given in the form data. If we can't find it then we'll grab
            // the latest booking from the clients bookings and redirect them to that instead.
            if (! $booking = $user->clientBookingRequests()->find($request->get('_booking'))) {
                $booking = $user->clientBookingRequests()
                    ->latest()
                    ->first();
            }

            if ($user->hasMarketingEmail()) {
                Email::whereEmail($user->email)->update(['campaign_stage' => 6]);
            }

            return response()
                ->redirectToWebApp(sprintf('welcome/%s?%s', $booking->id, $user->hasMarketingEmail() ? 'utm_content=STEP-5-CONVERTED' : '' ));
        }

        return redirect()->home()->with('message', 'We were unable to log you in.');
    }
}

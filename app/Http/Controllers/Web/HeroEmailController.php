<?php

namespace Pickstar\Http\Controllers\Web;

use Pickstar\Events\LeadCaptured;
use Pickstar\Marketing\Email;
use Illuminate\Http\JsonResponse;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Requests\Api\StoreMarketingEmailRequest;
use Pickstar\Marketing\Source;

class HeroEmailController extends Controller
{
    /**
     * Stores a given email.
     *
     * @param StoreMarketingEmailRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreMarketingEmailRequest $request)
    {
        $names = $request->get('name');

        $email = Email::firstOrCreate(['email' => $request->get('email')],
            [
                'first_name'        => $names[0],
                'last_name'         => $names[1],
                'collection_page'   => \Request::server('HTTP_REFERER')
            ]);

        Source::insertOrUpdateWithGA($request->get('email'));

        event(new LeadCaptured($email));

        return redirect()->route('ourstars.index');
    }
}

<?php

namespace Pickstar\Http\Controllers\Web;

use Pickstar\Marketing\Email;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Mail\System\PartnersForm;
use Pickstar\Http\Requests\ContactRequest;
use Illuminate\Support\Facades\Mail;

class PartnersController extends Controller
{
    /**
     * Show the partners page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('partners.index');
    }

    /**
     * Show the contact static page.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(ContactRequest $request)
    {
        $name = $request->get('name');
        list($first_name, $last_name) = Email::extractNamesFromString($name);
        $email = $request->get('email');
        $number = $request->get('number');
        $description = $request->get('description');

        Mail::to(env('CONTACT_FORM_EMAIL', 'info@pickstar.com.au'))
            ->queue(new PartnersForm($name, $email, $number, $description));

        $request->session()->flash('status', 'Message was successfully sent!');

        Email::firstOrCreate(
            ['email' => $email],
            ['first_name' => $first_name, 'last_name' => $last_name]
        );

        return redirect()->route('partners.index');
    }
}

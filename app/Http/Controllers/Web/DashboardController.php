<?php

namespace Pickstar\Http\Controllers\Web;

use Illuminate\Http\Request;
use Pickstar\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return response()
            ->redirectToWebApp($request->get('redirect'));
    }
}

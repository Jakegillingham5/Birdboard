<?php

namespace Pickstar\Http\Controllers\Web;

use Pickstar\Events\NewsletterRegistration;
use Pickstar\Events\LeadCaptured;
use Pickstar\Http\Requests\SubscribeNewsletterRequest;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Marketing\Email;
use Pickstar\Marketing\Source;

class NewsletterController extends Controller
{
    /**
     * Subscribe to the newsletter and send welcome email
     *
     * @param SubscribeNewsletterRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function index(SubscribeNewsletterRequest $request)
    {
        $firstName  = $request->input('firstName');
        $lastName   = $request->input('lastName');
        $email      = $request->input('email');

        $model = Email::firstOrCreate(
            [
                'email' => $email
            ],
            [
                'first_name'        => $firstName,
                'last_name'         => $lastName,
                'collection_form'   => 'Newsletter',
                'collection_page'   => \Request::server('HTTP_REFERER')
            ]
        );

        Source::InsertOrUpdateWithGA($email);
        event(new NewsletterRegistration($model));

        return redirect()->route('home');
    }
}

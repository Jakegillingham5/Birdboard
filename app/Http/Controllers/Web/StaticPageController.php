<?php

namespace Pickstar\Http\Controllers\Web;

use Pickstar\Http\Controllers\Controller;

class StaticPageController extends Controller
{
    /**
     * Show the how it works static page.
     *
     * @return \Illuminate\Http\Response
     */
    public function howItWorks()
    {
        return view('static.how_it_works');
    }

    /**
     * Show the about static page.
     *
     * @return \Illuminate\Http\Response
     */
    public function about()
    {
        return view('static.about');
    }

    /**
     * Show the privacy static page.
     *
     * @return \Illuminate\Http\Response
     */
    public function privacy()
    {
        return view('static.privacy');
    }

    /**
     * Show the case studies static page.
     *
     * @return \Illuminate\Http\Response
     */
    public function caseStudies()
    {
        return view('static.case_studies');
    }

    /**
     * Show the exclusive deals static page.
     *
     * @return \Illuminate\Http\Response
     */
    public function exclusiveDealsOne()
    {
        return view('static.exclusive_deals_one');
    }
}

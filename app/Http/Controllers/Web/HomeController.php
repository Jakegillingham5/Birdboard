<?php

namespace Pickstar\Http\Controllers\Web;

use Pickstar\User\User;
use Illuminate\Http\Request;
use Pickstar\Opportunity\Opportunity;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Transformers\TalentTransformer;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $stars = User::talent()
            ->featured()
            ->with('profile', 'sportTags', 'clubTags')
            ->inRandomOrder()
            ->take(8)
            ->get();

        $stars->each->append('sport_club');
        $stars->each->addHidden('sportTags', 'clubTags');
        $stars->each->transform([new TalentTransformer, 'transform'], [$request->user()]);

        $opportunities = Opportunity::select(['name', 'id'])
            ->onlyParents()
            ->inPerson()
            ->get()
            ->map(function ($opportunity) {
                return ['id' => $opportunity->id, 'text' => $opportunity->name];
            });

        return view('home', compact('stars', 'opportunities'));
    }

    public function landing(Request $request)
    {
        $opportunities = Opportunity::select(['name', 'id'])
            ->onlyParents()
            ->inPerson()
            ->get()
            ->map(function ($opportunity) {
                return ['id' => $opportunity->id, 'text' => $opportunity->name];
            });

        return view('influencers', compact('opportunities'));
    }
}

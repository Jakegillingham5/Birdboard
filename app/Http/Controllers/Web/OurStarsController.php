<?php

namespace Pickstar\Http\Controllers\Web;

use Pickstar\Tag\Tag;
use Pickstar\User\User;
use Illuminate\Http\Request;
use Pickstar\Services\OurStarsService;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Transformers\TalentTransformer;

class OurStarsController extends Controller
{
    /**
     * Show the user type selection.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request, OurStarsService $ourStarsService)
    {
        $stars = $ourStarsService->getStars($request);

        $preset_filters = null;
        $sentenceA = '';
        $sentenceHighlight = '';
        $sentenceB = '';
        $sentenceTwo = null;
        $meta = null;

        return view('our-stars.index', compact('stars', 'preset_filters', 'sentenceA', 'sentenceHighlight', 'sentenceB', 'sentenceTwo', 'meta'));
    }

    /**
     * Apply a predetermined filter
     *
     * @param Request $request
     * @param OurStarsService $ourStarsService
     * @param $filter
     */
    public function landing(Request $request, OurStarsService $ourStarsService, $filter)
    {
        $stars = $ourStarsService->getStars($request);
        $sentenceA = '';
        $sentenceHighlight = '';
        $sentenceB = '';
        $sentenceTwo = null;
        $meta = null;
        switch ($filter) {
            case 'afl':
                $preset_filters['sports'] = [['AFL', 4]];
                break;
            case 'men':
            case 'male':
                $preset_filters['gender'] = [['Male', 'male']];
                break;
            case 'women':
            case 'female':
                $preset_filters['gender'] = [['Female', 'female']];
                break;
            case 'emcee':
            case 'mc':
                $preset_filters['opportunity_types'] = [['MC an event', 11]];
                $sentenceA = 'Search & shortlist Australia\'s top sports star ';
                $sentenceHighlight = 'MC\'s ';
                $sentenceB = 'or go straight to your ';
                break;
            case 'keynote':
                $preset_filters['opportunity_types'] = [['Keynote presentation', 20]];
                $sentenceA = 'Search & shortlist Australia\'s top sports star ';
                $sentenceHighlight = 'keynote speakers ';
                $sentenceB = 'or go straight to your ';
                break;
            case 'ambassadors':
                $preset_filters['opportunity_types'] = [['Ambassador', 26]];
                $sentenceA = 'Search & shortlist Australia\'s top sports star ';
                $sentenceHighlight = 'ambassadors ';
                $sentenceB = 'or go straight to your ';
                break;
            case 'influencers':
                $preset_filters['social_following'] = [['10k – 30k Followers', 2],['30k – 50k Followers', 3],['50k – 100k Followers', 4],['100k – 500k Followers', 5],['500k+ Followers', 6]];
                $preset_filters['opportunity_types'] = [['Social media campaign', 29]];
                $sentenceA = 'Search & shortlist Australia\'s top sports star ';
                $sentenceHighlight = 'influencers ';
                $sentenceB = 'or go straight to your ';
                break;
            case 'celebs':
            case 'celebrities':
                $preset_filters['social_following'] = [['100k – 500k Followers', 5],['500k+ Followers', 6]];
                $sentenceA = 'Search & shortlist Australia\'s top sports star ';
                $sentenceHighlight = 'celebrities ';
                $sentenceB = 'or go straight to your ';
                break;
            case 'skills-clinic':
                $preset_filters['opportunity_types'] = [['Skills Clinic', 4]];
                $sentenceA = '';
                $sentenceHighlight = '';
                $sentenceB = '';
                break;
            case 'gaming':
            case 'esports':
                $preset_filters['sports'] = [['Esports', 1244]];
                $sentenceA = 'Search & shortlist Australia\'s top ';
                $sentenceHighlight = 'esports ';
                $sentenceB = 'stars or go straight to your ';
                break;
            case 'party':
                $preset_filters['opportunity_types'] = [['Birthday Party Appearance', 17]];
                break;
            case 'meet-and-greet':
                $preset_filters['opportunity_types'] = [['Meet And Greet', 9]];
                break;
            case 'speakers':
                $preset_filters['opportunity_types'] = [['Guest Speaker', 2]];
                $sentenceA = 'Search & shortlist Australia\'s top sports star ';
                $sentenceHighlight = 'guest speakers ';
                $sentenceB = 'or go straight to your ';
                break;
            case 'appearance':
                $preset_filters['opportunity_types'] = [['Appearance', 1]];
                $sentenceA = 'Search & shortlist Australia\'s top sports stars for ';
                $sentenceHighlight = 'appearances at events ';
                $sentenceB = 'or go straight to your ';
                break;
            case 'fundraiser':
                $preset_filters['opportunity_types'] = [['Fundraiser', 18]];
                $sentenceA = 'Search & shortlist Australia\'s top sports stars to be a part of your ';
                $sentenceHighlight = 'fundraiser, ';
                $sentenceB = 'or go straight to your ';
                break;
            case 'matildas':
                $preset_filters['clubs'] = [['Matildas', 94]];
                $sentenceA = 'Search & shortlist Australia\'s top ';
                $sentenceHighlight = 'Matilda\'s';
                $sentenceB = 'stars for your event or campaign, or go straight to your ';
                break;
            case 'indigenous-spokespeople':
                $preset_filters['themes'] = [['Indigenous Spokespeople', 1475]];
                $sentenceA = 'Search & shortlist ';
                $sentenceHighlight = 'NAIDOC week advocates';
                $sentenceB = '& sports stars to be involved in your NAIDOC week events and campaigns or go straight to your ';
                $sentenceTwo = 'Book Stars for Guest Speaking, Keynotes, Campaigns, Ambassadors Appearances & More.';
                $meta = 'Book NAIDOC Week Advocates & Sports Stars To Attend Or Be Involved Your NAIDOC Week Events and Campaigns. Book Stars for Guest Speaking, Keynotes, Campaigns, Ambassadors Appearances & More.';
                break;
            default:
                $preset_filters = null;
                $sentenceA = '';
                $sentenceHighlight = '';
                $sentenceB = '';
                $sentenceTwo = null;
                $meta = null;
                break;
        }
        return view('our-stars.index', compact('stars', 'preset_filters', 'sentenceA', 'sentenceHighlight', 'sentenceB', 'sentenceTwo', 'meta'));

    }

    /**
     * Show the talent profile for the given talent
     *
     * @return \Illuminate\View\View
     */
    public function show(Request $request, $id, $fullname)
    {
        $talent = User::talent()->findOrFail($id);

        if (! $talent->profile->public) {
            return redirect(route('ourstars.index'));
        }

        $similar = User::active()
            ->talent()
            ->select('users.*')
            ->selectRaw('IF(talent_profile.date_featured_until > ?, TRUE, FALSE) AS is_featured', [now()->toDateTimeString()])
            ->join('talent_profile', 'users.id', 'talent_profile.user_id')
            ->with('profile', 'sportTags', 'clubTags')
            ->whereHas('tags', function ($query) use ($talent) {
                $query->whereIn('tag_id', $talent->sportTags->pluck('id'));
            })
            ->where('users.id', '<>', $talent->id)
            ->whereHas('profile', function ($query) {
                $query->public();
            })
            ->inRandomOrder()
            ->take(3)
            ->get();

        $similar->each->append('sport_club');
        $similar->each->addHidden('sportTags', 'clubTags');
        $similar->each->transform([new TalentTransformer, 'transform'], [$request->user()]);

        $talent->addVisible([
            'id',
            'name',
            'first_name',
            'last_name',
            'slug',
            'profile',
            'response',
            'sport_club',
            'profile_image_url'
        ]);

        $talent->profile->addVisible([
            'id',
            'tagline',
            'highest_followers',
            'highest_followers_icon'
        ]);

        return view('our-stars.show', ['talent' => $talent, 'similarStars' => $similar]);
    }
}

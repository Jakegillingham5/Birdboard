<?php

namespace Pickstar\Http\Controllers\Web\Session;

use Illuminate\Http\Request;
use Pickstar\Http\Controllers\Controller;

class InitializeController extends Controller
{
    public function store(Request $request)
    {
        return response(null, 204, ['Authorization' => sprintf('Bearer %s', $request->user()->createJwtToken())]);
    }
}

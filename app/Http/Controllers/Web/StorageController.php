<?php

namespace Pickstar\Http\Controllers\Web;

use Illuminate\Support\Facades\Storage;
use Pickstar\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class StorageController extends Controller
{


    /**
     * Handles both profile image and marketing photo
     *
     * @param $path
     * @return mixed
     */
    public function show($path)
    {
        if (Storage::disk('local')->exists($path)) {
            return response()->file(
                Storage::disk('local')->path($path), [
                    'Cache-Control' => 'public',
                    'Cache-Control' => 'max-age=2592000'
                ]
            );
        }
        throw new NotFoundHttpException('Unable to find stored file to download.');
    }


    /**
     * This will set response header 'Content-Disposition' to 'attachment'.
     * This allows the file to be downloaded instead of viewed.
     *
     * @param $path
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function download($path)
    {
        if (Storage::disk('local')->exists($path)) {

            return response()->download(
                Storage::disk('local')->path($path)
            );
        }
        throw new NotFoundHttpException('Unable to find stored file to download.');
    }
}

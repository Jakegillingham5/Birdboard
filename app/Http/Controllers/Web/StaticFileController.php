<?php

namespace Pickstar\Http\Controllers\Web;

use Illuminate\Support\Facades\Storage;
use Pickstar\Http\Controllers\Controller;

class StaticFileController extends Controller
{

    public function sitemap()
    {
        return response( Storage::get('sitemap.xml'))
            ->header('Content-Type', 'text/xml');
    }

    public function sitemapstatic()
    {
        return response( Storage::get('sitemap-static.xml'))
            ->header('Content-Type', 'text/xml');
    }

    public function sitemaptalent()
    {
        return response( Storage::get('sitemap-talent.xml'))
            ->header('Content-Type', 'text/xml');
    }
}

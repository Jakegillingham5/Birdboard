<?php

namespace Pickstar\Http\Controllers\Web;

use Illuminate\Http\Request;
use Pickstar\Booking\Booking;
use Pickstar\Booking\TalentPivot;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Events\TalentConfirmedAttendance;

class ConfirmTalentAttendanceController extends Controller
{

    /**
     * Use given access token for a one time link to confirm attendance
     *
     * @param \Pickstar\Booking\Booking $booking
     * @param \Pickstar\User\User $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $token)
    {
        $bookingTalent = TalentPivot::where('attendance_access_token', $token)->first();

        if ($bookingTalent && $request->hasValidSignature()) {
            if ($bookingTalent->attendance_confirmed) {
                return view('booking.talent_attendance_confirmation', [
                    'success' => true,
                    'message' => 'Your attendance has already been confirmed.'
                ]);
            }

            $bookingTalent->update(['attendance_confirmed' => true]);

            TalentConfirmedAttendance::dispatch($bookingTalent->booking, $bookingTalent->talent, Booking::APPEARANCE_IN_PERSON);

            return view('booking.talent_attendance_confirmation', [
                'success' => true,
                'message' => 'You have been confirmed as attending.'
            ]);
        }

        return view('booking.talent_attendance_confirmation', ['success' => false]);
    }
}

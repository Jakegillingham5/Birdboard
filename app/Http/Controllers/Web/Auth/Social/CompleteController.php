<?php

namespace Pickstar\Http\Controllers\Web\Auth\Social;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Pickstar\Http\Controllers\Controller;

class CompleteController extends Controller
{
    /**
     * Handle redirect based on authenticated users role.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function index(Request $request): RedirectResponse
    {
        if ($request->user()->hasNoRole()) {
            return redirect()->route('register.social');
        }

        return redirect()->toWebApp();
    }
}

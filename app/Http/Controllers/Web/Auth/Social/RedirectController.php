<?php

namespace Pickstar\Http\Controllers\Web\Auth\Social;

use Exception;
use Laravel\Socialite\Facades\Socialite;
use Pickstar\Http\Controllers\Controller;

class RedirectController extends Controller
{
    /**
     * Redirect the user to the OAuth Provider or show a denied view.
     *
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index($provider)
    {
        $driver = Socialite::driver($provider);

        if ($provider === 'facebook') {
            $driver->asPopup();
        }

        try {
            return $driver->redirect();
        } catch (Exception $exception) {
            report($exception);

            return view('auth.socialite.denied');
        }
    }
}

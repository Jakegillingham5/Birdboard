<?php

namespace Pickstar\Http\Controllers\Web\Auth\Social;

use Exception;
use Pickstar\User\User;
use Laravel\Socialite\Facades\Socialite;
use Pickstar\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Pickstar\Services\Registration\SocialRegistrationService;

class CallbackController extends Controller
{
    /**
     * Obtain the user information from provider. Check if the user already exists in our
     * database by looking up their provider_id in the database.
     *
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that
     * redirect them to the authenticated users homepage.
     *
     * @return Response
     */
    public function index($provider, SocialRegistrationService $socialRegistrationService)
    {
        try {
            $socialiteUser = Socialite::driver($provider)->user();
        } catch (Exception $exception) {
            report($exception);

            return view('auth.socialite.denied');
        }

        $user = $socialRegistrationService->findOrCreateUser($socialiteUser, $provider);

        auth('web')->login($user, true);

        $token = auth('api')->login($user);

        return view('auth.socialite.complete', compact('token', 'user'));
    }
}

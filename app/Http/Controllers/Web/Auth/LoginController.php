<?php

namespace Pickstar\Http\Controllers\Web\Auth;

use Pickstar\User\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Pickstar\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        return auth('web')->attempt($this->credentials($request), $request->get('remember_me'));
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        // Check if the user exists as a social media login but only if they do not have a password
        // set as well.
        $userExistsFromSocial = User::whereNull('password')
            ->where($this->username(), $request->get($this->username()))
            ->where(function ($query) {
                $query->whereNotNull('twitter_provider_id')
                    ->orWhereNotNull('facebook_provider_id');
            })
            ->exists();

        if ($userExistsFromSocial) {
            throw ValidationException::withMessages([
                $this->username() => ['This email has registered with PickStar using Facebook or Twitter.'],
            ]);
        }

        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }

    /**
     * Redirect the user after authentication.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Pickstar\User\User $user
     * @param string $token
     *
     * @return \Illuminate\Http\Response
     */
    protected function authenticated(Request $request, User $user)
    {
        if ($user->isDeactivated()) {
            auth('web')->logout();

            throw ValidationException::withMessages(['email' => 'The account tied to this email is deactivated.']);
        }

        if ($request->filled('redirect')) {
            if ($request->get('context') === 'public') {
                $redirect = url($request->get('redirect'));
            } else {
                $redirect = url()->toWebApp($request->get('redirect'));
            }
        } else {
            $redirect = url()->toWebApp();
        }

        if ($request->expectsJson()) {
            $token = $user->createJwtToken();

            return response()->json(['redirect' => $redirect, 'user' => $user], 200, [
                'authorization' => sprintf('Bearer %s', $token)
            ]);
        } else {
            return redirect()->to($redirect);
        }
    }

    /**
     * Log the user out of the application.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        auth('web')->logout();

        $request->session()->invalidate();

        return redirect('/');
    }
}

<?php

namespace Pickstar\Http\Controllers\Web;

use Pickstar\User\User;
use Illuminate\Http\Request;
use Pickstar\Marketing\Email;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Transformers\TalentTransformer;
use Pickstar\Http\Requests\ContactRequest;
use Pickstar\Mail\System\EsportsForm;
use Illuminate\Support\Facades\Mail;

class EsportsController extends Controller
{
    /**
     * Show the esports page.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        //Fill the where in with the ids of the talent that are to be featured
        $stars = User::talent()
            ->whereIn('id', array(2565, 2564, 2561, 2585))
            ->with('profile', 'sportTags', 'clubTags')
            ->inRandomOrder()
            ->take(4)
            ->get();

        if ($stars->count() < 4) {
            $stars = User::talent()
                ->where('can_set_esports', true)
                ->with('profile', 'sportTags', 'clubTags')
                ->inRandomOrder()
                ->take(4)
                ->get();
        }

        $stars->each->append('sport_club');
        $stars->each->addHidden('sportTags', 'clubTags');
        $stars->each->transform([new TalentTransformer, 'transform'], [$request->user()]);

        return view('esports.index', compact('stars'));
    }

    /**
     * Show the esports static page.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(ContactRequest $request)
    {
        $email = $request->get('email');
        $description = $request->get('description');

        Mail::to(env('CONTACT_FORM_EMAIL', 'info@pickstar.com.au'))
            ->queue(new EsportsForm($email, $description));

        $request->session()->flash('status', 'Message was successfully sent!');

        Email::firstOrCreate(
            ['email' => $email]
        );

        return redirect()->route('esports.index');
    }
}

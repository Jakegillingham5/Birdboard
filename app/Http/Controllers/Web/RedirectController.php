<?php

namespace Pickstar\Http\Controllers\Web;

use Pickstar\Http\Controllers\Controller;

class RedirectController extends Controller
{
    public function about()
    {
        return redirect()->route('about.index')->status(301);
    }

    public function howitworks()
    {
        return redirect()->route('howitworks.index')->status(301);
    }

    public function athlete()
    {
        return redirect()->route('ourstars.index')->status(301);
    }

    public function home()
    {
        return redirect()->route('home')->status(301);
    }

    public function account()
    {
        return redirect()->route('login')->status(301);
    }

    public function billboard()
    {
        return redirect()->route('ourstars.index')->status(301);
    }

    public function contact()
    {
        return redirect()->route('contact.index')->status(301);
    }

    public function faq()
    {
        return redirect()->route('howitworks.index', ['#faq'])->status(301);
    }
}

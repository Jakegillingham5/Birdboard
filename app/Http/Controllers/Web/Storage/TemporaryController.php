<?php

namespace Pickstar\Http\Controllers\Web\Storage;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Pickstar\Http\Controllers\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TemporaryController extends Controller
{
    /**
     * Download and delete a temporary file.
     *
     * @param Request $request
     * @param string $hash
     *
     * @return void
     */
    public function show(Request $request, $hash)
    {
        $path = sprintf('tmp/%s', $hash);

        if (Storage::disk('local')->exists($path)) {
            return response()->download(Storage::disk('local')->path($path), $request->query('name'))
                ->deleteFileAfterSend(true);
        }

        throw new NotFoundHttpException('Unable to find stored file to download.');
    }
}

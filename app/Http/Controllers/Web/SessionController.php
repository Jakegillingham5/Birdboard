<?php

namespace Pickstar\Http\Controllers\Web;

use Illuminate\Http\Request;
use Pickstar\Http\Controllers\Controller;

class SessionController extends Controller
{
    /**
     * Update the Laravel session.
     *
     * This is used by the web application to regenerate the session periodically so
     * we don't end up logged out.
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        $request->session()->regenerate();

        $request->session()->save();

        if ($request->expectsJson()) {
            return response()->json(null, 204);
        }

        return redirect()->home();
    }
}

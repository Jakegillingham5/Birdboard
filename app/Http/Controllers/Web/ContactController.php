<?php

namespace Pickstar\Http\Controllers\Web;

use Pickstar\Events\LeadCaptured;
use Pickstar\Marketing\Email;
use Illuminate\Support\Facades\Mail;
use Pickstar\Mail\System\ContactForm;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Requests\ContactRequest;

class ContactController extends Controller
{
    /**
     * Show the contact static page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('static.contact');
    }

    /**
     * Show the contact static page.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(ContactRequest $request)
    {
        $name = $request->get('name');
        list($first_name, $last_name) = Email::extractNamesFromString($name);
        $email = $request->get('email');
        $number = $request->get('number');
        $description = $request->get('description');

        Mail::to(env('CONTACT_FORM_EMAIL', 'info@pickstar.com.au'))
            ->queue(new ContactForm($name, $email, $number, $description));

        $request->session()->flash('status', 'Message was successfully sent!');

        $emailModel = Email::firstOrCreate(
            ['email' => $email],
            ['first_name' => $first_name, 'last_name' => $last_name,'collection_form' => 'Contact']
        );

        event(new LeadCaptured($emailModel));

        return redirect()->route('contact.index');
    }
}

<?php

namespace Pickstar\Http\Controllers\Web\Register;

use Illuminate\Http\RedirectResponse;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Registration\ManagerRegistration;
use Pickstar\Http\Requests\Register\ManagerRegisterRequest;
use Pickstar\User\ActiveCampaignUser;

class ManagerController extends Controller
{
    /**
     * Show manager registration form.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('register.manager');
    }

    /**
     * Register a manager user.
     *
     * @param \Pickstar\Http\Requests\Register\ManagerRegisterRequest $request
     * @param \Pickstar\Registration\ManagerRegistration $registration
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ManagerRegisterRequest $request, ManagerRegistration $registration): RedirectResponse
    {
        $user = $registration->handle($request->all());

        auth('web')->login($user);

        $email  = $user->email;
        $ac     = ActiveCampaignUser::findByEmail($email);
        $ac->pushToActiveCampaign();

        return redirect()->toWebApp();
    }
}

<?php

namespace Pickstar\Http\Controllers\Web\Register;

use Illuminate\Http\Request;
use Pickstar\Location\Location;
use Pickstar\Events\UserRegistered;
use Illuminate\Support\Facades\Auth;
use Pickstar\Opportunity\Opportunity;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Registration\TalentRegistration;
use Pickstar\Http\Requests\Register\TalentRegisterRequest;
use Pickstar\Http\Requests\Register\TalentSecondStepRequest;
use Pickstar\User\ActiveCampaignUser;

class TalentController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('register.talent');
    }

    /**
     * Store the first step details and redirect to second step.
     *
     * @param \Pickstar\Http\Requests\TalentRegisterRequest $request
     * @param \Pickstar\Talent\Registrar $registrar
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(TalentRegisterRequest $request, TalentRegistration $registration)
    {
        $user = $registration->handle($request->all());

        auth('web')->login($user);

        return redirect()->route('register.talent.two');
    }

    /**
     * Show the second step for talent registration.
     *
     * @return \Illuminate\View\View
     */
    public function showStepTwo()
    {
        $locations = Location::withoutAustralia()->orderBy('name', 'asc')->get();

        return view('register.talent_step_two', compact('locations'));
    }

    /**
     * Store the second step details and redirect to the third step.
     *
     * @param \Pickstar\Http\Requests\TalentSecondStepRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeStepTwo(TalentSecondStepRequest $request)
    {
        $user = auth()->user();

        $user->profile->fill($request->only('gender'))->save();

        if ($request->filled('locations')) {
            $user->locations()->sync($request->get('locations'));
        }

        if ($request->filled('based_in')) {
            $location = Location::find($request->get('based_in'));
            $user->basedIn()->associate($location)->save();
        }

        $user->syncTags($request->only('sport_tags', 'club_tags', 'sponsor_tags', 'charity_tags'));

        $user->profile->handleProfileImage($request->file('profile_image'), $request->get('profile_image_crop'));

        return redirect()->route('register.talent.three');
    }

    /**
     * Show the final step.
     *
     * @return \Illuminate\View\View
     */
    public function showStepThree()
    {
        $inPersonOpportunities = Opportunity::select(['name', 'id'])
            ->with(['children' => function ($query) {
                $query->select(['opportunities.name', 'opportunities.id']);
            }])
            ->onlyParents()
            ->inPerson()
            ->get()
            ->toJson();

        $onlineOpportunities = Opportunity::select(['name', 'id'])
            ->with(['children' => function ($query) {
                $query->select(['opportunities.name', 'opportunities.id']);
            }])
            ->onlyParents()
            ->online()
            ->get()
            ->toJson();

        return view('register.talent_step_three', compact('inPersonOpportunities', 'onlineOpportunities'));
    }

    /**
     * Store the final step details and redirect to JS application.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeStepThree(Request $request)
    {
        $opportunities = Opportunity::whereIn('id', $request->get('opportunities'))->get();

        $request->user()->opportunities()->sync($opportunities->pluck('id'));
        $request->user()->profile->fill(['description' => $request->get('description')])->save();

        $path = 'welcome/talent';

        $email  = $request->user()->email;
        $ac     = ActiveCampaignUser::findByEmail($email);
        $ac->pushToActiveCampaign();
        
        return response()
            ->redirectToWebApp($path);
    }
}

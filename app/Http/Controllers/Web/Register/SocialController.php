<?php

namespace Pickstar\Http\Controllers\Web\Register;

use Illuminate\Http\Request;
use Pickstar\Http\Controllers\Controller;
use Pickstar\Registration\ClientRegistration;
use Pickstar\Registration\TalentRegistration;
use Pickstar\Registration\ManagerRegistration;
use Pickstar\Http\Requests\Register\SocialRegisterRequest;

class SocialController extends Controller
{
    /**
     * Show the user type selection.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        if ($request->user()->hasRole()) {
            return redirect()->home();
        }

        return view('register.social_account_type', ['email' => $request->user()->email]);
    }

    /**
     * Store the users new role and create releveant profile data.
     *
     * @param \Pickstar\Http\Requests\Register\SocialRegisterRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(SocialRegisterRequest $request)
    {
        if ($request->user()->hasRole()) {
            return response()->json(['message' => 'User has already selected a role.'], 403);
        }

        switch ($request->input('role')) {
            case 'client':
                (new ClientRegistration)
                    ->handle($request->only('email', 'phone'));

                return response()->json('', 204);
                break;
            case 'talent':
                (new TalentRegistration)
                    ->handle($request->only('email', 'gender', 'phone'));

                return response()->json('', 204);
                break;
            case 'manager':
                (new ManagerRegistration)
                    ->handle($request->only('email', 'phone', 'company', 'position'));

                return response()->json('', 204);
                break;
            default:
                return response()->json(['message' => 'There was a problem completing social registration.'], 400);
        }
    }
}

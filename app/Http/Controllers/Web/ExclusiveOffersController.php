<?php

namespace Pickstar\Http\Controllers\Web;

use Pickstar\Events\BidMade;
use Pickstar\User\AdminUser;
use Pickstar\User\User;
use Pickstar\Notifications;
use Illuminate\Http\Request;
use Pickstar\Http\Requests\ExclusiveOffersRequest;
use Pickstar\Http\Controllers\Controller;
use GuzzleHttp\Client;

class ExclusiveOffersController extends Controller
{
    public function mcdermott(ExclusiveOffersRequest $request)
    {
        $user = User::find(3);

        $user->notify(new Notifications\System\BidMade($request->input('name.0'), $request->input('email.0'), $request->input('company.0'), $request->input('bid.0'), $request->input('type.0')));

        return redirect()->route('exclusive-offer.success');
    }

    public function swepson(ExclusiveOffersRequest $request)
    {
        $user = User::find(3);

        $user->notify(new Notifications\System\BidMade($request->input('name.1'), $request->input('email.1'), $request->input('company.1'), $request->input('bid.1'), $request->input('type.1')));

        return redirect()->route('exclusive-offer.success');
    }

    public function stanlake(ExclusiveOffersRequest $request)
    {
        $user = User::find(3);

        $user->notify(new Notifications\System\BidMade($request->input('name.2'), $request->input('email.2'), $request->input('company.2'), $request->input('bid.2'), $request->input('type.2')));

        return redirect()->route('exclusive-offer.success');
    }

    public function success()
    {
        header("refresh:6;url=/exclusive-deals");
        return view('static.bid-success');
    }
}

<?php

namespace Pickstar\Http\Controllers\Web;

use Pickstar\Http\Controllers\Controller;
use Pickstar\Http\Requests\VerificationPasswordRequest;

class VerificationPasswordController extends Controller
{
    /**
     * Attempt to set a users password after verification.
     *
     * @param \Pickstar\Http\Requests\VerificationPasswordRequest $request
     *
     * @return \Illuminate\View\View|\Illuminate\Http\Response
     */
    public function update(VerificationPasswordRequest $request)
    {
        $this->authorize('update', $request->user());

        if (! $request->session()->has('verified')) {
            return redirect()->home();
        }

        $request->session()->forget('verified');

        $request->user()
            ->fill(['password' => $request->get('password')])
            ->save();

        return response()->redirectToWebApp();
    }
}

<?php

namespace Pickstar\Http\Controllers\Web;

use Pickstar\User\User;
use Illuminate\Http\Request;
use Pickstar\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class VerificationController extends Controller
{
    /**
     * Attempt to verify a user by their verification token.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View|\Illuminate\Http\Response
     */
    public function update(Request $request, $token)
    {
        if ($request->user() && $request->user()->isVerified()) {
            return view('auth.already_verified');
        }

        auth('web')->logout();

        try {
            $user = User::verification($token);

            if ($user->isDeactivated()) {
                return view('auth.verified_but_deactivated');
            }

            auth('web')->login($user, true);

            $token = auth('api')->login($user);

            $request->session()->regenerate();

            $request->session()->put('verified', true);

            return redirect()
                ->route('register.verification.success');
        } catch (ModelNotFoundException $exception) {
            return view('auth.unverified');
        }
    }

    /**
     * Show the verification success page.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View|\Illuminate\Http\Response
     */
    public function success(Request $request)
    {
        if (! $request->session()->has('verified')) {
            return redirect()->home();
        }

        return view('auth.verified');
    }
}

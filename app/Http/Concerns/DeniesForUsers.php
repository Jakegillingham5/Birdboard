<?php

namespace Pickstar\Http\Concerns;

use Pickstar\User\User;

trait DeniesForUsers
{
    /**
     * Deny admin from deleting themselves.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\User\User $deleting
     *
     * @return void
     */
    protected function denyIfAdminDeletingThemselves(User $user, User $deleting)
    {
        if ($user->isAdmin() && $user->id === $deleting->id) {
            $this->deny('Admin is not able to delete themselves.');
        }
    }

    /**
     * Deny user if they have pending bookings.
     *
     * @param \Pickstar\User\User $deleting
     *
     * @return void
     */
    protected function denyIfUserHasPendingBookings(User $deleting)
    {
        if ($deleting->isClient()) {
            if ($deleting->clientBookings()->all()->pending()->count() > 0) {
                $this->deny('Unable to delete user with pending bookings or requests.');
            }
        } elseif ($deleting->isTalent()) {
            // get all the talent pending requests
            $pendingRequests = $deleting->talentBookings()->all()->pending();

            // only if all the pending requests have been shortlisted we can delete the talent
            if (
                $pendingRequests->count() > 0 &&
                $pendingRequests->where('booking_talent.shortlisted', true)->count() !== $pendingRequests->count()
            ) {
                $this->deny('Unable to delete user with pending bookings or requests.');
            }
        }
    }

    /**
     * Deny if a user is already verified.
     *
     * @param \Pickstar\User\User $verifying
     *
     * @return void
     */
    protected function denyIfUserAlreadyVerified(User $verifying)
    {
        if ($verifying->isVerified()) {
            $this->deny('Unable to perform action on an already verified user.');
        }
    }

    /**
     * Deny if user is unverified.
     *
     * @param \Pickstar\User\User $user
     *
     * @return void
     */
    protected function denyIfUserUnverified(User $user)
    {
        if (!$user->isVerified()) {
            $this->deny('Unable to perform action on an unverified user.');
        }
    }

    /**
     * Deny if a user is pending activation.
     *
     * @param \Pickstar\User\User $user
     *
     * @return void
     */
    protected function denyIfUserPendingActivation(User $user)
    {
        if ($user->isPendingActivation()) {
            $this->deny('Unable to perform action on a user pending activation.');
        }
    }

    /**
     * Deny if a user is already activated.
     *
     * @param \Pickstar\User\User $activating
     *
     * @return void
     */
    protected function denyIfUserAlreadyActivated(User $activating)
    {
        if ($activating->isActivated()) {
            $this->deny('Unable to activate an already activated user.');
        }
    }

    /**
     * Deny if a user is already dactivated.
     *
     * @param \Pickstar\User\User $activating
     *
     * @return void
     */
    protected function denyIfUserAlreadyDeactivated(User $deactivating)
    {
        if ($deactivating->isDeactivated()) {
            $this->deny('Unable to deactivate an already deactivated user.');
        }
    }

    /**
     * Deny if a user is already restored.
     *
     * @param \Pickstar\User\User $restoring
     *
     * @return void
     */
    protected function denyIfUserAlreadyRestored(User $restoring)
    {
        if (is_null($restoring->deleted_at)) {
            $this->deny('Unable to restore an already restored user.');
        }
    }

    /**
     * Deny user if they are not a talent.
     *
     * @param \Pickstar\User\User $user
     *
     * @return void
     */
    protected function denyIfUserIsNotTalent(User $user)
    {
        if (!$user->isTalent()) {
            $this->deny('Unable to perform action on a user that is not a talent.');
        }
    }

    /**
     * Deny user if they are not a given role.
     *
     * @param \Pickstar\User\User $user
     * @param array $roles
     *
     * @return void
     */
    protected function denyIfUserIsNotRole(User $user, array $roles)
    {
        if (!in_array($user->role, $roles)) {
            $this->deny(sprintf('Unable to perform action on a user that is not one of the given roles: %s', implode(', ', $roles)));
        }
    }

    /**
     * Deny user if they are not a staff manager.
     *
     * @param \Pickstar\User\User $user
     *
     * @return void
     */
    protected function denyIfUserIsNotStaffManager(User $user)
    {
        if (!$user->isStaffManager()) {
            $this->deny('Unable to perform action on a user that is not a staff manager.');
        }
    }
}

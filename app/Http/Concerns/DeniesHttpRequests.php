<?php

namespace Pickstar\Http\Concerns;

use Symfony\Component\HttpKernel\Exception\HttpException;

trait DeniesHttpRequests
{
    /**
     * Deny the request with an HTTP 400 Bad Request.
     *
     * @param string $message
     *
     * @return void
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    protected function deny($message = 'You cannot perform this action.')
    {
        throw new HttpException(400, $message);
    }
}

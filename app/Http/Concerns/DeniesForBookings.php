<?php

namespace Pickstar\Http\Concerns;

use Carbon\Carbon;
use Pickstar\User\User;
use Pickstar\Booking\Booking;
use Pickstar\Booking\TalentPivot;

trait DeniesForBookings
{
    /**
     * Deny if booking is not scheduled.
     *
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return void
     */
    protected function denyIfBookingIsNotScheduled(Booking $booking)
    {
        if ($booking->status !== Booking::STATUS_SCHEDULED) {
            $this->deny('Unable to perform action on a booking that is not scheduled.');
        }
    }

    /**
     * Deny if booking is not completed.
     *
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return void
     */
    protected function denyIfBookingIsNotCompleted(Booking $booking)
    {
        if ($booking->status !== Booking::STATUS_COMPLETED) {
            $this->deny('Unable to perform action on a booking that is not completed.');
        }
    }

    /**
     * Deny if booking is not pending.
     *
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return void
     */
    protected function denyIfBookingIsNotPending(Booking $booking)
    {
        if ($booking->status !== Booking::STATUS_PENDING) {
            $this->deny('Unable to perform action on a booking that is not pending.');
        }
    }

    /**
     * Deny if booking is not booked.
     *
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return void
     */
    protected function denyIfBookingIsNotBooked(Booking $booking)
    {
        if ($booking->status !== Booking::STATUS_BOOKED) {
            $this->deny('Unable to perform action on a booking that is not booked.');
        }
    }

    /**
     * Deny if booking is completed.
     *
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return void
     */
    protected function denyIfBookingIsCompleted(Booking $booking)
    {
        if ($booking->status === Booking::STATUS_COMPLETED) {
            $this->deny('Unable to perform action on a booking that is completed.');
        }
    }

    /**
     * Deny if booking is cancelled.
     *
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return void
     */
    protected function denyIfBookingIsCancelled(Booking $booking)
    {
        if ($booking->status === Booking::STATUS_CANCELLED) {
            $this->deny('Unable to perform action on a booking that is cancelled.');
        }
    }

    /**
     * Deny if booking is not cancelled.
     *
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return void
     */
    protected function denyIfBookingIsNotCancelled(Booking $booking)
    {
        if ($booking->status !== Booking::STATUS_CANCELLED) {
            $this->deny('Unable to perform action on a booking that is not cancelled.');
        }
    }

    /**
     * Deny if booking is withdrawn.
     *
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return void
     */
    protected function denyIfBookingIsWithdrawn(Booking $booking)
    {
        if ($booking->status === Booking::STATUS_WITHDRAWN) {
            $this->deny('Unable to perform action on a booking that is withdrawn.');
        }
    }

    /**
     * Deny if booking is not withdrawn.
     *
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return void
     */
    protected function denyIfBookingIsNotWithdrawn(Booking $booking)
    {
        if ($booking->status !== Booking::STATUS_WITHDRAWN) {
            $this->deny('Unable to perform action on a booking that is not withdrawn.');
        }
    }

    /**
     * Deny if booking is not withdrawn.
     *
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return void
     */
    protected function denyIfBookingIsNotWithdrawnOrDeclined(Booking $booking)
    {
        if ($booking->status !== Booking::STATUS_WITHDRAWN && $booking->status !== Booking::STATUS_DECLINED) {
            $this->deny('Unable to perform action on a booking that is not withdrawn or declined.');
        }
    }

    /**
     * Deny if booking is on hold.
     *
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return void
     */
    protected function denyIfBookingIsOnHold(Booking $booking)
    {
        if ($booking->status === Booking::STATUS_HOLD) {
            $this->deny('Unable to perform action on a booking that is on hold.');
        }
    }

    /**
     * Deny if booking is not on hold.
     *
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return void
     */
    protected function denyIfBookingIsNotOnHold(Booking $booking)
    {
        if ($booking->status !== Booking::STATUS_HOLD) {
            $this->deny('Unable to perform action on a booking that is not on hold.');
        }
    }

    /**
     * Deny if booking is declined.
     *
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return void
     */
    protected function denyIfBookingIsDeclined(Booking $booking)
    {
        if ($booking->status === Booking::STATUS_DECLINED) {
            $this->deny('Unable to perform action on a booking that is declined.');
        }
    }

    /**
     * Deny if booking is declined.
     *
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return void
     */
    protected function denyIfBookingIsExpired(Booking $booking)
    {
        if ($booking->date && $booking->date < Carbon::now()) {
            $this->deny('Unable to perform action on a booking that is expired.');
        }
    }

    /**
     * Deny if talent cannot comment.
     *
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return void
     */
    protected function denyIfTalentCannotComment(Booking $booking)
    {
        if (!$booking->talent_can_comment) {
            $this->deny('You are unable to comment on this booking.');
        }
    }

    /**
     * Deny if booking is not booked.
     *
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return void
     */
    protected function denyIfBookingIsNotOnline(Booking $booking)
    {
        if ($booking->appearance_type !== Booking::APPEARANCE_BOTH && $booking->appearance_type !== Booking::APPEARANCE_ONLINE) {
            $this->deny('Unable to perform action on a booking that is not an online appearance type.');
        }
    }

    /**
     * Deny if booking is not public (on the billboard).
     *
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return void
     */
    protected function denyIfBookingIsNotPublic(Booking $booking)
    {
        if ($booking->others_can_apply === false) {
            $this->deny('Unable to perform action on a booking that is not public.');
        }
    }

    /**
     * Deny if the booking date is not the same as todays date or after.
     *
     * Do not deny if no date OR if booking appearance type is strictly online only.
     *
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return void
     */
    protected function denyIfNotDayOfBooking(Booking $booking)
    {
        if (! $booking->tz_date || $booking->appearance_type === Booking::APPEARANCE_ONLINE) {
            return;
        }

        if (! now($booking->timezone)->startOfDay()->gte(Carbon::parse($booking->tz_date, $booking->timezone)->startOfDay())) {
            $this->deny('Unable to perform action when not the day of the booking.');
        }
    }

    /**
     * Deny if the booking date is not within 24 hours from the current date.
     *
     * Do not deny if no date OR if booking appearance type is strictly online only.
     *
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return void
     */
    protected function denyIfCannotConfirmAttendanceBasedOnDate(Booking $booking)
    {
        if (! $booking->tz_date || $booking->appearance_type === Booking::APPEARANCE_ONLINE) {
            return;
        }

        if (now($booking->timezone)->addHours(24)->lte(Carbon::parse($booking->tz_date, $booking->timezone))) {
            $this->deny('Unable to confirm attendance outside of the 24 hour timeframe.');
        }
    }

    /**
     * Deny if talent is not attending the booking.
     *
     * @param \Pickstar\Booking\Booking $booking
     * @param \Pickstar\User\User $user
     *
     * @return void
     */
    protected function denyIfTalentNotAttendingBooking(Booking $booking, User $user)
    {
        $talent = $booking->talent()->find($user->id);

        if (
            ! $talent ||
            $talent->response->status !== TalentPivot::STATUS_APPLIED ||
            $talent->response->client_accepted === false
        ) {
            $this->deny('Unable to confirm attendance for a talent that is not attending.');
        }
    }

    /**
     * Deny if talent is already applied
     *
     * @param \Pickstar\Booking\Booking $booking
     * @param \Pickstar\User\User $user
     *
     * @return void
     */
    protected function denyIfTalentHasAlreadyApplied(Booking $booking, User $user)
    {
        if ($booking->talentApplied->pluck('id')->contains($user->id)) {
            $this->deny('Unable to apply for talent that have already applied.');
        }
    }

    /**
     * Deny if talent does not have at least one tag the booking has
     *
     * @param \Pickstar\Booking\Booking $booking
     * @param \Pickstar\User\User $user
     *
     * @return void
     */
    protected function denyIfTalentDoesNotHaveTag(Booking $booking, User $user)
    {
        if ($booking->talent->contains($user)) {
            return;
        }
        if ($booking->tags->count()) {
            foreach ($booking->tags as $tag) {
                if ($user->tags->pluck('id')->contains($tag->id)) {
                    return;
                }
            }
            $this->deny('Unable to apply for talent that does not have the appropriate tags.');
        }
    }

    /**
     * Deny if the booking terms are already accepted.
     *
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return void
     */
    protected function denyIfBookingTermsAlreadyAccepted(Booking $booking)
    {
        if ($booking->client_accepted_terms === true) {
            $this->deny('Unable to accept terms and conditions when they are already accepted.');
        }
    }

    /**
     * Deny if the booking terms are already accepted.
     *
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return void
     */
    protected function denyIfBookingTermsDeniedEmailSent(Booking $booking)
    {
        if ($booking->notification->client_declined_terms == true) {
            $this->deny('Unable to send client denied terms and conditions email more than once.');
        }
    }

    /**
     * Deny if the booking terms are not accepted.
     *
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return void
     */
    protected function denyIfBookingTermsNotAccepted(Booking $booking)
    {
        if ($booking->client_accepted_terms === false) {
            $this->deny('Unable to perform action when booking terms and conditions are not accepted.');
        }
    }

    /**
     * Deny if the booking is a request
     *
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return void
     */
    protected function denyIfBookingTypeIsRequest(Booking $booking)
    {
        if ($booking->type === Booking::TYPE_REQUEST) {
            $this->deny('Unable to perform action when booking is in the request stage.');
        }
    }

    /**
     * Deny if the booking is a request
     *
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return void
     */
    protected function denyIfBookingTypeIsBooking(Booking $booking)
    {
        if ($booking->type === Booking::TYPE_BOOKING) {
            $this->deny('Unable to perform action when booking is in the booking stage.');
        }
    }
}

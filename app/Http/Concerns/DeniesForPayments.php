<?php

namespace Pickstar\Http\Concerns;

use Pickstar\Booking\Booking;
use Pickstar\Payment\Payment;
use Illuminate\Support\Collection;

trait DeniesForPayments
{
    /**
     * Deny if there are no payments to process.
     *
     * @param \Illuminate\Support\Collection $payments
     *
     * @return void
     */
    public function denyIfNoPaymentsToProcess(Collection $payments)
    {
        if ($payments->isEmpty()) {
            $this->deny('Unable to process when there are no payments.');
        }
    }

    /**
     * Deny if attempting to process a final payment before the deposit.
     *
     * @param \Illuminate\Support\Collection $payments
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return void
     */
    public function denyIfProcessingFinalPaymentBeforeDeposit(Collection $payments, Booking $booking)
    {
        $deposit = $booking->payments->firstWhere('type', Payment::TYPE_DEPOSIT);
        $final = $booking->payments->firstWhere('type', Payment::TYPE_FINAL);

        if ($deposit->status !== Payment::STATUS_PAID) {
            if ($payments->contains($final) && ! $payments->contains($deposit)) {
                $this->deny('Unable to process final payment until deposit has been paid.');
            }
        }
    }

    /**
     * Deny if attempting to send a final payment before the deposit.
     *
     * @param \Illuminate\Support\Collection $payments
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return void
     */
    public function denyIfSendingFinalPaymentBeforeDeposit(Collection $payments, Booking $booking)
    {
        $deposit = $booking->payments->firstWhere('type', Payment::TYPE_DEPOSIT);
        $final = $booking->payments->firstWhere('type', Payment::TYPE_FINAL);

        if ($deposit->status === Payment::STATUS_UNPAID) {
            if ($payments->contains($final) && ! $payments->contains($deposit)) {
                $this->deny('Unable to process final payment until deposit has been sent.');
            }
        }
    }
}

<?php

namespace Pickstar\Http\Transformers;

use Pickstar\User\User;
use Pickstar\Booking\Booking;

class BookingTransformer
{
    /**
     * Transform a booking for API consumption.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return array
     */
    public function transform(Booking $booking, User $user) : array
    {
        // If user is a talent/manager then restrict some column visibility as it may
        // contain some sensitive information that they cannot see.
        if ($user->isTalent() || $user->isManager()) {
            $booking->client->addVisible([
                'id',
                'first_name',
                'last_name',
                'name',
                'company'
            ]);

            if (
                $booking->talent->first(function ($talent) use ($user) {
                    return ($talent->id === $user->id || $talent->managedBy($user)) &&
                        $talent->response->client_accepted == true;
                })
            ) {
                $booking->client->addVisible([
                    'email',
                    'phone'
                ]);
            }

            $booking->payments->each(function ($payment) {
                $payment->addVisible([
                    'status',
                    'type'
                ]);
            });

            $booking->addHidden([
                'budget',
                'budget_per_star',
                'commission_rate',
                'commission_dollar',
                'commission_type',
                'client_accepted_terms',
                'client_accepted_terms_date',
                'coupon'
            ]);
        }

        if (!$user->isAdmin()) {
            foreach (['talentNotified', 'talentUnconfirmed', 'talentAttending', 'talentApplied', 'talentShortlisted'] as $relation) {
                if ($booking->relationLoaded($relation)) {
                    $booking->unsetRelation($relation);
                }
            }
            $booking->addHidden(['market_segment', 'admin_opportunity_id']);
        }

        if ($user->isAdmin()) {
            $booking->append('latest_note');
        }

        // If the user is a client make sure we hide any personal information regarding talent
        // that have applied or have been shortlisted for the booking. Only return the
        // absolute necessities.
        if ($user->isClient()) {
            $booking->talent->map(function ($talent) use ($user) {
                return $talent->transform([new TalentTransformer, 'transform'], [$user]);
            });
        }

        return $booking->toArray();
    }
}

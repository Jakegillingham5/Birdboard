<?php

namespace Pickstar\Http\Transformers;

use Pickstar\User\User;

class UserTransformer
{
    /**
     * Transform a user for API consumption.
     *
     * @param \Pickstar\User\User $user
     *
     * @return array
     */
    public function transform(User $user, User $authenticated) : array
    {
        $user->locations->transform(function ($location) {
            return $location->id;
        });

        $user->opportunities->transform(function ($opportunity) {
            return $opportunity->id;
        });

        // If our user is a talent and we have the profile relationship loaded then we'll automatically
        // append the profile completion percentage.
        if ($user->isTalent() && $user->relationLoaded('profile')) {
            $user->append('profile_completion_percentage');
        }

        if (!$authenticated->isAdmin()) {
            $user->addHidden(['commission_rate', 'commission_dollar', 'commission_type']);
        } else {
            $user->append('market_segments');
        }

        $user = $user->toArray();
        $user['profile']['profile_image_url'] = $user['profile_image_url'];
        return $user;
    }
}

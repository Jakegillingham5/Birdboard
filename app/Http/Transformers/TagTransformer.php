<?php

namespace Pickstar\Http\Transformers;

use Pickstar\Tag\Tag;
use Pickstar\User\User;

class TagTransformer
{
    /**
     * Transform a tag for API consumption.
     *
     * @param \Pickstar\Tag\Tag $tag
     * @param \Pickstar\User\User $user
     *
     * @return array
     */
    public function transform(Tag $tag, User $user = null): array
    {
        if (is_null($user) || $user->isTalent() || $user->isClient()) {
            $tag->addHidden('occurances');
        }

        return $tag->toArray();
    }
}

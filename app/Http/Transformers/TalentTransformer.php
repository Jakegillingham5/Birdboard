<?php

namespace Pickstar\Http\Transformers;

use Pickstar\User\User;

class TalentTransformer
{
    /**
     * Transform a talent for API consumption.
     *
     * @param \Pickstar\User\User $talent
     * @param \Pickstar\User\User $user
     *
     * @return array
     */
    public function transform(User $talent, User $user = null): array
    {
        // If the talent is the same user as the authenticated user we'll just return them as they are.
        // Same applies of the authenticated user is an administrator.
        if ($user && ($user->id === $talent->id || $talent->isManagedBy($user) || $user->isAdmin())) {
            return $talent->toArray();
        }

        $talent->addVisible([
            'profile_image_url'
        ]);

        if (! $user || $user->isClient() || $user->isManager() || $user->isTalent()) {
            $talent->addVisible([
                'id',
                'name',
                'first_name',
                'last_name',
                'slug',
                'profile',
                'response',
                'sport_club',
                'is_featured',
                'photos_count',
                'profile_image_present',
                'based_in'
            ]);

            if ($talent->relationLoaded('profile')) {
                $talent->profile->addVisible([
                    'id',
                    'tagline',
                    'highest_followers',
                    'highest_followers_icon'
                ]);
            }

            if ($talent->relationLoaded('response')) {
                $talent->response->addHidden([
                    'decline_reason'
                ]);
            }
        }

        return $talent->toArray();
    }
}

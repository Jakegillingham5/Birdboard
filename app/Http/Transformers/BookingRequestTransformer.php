<?php

namespace Pickstar\Http\Transformers;

use Pickstar\User\User;
use Pickstar\Booking\Booking;

class BookingRequestTransformer extends BookingTransformer
{
    /**
     * Transform a booking for API consumption.
     *
     * @param \Pickstar\User\User $user
     * @param \Pickstar\Booking\Booking $booking
     *
     * @return array
     */
    public function transform(Booking $booking, User $user): array
    {
        if ($user->isTalent() || $user->isClient() || $user->isManager()) {
            $booking->addHidden([
                'talentNotified'
            ]);
        }

        if ($booking->typeIsRequest() && ($user->isTalent() || $user->isManager())) {
            $booking->client->addHidden([
                'first_name',
                'last_name',
                'name'
            ]);
        }

        if (!$user->isAdmin()) {
            foreach (['talentNotified', 'talentUnconfirmed', 'talentApplied', 'talentShortlisted'] as $relation) {
                if ($booking->relationLoaded($relation)) {
                    $booking->unsetRelation($relation);
                }
            }
        }


        return parent::transform($booking, $user);
    }
}

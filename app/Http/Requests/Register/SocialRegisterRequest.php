<?php

namespace Pickstar\Http\Requests\Register;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class SocialRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => ['required', 'email', Rule::unique('users')->ignore($this->user()->id)],
            'phone' => 'required|mobile',
            'role' => 'required|in:client,talent,manager',
            'gender' => 'required_if:role,talent',
            'company' => 'required_if:role,manager',
            'phone' => 'required|mobile',
            'terms_and_conditions' => 'accepted'
        ];
    }
}

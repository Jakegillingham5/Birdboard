<?php

namespace Pickstar\Http\Requests\Register;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class TalentRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'gender' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => ['required', 'email', Rule::unique('users')->where(function ($query) {
                // Intentionally skip the soft deleted users as we'll check them in the controller so
                // we can provider better error messages.
                return $query->whereNull('deleted_at');
            })],
            'phone' => 'required|mobile',
            'password' => 'required|password|confirmed',
            'password_confirmation' => 'required',
            'terms_and_conditions' => 'accepted'
        ];
    }

    public function withValidator(Validator $validator)
    {
        $validator->sometimes('recaptcha_response', 'required|recaptcha', function () {
            return env('APP_ENV') !== 'testing';
        });
    }
}

<?php

namespace Pickstar\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class ExclusiveOffersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'email.*' => 'required|email',
                'name.*' => 'required',
                'bid.*' => 'required'
        ];
    }

    public function withValidator(Validator $validator)
    {
        $validator->sometimes('recaptcha_response', 'required|recaptcha', function () {
            return env('APP_ENV') !== 'testing' && env('APP_ENV') !== 'local';
        });
    }
}

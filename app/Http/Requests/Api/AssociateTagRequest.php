<?php

namespace Pickstar\Http\Requests\Api;

use Pickstar\Tag\Tag;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class AssociateTagRequest extends FormRequest
{
    /**
     * Only admin's can associate a tag
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('associate', Tag::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }
}

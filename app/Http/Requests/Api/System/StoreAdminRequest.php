<?php

namespace Pickstar\Http\Requests\Api\System;

use Pickstar\User\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class StoreAdminRequest extends FormRequest
{
    /**
     * Authorize a request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('createAdmin', User::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => ['required', 'email', Rule::unique('users')]
        ];
    }
}

<?php

namespace Pickstar\Http\Requests\Api\System;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Http\FormRequest;

class ImportTalentSocialMediaCsvRequest extends FormRequest
{
    /**
     * Admins can import talent CSVs.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('import-talent-social-media-csv');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'csv' => 'required|file'
        ];
    }
}

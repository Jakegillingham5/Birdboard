<?php

namespace Pickstar\Http\Requests\Api;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class UpdateUserRequest extends FormRequest
{
    /**
     * Authorize incoming request.
     *
     * @return boolean
     */
    public function authorize()
    {
        return $this->user()->can('update', $this->route('user'));
    }

    /**
     * Validation rules.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'sometimes|required',
            'last_name' => 'sometimes|required',
            'phone' => 'sometimes|required|mobile',
            'email' => ['sometimes', 'required', 'email', Rule::unique('users')->ignore($this->route('user')->id)],
            'profile.facebook_link' => 'sometimes|domain:facebook.com',
            'profile.twitter_link' => 'sometimes|domain:twitter.com',
            'profile.instagram_link' => 'sometimes|domain:instagram.com',
            'profile.linkedin_link' => 'sometimes|domain:linkedin.com',
            'profile.public' => 'sometimes|required'
        ];
    }

    /**
     * Conditional validation rules.
     *
     * @param \Illuminate\Contracts\Validation\Validator $validator
     *
     * @return void
     */
    public function withValidator(Validator $validator)
    {
        $validator->sometimes('commission_rate', 'numeric|between:0,100', function () {
            return $this->filled('commission_rate') &&
                ($this->route('user')->isTalent() || $this->route('user')->isManager());
        });

        $validator->sometimes('company', 'required', function () {
            return $this->has('company') && $this->route('user')->isManager();
        });

        // Talent profile validation that is conditionally required based on user and whether or
        // not the fields have been filled.
        $validator->sometimes('profile.date_featured_until', 'date_format:Y-m-d H:i:s', function () {
            return $this->filled('profile.date_featured_until') && $this->route('user')->isTalent();
        });

        $validator->sometimes('profile.notification_preference', 'numeric|in:0,1,2', function () {
            return $this->has('profile.notification_preference') && $this->route('user')->isTalent();
        });

        $validator->sometimes('profile.tagline', 'max:30', function () {
            return $this->filled('profile.tagline') && $this->route('user')->isTalent();
        });

        $validator->sometimes('profile.gender', 'required|in:male,female', function () {
            return $this->has('profile.gender') && $this->route('user')->isTalent();
        });
    }
}

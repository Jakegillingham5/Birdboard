<?php

namespace Pickstar\Http\Requests\Api\Booking;

class UpdateVettingBookingRequestRequest extends UpdateBookingRequestRequest
{
    /**
     * Authorize incoming request.
     *
     * @return boolean
     */
    public function authorize()
    {
        return $this->user()->can('updateVettingRequest', $this->route('vetting'));
    }
}

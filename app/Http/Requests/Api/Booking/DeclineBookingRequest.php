<?php

namespace Pickstar\Http\Requests\Api\Booking;

use Illuminate\Foundation\Http\FormRequest;

class DeclineBookingRequest extends FormRequest
{
    /**
     * Authorize incoming request.
     *
     * @return boolean
     */
    public function authorize()
    {
        return $this->user()->can('respondRequest', $this->route('request'));
    }

    /**
     * Validation rules.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'reason' => 'required|in:budget,scheduling,information,interest,app'
        ];
    }
}

<?php

namespace Pickstar\Http\Requests\Api\Booking;

use Pickstar\Booking\Booking;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Pickstar\Validation\Rules\NotEmail;
use Pickstar\Validation\Rules\NotPhone;
use Pickstar\Validation\Rules\NotBudget;

class ApplyRequestRequest extends FormRequest
{
    /**
     * Authorize incoming request.
     *
     * @return boolean
     */
    public function authorize()
    {
        return $this->user()->can('respondRequest', $this->route('request'));
    }

    /**
     * Validation rules.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'comment' => [new NotEmail, new NotBudget, new NotPhone]
        ];
    }
}

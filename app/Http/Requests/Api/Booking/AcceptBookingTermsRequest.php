<?php

namespace Pickstar\Http\Requests\Api\Booking;

use Pickstar\Booking\Booking;
use Illuminate\Foundation\Http\FormRequest;

class AcceptBookingTermsRequest extends FormRequest
{
    /**
     * Authorize incoming request.
     *
     * @return boolean
     */
    public function authorize()
    {
        return $this->user()->can('acceptTerms', $this->route('bookingOrRequest'));
    }

    /**
     * Validation rules.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'terms' => 'required|filled|array',
            'terms.*' => 'accepted'
        ];
    }
}

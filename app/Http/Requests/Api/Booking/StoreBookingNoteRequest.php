<?php

namespace Pickstar\Http\Requests\Api\Booking;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class StoreBookingNoteRequest extends FormRequest
{
    /**
     * Authorize incoming request.
     *
     * @return boolean
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Validation rules.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'text' => 'required',
            'user_id' => 'required'
        ];
    }
}

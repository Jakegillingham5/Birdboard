<?php

namespace Pickstar\Http\Requests\Api\Booking;

use Pickstar\Booking\Booking;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateBookingRequestRequest extends FormRequest
{
    /**
     * Authorize incoming request.
     *
     * @return boolean
     */
    public function authorize()
    {
        return $this->user()->can('updateRequest', $this->route('request'));
    }

    /**
     * Validation rules.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'appearance_type' => ['required', Rule::in([Booking::APPEARANCE_IN_PERSON, Booking::APPEARANCE_ONLINE, Booking::APPEARANCE_BOTH])]
        ];
    }
}

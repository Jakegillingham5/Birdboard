<?php

namespace Pickstar\Http\Requests\Api\Booking;

use Pickstar\Payment\Payment;
use Illuminate\Contracts\Validation\Validator;

class UpdateBookingRequest extends UpdateBookingRequestRequest
{
    /**
     * Authorize incoming request.
     *
     * @return boolean
     */
    public function authorize()
    {
        return $this->user()->can('update', $this->route('booking'));
    }

    /**
     * Conditional validation rules.
     *
     * @param \Illuminate\Contracts\Validation\Validator $validator
     *
     * @return void
     */
    public function withValidator(Validator $validator)
    {
        // Add a custom validation rule to determine if the budget is going to be less than the deposit if
        // the deposit is already paid. We can't have them making the total less than 0 as it will not be
        // able to be paid for.
        $validator->after(function ($validator) {
            $booking = $this->route('booking');

            $budget = $booking->talentAttending->count() * $this->input('budget_per_star', $booking->budget_per_star);
            $deposit = $booking->payments->firstWhere('type', Payment::TYPE_DEPOSIT);

            if ($deposit && $deposit->status === Payment::STATUS_PAID && $budget <= $deposit->amount) {
                $validator->errors()->add('budget_per_star', 'Budget is less than the deposit amount already paid.');
            }
        });
    }
}

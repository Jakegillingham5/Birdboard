<?php

namespace Pickstar\Http\Requests\Api\Booking;

use Illuminate\Validation\Rule;
use Pickstar\Booking\TalentPivot;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class UpdateTalentResponseRequest extends FormRequest
{
    /**
     * Authorize incoming request.
     *
     * @return boolean
     */
    public function authorize()
    {
        return $this->user()->can('updateTalent', $this->route('request'));
    }

    /**
     * Validation rules.
     *
     * @return array
     */
    public function rules()
    {
        return ['status' => 'required'];
    }

    /**
     * Conditional validation rules.
     *
     * @param \Illuminate\Contracts\Validation\Validator $validator
     *
     * @return void
     */
    public function withValidator(Validator $validator)
    {
        $validator->sometimes(
            'status',
            Rule::in([
                TalentPivot::STATUS_HIDDEN,
                TalentPivot::STATUS_PENDING,
                TalentPivot::STATUS_APPLIED,
                TalentPivot::STATUS_DECLINED,
                TalentPivot::STATUS_REVIEWING
            ]),
            function () {
                return $this->user()->isAdmin();
            }
        );

        $validator->sometimes(
            'status',
            Rule::in([
                TalentPivot::STATUS_APPLIED,
                TalentPivot::STATUS_DECLINED,
                TalentPivot::STATUS_REVIEWING
            ]),
            function () {
                return $this->user()->isManager();
            }
        );
    }
}

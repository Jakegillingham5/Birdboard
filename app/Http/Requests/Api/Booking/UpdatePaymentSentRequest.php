<?php

namespace Pickstar\Http\Requests\Api\Booking;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class UpdatePaymentSentRequest extends FormRequest
{
    /**
     * Authorize the request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('processPayment', $this->route('booking'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'payments' => 'required|array'
        ];
    }
}

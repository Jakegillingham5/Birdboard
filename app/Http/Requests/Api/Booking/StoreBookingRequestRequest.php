<?php

namespace Pickstar\Http\Requests\Api\Booking;

use Pickstar\Booking\Booking;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class StoreBookingRequestRequest extends FormRequest
{
    /**
     * Authorize incoming request.
     *
     * @return boolean
     */
    public function authorize()
    {
        if ($this->user()) {
            return $this->user()->can('create', Booking::class);
        }

        return true;
    }

    /**
     * Validation rules.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'budget_per_star' => ['required', 'numeric', 'min:1'],
            'appearance_type' => ['required', Rule::in([Booking::APPEARANCE_IN_PERSON, Booking::APPEARANCE_ONLINE, Booking::APPEARANCE_BOTH])],
        ];
    }

    public function withValidator(Validator $validator)
    {
        $validator->sometimes('shortlist.*.id', 'required', function () {
            return $this->filled('shortlist');
        });

        $validator->sometimes('opportunity_id', ['required', Rule::exists('opportunities', 'id')], function () {
            return in_array($this->input('appearance_type'), [Booking::APPEARANCE_IN_PERSON, Booking::APPEARANCE_BOTH]);
        });

        $validator->sometimes('online_opportunity_id', ['required', Rule::exists('opportunities', 'id')], function () {
            return in_array($this->input('appearance_type'), [Booking::APPEARANCE_ONLINE, Booking::APPEARANCE_BOTH]);
        });

        $validator->sometimes('complete_by_date', ['required'], function () {
            return in_array($this->input('appearance_type'), [Booking::APPEARANCE_ONLINE, Booking::APPEARANCE_BOTH]);
        });

        // The following fields will be required when the user is not authenticated. This is for
        // when a brand new user is creating a new request and they have filled out the
        // details at the bottom of the request form.
        $requiredWhenNotAuthenticated = function () {
            return $this->user() === null;
        };

        $validator->sometimes('first_name', 'required', $requiredWhenNotAuthenticated);
        //$validator->sometimes('last_name', 'required', $requiredWhenNotAuthenticated);
        $validator->sometimes('phone', 'required', $requiredWhenNotAuthenticated);
        $validator->sometimes('password', 'required|confirmed', $requiredWhenNotAuthenticated);
        $validator->sometimes(
            'email',
            ['required', Rule::unique('users')->where(function ($query) {
                return $query->whereNull('deleted_at');
            })],
            $requiredWhenNotAuthenticated
        );
    }
}

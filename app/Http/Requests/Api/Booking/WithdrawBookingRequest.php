<?php

namespace Pickstar\Http\Requests\Api\Booking;

use Illuminate\Foundation\Http\FormRequest;

class WithdrawBookingRequest extends FormRequest
{
    /**
     * Authorize incoming request.
     *
     * @return boolean
     */
    public function authorize()
    {
        return $this->user()->can('withdrawRequest', $this->route('request'));
    }

    /**
     * Validation rules.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'reason' => 'required|in:call,not_proceeding,budget,applicants,elsewhere,pickstar,talent_uninterested,client_uninterested,test'
        ];
    }
}

<?php

namespace Pickstar\Http\Requests\Api\Booking;

use Illuminate\Foundation\Http\FormRequest;

class ReactivateBookingRequest extends FormRequest
{
    /**
     * Authorize incoming request.
     *
     * @return boolean
     */
    public function authorize()
    {
        return $this->user()->can('reactivateRequest', $this->route('request'));
    }

    /**
     * Validation rules.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }
}

<?php

namespace Pickstar\Http\Requests\Api\Booking;

use Illuminate\Validation\Rule;
use Pickstar\Marketing\Source;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class UpdateSourceRequest extends FormRequest
{
    /**
     * Authorize incoming request.
     *
     * @return boolean
     */
    public function authorize()
    {
        return $this->user()->can('updateSource', Source::class);
    }

    /**
     * Validation rules.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * Conditional validation rules.
     *
     * @param \Illuminate\Contracts\Validation\Validator $validator
     *
     * @return void
     */
    public function withValidator(Validator $validator)
    {
        $validator->sometimes(
            'campaign_admin_source',
            Rule::in([
                Source::ADMIN_SOURCE_FACEBOOK_ADS,
                Source::ADMIN_SOURCE_GOOGLE,
                Source::ADMIN_SOURCE_LINKEDIN,
                Source::ADMIN_SOURCE_INSTAGRAM,
                Source::ADMIN_SOURCE_NEWSLETTER,
                Source::ADMIN_SOURCE_TELEVISION,
                Source::ADMIN_SOURCE_REFERRAL,
                Source::ADMIN_SOURCE_PREVIOUS_CLIENT,
                Source::ADMIN_SOURCE_INFLUENCER,
                Source::ADMIN_SOURCE_OUTBOUND,
                null
            ]),
            function () {
                return $this->user()->isAdmin();
            }
        );
    }
}

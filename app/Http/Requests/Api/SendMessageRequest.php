<?php

namespace Pickstar\Http\Requests\Api;

use Pickstar\Message\Thread;
use Pickstar\Tag\Tag;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Pickstar\Validation\Rules\NoSwearWords;
use Pickstar\Validation\Rules\NotEmail;
use Pickstar\Validation\Rules\NotPhone;

class SendMessageRequest extends FormRequest
{
    /**
     * Anyone can add a tag.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('sendMessage', $this->route('thread'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'text' => [new NoSwearWords, new NotEmail, new NotPhone]
        ];
    }
}

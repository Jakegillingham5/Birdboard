<?php

namespace Pickstar\Http\Requests\Api;

use Pickstar\Tag\Tag;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class MergeTagsRequest extends FormRequest
{
    /**
     * Anyone can add a tag.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('merge', Tag::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required',
            'name' => ['required', Rule::unique('tags')->where(function ($query) {
                $query->where('type', $this->input('type'))
                    ->whereNotIn('id', $this->input('tags'))
                    ->whereNull('deleted_at');
            })],
            'tags' => ['required', 'array', 'min:2']
        ];
    }
}

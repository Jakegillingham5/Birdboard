<?php

namespace Pickstar\Http\Requests\Api\Talent;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class UpdatePaymentDetailsRequest extends FormRequest
{
    /**
     * Authorize the request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('update', $this->route('user'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * Conditional validation rules.
     *
     * @param \Illuminate\Contracts\Validation\Validator $validator
     *
     * @return void
     */
    public function withValidator(Validator $validator)
    {
        // account number rate sometimes needs to be a numeric between 0 and 100 when it is filled out.
        $validator->sometimes('account_number', 'numeric', function () {
            return $this->filled('account_number');
        });
    }
}

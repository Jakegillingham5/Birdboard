<?php

namespace Pickstar\Http\Requests\Api\Talent;

class UpdateEndorsementRequest extends StoreEndorsementRequest
{
    /**
     * Authorize incoming request.
     *
     * @return boolean
     */
    public function authorize()
    {
        return $this->user()->can('update', $this->route('endorsement'));
    }
}

<?php

namespace Pickstar\Http\Requests\Api\Talent;

use Pickstar\User\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class StoreTalentRequest extends FormRequest
{
    /**
     * Authorize a request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('createTalent', User::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required|mobile',
            'email' => ['required', 'email', Rule::unique('users')],
            'profile.gender' => 'required|in:male,female'
        ];
    }
}

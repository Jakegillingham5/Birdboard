<?php

namespace Pickstar\Http\Requests\Api\Talent;

use Pickstar\Talent\Endorsement;
use Illuminate\Foundation\Http\FormRequest;

class StoreEndorsementRequest extends FormRequest
{
    /**
     * Authorize incoming request.
     *
     * @return boolean
     */
    public function authorize()
    {
        return $this->user()->can('create', Endorsement::class);
    }

    /**
     * Validation rules.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_name' => 'required',
            'date_endorsed' => 'required|date',
            'description' => 'required'
        ];
    }
}

<?php

namespace Pickstar\Http\Requests\Api\Talent;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Pickstar\Booking\Booking;

class UpdateMarketingPhotoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = $this->user();
        $talentUserID = $this->route('user')->id;

        // If user is client and request is GET, then check to see if client has booking which talent is attending
        if ($user->isClient() && $this->isMethod('get')) {
            return ($user->clientBookings()
                ->with('talentAttending')
                ->get()
                ->pluck('talentAttending')
                ->collapse()
                ->pluck('id')
                ->unique()
                ->contains($talentUserID));
        } else {
            return $user->can('update', $this->route('user'));
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'photoUrl' => 'sometimes|string',
            'newPhotoFile' => 'sometimes|mimes:jpeg,png'
        ];
    }
}

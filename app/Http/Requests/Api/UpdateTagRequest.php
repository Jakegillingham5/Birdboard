<?php

namespace Pickstar\Http\Requests\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateTagRequest extends FormRequest
{
    /**
     * Anyone can add a tag.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('update', $this->route('tag'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required',
            'name' => ['required', Rule::unique('tags')->where(function ($query) {
                $query->where('type', $this->input('type'))
                    ->where('id', '<>', $this->route('tag')->id)
                    ->whereNull('deleted_at');
            })]
        ];
    }
}

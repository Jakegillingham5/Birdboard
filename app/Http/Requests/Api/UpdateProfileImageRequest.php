<?php

namespace Pickstar\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class UpdateProfileImageRequest extends FormRequest
{
    /**
     * Authorize the request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('update', $this->route('user'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'profile_image_crop.x' => 'required|numeric',
            'profile_image_crop.y' => 'required|numeric',
            'profile_image_crop.width' => 'required|numeric',
            'profile_image_crop.height' => 'required|numeric'
        ];
    }

    /**
     * Conditional validation rules.
     *
     * @param \Illuminate\Contracts\Validation\Validator $validator
     *
     * @return void
     */
    public function withValidator(Validator $validator)
    {
        $validator->sometimes('profile_image', 'mimes:jpeg,png,webp', function () {
            return $this->filled('profile_image');
        });
    }
}

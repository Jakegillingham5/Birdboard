<?php

namespace Pickstar\Http\Requests\Api\Manager;

use Pickstar\User\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class StoreManagerRequest extends FormRequest
{
    /**
     * Authorize a request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('createManager', User::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required|mobile',
            'email' => ['required', 'email', Rule::unique('users')],
            'company' => 'required'
        ];
    }
}

<?php

namespace Pickstar\Http;

use Spatie\Cors\CorsProfile\DefaultProfile;

class CorsProfile extends DefaultProfile
{
    /**
     * Gets the specified key from the environment configuration file.
     *
     * @param string $key
     *
     * @return mixed
     */
    protected function environmentCorsConfig($key)
    {
        $config = 'cors.%s.%s';

        $default = config(sprintf($config, 'local', $key));

        return config(sprintf($config, app()->environment(), $key), $default);
    }

    /**
     * Gets the "allow_origins" setting from the CORs configuration.
     *
     * @return array
     */
    public function allowOrigins(): array
    {
        return $this->environmentCorsConfig('allow_origins');
    }

    /**
     * Gets the "allow_methods" setting from the CORs configuration.
     *
     * @return array
     */
    public function allowMethods(): array
    {
        return $this->environmentCorsConfig('allow_methods');
    }

    /**
     * Gets the "allow_headers" setting from the CORs configuration.
     *
     * @return array
     */
    public function allowHeaders(): array
    {
        return $this->environmentCorsConfig('allow_headers');
    }

    /**
     * Gets the "expose_headers" setting from the CORs configuration.
     *
     * @return array
     */
    public function exposeHeaders(): array
    {
        return $this->environmentCorsConfig('expose_headers');
    }

    /**
     * Gets the "max_age" setting from the CORs configuration.
     *
     * @return bool
     */
    public function maxAge(): int
    {
        return $this->environmentCorsConfig('max_age');
    }

    /**
     * Gets the "allow_credentials" setting from the CORs configuration.
     *
     * @return bool
     */
    public function allowCredentials(): bool
    {
        return $this->environmentCorsConfig('allow_credentials');
    }

    /**
     * Add CORS headers.
     *
     * @param \Illuminate\Http\Response $response
     *
     * @return \Illuminate\Http\Response
     */
    public function addCorsHeaders($response)
    {
        $response->headers->set('Access-Control-Allow-Origin', $this->allowedOriginsToString());
        $response->headers->set('Access-Control-Expose-Headers', $this->toString($this->exposeHeaders()));

        if ($this->allowCredentials()) {
            $response->headers->set('Access-Control-Allow-Credentials', 'true');
        }

        return $response;
    }

    /**
     * Add preflight headers.
     *
     * @param \Illuminate\Http\Response $response
     *
     * @return \Illuminate\Http\Response
     */
    public function addPreflightHeaders($response)
    {
        $response->headers->set('Access-Control-Allow-Methods', $this->toString($this->allowMethods()));
        $response->headers->set('Access-Control-Allow-Headers', $this->toString($this->allowHeaders()));
        $response->headers->set('Access-Control-Allow-Origin', $this->allowedOriginsToString());
        $response->headers->set('Access-Control-Max-Age', $this->maxAge());
        $response->headers->set('Access-Control-Expose-Headers', $this->toString($this->exposeHeaders()));

        if ($this->allowCredentials()) {
            $response->headers->set('Access-Control-Allow-Credentials', 'true');
        }

        return $response;
    }
}

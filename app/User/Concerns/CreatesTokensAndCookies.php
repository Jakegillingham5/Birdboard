<?php

namespace Pickstar\User\Concerns;

use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Cookie;

trait CreatesTokensAndCookies
{
    /**
     * Create a token from the user object.
     *
     * @return string
     */
    public function createJwtToken()
    {
        return JWTAuth::fromUser($this);
    }

    /**
     * Create a token cookie from a given token or create a new token.
     *
     * @param string|null $token
     *
     * @return \Symfony\Component\HttpFoundation\Cookie
     */
    public function createCookie(string $token = null)
    {
        return Cookie::make('token', $token ?? $this->createJwtToken(), 23940, null, env('SESSION_DOMAIN'), false, false);
    }

    /**
     * Create and queues token cookie.
     *
     * @param string|null $token
     *
     * @return \Symfony\Component\HttpFoundation\Cookie
     */
    public function createAndQueueCookie(string $token = null)
    {
        return Cookie::queue($this->createCookie($token));
    }

    /**
     * Create a token forget cookie to remove the cookie.
     *
     * @return \Symfony\Component\HttpFoundation\Cookie
     */
    public function forgetCookie()
    {
        return Cookie::forget('token', null, env('SESSION_DOMAIN'));
    }
}

<?php

namespace Pickstar\User\Concerns;

use Pickstar\Booking\TalentTagPivot;
use Pickstar\Tag\Tag;

trait InteractsWithTags
{
    /**
     * A talent can belong to many tags.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'talent_tags', 'talent_id', 'tag_id')
                ->using(TalentTagPivot::class)
                ->withPivot('retired');
    }

    /**
     * Shortcut to just the charity tags.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function charityTags()
    {
        return $this->tags()->charity();
    }

    /**
     * Shortcut to just the sponsor tags.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function sponsorTags()
    {
        return $this->tags()->sponsor();
    }

    /**
     * Shortcut to just the sport tags.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function sportTags()
    {
        return $this->tags()->sport();
    }

    /**
     * Shortcut to just the club tags.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function clubTags()
    {
        return $this->tags()->club();
    }

    /**
     * Shortcut to just the club tags.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function themeTags()
    {
        return $this->tags()->theme();
    }

    /**
     * Sync the given tags with the user.
     *
     * @param array $selectedTags
     *
     * @return void
     */
    public function syncTags(array $tags) : void
    {
        $tags = array_filter(array_flatten($tags));

        if (! empty($tags)) {
            $this->tags()->sync($tags);
        }
    }

    /**
     * Sync the given tags with the user by the given type.
     *
     * This manually detaches all tags of the given type and then re-attaches with the given tags.
     *
     * @param string $type
     * @param array $tags
     *
     * @return void
     */
    public function syncTagsByType(string $type, array $tags) : void
    {
        //NOTE: This is a temporary change until the app is up to date
        if ($tags && $tags[0]['id']) {
            $tagsFormatted = collect($tags)->map(function ($tag) {
                return ['tag_id' => $tag['id'], 'talent_id' => $this->id, 'retired' => $tag['pivot']['retired']];
            });
        } else if ($tags && is_numeric($tags[0])) {
            $tagsFormatted = collect($tags)->map(function ($tag) {
                return ['tag_id' => $tag, 'talent_id' => $this->id, 'retired' => null];
            });
        }


        $relation = $type.'Tags';

        if (! empty($tagsFormatted) && method_exists($this, $relation)) {
            // If we use "sync" it detaches all the other tags as well, so we need to manually query the
            // the tags we want to detach.
            $this->tags()->detach($this->$relation()->pluck('id'));

            // Then we can attach the tags again and add any of the new ones...
            $this->$relation()->attach($tagsFormatted);
        } else {
            $this->tags()->detach($this->$relation()->pluck('id'));
        }
    }
}

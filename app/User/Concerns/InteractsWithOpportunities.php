<?php

namespace Pickstar\User\Concerns;

use Pickstar\Opportunity\Opportunity;

trait InteractsWithOpportunities
{
    /**
     * A talent can belong to many opportunities.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function opportunities()
    {
        return $this->belongsToMany(Opportunity::class, 'talent_opportunities', 'talent_id', 'opportunity_id');
    }
}

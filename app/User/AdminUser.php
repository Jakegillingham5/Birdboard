<?php

namespace Pickstar\User;

use OutOfBoundsException;
use Pickstar\Setting\Setting;
use Illuminate\Notifications\Notifiable;
use Pickstar\Notifications\ResolvesDeliveryChannels;

class AdminUser implements ResolvesDeliveryChannels
{
    use Notifiable;

    /**
     * Admin user attributes.
     *
     * @var array
     */
    protected $attributes = [
        'email' => null,
        'phone_number' => null
    ];

    /**
     * Create a new admin user instance.
     *
     * Sets the default email and phone used for notifications.
     */
    public function __construct()
    {
        $this->attributes['email'] = Setting::notificationEmail();
        $this->attributes['phone_number'] = Setting::notificationPhone();
    }

    /**
     * Get the users delivery channels.
     *
     * @return array
     */
    public function viaChannels($channels): array
    {
        $channels = collect($channels);

        if (!empty($disabled = explode(',', env('NOTIFICATIONS_DISABLED', null))) ) {
            foreach ($disabled as $channel) {
                if (($key = $channels->search(trim($channel))) !== false) {
                    $channels->forget($key);
                }

                unset($key);
            }
        }

        return $channels->toArray();
    }

    /**
     * Route notifications for the Slack channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForSlack($notification)
    {
        return env('SLACK_NOTIFICATION_CHANNEL');
    }

    /**
     * Dynamically get attributes.
     *
     * @param string $key
     *
     * @return mixed
     */
    public function __get($key)
    {
        if (isset($this->attributes[$key])) {
            return $this->attributes[$key];
        }

        throw new OutOfBoundsException("Attribute [{$key}] does not exist on class AdminUser.");
    }

    /**
     * Dummy method for faking notifications.
     *
     * @return int
     */
    public function getKey(): int
    {
        return 1;
    }
}

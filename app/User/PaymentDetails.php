<?php

namespace Pickstar\User;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PaymentDetails
 * @package Pickstar\User
 *
 * @property string $bsb_number
 * @property string $account_name
 * @property string $account_number
 * @property string $abn
 * @property bool $gst_registered
 *
 */

class PaymentDetails extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    public $table = 'user_payment_details';

    /**
     * Enable timestamps.
     *
     * @var boolean
     */
    public $timestamps = true;

    /**
     * Fillable columsn.
     *
     * @var array
     */
    protected $fillable = [
        'bsb_number',
        'account_name',
        'account_number',
        'abn',
        'gst_registered'
    ];

    /**
     * Payment details belongs to a user.
     *
     * @return void
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

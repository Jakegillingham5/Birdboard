<?php
/**
 * Created by PhpStorm.
 * User: jpickstar
 * Date: 2019-04-24
 * Time: 14:51
 */

namespace Pickstar\User;


use Pickstar\Marketing\Email;
use Pickstar\Marketing\Source;
use Pickstar\Services\ActiveCampaignService;

/**
 * Class ActiveCampaignUser
 * @package Pickstar\User
 */
class ActiveCampaignUser
{

    /**
     * @var User
     */
    protected $_user;

    /**
     * @var Email
     */
    protected $_email;

    /**
     * @var Source
     */
    protected $_source;

    /**
     * @var ActiveCampaignService
     */
    protected $_api;

    /**
     * @var array
     */
    protected static $fieldMappings = [
        'id'            => 'client_id',
        'first_name'    => 'firstName',
        'last_name'     => 'lastName',
        'phone'         => 'work_phone',
        'company'       => 'individual_organisation',
        'position'      => 'role',
        'first_created_request_created_date'                => 'first_request_created',
        'most_recently_created_request_created_date'        => 'most_recent_request_created',
        'most_recently_completed_booking_opportunity_type'  => 'last_successful_opportunity_type',
        'most_recently_completed_booking_completed_date'    => 'last_successful_booking',
        'most_recently_withdrawn_booking_completed_date'    => 'last_withdrawn_booking',
        'most_recently_withdrawn_booking_opportunity_type'  => 'last_withdrawn_booking_opportunity_type',
        'most_recently_withdrawn_booking_withdrawn_reason'  => 'last_withdrawn_booking_withdrawn_reason',
        'collection_form' => 'collection_location',
    ];

    /**
     * ActiveCampaignUser constructor.
     * @param Source $source
     * @param Email $email
     * @param User $user
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function __construct(Source $source, Email $email, User $user)
    {
        $this->_source  = $source;
        $this->_email   = $email;
        $this->_user    = $user;
        $this->_api     = app()->make(ActiveCampaignService::class, []);
    }

    /**
     * @return array
     */
    public function buildContactPayload()
    {
        $objects = [];
        $payload = [];

        if($this->_email->email !== null){
            $objects['MarketingEmails'] = $this->_email;
        }

        if($this->_source->email !== null){
            $objects['MarketingSource'] = $this->_source;
        }

        if($this->_user->email !== null){
            $objects['User'] = $this->_user;
        }

        if($this->getUserSubscribedListAttribute() === 'Talent'){
            $objects['TalentProfile'] = $this->_user->profile;
        }

        foreach ($objects as $type => $object){
            switch ($type) {
                case 'User':
                    $attributes = [
                        'email',
                        'first_name',
                        'last_name',
                        'phone',
                        'company',
                        'position',
                        'first_created_request_created_date',
                        'most_recently_created_request_created_date',
                        'most_recently_completed_booking_opportunity_type',
                        'most_recently_completed_booking_completed_date',
                        'most_recently_withdrawn_booking_completed_date',
                        'most_recently_withdrawn_booking_opportunity_type',
                        'most_recently_withdrawn_booking_withdrawn_reason',
                    ];
                    break;
                case 'MarketingSource':
                    $attributes = [
                        'email',
                        'campaign_source',
                        'campaign_name',
                        'campaign_medium',
                        'campaign_content',
                        'campaign_term',
                        'campaign_admin_source'
                    ];
                    break;
                case 'MarketingEmails':
                    $attributes = [
                        'email',
                        'first_name',
                        'last_name',
                        'campaign_stage',
                        'collection_form',
                        'collection_page',
                    ];
                    break;
                case 'TalentProfile':
                    $attributes = [
                        'state',
                        'manager_email',
                        'gender',
                    ];
                    break;
            }

            foreach ($attributes as $attribute){
                $key = static::getActiveCampaignKey($attribute);
                $payload[$key] = $object->$attribute;
            }
        }

        return $payload;
    }

    /**
     * Manually subscribes uses to a mailing list.  Note, should only be used for non-funnel
     * lists.  For funnel lists use the API method moveSubscriptionIfRequired.
     *
     * @param string $listName
     */
    public function subscribeToList($listName)
    {
        $this->_api->updateListSubscription($this->getBestEmail(), $listName);

    }

    public function getBestEmail()
    {

        if($this->_email->email !== null){
            return $this->_email->email;
        }

        if($this->_source->email !== null){
            return $this->_source->email;
        }

        if($this->_user->email !== null){
            return $this->_user->email;
        }

    }

    /**
     * @return string
     */
    public function getUserSubscribedListAttribute()
    {
        if($this->_user->role === 'talent'){
            return 'Talent';
        }
        if($this->_user->role === 'manager'){
            return 'Manager';
        }
        if($this->_user->role === 'admin'){
            return 'Admin';
        }
        if($this->_user->role === 'client'){
            $bookings = $this->_user->clientBookings()->where('type', '=', 'booking')->count();
            if($bookings > 0){
                return 'Clients';
            } else {
                return 'Extended Lead';
            }
        }

        if($this->_email->collection_form === 'Contact'){
            return 'Extended Lead';
        }

        return 'Lead';

    }

    /**
     * @return array
     */
    public function getTagsAndSegments()
    {
        $tags = array_merge($this->getTagsAttribute(), $this->getBookingSegments(), $this->getBookingTypes());

        return $tags;

    }

    /**
     * @return array
     */
    public function getTagsAttribute()
    {
        $tags = [];

        foreach ($this->_user->tags as $tag){
            $tags[$tag->id] = $tag->type . ': ' . $tag->name;
        }

        return $tags;

    }


    /**
     * @return array
     */
    public function getBookingSegments()
    {
        $segments   = $this->_user->getMarketSegmentsAttribute();
        $return     = [];

        foreach ($segments as $segment) {
            $return[] = 'segment: ' . $segment;
        }

        return $return;

    }

    /**
     * @return array
     */
    public function getBookingTypes()
    {
        $segments   = $this->_user->getOpportunityTypesAttribute();
        $return     = [];

        foreach ($segments as $segment) {
            $return[] = 'opportunity: ' . $segment;
        }

        return $return;

    }


    /**
     *
     */
    public function pushToActiveCampaign()
    {
        $payload    = $this->buildContactPayload();
        $email      = $payload['email'];

        $this->_api->updateContact($email, $payload);
        $this->_api->updateCustomFields($email, $payload);
        $tags = $this->getTagsAndSegments();
        if(count($tags) > 0){
            $this->_api->updateTags($email, $tags);
        }
        $this->_api->moveListSubscriptionIfRequired($email, $this->getUserSubscribedListAttribute());
    }

    /**
     * @param $email
     */
    public static function pushByEmail($email)
    {
        $user = static::findByEmail($email);
        $user->pushToActiveCampaign();
    }

    /**
     * @param $email
     * @return ActiveCampaignUser
     */
    public static function findByEmail($email)
    {
        $objects['marketingSource'] = new Source();
        $objects['marketingEmail']  = new Email();
        $objects['user']            = new User();

        foreach ($objects as $index => $object) {
            /* @var \Illuminate\Database\Eloquent\Model $object */
            $result = $object::query()->where(['email' => $email])->first();
            if($result !== null){
                $objects[$index] = $result;
            }
        }

        return new static($objects['marketingSource'],$objects['marketingEmail'],$objects['user']);
    }

    /**
     * @param $attribute
     * @return mixed
     */
    protected static function getActiveCampaignKey($attribute)
    {
        if(array_key_exists($attribute, static::$fieldMappings)){
            return static::$fieldMappings[$attribute];
        } else {
            return $attribute;
        }
    }

}
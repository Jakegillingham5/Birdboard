<?php

namespace Pickstar\User;

use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'password_resets';

    /**
     * Enable timestamps.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Disable auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * Fillable columns.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'token'
    ];

    /**
     * Set the value of the "updated at" attribute.
     *
     * @param mixed $value
     *
     * @return $this
     */
    public function setUpdatedAt($value)
    {
        return $this;
    }
}

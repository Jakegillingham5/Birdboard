<?php

namespace Pickstar\User;

use Exception;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Pickstar\Audit;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Pickstar\Booking\Booking;
use Pickstar\Marketing\Email;
use OwenIt\Auditing\Auditable;
use Pickstar\Message\Message;
use Pickstar\Message\Thread;
use Pickstar\Location\Location;
use Pickstar\Tag\TagCollection;
use Pickstar\Talent\Endorsement;
use Pickstar\Booking\TalentPivot;
use Pickstar\Helpers\ArrayHelper;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Mail;
use Pickstar\Mail\System\PasswordReset;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Pickstar\Message\SystemNotification;
use Pickstar\Model\Concerns\Transformable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Pickstar\Talent\Profile as TalentProfile;
use Pickstar\Booking\ManagerNotificationPivot;
use Illuminate\Database\Eloquent\Relations\HasMany;
use NotificationChannels\OneSignal\OneSignalChannel;
use Pickstar\Notifications\ResolvesDeliveryChannels;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Foundation\Auth\User as Authenticatable;
use League\OAuth2\Server\Exception\OAuthServerException;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class User
 * @package Pickstar\User
 *
 * @property int $id
 * @property string $role
 * @property string $name
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $password
 * @property string $phone
 * @property string $company
 * @property string $position
 * @property string $commission_rate
 * @property int $twitter_provider_id
 * @property int $facebook_provider_id
 * @property string $status
 * @property string $email_verification_token
 * @property string $email_verified
 * @property string $push_notifications
 * @property string $can_set_esports
 * @property PaymentDetails $paymentDetails
 *
 */

class User extends Authenticatable implements JWTSubject, ResolvesDeliveryChannels, AuditableContract
{
    use Auditable,
        Notifiable,
        SoftDeletes,
        HasApiTokens,
        Transformable,
        Concerns\InteractsWithTags,
        Concerns\CreatesTokensAndCookies,
        Concerns\InteractsWithOpportunities,
        Concerns\InteractsWithProfileImages;

    /**
     * User Date and Time Formats
     */
    const DATE_FORMAT = 'd/m/Y';
    const DATE_FORMAT_TZ = 'd/m/Y (T)';
    const TIME_FORMAT = 'h:i A';

    /**
     * User roles.
     *
     * @var string
     */
    const ROLE_MANAGER = 'manager';
    const ROLE_CLIENT = 'client';
    const ROLE_TALENT = 'talent';
    const ROLE_ADMIN = 'admin';
    const ROLE_UNKNOWN = 'unknown';

    /**
     * User statuses.
     *
     * @var string
     */
    const STATUS_PENDING = 'pending';
    const STATUS_ACTIVE = 'active';
    const STATUS_DEACTIVATED = 'deactivated';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role',
        'name',
        'first_name',
        'last_name',
        'email',
        'password',
        'phone',
        'company',
        'position',
        'commission_rate',
        'twitter_provider_id',
        'facebook_provider_id',
        'status',
        'email_verification_token',
        'email_verified',
        'push_notifications',
        'can_set_esports',
        'invoice_to',
        'currency_code',
        'message_notified_at',
        'profile_image_hash',
        'profile_image_crop_payload'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'email_verification_token',
        'twitter_provider_id',
        'facebook_provider_id',
        'talent_notification_pivot',
        'manager_notification_pivot',
        'xero_contact_reference',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * Dynamically append columns.
     *
     * @var array
     */
    protected $appends = [
        'name',
        'slug',
        'featured',
        'photos_count',
        'profile_image_present',
        'based_in',
        'most_recently_completed_booking_completed_date',
        'profile_image_url'
    ];

    /**
     * Type-casted columns.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'int',
        'email_verified' => 'bool',
        'featured' => 'bool',
        'commission_rate' => 'int',
        'push_notifications' => 'bool',
        'can_set_esports' => 'bool'
    ];

    /**
     * Attributes to exclude from the Audit.
     *
     * @var array
     */
    protected $auditExclude = [
        'password'
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot(): void
    {
        parent::boot();

        static::deleting(function ($user) {
            // http://support.codium.com.au/a/tickets/8174
            // remove talent from booking shortlist when deleting a talent
            if ($user->isTalent()) {
                TalentPivot::where('talent_id', $user->id)->delete();
            }
        });
    }

    /**
     * The channels the user receives notification broadcasts on.
     *
     * @return string
     */
    public function receivesBroadcastNotificationsOn()
    {
        return 'chat.' . $this->id;
    }

    public static function filterNotificationChannels($channels)
    {
        if (!empty($disabled = explode(',', env('NOTIFICATIONS_DISABLED', null)))) {
            foreach ($disabled as $channel) {
                $key = array_search($channel, $channels);
                if ($key !== false) {
                    unset($channels[$key]);
                }
            }
        }
        return $channels;
    }

    /**
     * A user may have a parent user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent(): BelongsTo
    {
        return $this->belongsTo(User::class, 'parent_id');
    }

    /**
     * A user many have many staff users.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function staff(): HasMany
    {
        return $this->hasMany(User::class, 'parent_id');
    }

    /**
     * Get talent's locations.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function locations()
    {
        return $this->belongsToMany(Location::class, 'talent_locations', 'talent_id', 'location_id');
    }

    /**
     * Get a talent's based in location.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function basedIn()
    {
        return $this->belongsTo(Location::class, 'location_id');
    }

    /**
     * Get talent's endorsements.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function endorsements()
    {
        return $this->hasMany(Endorsement::class);
    }

    /**
     * A user can have many bookings where they are the client.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clientBookings()
    {
        return $this->hasMany(Booking::class, 'client_id');
    }

    /**
     * @return |null
     */
    public function getMostRecentlyCompletedBookingCompletedDateAttribute()
    {
        $record = $this->clientBookings()->mostRecentlyCompleted()->pluck('completed_date')->first();
        return $record !== null ? $record->toDateString() : null;
    }

    /**
     * @return |null
     */
    public function getMostRecentlyCompletedBookingOpportunityTypeAttribute()
    {
        $record = $this->clientBookings()->mostRecentlyCompleted()->first();
        return $record !== null ? $record->opportunity_name : null;
    }

    /**
     * @return |null
     */
    public function getMostRecentlyWithdrawnBookingCompletedDateAttribute()
    {
        $record = $this->clientBookingsAndRequests()->mostRecentlyWithdrawn()->pluck('updated_at')->first();
        return $record !== null ? $record->toDateString() : null;
    }

    /**
     * @return |null
     */
    public function getMostRecentlyWithdrawnBookingOpportunityTypeAttribute()
    {
        $record = $this->clientBookingsAndRequests()->mostRecentlyWithdrawn()->first();
        return $record !== null ? $record->opportunity_name : null;
    }

    /**
     * @return |null
     */
    public function getMostRecentlyWithdrawnBookingWithdrawnReasonAttribute()
    {
        $record = $this->clientBookingsAndRequests()->mostRecentlyWithdrawn()->first();
        return $record !== null ? $record->withdraw_reason : null;
    }

    /**
     * @return |null
     */
    public function getFirstCreatedRequestCreatedDateAttribute()
    {
        $record = $this->clientBookingsAndRequests()->earliestCreated()->pluck('created_at')->first();
        return $record !== null ? $record->toDateString() : null;
    }

    /**
     * @return |null
     */
    public function getMostRecentlyCreatedRequestCreatedDateAttribute()
    {
        $record = $this->clientBookingsAndRequests()->mostRecentlyCreated()->pluck('created_at')->first();
        return $record !== null ? $record->toDateString() : null;
    }

    /**
     * A user can have many bookings requests where they are the client.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clientBookingRequests()
    {
        return $this->clientBookings()->requests();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clientBookingsAndRequests()
    {
        return $this->clientBookings()->everything()->whereNotNull('market_segment');
    }

    /**
     * A user can belong to many bookings where they are the talent.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function talentBookings()
    {
        return $this->belongsToMany(Booking::class, 'booking_talent', 'talent_id', 'booking_id')
            ->using(TalentPivot::class)
            ->withTimestamps()
            ->withPivot('shortlisted', 'status', 'priority');
    }

    /**
     * A user can belong to many booking requests where they are the talent.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function talentBookingRequests(): BelongsToMany
    {
        return $this->talentBookings()->requests();
    }

    /**
     * A talent can belong to many bookings where they have been notified.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function talentBookingNotifications(): BelongsToMany
    {
        return $this->belongsToMany(Booking::class, 'booking_talent_notifications', 'talent_id', 'booking_id')
            ->using(TalentNotificationPivot::class)
            ->all();
    }

    /**
     * A manager can belong to many bookings where they have been notified.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function managerBookingNotifications(): BelongsToMany
    {
        return $this->belongsToMany(Booking::class, 'booking_manager_notifications', 'manager_id', 'booking_id')
            ->using(ManagerNotificationPivot::class)
            ->all();
    }

    /**
     * A user can belong to one management user.
     *
     * This applies to talent and the management user is the "master manager" user that the
     * talent belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function management(): BelongsTo
    {
        return $this->belongsTo(User::class, 'management_id');
    }

    /**
     * A user can belong to one manager user.
     *
     * This applies to talent and the management user is the "staff manager" user that the
     * talent belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function manager(): BelongsTo
    {
        return $this->belongsTo(User::class, 'manager_id');
    }

    /**
     * Alias for getting master or staff manager talent.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function managedTalent(): HasMany
    {
        if ($this->isMasterManager()) {
            return $this->managementTalent();
        } elseif ($this->isStaffManager()) {
            return $this->managerTalent();
        }

        throw new Exception('Unable to get talent for non-manager user.');
    }

    /**
     * A management user can have many talent assigned to them.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function managementTalent(): HasMany
    {
        return $this->hasMany(User::class, 'management_id');
    }

    /**
     * A staff manager user can have many talent assigned to them.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function managerTalent(): HasMany
    {
        return $this->hasMany(User::class, 'manager_id');
    }

    /**
     * A user can have many threads that they are a participant in
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function threads()
    {
        return $this->belongsToMany(Thread::class, 'participants', 'user_id', 'thread_id')->with('messages');
    }

    public function latestThreadMessageDate($thread_id)
    {
        $latest_message = Thread::find($thread_id)
                    ->messages()
                    ->where('user_id', '=', $this->user_id)
                    ->latest()
                    ->first();
        if ($latest_message) {
            return $latest_message->created_at;
        }
        return null;
    }

    /**
     * Scope only active users.
     *
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('users.status', static::STATUS_ACTIVE);
    }

    /**
     * Scope users where public is true
     */
    public function scopePublic($query)
    {
        return $query->whereHas('profile', function ($query) {
            $query->where('public', true);
        });
    }

    /**
     * Scope only non deleted users
     *
     * @return mixed
     */
    public function scopeNotDeleted($query)
    {
        return $query->where('deleted_at', null);
    }

    /**
     * Scope only deleted users
     *
     * @return mixed
     */
    public function scopeDeleted($query)
    {
        return $query->where('deleted_at', '!=', null);
    }

    /**
     * Retreive only talent users.
     *
     * @return void
     */
    public function scopeTalent($query): void
    {
        $query->where('role', static::ROLE_TALENT);
    }

    /**
     * Retreive only talent users.
     *
     * @return void
     */
    public function scopeAdmin($query): void
    {
        $query->where('role', static::ROLE_ADMIN);
    }

    /**
     * Retreive only client users.
     *
     * @return void
     */
    public function scopeOnlyClients($query): void
    {
        $query->where('role', static::ROLE_CLIENT);
    }

    /**
     * Retreive only master manager users.
     *
     * @return void
     */
    public function scopeMasterManagers($query): void
    {
        $query->where('role', static::ROLE_MANAGER)
            ->whereNull('parent_id');
    }

    /**
     * Retreive only staff manager users.
     *
     * @return void
     */
    public function scopeStaffManagers($query): void
    {
        $query->where('role', static::ROLE_MANAGER)
            ->whereNotNull('parent_id');
    }

    public function scopeManagers($query): void
    {
        $query->where('role', static::ROLE_MANAGER);
    }

    /**
     * Get accounts that are pending vetting.
     *
     * @return mixed
     */
    public function scopePendingVetting($query)
    {
        return $query->where('status', static::STATUS_PENDING);
    }

    /**
     * Featured talent users.
     *
     * @return mixed
     */
    public function scopeFeatured($query)
    {
        return $query->whereHas('profile', function ($query) {
            $query->where('date_featured_until', '>=', now());
        });
    }

    /**
     * Query where the user is a child of the given user.
     *
     * @param \Illuminate\Database\Query\Builder $query
     * @param \Pickstar\User\User $user
     *
     * @return void
     */
    public function scopeChildOf($query, User $user): void
    {
        $query->where('parent_id', $user->id);
    }

    /**
     * Scope only the given user if the given user is a talent.
     *
     * This is used when eagerly loading talent and we wish to restrict which talent are loaded in
     * based on the users role.
     *
     * @param \Illumiante\Database\Eloquent\Builder $query
     * @param \Pickstar\User\User $user
     *
     * @return void
     */
    public function scopeOnlyMyselfIfTalent($query, User $user)
    {
        if ($user->isTalent()) {
            $query->where('users.id', $user->id);
        }
    }

    /**
     * Scope only the applied or shortlisted talent if the given user is a client.
     *
     * This is used when eagerly loading talent and we wish to restrict which talent are loaded in
     * based on the users role.
     *
     * @param \Illumiante\Database\Eloquent\Builder $query
     * @param \Pickstar\User\User $user
     *
     * @return void
     */
    public function scopeOnlyAppliedOrShortlistedIfClient($query, User $user)
    {
        if ($user->isClient()) {
            $query->where('booking_talent.shortlisted', true)
                ->orWhere('booking_talent.status', TalentPivot::STATUS_APPLIED);
        }
    }

    /**
     * Scope only the managed talent for the given manager user.
     *
     * This is used when eagerly loading talent and need to restrict the talent loaded based on
     * the logged in user being a manager.
     *
     * @param \Illuminate\Database\Query\Builder $query
     * @param \Pickstar\User\User $user
     *
     * @return void
     */
    public function scopeOnlyManagedIfManager($query, User $user): void
    {
        $query->when($user->isMasterManager(), function ($query) use ($user) {
            $query->where('users.management_id', $user->id);
        });

        $query->when($user->isStaffManager(), function ($query) use ($user) {
            $query->where('users.manager_id', $user->id);
        });
    }

    /**
     * Scope where the talent have been accepted.
     *
     * This is the inverse of "haveBeenAcceptedFor" on the booking model.
     *
     * @param \Illumiante\Database\Eloquent\Builder $query
     *
     * @return void
     */
    public function scopeHaveBeenAccepted($query, User $user = null)
    {
        $query->whereIn('booking_talent.status', [TalentPivot::STATUS_APPLIED, TalentPivot::STATUS_CANCELLED])
            ->where('booking_talent.client_accepted', true)
            ->when($user, function ($query, $user) {
                $query->where('booking_talent.talent_id', $user->id);
            });
    }

    /**
     * Scope where managed by the given manager user.
     *
     * @param \Illuminate\Database\Query\Builder $query
     * @param \Pickstar\User\User $user
     *
     * @return void
     */
    public function scopeManagedBy($query, User $user): void
    {
        $query->when($user->isMasterManager(), function ($query) use ($user) {
            $query->where('users.management_id', $user->id);
        });

        $query->when($user->isStaffManager(), function ($query) use ($user) {
            $query->where('users.manager_id', $user->id);
        });
    }

    /**
     * A user may have a talent profile.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function profile()
    {
        return $this->hasOne(TalentProfile::class)
            ->withDefault();
    }

    /**
     * A user can have one payment details.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function paymentDetails()
    {
        return $this->hasOne(PaymentDetails::class);
    }

    /**
     * Determine if user is an admin.
     *
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->role === static::ROLE_ADMIN;
    }

    /**
     * Determine if user is a talent.
     *
     * @return bool
     */
    public function isTalent(): bool
    {
        return $this->role === static::ROLE_TALENT;
    }

    /**
     * Determine if user is a client.
     *
     * @return bool
     */
    public function isClient(): bool
    {
        return $this->role === static::ROLE_CLIENT;
    }

    /**
     * Determine if user is a manager.
     *
     * @return bool
     */
    public function isManager(): bool
    {
        return $this->role === static::ROLE_MANAGER;
    }

    /**
     * Determine if user is a master manager.
     *
     * @return bool
     */
    public function isMasterManager(): bool
    {
        return $this->isManager() &&
            $this->parent_id === null;
    }

    /**
     * Determine if user is a staff manager.
     *
     * @return bool
     */
    public function isStaffManager(): bool
    {
        return $this->isManager() &&
            $this->parent_id !== null;
    }

    /**
     * Determine if user has a manager.
     *
     * @return bool
     */
    public function hasManager(): bool
    {
        return $this->management_id !== null && $this->management !== null;
    }

    /**
     * Indicates if the user has no role.
     *
     * @return bool
     */
    public function hasNoRole(): bool
    {
        return is_null($this->role) || $this->role === '';
    }

    /**
     * Indicates if the user has a role.
     *
     * @return bool
     */
    public function hasRole(): bool
    {
        return !is_null($this->role);
    }

    /**
     * Determine if the current user is managed by the given user.
     *
     * @param \Pickstar\User\User $user
     *
     * @return bool
     */
    public function isManagedBy(User $user): bool
    {
        if ($user->isAdmin()) {
            return true;
        } elseif ($user->isMasterManager()) {
            return $user->id === $this->management_id;
        } elseif ($user->isStaffManager()) {
            return $user->id === $this->manager_id;
        }

        return false;
    }

    /**
     * Determine if the current user is the parent of another user.
     *
     * @param \Pickstar\User\User $user
     *
     * @return bool
     */
    public function isParentOf(User $user): bool
    {
        return $this->id === $user->parent_id;
    }

    /**
     * Determine if user is verified.
     *
     * @return bool
     */
    public function isVerified(): bool
    {
        return $this->email_verified === true;
    }

    /**
     * Determine if user is unverified.
     *
     * @return bool
     */
    public function isUnverified(): bool
    {
        return $this->email_verified === false;
    }

    /**
     * Determine if user is activated.
     *
     * @return bool
     */
    public function isActivated(): bool
    {
        return $this->status === static::STATUS_ACTIVE;
    }

    /**
     * Determine if user is pending activation.
     *
     * @return bool
     */
    public function isPendingActivation(): bool
    {
        return $this->status === static::STATUS_PENDING;
    }

    /**
     * Determine if user is deactivated.
     *
     * @return bool
     */
    public function isDeactivated(): bool
    {
        return $this->status === static::STATUS_DEACTIVATED;
    }

    /**
     * Setter for the users "name" which isn't actually a column. Instead we'll split
     * the value and attempt to set the first and last names respectively.
     *
     * @param string $value
     *
     * @return void
     */
    public function setNameAttribute($name)
    {
        $names = explode(' ', $name);

        // If we have more than 2 words after the split then we'll assume the last word is the
        // last name and the remaining words go in the first name.
        if (count($names) > 2) {
            $this->attributes['last_name'] = array_pop($names);
            $this->attributes['first_name'] = implode(' ', $names);

            // If we have less than 2 words then we'll simply put the value as it was in the first name
            // and leave the last name empty.
        } elseif (count($names) < 2) {
            $this->attributes['first_name'] = $name;

            // Finally we should have a simple first and last name.
        } else {
            $this->attributes['first_name'] = array_shift($names);
            $this->attributes['last_name'] = array_shift($names);
        }
    }

    /**
     * Get the users name as a single string.
     *
     * @return null|string
     */
    public function getNameAttribute(): ?string
    {
        if (!empty($this->first_name) && !empty($this->last_name)) {
            return sprintf('%s %s', $this->first_name, $this->last_name);
        } elseif (!empty($this->first_name)) {
            return $this->first_name;
        } elseif (!empty($this->last_name)) {
            return $this->last_name;
        }

        return null;
    }

    public function getPinnedThreadsAttribute()
    {
        return $this->threads()->withPivot('pinned')->where('participants.pinned', true)->get()->pluck('id');
    }

    /**
     * Get the users based in as a string
     *
     * @return null|Location
     */
    public function getBasedInAttribute() : ?Location
    {
        return $this->basedIn()->first();
    }

    /**
     * Get the users email as a de-identified string.
     *
     * @return string
     */

    public function getEmailHashAttribute()
    {
        return hash_hmac('sha256', $this->email, env('INTERCOM_HASH_SECRET'));
    }

    /**
     * Get the users market segment(s)
     *
     * @return Collection
     */
    public function getMarketSegmentsAttribute()
    {
        return $this->clientBookingsAndRequests()->pluck('market_segment')->unique();
    }

    /**
     * Get the users opportunity type(s)
     *
     * @return Collection
     */
    public function getOpportunityTypesAttribute()
    {
        return $this->clientBookingsAndRequests()->with('opportunity')->whereNotNull('opportunity_id')->get()->pluck('opportunity.name','opportunity_id')->unique();
    }


    /**
     * Get sport and club string for the talent (e.g. first sport and club we find against the user).
     *
     * @return null|string
     */
    public function getSportClubAttribute(): ?string
    {
        if ($this->sportTags->count() > 1) {
            return $this->sportTags->take(2)->implode('name', ', ');
        } else {
            return collect([$this->sportTags->first(), $this->clubTags->first()])
                ->filter()
                ->sortBy('name')
                ->implode('name', ', ');
        }
    }

    /**
     * Get the users slug as a single string.
     *
     * @return null|string
     */
    public function getSlugAttribute(): ?string
    {
        return is_null($this->name)
            ? null
            : Str::slug($this->name);
    }

    /**
     * Get featured attribute.
     *
     * @return null|string
     */
    public function getFeaturedAttribute() : ?string
    {
        if (Carbon::now() <= $this->date_featured_until) {
            return true;
        }

        return false;
    }

    /**
     * Get the profile completion percentage.
     *
     * @return float
     */
    public function getProfileCompletionPercentageAttribute(): float
    {
        $maximum = 7;
        $actual = 0;

        $actual += ! empty($this->profile->tagline) ? 1 : 0;
        $actual += ! empty($this->profile->description) ? 1 : 0;
        $actual += ! empty($this->profile_image_hash) ? 1 : 0;
        $actual += $this->locations->isNotEmpty() ? 1 : 0;
        $actual += $this->tags->whereIn('type', ['sport', 'club'])->isNotEmpty() ? 1 : 0;
        $actual += $this->profile->filledSocialMediaLink() ? 1 : 0;
        $actual += ! is_null($this->paymentDetails) ? 1 : 0;

        $percentage = intval(($actual / $maximum) * 100);

        // Make sure we didn't spill over 100 ...not that we should.
        return min($percentage, 100);
    }

    /**
     * Get the count of talent that have been assigned to the current user as a management company.
     *
     * @return int
     */
    public function getManagementTalentCountAttribute(): int
    {
        return $this->managementTalent()->count();
    }

    /**
     * Get the count of requests that talent have responded to that have been assigned to the current
     * user as a management company.
     *
     * @return int
     */
    public function getManagementRequestCountAttribute(): int
    {
        return Booking::activeRequests()->isNotExpired()
            ->whereHas('talent', function ($query) {
                $query->where('management_id', $this->id);
            })
            ->count();
    }

    /**
     * Get the count of bookings that talent are attending that have been assigned to the current
     * user as a management company.
     *
     * @return int
     */
    public function getManagementBookingCountAttribute(): int
    {
        return Booking::active()->isNotExpired()
            ->whereHas('talentAttending', function ($query) {
                $query->where('management_id', $this->id);
            })
            ->count();
    }

    public function getBestName()
    {
        if($this->invoice_to !== null) {
            return $this->invoice_to;
        }

        if($this->company !== null) {
            return $this->company;
        }

        return $this->name;
    }

    /**
     * Get the users delivery channels.
     *
     * @return array
     */
    public function viaChannels($channels): array
    {
        $channels = collect($channels);

        if (
            ($key = $channels->search(OneSignalChannel::class)) !== false &&
            $this->push_notifications === false
        ) {
            $channels->forget($key);
        }

        if (!empty($disabled = explode(',', env('NOTIFICATIONS_DISABLED', null)))) {
            foreach ($disabled as $channel) {
                if (($key = $channels->search(trim($channel))) !== false) {
                    $channels->forget($key);
                }

                unset($key);
            }
        }

        return $channels->toArray();
    }

    /**
     * Route notifications for the OneSignal channel.
     *
     * @return array|null
     */
    public function routeNotificationForOneSignal(): ?array
    {
        // If user has management then do not route notifications through OneSignal if notification
        // delivery is set to managers only.
        if (
            $this->isTalent() &&
            $this->hasManager() &&
            $this->profile->notification_preference === TalentProfile::NOTIFICATION_DELIVERY_MANAGER
        ) {
            return null;
        }

        return ['tags' => ['key' => 'user_id', 'relation' => '=', 'value' => $this->id]];
    }

    /**
     * Route notifications for the Nexmo channel.
     *
     * @return string
     */
    public function routeNotificationForNexmo()
    {
        return $this->sanitised_number;
    }

    /**
     * Route notifications for the Mail channel.
     *
     * @return array|string
     */
    public function routeNotificationForMail()
    {
        return $this->notification_email;
    }

    /**
     * Get the notification email for the user.
     *
     * @return array|string
     */
    public function getNotificationEmailAttribute()
    {
        if ($this->isTalent() && $this->hasManager()) {
            $manager = $this->manager ?? $this->management;

            if ($this->profile->notification_preference === TalentProfile::NOTIFICATION_DELIVERY_MANAGER) {
                return $manager->email;
            } elseif ($this->profile->notification_preference === TalentProfile::NOTIFICATION_DELIVERY_BOTH) {
                return array_filter([$this->email, $manager->email]);
            }
        }

        return $this->email;
    }

    /**
     * Get sanitised mobile number for Nexmo.
     *
     * @return null|string
     */
    public function getSanitisedNumberAttribute(): ?string
    {
        if (! empty($this->phone) || ! $this->phoneIsLandline()) {
            $this->phone = preg_replace("/[^0-9]/", "", $this->phone);

            if (starts_with($this->phone, '0')) {
                return str_replace_first('0', '61', $this->phone);
            }
        }

        return null;
    }

    /**
     * Whether phone number is a landline
     */
    public function phoneIsLandline()
    {
        // Landline area codes are 02, 03, 07, 08
        // Only mobile numbers begin with 04
        return strpos($this->phone, '04') === 0 ? false : true;
    }

    /**
     * When a password is updated ensure it is encrypted.
     *
     * @param string $password
     *
     * @return string
     */
    public function setPasswordAttribute($password)
    {
        if (! is_null($password)) {
            $this->attributes['password'] = bcrypt($password);
        } else {
            $this->attributes['password'] = null;
        }
    }

    /**
     * Get a meta description for a talent
     *
     * @param bool $shortName
     *
     * @return void
     */
    public function talentDescription(bool $shortName = false)
    {
        $metaDescription = ['Book'];
        if ($this->sportTags->first()) {
            $metaDescription[] = $this->sportTags->first()->name;
        }

        if ($this->clubTags->first() && !in_array($this->clubTags->first()->name, $metaDescription)) {
            if (count($metaDescription) == 2) {
                $metaDescription[] = 'and';
            }
            $metaDescription[] = $this->clubTags->first()->name;
        }

        $metaDescription[] = 'star ' . $this->name . ' for guest speaking, events, marketing, influencers, fan experiences and more, in';
        if ($this->locations->first()) {
            if ($shortName) {
                $metaDescription[] = $this->locations->first()->shortname . ' and around Australia';
            } else {
                $metaDescription[] = $this->locations->first()->name . ' and around Australia';
            }
        } else {
            $metaDescription[] = 'Australia';
        }
        return implode(' ', $metaDescription);
    }

    /**
     * Get the associated tags for a talent
     *
     * @return \Illuminate\Support\Collection
     */
    public function associatedTags(): \Illuminate\Support\Collection
    {
        return (new TagCollection($this->tags))->withAssociated();
    }

    /**
     * Verify a user account based on token.
     *
     * @param string $token
     *
     * @return \Pickstar\User\User
     */
    public static function verification($token) : User
    {
        $user = User::where('email_verification_token', $token)
            ->where('email_verified', false)
            ->firstOrFail();

        $user->fill([
            'email_verified' => true,
            'email_verification_token' => null
        ])->save();

        return $user;
    }

    /**
     * Send the password reset notification.
     *
     * @param string $token
     *
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        Mail::to($this->notification_email)
            ->queue(new PasswordReset($this, $token));
    }

    /**
     * Get the JWT identifier.
     *
     * @return string
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Get the JWT custom claims when serializing the token.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [
            'user' => [
                'id' => $this->id,
             ]
        ];
    }

    /**
     * Find a user for Passport authentication.
     *
     * @param string $email
     *
     * @return \Pickstar\User\User|null
     */
    public function findForPassport($email): ?User
    {
        if (
            ($user = $this->where('email', $email)->first()) &&
            !$user->isTalent()
        ) {
            throw OAuthServerException::accessDenied('Only talent can create access tokens.');
        }

        return $user;
    }

    /**
     * Check marketing emails table to see if current user has an email in that table
     *
     * @return bool
     */
    public function hasMarketingEmail(): bool
    {
        return Email::whereEmail($this->email)->exists();
    }

    /**
     * Update a talents management and manager from the request payload.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function updateTalentManagementFromRequest(Request $request): void
    {
        if (!$this->isTalent()) {
            return;
        }

        if (
            $request->user()->can('updateManagement', $this) &&
            $request->input('management_id', $this->management_id) !== $this->management_id
        ) {
            if ($this->management) {
                foreach($this->threads as $thread) {
                    $thread->participants()->wherePivot('user_id', '=', $this->management->id)->detach();
                }
            }
            if ($request->input('management_id') === null) {
                $this->management()->dissociate();
            } else {
                try {
                    $this->management()->associate(
                        User::masterManagers()->find($request->input('management_id'))
                    );
                    foreach ($this->threads()->active()->get() as $thread) {
                        $thread->participants()->syncWithoutDetaching(User::masterManagers()->find($request->input('management_id')));
                    }
                } catch (Exception $exception) {
                    $this->management()->dissociate();
                }
            }
        }

        if (
            $request->user()->can('updateManager', $this) &&
            $request->input('manager_id', $this->manager_id) !== $this->manager_id
        ) {
            if ($this->manager) {
                foreach ($this->threads as $thread) {
                    $thread->participants()->wherePivot('user_id', '=', $this->manager->id)->detach();
                }
            }
            if ($request->input('manager_id') === null) {
                $this->manager()->dissociate();
            } else {
                try {
                    $this->manager()->associate(
                        User::staffManagers()->childOf($this->management)->find($request->input('manager_id'))
                    );
                    foreach ($this->threads()->active()->get() as $thread) {
                        $thread->participants()->syncWithoutDetaching(User::staffManagers()->childOf($this->management)->find($request->input('management_id')));
                    }
                } catch (Exception $exception) {
                    $this->manager()->dissociate();
                }
            }
        }
    }

    /**
     * Returns whether the profile has a profile image
     *
     * @return bool
     */
    public function getProfileImagePresentAttribute ()
    {
        return ($this->profile_image_hash !== null);
    }

    /**
     * Returns the number of marketing photos belonging to the profile
     *
     * @return int|void
     */
    public function getPhotosCountAttribute()
    {
        return count($this->profile->photos);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function talentBookingAudits()
    {
        return $this->hasManyThrough(Audit::class, TalentPivot::class, 'talent_id', 'auditable_id', 'id');
    }

    /**
     * Mutator for profile image crop payload to store it as a JSON encoded string.
     *
     * @param array|null $value
     *
     * @return void
     */
    public function setProfileImageCropPayloadAttribute($value)
    {
        $this->attributes['profile_image_crop_payload'] = is_array($value)
            ? json_encode($value)
            : $value;
    }

    /**
     * Accessor for profile image crop payload to return it as an array if stored as JSON.
     *
     * @param string|null $value
     *
     * @return array|null
     */
    public function getProfileImageCropPayloadAttribute($value)
    {
        if (($payload = json_decode($value)) === null) {
            return $value;
        }

        return $payload;
    }

    /**
     * Get the profile image URL attribute.
     *
     * @return string
     */
    public function getProfileImageUrlAttribute()
    {
        return $this->getProfileImageUrl();
    }

    /**
     * Get a users best payment details. If they have details, we return
     * that. If not, we check if they have a manager and then return the
     * managers details.
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Relations\HasOne|object|null
     */
    public function getBestPaymentDetails()
    {
        if ($this->paymentDetails()->first()) {
            return $this->paymentDetails()->first();
        } else if ($this->isTalent() && $this->management && $this->management->paymentDetails()->first()) {
            $paymentDetails = $this->management->paymentDetails()->first();
            $paymentDetails->managers_details = true;
            return $paymentDetails;
        }

        return null;
    }

    /**
     * Get talent's manager or management.
     *
     * @return mixed|null
     */
    public function directManager() {
        // Some management has multiple managers, so we fetch the manager if exists.
        // Otherwise we fetch the management.

        if ($this->manager_id !== null) {
            return $this->manager;

        } else if ($this->management_id !== null) {
            return $this->management;

        } else {
            return null;
        }
    }
}

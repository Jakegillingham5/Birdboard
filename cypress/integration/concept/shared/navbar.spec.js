/// <reference types="Cypress" />

describe('/navbar', () =>{

    before(() => {
        cy.visit('http://localhost:8000/projects')
    })

    it('has a navbar brand which redirects users to /projects if authenticated', () => {
        //Login and preserve cookie
        cy.login().visit('http://localhost:8000/projects')

        //Find birdboard logo and click the anchor
        cy.get('.navbar-brand')
            .should('have.attr','href','http://localhost:8000/projects')
            .click()

        //Ensure the authenticated user is redirected correctly to the projects screen
        cy.location('pathname')
            .should('eq','/projects')
    })

    it('has a navbar brand which redirects users to /login if unauthenticated', () => {

        //Find birdboard logo and click the anchor
        cy.get('.navbar-brand')
            .should('have.attr','href','http://localhost:8000/projects')
            .click()

        //Ensure the unauthenticated user is redirected correctly to the login screen
        cy.location('pathname')
            .should('eq','/login')
    })

    it('shows the buttons to sign in if unauthenticated', () => {

        //Find the login button/anchor and click it
        cy.get('a')
            .contains('Login')
            .should('have.attr','href','http://localhost:8000/login')
            .click()

        //Ensure the user has been redirected to the login screen
        cy.location('pathname')
            .should('eq','/login')
    })

    it('shows the buttons to sign in if unauthenticated', () => {

        //Find the registration button/anchor and click it
        cy.get('a')
            .contains('Register')
            .should('have.attr','href','http://localhost:8000/register')
            .click()

        //Ensure the user has been redirected to the registration screen
        cy.location('pathname')
            .should('eq','/register')
    })


    it('allows an authenticated user to switch the application styling through the nav bar', () => {
        cy.login().visit('http://localhost:8000/projects')

        //Find the unselected option through active class and click it
        cy.get('.rounded-full.border-accent')
            .siblings()
            .click()

        //Wait to capture visual change
        cy.wait(150)

        //Find the unselected option through active class and click it
        cy.get('.rounded-full.border-accent')
            .siblings()
            .click()
    })

    it('allows an authenticated user to logout', () => {
        cy.login().visit('http://localhost:8000/projects')

        //Check if the dropdown menu is open
        cy.get('.dropdown-menu')
            .should('have.attr','style','width: 200px; display: none;')

        //Click on the current user
        cy.get('button')
            .contains('Jake')
            .click()

        //Check whether the dropdown is visible
        cy.get('.dropdown-menu')
            .should('be.visible')

        //Click the logout button
        cy.get('#logout-form button')
            .click()

        //Ensure the user has been redirected to the default page
        cy.location('pathname')
            .should('eq','/')
    })
})

/// <reference types="Cypress" />

describe('/register', () =>{

    let randomString = Math.random().toString(36).substr(2, 5);

    beforeEach(() => {
        cy.visit('http://localhost:8000/register')
    })

    it('has a register panel heading', () => {
        cy.get('form')
            .contains('h1','Register')
    })

    //TODO Figure out how to detect 'required' HTML5 attribute
    it('requires an name', () => {
        cy.get('#name')
            .should('have.attr','required')
    })

    it('requires an email address', () => {
        cy.get('#email')
            .should('have.attr','required')
    })

    it('requires a new password', () => {
        cy.get('#password')
            .should('have.attr','required')

    })

    it('requires you to confirm your password', () => {
        cy.get('#password-confirmation')
            .should('have.attr','required')
    })

    it('navigates to the home page if creation successful', () => {
        cy.get('#name').type(randomString)
        cy.get('#email').type(randomString+'@test.com')
        cy.get('#password').type('password')
        cy.get('#password-confirmation').type('password{enter}')

        cy.location('pathname')
            .should('eq','/projects')
    })

    it('requires a unique email address', () => {
        cy.get('#name').type('jake')
        cy.get('#email').type('jake@test.com')
        cy.get('#password').type('password')
        cy.get('#password-confirmation').type('password{enter}')
        cy.get('.help-block')
            .should('contain','The email has already been taken.')
    })

});

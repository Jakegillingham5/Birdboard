/// <reference types="Cypress" />

describe('/login', () =>{

    beforeEach(() => {
       cy.visit('http://localhost:8000/login')
    })

    it('has a login panel heading', () => {

        //Locate form and ensure header is set correctly
        cy.get('form')
            .contains('h1','Login')

    })

    //TODO Figure out how to detect 'required' HTML5 attribute
    it('requires an email address', () => {

        //Locate form and click an empty form
        cy.get('form')
            .contains('button','Login')
            .click()

        //cy.contains('Please fill in this field.')
    })

    it('requires a password', () => {

        //Locate and enter into the email field
        cy.get('#email')
            .type('jake@test.com{enter}')

        //cy.contains('Please fill in this field.')
    })

    it('can select and deselect remember me option', () => {

        //Ensure user can select and deselect the remember-me
        cy.get('[type="checkbox"]')
            .check()
            .wait(150)
            .uncheck()
    })

    it('requires a valid email address and password', () => {
        //Enter in some incorrect user credentials
        cy.get('#email').type('jake@test.com')
        cy.get('#password').type('invalid{enter}')

        //Check dom is showing the help-block
        cy.get('.help-block')
            .should('contain','These credentials do not match our records.')
    })

    it('navigates to the home page if successful',() => {
        //Enter in some correct user credentials
        cy.get('#email').type('jake@test.com')
        cy.get('#password').type('password{enter}')

        //Ensure the user is redirected correctly
        cy.location('pathname')
            .should('eq','/projects')
    });

});

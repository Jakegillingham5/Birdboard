/// <reference types="Cypress" />

describe('/projects', () => {

    before(() => {
        cy.login()
    })

    // Without this function only the first test will work.
    beforeEach(() => {
        cy.visit('http://localhost:8000/projects')
        Cypress.Cookies.preserveOnce('laravel_session', 'XSRF-TOKEN')
    })

    it(' allows the user to open and close the create a project modal', () => {

        //Find and select the new project button
        cy.get('a')
            .contains('New Project')
            .click()

        //Check if modal is visible
        cy.get('.v--modal-overlay')
            .should('be.visible')

        //Click the cancel button
        cy.get('.button.is-outlined')
            .contains('Cancel')
            .click()

        //Check if modal has been closed
        cy.get('.v--modal-overlay')
            .should('not.exist')
    })

    it(' allows the user to select an individual project', () => {

        //Find and select project
        cy.get('.card h3 a:first')
            .click()

        //Ensure the user is redirected correctly (id unknown so using contain)
        cy.location('pathname')
            .should('contain','/projects/')
    })

    it(' allows the user to select delete a selected project', () => {

        //Find and select the first projects
        cy.get('.card h3 a:first')
            .click()

        //Ensure the user is redirected correctly (id unknown so using contain)
        cy.location('pathname')
            .should('contain','/projects/')

        //Remember current path
        const currentId = cy.url().toString()

        cy.log(currentId)

        //Go back to projects screen
        cy.go('back')

        //Find and select the first project delete option and click it
        cy.get('.card footer form button:first')
            .contains("Delete")
            .click()

        //Search for deleted href
        cy.get('.card h3 a')
            .should('not.include.attr','href', currentId)

    })

    it(' allows the user to create a project', () => {

        //Find and select the new project button
        cy.get('a')
            .contains('New Project')
            .click()

        //Check if modal is visible
        cy.get('.v--modal-overlay')
            .should('be.visible')

        //Fill form content
        cy.get('#title').type('zyxwvutsrqponm')
        cy.get('#description').type('This is a long winded description!')
        cy.get('.flex-1.ml-4 .mb-4 input').type('A new first task')

        //Click the submit button
        cy.get('.button')
            .contains('Create Project')
            .click()

    })

})

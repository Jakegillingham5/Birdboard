/// <reference types="Cypress" />

describe('/projects/*', () => {

    before(() => {
        cy.login()
    })

    beforeEach(() => {
        cy.visit('http://localhost:8000/projects/35')
        Cypress.Cookies.preserveOnce('laravel_session', 'XSRF-TOKEN')
    })

    it('allows the user to return to the project list screen', ()=>{

        //Find and select the my projects anchor
        cy.get('a')
            .contains('My Projects')
            .click()


        //Ensure the user is redirected correctly
        cy.location('pathname')
            .should('eq','/projects')

    })

    it('allows the user to complete and incomplete a task', ()=>{

        //Find text for first checkbox and body of task
        cy.get('form div.flex input[name=body]:first').then(($btn) => {

            // store the task text
            const task = $btn.val()

            //Find and select first task checkbox
            cy.get('form div.flex input[name=completed]:first')
                .check()

            //Check activity log contains completed string
            cy.get('ul.list-reset li:first')
                .contains("Jake completed \""+task+"\"")

            cy.wait(500)

            //Find and unselect first task checkbox

            //Check activity log contains incompleted string
            cy.get('form div.flex input[name=completed]:first')
                .uncheck()
            cy.get('ul.list-reset li:first')
                .contains(Cypress.env('currentName')+" incompleted \""+task+"\"")
        })

    })

    it('allows the user to add some general notes', ()=>{

        //Find text for first checkbox and body of task
        cy.get('form textarea[name=notes]:first')
            .type(Cypress.env('lorem'))

    })



})

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssociatedTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('associated_tags', function (Blueprint $table) {
            $table->integer('parent_tag_id')->unsigned();
            $table->integer('associated_tag_id')->unsigned();
            $table->boolean('protected')->default(1);

            $table->foreign('parent_tag_id')->references('id')->on('tags');
            $table->foreign('associated_tag_id')->references('id')->on('tags');
            $table->unique(array('parent_tag_id', 'associated_tag_id'));

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('associated_tags');
    }
}

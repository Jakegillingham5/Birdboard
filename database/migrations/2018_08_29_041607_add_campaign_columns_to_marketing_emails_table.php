<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCampaignColumnsToMarketingEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('marketing_emails', function (Blueprint $table) {
            $table->integer('campaign_stage')->nullable();
            $table->timestamp('campaign_last_sent_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('marketing_emails', function (Blueprint $table) {
            $table->dropColumn(['campaign_stage', 'campaign_last_sent_date']);
        });
    }
}

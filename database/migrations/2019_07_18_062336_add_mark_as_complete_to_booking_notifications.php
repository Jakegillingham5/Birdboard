<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMarkAsCompleteToBookingNotifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_notifications', function (Blueprint $table) {
            $table->boolean('mark_as_complete_initial_online_reminder')->default(0);
            $table->boolean('mark_as_complete_online_auto_fill')->default(0);
            $table->boolean('mark_as_complete_initial_in_person_reminder')->default(0);
            $table->boolean('mark_as_complete_in_person_auto_fill')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_notifications', function (Blueprint $table) {
            $table->dropColumn('mark_as_complete_initial_online_reminder');
            $table->dropColumn('mark_as_complete_online_auto_fill');
            $table->dropColumn('mark_as_complete_initial_in_person_reminder');
            $table->dropColumn('mark_as_complete_in_person_auto_fill');
        });
    }
}

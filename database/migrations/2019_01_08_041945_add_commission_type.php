<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCommissionType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (['bookings', 'users', 'settings'] as $table) {
            Schema::table($table, function (Blueprint $table) {
                $table->string('commission_type')->after('commission_rate')->nullable();
                $table->decimal('commission_dollar', 8, 2)->after('commission_rate')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach (['bookings', 'users', 'settings'] as $table) {
            Schema::table($table, function (Blueprint $table) {
                $table->dropColumn(['commission_dollar', 'commission_type']);
            });
        }
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpportunityRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opportunity_relations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_opportunity_id')->unsigned();
            $table->integer('child_opportunity_id')->unsigned();

            $table->foreign('parent_opportunity_id')->references('id')->on('opportunities');
            $table->foreign('child_opportunity_id')->references('id')->on('opportunities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opportunity_relations');
    }
}

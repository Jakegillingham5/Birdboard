<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('commission_rate');
            $table->integer('deposit_payment');
            $table->string('notification_email');
            $table->string('notification_phone');

            $table->integer('billboard_post_length');
            $table->integer('deposit_payment_warning_admin');
            $table->integer('deposit_payment_warning_client');
            $table->integer('final_payment_warning_admin');
            $table->integer('final_payment_warning_client');
            $table->integer('initial_talent_reminder');
            $table->integer('second_talent_reminder');
            $table->integer('final_talent_reminder');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}

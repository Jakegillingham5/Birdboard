<?php

use Pickstar\Booking\Booking;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingTalentCommissionsView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('DROP VIEW IF EXISTS `booking_talent_commissions_view`');

        DB::statement(
            "CREATE VIEW `booking_talent_commissions_view` AS
                (SELECT
                    `booking_talent`.`booking_id` AS `booking_id`,
                    `booking_talent`.`talent_id` AS `talent_id`,
                    `bookings`.`budget_per_star` AS `booking_budget_per_star`,
                    ((100 - (CASE
                        WHEN `bookings`.`commission_rate` IS NOT NULL THEN `bookings`.`commission_rate`
                        WHEN `talent_profile`.`commission_rate` IS NOT NULL THEN `talent_profile`.`commission_rate`
                        ELSE `settings`.`commission_rate`
                    END)) / 100) AS `talent_commission_rate`
                FROM
                    `bookings`
                    LEFT JOIN `booking_talent` ON `bookings`.`id`=`booking_talent`.`booking_id`
                    LEFT JOIN `talent_profile` ON `booking_talent`.`talent_id`=`talent_profile`.`user_id`
                    LEFT JOIN `settings` ON 1=1
                WHERE
                    `bookings`.`type`='booking')"
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS `booking_talent_commissions_view`');
    }
}

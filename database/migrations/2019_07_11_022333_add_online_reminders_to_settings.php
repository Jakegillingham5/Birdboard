<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOnlineRemindersToSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->unsignedInteger('initial_talent_reminder_online')->default(7);
            $table->unsignedInteger('final_talent_reminder_online')->default(3);
            $table->unsignedInteger('admin_reminder_online')->default(2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function (Blueprint $table) {
            $table->dropColumn('initial_talent_reminder_online');
            $table->dropColumn('final_talent_reminder_online');
            $table->dropColumn('admin_reminder_online');
        });
    }
}

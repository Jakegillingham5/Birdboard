<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackedSearchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tracked_searches', function (Blueprint $table) {
            $table->increments('id');

            $table->string('sport_category');
            $table->string('club_event');
            $table->string('location');
            $table->string('gender');
            $table->string('social_following');
            $table->string('opportunity_type');
            $table->string('search_keywords');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tracked_searches');
    }
}

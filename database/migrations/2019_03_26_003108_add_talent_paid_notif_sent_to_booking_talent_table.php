<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTalentPaidNotifSentToBookingTalentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_talent', function (Blueprint $table) {
            $table->boolean('talent_paid_notif_sent')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_talent', function (Blueprint $table) {
            $table->dropColumn('talent_paid_notif_sent');
        });
    }
}

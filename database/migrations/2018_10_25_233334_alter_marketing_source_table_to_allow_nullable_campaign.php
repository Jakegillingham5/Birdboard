<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMarketingSourceTableToAllowNullableCampaign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('marketing_source', function (Blueprint $table) {
            $table->string('campaign_source')->nullable()->change();
            $table->string('campaign_name')->nullable()->change();
            $table->string('campaign_medium')->nullable()->change();
            $table->string('campaign_content')->nullable()->change();
            $table->string('campaign_term')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

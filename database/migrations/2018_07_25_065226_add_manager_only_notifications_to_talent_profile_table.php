<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddManagerOnlyNotificationsToTalentProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('talent_profile', function (Blueprint $table) {
            $table->string('manager_only_notifications')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('talent_profile', function (Blueprint $table) {
            $table->dropColumn(['manager_only_notifications']);
        });
    }
}

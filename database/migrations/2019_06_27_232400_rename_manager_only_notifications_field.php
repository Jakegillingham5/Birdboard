<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameManagerOnlyNotificationsField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('talent_profile', function (Blueprint $table) {
            $table->renameColumn('manager_only_notifications','notification_preference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('talent_profile', function (Blueprint $table) {
            $table->renameColumn('notification_preference','manager_only_notifications');
        });
    }
}

<?php

use Pickstar\User\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MigrateManagerUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Find all talent with a manager email on their profile. These are talent users that are being
        // managed by a manager, and that manager needs a new user account. Pluck the email from the
        // profile.
        $emails = User::talent()
            ->with('profile')
            ->whereHas('profile', function ($query) {
                $query->whereNotNull('manager_email');
            })
            ->get()
            ->pluck('profile.manager_email')
            ->unique(function ($email) {
                return strtolower($email);
            })
            ->values();

        DB::beginTransaction();

        // Find all existing users that are already using any of the manager emails we have found.
        // We'll "null" out these emails and associate them with their new manager.
        $existing = User::whereIn('email', $emails)->get();

        $existing->each(function ($user) {
            $user->fill(['email' => null])->save();
        });

        // Get all talent that have a manager email, this will be used to match talent back to a new
        // maneger user.
        $talent = User::talent()
            ->with('profile')
            ->whereHas('profile', function ($query) {
                $query->whereNotNull('manager_email');
            })
            ->get();

        // Create the new manager accounts for each of emails.
        $managers = collect([]);

        $emails->each(function ($email) use ($talent, $managers) {
            $props = array_only(
                $talent->firstWhere('profile.manager_email', $email)->profile->toArray(),
                ['manager', 'agency']
            );

            $getName = function ($name) use ($talent) {
                $nameParts = explode(' ', $name);

                if (!$name) {
                    return ['first_name' => null, 'last_name' => null];
                }

                if (count($nameParts) === 1) {
                    return ['first_name' => array_pop($nameParts), 'last_name' => null];
                }

                $lastName = array_pop($nameParts);
                $firstName = implode(' ', $nameParts);

                return ['first_name' => $firstName, 'last_name' => $lastName];
            };

            $managers->push(User::create([
                'role' => User::ROLE_MANAGER,
                'email' => $email,
                'first_name' => $getName($props['manager'])['first_name'] ?? null,
                'last_name' => $getName($props['manager'])['last_name'] ?? null,
                'password' => null,
                'phone' => null,
                'company' => $props['agency'] ?? null,
                'position' => null,
                'email_verified' => true,
                'status' => User::STATUS_PENDING
            ]));
        });

        // Associate talent with their new manager user.
        $talent->each(function ($talent) use ($managers) {
            $manager = $managers->first(function ($manager) use ($talent) {
                return strtolower($manager->email) === strtolower($talent->profile->manager_email);
            });

            $talent->management()->associate($manager);

            $talent->save();
        });

        DB::commit();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExpiringToBookingTalentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_talent', function (Blueprint $table) {
            $table->boolean('expired')->default(0);
            $table->dateTime('expiry_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_talent', function (Blueprint $table) {
            $table->dropColumn('expired');
            $table->dropColumn('expiry_date');
        });
    }

}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTalentProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('talent_profile', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('gender')->nullable();
            $table->string('manager')->nullable();
            $table->string('agency')->nullable();
            $table->string('tagline', 30)->nullable();
            $table->text('description')->nullable();
            $table->string('facebook_link')->nullable();
            $table->string('twitter_link')->nullable();
            $table->string('instagram_link')->nullable();
            $table->string('linkedin_link')->nullable();
            $table->integer('facebook_follower_count')->nullable();
            $table->integer('twitter_follower_count')->nullable();
            $table->integer('instagram_follower_count')->nullable();
            $table->integer('commission_rate')->nullable();
            $table->dateTime('date_featured_until')->nullable();
            $table->string('profile_image_hash')->nullable();
            $table->string('profile_image_crop_payload')->nullable();

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('talent_profile');
    }
}

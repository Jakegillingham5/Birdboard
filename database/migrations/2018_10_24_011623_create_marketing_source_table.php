<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketingSourceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marketing_source', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');

            $table->string('campaign_source');
            $table->string('campaign_name');
            $table->string('campaign_medium');
            $table->string('campaign_content');
            $table->string('campaign_term');

            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('request_id')->nullable();
            $table->unsignedInteger('sendgrid_stage')->nullable();

            $table->boolean('existing_user')->default(false);

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('request_id')->references('id')->on('bookings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marketing_source');
    }
}

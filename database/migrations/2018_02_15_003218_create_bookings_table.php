<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')->unsigned();
            $table->integer('opportunity_id')->unsigned()->nullable();
            $table->integer('online_opportunity_id')->unsigned()->nullable();
            $table->integer('booking_notifications_id')->unsigned()->nullable();

            $table->string('type');
            $table->string('status');
            $table->string('appearance_type');
            $table->string('name');
            $table->text('description')->nullable();
            $table->text('requirements')->nullable();
            $table->string('duration')->nullable();
            $table->integer('required_stars')->nullable();
            $table->decimal('budget_per_star', 10, 2)->nullable();
            $table->decimal('budget', 10, 2)->nullable();
            $table->integer('commission_rate')->nullable();
            $table->text('social_media_content')->nullable();

            $table->string('street_address')->nullable();
            $table->string('suburb')->nullable();
            $table->string('state')->nullable();
            $table->string('country')->nullable();
            $table->string('postcode')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();

            $table->date('date')->nullable();
            $table->dateTime('utc_date')->nullable();
            $table->dateTime('tz_date')->nullable();
            $table->string('timezone')->nullable();
            $table->time('arrival_time')->nullable();
            $table->time('athlete_start_time')->nullable();

            $table->boolean('others_can_apply')->default(0);
            $table->boolean('date_flexible_or_unconfirmed')->default(0);
            $table->boolean('time_flexible_or_unconfirmed')->default(0);
            $table->boolean('budget_not_confirmed')->default(0);
            $table->boolean('expenses_covered')->default(0);
            $table->boolean('promoted_publicly')->default(0);
            $table->boolean('client_accepted_terms')->default(0);

            $table->dateTime('billboard_expiry_date')->nullable();
            $table->dateTime('vetted_date')->nullable();
            $table->dateTime('scheduled_date')->nullable();
            $table->dateTime('completed_date')->nullable();
            $table->dateTime('client_accepted_terms_date')->nullable();
            $table->timestamps();

            $table->foreign('client_id')->references('id')->on('users');
            $table->foreign('opportunity_id')->references('id')->on('opportunities');
            $table->foreign('online_opportunity_id')->references('id')->on('opportunities');
            $table->foreign('booking_notifications_id')->references('id')->on('booking_notifications');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}

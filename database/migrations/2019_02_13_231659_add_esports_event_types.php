<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEsportsEventTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Add new opportunities types
        $gamingId = DB::table('opportunities')->insertGetId(
            array(
                'name' => 'Gaming',
                'type' => 'esports',
                'parent' => 1
            )
        );
        $coachingId = DB::table('opportunities')->insertGetId(
            array(
                'name' => 'League of Legends - 1 on 1 Coaching',
                'type' => 'esports',
                'parent' => 0
            )
        );
        $normalGamePackId = DB::table('opportunities')->insertGetId(
            array(
                'name' => 'League of Legends - Normal Game Pack',
                'type' => 'esports',
                'parent' => 0
            )
        );
        $oneOnOneGamePackId = DB::table('opportunities')->insertGetId(
            array(
                'name' => 'League of Legends - 1 on 1 Game Pack',
                'type' => 'esports',
                'parent' => 0
            )
        );

        //Add opportunity relations
        DB::table('opportunity_relations')->insertGetId(
            array(
                'parent_opportunity_id' => $gamingId,
                'child_opportunity_id' => $coachingId
            )
        );
        DB::table('opportunity_relations')->insert(
            array(
                'parent_opportunity_id' => $gamingId,
                'child_opportunity_id' => $normalGamePackId
            )
        );
        DB::table('opportunity_relations')->insert(
            array(
                'parent_opportunity_id' => $gamingId,
                'child_opportunity_id' => $oneOnOneGamePackId
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $parent_opp = DB::table('opportunities')->where('name', '=', 'Gaming')->first();
        DB::table('opportunity_relations')->where('parent_opportunity_id', '=', $parent_opp->id)->delete();
        DB::table('opportunities')->where('type', '=', 'esports')->delete();
    }
}

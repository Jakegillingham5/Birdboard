<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOnlineRemindersToBookingNotifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_notifications', function (Blueprint $table) {
            $table->boolean('initial_talent_reminder_online')->default(0);
            $table->boolean('final_talent_reminder_online')->default(0);
            $table->boolean('admin_reminder_online')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_notifications', function (Blueprint $table) {
            $table->dropColumn('initial_talent_reminder_online');
            $table->dropColumn('final_talent_reminder_online');
            $table->dropColumn('admin_reminder_online');
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_notifications', function (Blueprint $table) {
            $table->increments('id');

            $table->boolean('talent_unconfirmed')->default(0);
            $table->boolean('initial_talent_reminder')->default(0);
            $table->boolean('second_talent_reminder')->default(0);
            $table->boolean('final_talent_reminder')->default(0);
            $table->boolean('deposit_payment_warning_client')->default(0);
            $table->boolean('deposit_payment_warning_admin')->default(0);
            $table->boolean('request_expiring')->default(0);
            $table->boolean('request_expired')->default(0);
            $table->boolean('final_payment_warning_admin')->default(0);
            $table->boolean('final_payment_warning_client')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_notifications');
    }
}

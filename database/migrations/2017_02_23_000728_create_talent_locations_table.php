<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTalentLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('talent_locations', function(Blueprint $table) {
            $table->integer('location_id')->unsigned();
            $table->integer('talent_id')->unsigned();

            $table->foreign('location_id')->references('id')->on('locations');
            $table->foreign('talent_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('talent_locations');
    }
}

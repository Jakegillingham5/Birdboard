<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingManagementLookupView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('DROP VIEW IF EXISTS `booking_management_lookup_view`');

        DB::statement(
            "CREATE VIEW `booking_management_lookup_view` AS
                (SELECT
                    `bookings`.`id` AS `booking_id`,
                    `management`.`id` AS `management_id`,
                    `talent`.`id` AS `talent_id`,
                    `booking_talent`.`status` AS `booking_talent_status`,
                    `booking_talent`.`client_accepted` AS `booking_talent_accepted`,
                    `bookings`.`status` AS `booking_status`,
                    `bookings`.`type` AS `booking_type`,
                    IF(`bookings`.`vetted_date` IS NULL, false, true) AS `booking_was_vetted`
                FROM
                    `bookings`
                        LEFT JOIN
                    `booking_talent` ON `bookings`.`id` = `booking_talent`.`booking_id`
                        LEFT JOIN
                    `users` `talent` ON `booking_talent`.`talent_id` = `talent`.`id`
                        LEFT JOIN
                    `users` `management` ON `talent`.`management_id` = `management`.`id`)"
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS `booking_management_lookup_view`');
    }
}

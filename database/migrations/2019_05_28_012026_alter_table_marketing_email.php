<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableMarketingEmail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('marketing_emails', function (Blueprint $table) {
            $table->renameColumn('collection_location', 'collection_form');
            $table->string('collection_page')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('marketing_emails', function (Blueprint $table) {
            $table->renameColumn('collection_form', 'collection_location');
            $table->dropColumn('collection_page');
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Pickstar\Booking\Booking;

class UpdateDateAppliedForField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Booking::requests()->each(function($bookingQuery) {
            if ($bookingQuery->date !== null) {
                $bookingQuery->talentApplied->each(function($talentQuery) use ($bookingQuery) {
                    $talentQuery->response->fill(['date_applied_for' => $bookingQuery->date])->save();
                });
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddXeroReferenceToBookingTalent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_talent', function (Blueprint $table) {
            $table->uuid('xero_bill_reference')->nullable();
            $table->uuid('xero_deposit_invoice_reference')->nullable();
            $table->uuid('xero_final_invoice_reference')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_talent', function (Blueprint $table) {
            $table->removeColumn('xero_bill_reference');
            $table->removeColumn('xero_deposit_invoice_reference');
            $table->removeColumn('xero_final_invoice_reference');
        });
    }
}

`<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClientAgreedTermsOnRequestCreationToBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->boolean('client_agreed_terms_on_request_creation')->nullable();
            $table->dateTime('client_agreed_terms_on_request_creation_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bookings', function (Blueprint $table) {
            $table->dropColumn('client_agreed_terms_on_request_creation');
            $table->dropColumn('client_agreed_terms_on_request_creation_date');
        });
    }
}

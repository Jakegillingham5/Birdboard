<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUrgentNotificationsToNotification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_notifications', function (Blueprint $table) {

            $table->boolean('urgent_payment_warning_admin')->default('0');
            $table->boolean('urgent_payment_warning_client')->default('0');
        });

        Schema::table('settings', function (Blueprint $table) {
            $table->boolean('urgent_payment_warning_admin')->default('2');
            $table->boolean('urgent_payment_warning_client')->default('2');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_notifications', function (Blueprint $table) {
            $table->dropColumn('urgent_payment_warning_admin');
            $table->dropColumn('urgent_payment_warning_client');
        });

        Schema::table('settings', function (Blueprint $table) {
            $table->dropColumn('urgent_payment_warning_admin');
            $table->dropColumn('urgent_payment_warning_client');
        });

    }
}

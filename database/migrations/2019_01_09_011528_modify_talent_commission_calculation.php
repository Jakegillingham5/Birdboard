<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyTalentCommissionCalculation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement(
            "CREATE OR REPLACE VIEW `booking_talent_commissions_view` AS
                (SELECT
                    `booking_talent`.`booking_id` AS `booking_id`,
                    `talent`.`id` AS `talent_id`,
                    `management`.`id` AS `management_id`,
                    `bookings`.`budget_per_star` AS `booking_budget_per_star`,
                    IF (
                        `bookings`.`commission_type` = 'dollar',
                        (CASE
                            WHEN `bookings`.`commission_dollar` IS NOT NULL THEN `bookings`.`commission_dollar`
                            WHEN `talent`.`commission_dollar` IS NOT NULL THEN `talent`.`commission_dollar`
                            WHEN `management`.`commission_dollar` IS NOT NULL THEN `management`.`commission_dollar`
                            ELSE `settings`.`commission_dollar`
                        END) / `bookings`.`budget_per_star`,
                        (100 - (CASE
                            WHEN `bookings`.`commission_rate` IS NOT NULL THEN `bookings`.`commission_rate`
                            WHEN `talent`.`commission_rate` IS NOT NULL THEN `talent`.`commission_rate`
                            WHEN `management`.`commission_rate` IS NOT NULL THEN `management`.`commission_rate`
                            ELSE `settings`.`commission_rate`
                        END)) / 100
                    ) AS `talent_commission_rate`
                FROM
                    `bookings`
                        LEFT JOIN
                    `booking_talent` ON `bookings`.`id` = `booking_talent`.`booking_id`
                        LEFT JOIN
                    `users` `talent` ON `booking_talent`.`talent_id` = `talent`.`id`
                        LEFT JOIN
                    `users` `management` ON `talent`.`management_id` = `management`.`id`
                        LEFT JOIN
                    `settings` ON 1 = 1
                WHERE
                    `bookings`.`type` = 'booking')"
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement(
            "CREATE OR REPLACE VIEW `booking_talent_commissions_view` AS
                (SELECT
                    `booking_talent`.`booking_id` AS `booking_id`,
                    `talent`.`id` AS `talent_id`,
                    `management`.`id` AS `management_id`,
                    `bookings`.`budget_per_star` AS `booking_budget_per_star`,
                    ((100 - (CASE
                        WHEN `bookings`.`commission_rate` IS NOT NULL THEN `bookings`.`commission_rate`
                        WHEN `talent`.`commission_rate` IS NOT NULL THEN `talent`.`commission_rate`
                        WHEN `management`.`commission_rate` IS NOT NULL THEN `management`.`commission_rate`
                        ELSE `settings`.`commission_rate`
                    END)) / 100) AS `talent_commission_rate`
                FROM
                    `bookings`
                        LEFT JOIN
                    `booking_talent` ON `bookings`.`id` = `booking_talent`.`booking_id`
                        LEFT JOIN
                    `users` `talent` ON `booking_talent`.`talent_id` = `talent`.`id`
                        LEFT JOIN
                    `users` `management` ON `talent`.`management_id` = `management`.`id`
                        LEFT JOIN
                    `settings` ON 1 = 1
                WHERE
                    `bookings`.`type` = 'booking')"
        );
    }
}

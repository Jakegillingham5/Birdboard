<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingTalentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_talent', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('booking_id')->unsigned();
            $table->integer('talent_id')->unsigned();
            $table->boolean('shortlisted')->default(0);
            $table->integer('priority')->nullable();
            $table->string('status');
            $table->timestamp('applied_date')->nullable();
            $table->string('decline_reason')->nullable();
            $table->boolean('client_accepted')->default(0);
            $table->boolean('attendance_confirmed')->default(0);
            $table->string('attendance_access_token')->nullable()->unique();
            $table->timestamps();

            $table->foreign('booking_id')->references('id')->on('bookings');
            $table->foreign('talent_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_talent');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompletedDateToBookingTalent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_talent', function (Blueprint $table) {
            $table->date('online_completed_date')->nullable();
            $table->date('in_person_completed_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_talent', function (Blueprint $table) {
            $table->dropColumn('online_completed_date');
            $table->dropColumn('in_person_completed_date');
        });
    }
}

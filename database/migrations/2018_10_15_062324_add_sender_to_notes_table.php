<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSenderToNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking_notes', function (Blueprint $table) {
            //retroactively assign all notes to Jess
            $table->unsignedInteger('user_id')->nullable()->after('booking_id')->default(4);

            $table->foreign('user_id')->references('id')->on('users');
        });
        Schema::table('booking_notes', function (Blueprint $table) {
            $table->unsignedInteger('user_id')->nullable()->after('booking_id')->default(null)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking_notes', function (Blueprint $table) {
            $table->dropForeign(['user_id']);

            $table->dropColumn(['user_id']);
        });
    }
}
